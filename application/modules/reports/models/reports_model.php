<?php
class Reports_model extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
		$this->load->library('reports_lib',NULL,'rep');
                $this->load->library('leave_lib',NULL,'leave');
		
    }
	function rep_field_list()
	{
		
		$x=$this->rep->rep_field_list();
		return $x;
		
	}
	function rep_field_list2($out,$temp2)
	{
		
		$x=$this->rep->rep_field_list2($out,$temp2);
		return $x;
		
	}
	function team_list()
	{
		
		$x=$this->rep->team_list();
		return $x;
		
	}
	function branch_list()
	{
		
		$x=$this->rep->branch_list();
		return $x;
		
	}
        function desig_list()
	{
		
		$x=$this->rep->desig_list();
		return $x;
		
	} 
        function project_list()
	{
		
		$x=$this->rep->project_list();
		return $x;
		
	}
	function emp_field_list()
	{
		
		$x=$this->rep->emp_field_list();
		return $x;
		
	}
	function per_field_list()
	{
		
		$x=$this->rep->per_field_list();
		return $x;
		
	}
	public function it2()
	{
//	  $res=$this->rep->it2();
             $res=$this->rep->it2();
	  return $res;
	}
	
	function master_salary(){
		$x=$this->rep->master_salary();
		
		return $x;
		
	}
	function leave_salary(){
		
            $branch = trim($this->input->post('bname'));
            /* $result=$this->db->query("SELECT e.ctc,e.Ctc_range,l.* FROM `emp_leave` l,employee e WHERE e.`Emp_id`=l.`Emp_id` and l.`pl_available`>8 group by `Emp_id`"); */
		
		/* $str="SELECT e.*,l.*,l.pl_available as rem_pl FROM employee e,`emp_leave` l WHERE year(`Leave_sdate`)=YEAR(CURRENT_DATE)-1 and l.`Emp_id`=e.`Emp_id` and l.pl_available>0 and e.status=1 group by l.`Emp_id` ";  */
		$str="SELECT e.*,l.*,s.*,l.pl_available as rem_pl FROM employee e,`emp_leave` l,employ_salary s WHERE year(`Leave_sdate`)=YEAR(CURRENT_DATE)-1 and l.`Emp_id`=e.`Emp_id` and e.`Emp_id`=s.Emp_id and l.pl_available>0 and e.Branch = '$branch' and e.status=1 group by l.`Emp_id` "; 
		$result=$this->db->query($str);
		
		if(is_object($result)){
		if($result->num_rows()>0)
		{
			$res_arr= $result->result_array();
			$x=array();
			
			foreach($res_arr as $key => $value)
			{
				$x[$value['Emp_id']]=$value;
			
				if($value['Ctc_range']=='4')
				  $x[$value['Emp_id']]=$this->rep->leave_sal_catz($value['Ctc'],$value['rem_pl']);
			    if($value['Ctc_range']=='1')
					$x[$value['Emp_id']]=$this->rep->leave_sal_cata($value['Ctc'],$value['rem_pl']);
				 if($value['Ctc_range']=='2')
					$x[$value['Emp_id']]=$this->rep->leave_sal_catb($value['Ctc'],$value['rem_pl']);
				 if($value['Ctc_range']=='3')
					$x[$value['Emp_id']]=$this->rep->leave_sal_cats($value['Ctc'],$value['rem_pl']);
				$x[$value['Emp_id']]['tactc']=0;
				$x[$value['Emp_id']]['ectc']=0;
				$x[$value['Emp_id']]['tbasic']=0;
				$x[$value['Emp_id']]['thra']=0;
				$x[$value['Emp_id']]['tconv']=0;
				$x[$value['Emp_id']]['tallowance']=0;
				$x[$value['Emp_id']]['tgross']=0;
				$x[$value['Emp_id']]['trev_gross']=0;
				$x[$value['Emp_id']]['tepf_12']=0;
				$x[$value['Emp_id']]['tnet_pay']=0;
			    $x[$value['Emp_id']]['details']=$value;
			}
			
			foreach($x as $key=>$values)
				{
				foreach($values as $key2=>$v2){	
				
		
			    $x[$key]['tactc']=$x[$key]['tactc']+$v2['ctc'];
				$x[$key]['ectc']=$x[$key]['ectc']+$v2['gross'];
				$x[$key]['tbasic']=$x[$key]['tbasic']+$v2['basic'];
				$x[$key]['tpt']=$x[$key]['tpt']+$v2['pt'];
				$x[$key]['tit']=$x[$key]['tit']+$v2['it'];
				$x[$key]['thra']=$x[$key]['thra']+$v2['hra'];
				$x[$key]['tconv']=$x[$key]['tconv']+$v2['conv'];
				$x[$key]['tallowance']=$x[$key]['tallowance']+$v2['allowance'];
				$x[$key]['tgross']=$x[$key]['tgross']+$v2['gross'];
				$x[$key]['trev_gross']=$x[$key]['trev_gross']+$v2['rev_gross'];
				$x[$key]['tepf_12']=$x[$key]['tepf_12']+$v2['epf_12'];
				$x[$key]['tnet_pay']=$x[$key]['tnet_pay']+$v2['net_pay'];
				}
				
					
				}
 
                               
                        foreach ($x as $row){
                            $insert_array = array( "Emp_id"=>$row['details']['Emp_id'],
                                                    "employ_basic_pay"=>$row['tbasic'],
                                                    "employ_hra"=>$row['thra'],
                                                    "employ_cony_allow"=>$row['tconv'],
                                                    "employ_other_allow"=>$row['tallowance'],
                                                    "employ_pt"=>$row['tpt'],
                                                    "employ_gross"=>$row['tgross'],
                                                    "employ_net"=>$row['tnet_pay'],
                                                    "earned_ctc"=>$row['ectc'],
                                                    "employ_it"=>$row['tit'],
                                                    "pl_credit"=>$row['details']['Pl_credit'],
                                                    "leave_balance"=>$row['details']['rem_pl'],
                                                    "employ_ctc"=>$row['tactc'],
                                                    "employ_ctc_range"=>$row['details']['Ctc_range'],
                                                    "sal_year"=>date('Y') - 1,
                                                    "employ_bank_ac_no"=>$row['details']['employ_bank_ac_no'],
                                                    "employ_uan_no"=>$row['details']['employ_uan_no'],
                                                    "employ_pf_no"=>$row['details']['employ_pf_no'],
                                                    "employ_esi_no"=>$row['details']['employ_esi_no'],
                                                    "Bank_operating_name"=>$row['details']['Bank_operating_name'],
                                                    "bank_name"=>$row['details']['bank_name'],
                                                    "bbranch"=>$row['details']['bbranch']);
                            $emp_id = $row['details']['Emp_id'];                            
                            $sal_year = date('Y') - 1;
                            $query = $this->db->query("select Emp_id from leave_salary where Emp_id = '$emp_id' and sal_year = '$sal_year' ");                           if(is_object($query)){
                            if($query->num_rows() > 0){
//                                echo "updated<br>";
                                unset($insert_array['Emp_id']);
                                $this->db->where('Emp_id',$emp_id);
                                $this->db->update('leave_salary',$insert_array);
                            }else{    
//                                echo "insertted<br>";
                                $this->db->insert('leave_salary',$insert_array);
                            }
                            }
                        }
                                

                                
                                
                                
			return ($x);
		}else{
                    return false;
                }
		}
                else{
                    return false;
                }
		
		
	}
public function forms()
	{
		
	  $res=$this->rep->forms();
	  
	  return $res;
	}
	public function form3()
	{
		
	  $res=$this->rep->form3();
	  
	  return $res;
	}
	public function form5()
	{
		
	  $res=$this->rep->form5();
	  
	  return $res;
	}
	 public function formo()
	{
		
	  $res=$this->rep->formo();
	  
	  return $res;
	}

    
        public function  payslip(){
            $res=$this->rep->payslip();	  
            return $res;
        }
        function dept_list()
	{
		
		$x=$this->rep->dept_list();
		return $x;
		
	} 
        
        public function nsa_salary(){
            $uid = $this->session->userdata('uid');
            $result = $this->leave->nsa_calc();
            if($result){
                $sal_month = date('m') - 1;
                $sal_year = date('Y');
                foreach($result as $row){
                    
                    $db_array = array("Emp_id"=>$row['Emp.no'] , "Total_days"=>$row['tnsa'] , "Total_amt"=>$row['Allowance'], "esi_amt"=>$row['Emp_esi'] , "Net_amt"=>$row['net_pay'] , "Catagory"=>$row['Categoty'],"sal_month"=>$sal_month , "sal_year"=>$sal_year, "Uid"=>$uid);
                    $emp_id = $row['Emp.no'];
                    $query = $this->db->query("select Emp_id,Id from nsa_salary where Emp_id = '$emp_id' and sal_month = '$sal_month' and sal_year = '$sal_year' ");                          
                    if($query->num_rows() > 0){
                        unset($db_array['Emp_id']);
                        $query_result = $query->result_array();                                
                        $Id = $query_result[0]['Id'];
                        $this->db->where("Id = $Id and Emp_id = '$emp_id' ");
                        $this->db->update('nsa_salary',$db_array);
                    }else{    
                        $this->db->insert('nsa_salary',$db_array);
                    }   
                }
                return $result;
            }else{
                return $result;
            }
        }
        
        public function incometax_update(){
            $res=$this->rep->incometax_update();	  
            return $res;
        }
        public function incometax_updatesal(){
            $res=$this->rep->incometax_updatesal();	  
            return $res;
        }
}