<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of leave
 *
 */
class reports extends CI_Controller{
    
  var $gdata=array();
    public function __construct() {
        parent::__construct();
		 $this->load->model("reports/reports_model","reports");
	  $this->load->library('auth','session');
	   $this->load->helper('download','file');           
	  $this->gdata['test']="hai";
	
    }
    public function query_build()
	{
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
               $this->load->view('query/querybuilder');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
		
		
	}
	 public function rep_field_list()
	{
             
          if($this->auth->checku())
          {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
              
                $this->form_validation->set_rules('branch', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('rep_list', 'Report', 'required|trim|xss_clean');               
                if ($this->form_validation->run()){
                         
		if($this->session->userdata('team')!='')
			$this->session->unset_userdata('team');
		
		$res=$this->reports->rep_field_list();
		$emp=$this->reports->emp_field_list();
		$per=$this->reports->per_field_list();
		$k=array();
		$temp=array();
		$data['rep_no']=$res['rep_no'];
		unset($res['rep_no']);
		if(isset($res) && is_array($res)){
		foreach($res as $id => $value){
			
			$this->rem_val($emp,"field",'Status');
			$this->rem_val($emp,"field",'Old_emp_id');
			$this->rem_val($emp,"field",$value['field']);
			$this->rem_val($per,"field",$value['field']);
			
			if($value['field']=="Emp_id")
				$k[$value['id']]="Emp.No";
			else if($value['field']=="Name")
				$k[$value['id']]="Emp.Name";
			else if($value['field']=="Design")
				$k[$value['id']]="Designation";
			else if($value['field']=="D_of_join")
				$k[$value['id']]="DOJ";
			else if($value['field']=="Dept")
				$k[$value['id']]="Department";
			else if($value['field']=="Project_team")
				$k[$value['id']]="Report team";
			/* else if($value['field']=="Project_team")
				$k[$value['id']]="Report team"; */
			else if($value['field']=="Branch")
				$k[$value['id']]="Location";
			else if($value['field']=="employ_gross")
				$k[$value['id']]="Gross";
			else if($value['field']=="employ_ctc")
				$k[$value['id']]="Ctc";
			else if($value['field']=="employ_net")
				$k[$value['id']]="Net";
			else
			$k[$value['id']]=$value['field'];
			if(isset($value['addf']))
				array_push($temp,$value['id']);
		}
		
		array_push($res,$temp);
		}
                
		
		$data['res']=$k;
		$data['temp']=$temp;
		
		$data['employee']=$emp;
		$data['personal']=$per;
                
$emp_team = '';
$emp_desig = '';
$emp_prj = '';
$emp_dept = '';
$emp_status = '';
$emp_gender = '';                
                
                
//                $rep_list = $this->input->post('rep_list');
$branch = $this->input->post('branch');

$emp_repgender = $this->input->post('emp_repgender');
$gen = $this->input->post('gen');

if(trim($emp_repgender) != ''){
    $emp_gender = $emp_repgender;
}else if(trim($gen) != ''){
    $emp_gender = $gen;
}

$emp_rdesig = $this->input->post('emp_rdesig');
$emp_repdesig = $this->input->post('emp_repdesig');

if(trim($emp_rdesig != '')){
    $emp_desig = $emp_rdesig;
}else if(trim($emp_repdesig)){
    $emp_desig = $emp_repdesig;
}

$emp_rtprj = $this->input->post('emp_rtprj');
$emp_repprj = $this->input->post('emp_repprj');
$emp_reecprj = $this->input->post('emp_reecprj');

if(trim($emp_rtprj != '')){
    $emp_prj = $emp_rtprj;
}else if(trim($emp_repprj)){
    $emp_prj = $emp_repprj;
}else if(trim($emp_reecprj)){
    $emp_prj = $emp_reecprj;
}

$emp_reemin = $this->input->post('emp_reemin');
$emp_reemax = $this->input->post('emp_reemax');

$sdate = $this->input->post('sdate');
$edate = $this->input->post('edate');

$emp_rtdept = $this->input->post('emp_rtdept');
$emp_repdept = $this->input->post('emp_repdept');
$emp_reedept = $this->input->post('emp_reedept');
$emp_ardept = $this->input->post('emp_ardept');

if(trim($emp_rtdept != '')){
    $emp_dept = $emp_rtdept;
}else if(trim($emp_repdept)){
    $emp_dept = $emp_repdept;
}else if(trim($emp_reedept)){
    $emp_dept = $emp_reedept;
}else if(trim($emp_ardept)){
    $emp_dept = $emp_ardept;
}

$emp_rteam = $this->input->post('emp_rteam');
$emp_repteam = $this->input->post('emp_repteam');
$emp_reeteam = $this->input->post('emp_reeteam');
$emp_arteam = $this->input->post('emp_arteam');

if(trim($emp_rteam != '')){
    $emp_team = $emp_rteam;
}else if(trim($emp_repteam)){
    $emp_team = $emp_repteam;
}else if(trim($emp_reeteam)){
    $emp_team = $emp_reeteam;
}else if(trim($emp_arteam)){
    $emp_team = $emp_arteam;
}

$emp_arstatus = $this->input->post('emp_arstatus');
$emp_repstatus = $this->input->post('emp_repstatus');
$emp_reestatus = $this->input->post('emp_reestatus');
$emp_crstatus = $this->input->post('emp_crstatus');
 
if(trim($emp_arstatus != '')){
    $emp_status = $emp_arstatus;
}else if(trim($emp_repstatus)){
    $emp_status = $emp_repstatus;
}else if(trim($emp_reestatus)){
    $emp_status = $emp_reestatus;
}else if(trim($emp_crstatus)){
    $emp_status = $emp_crstatus;
}
                
                
		$data['branch']= $branch;
                
                $data['emp_team']=$emp_team;
                $data['emp_desig']=$emp_desig;
                $data['emp_prj']=$emp_prj;
                $data['emp_dept']=$emp_dept;
                $data['emp_status']=$emp_status;
                $data['emp_repgender']=$emp_gender;
                
                $data['lyear'] = $this->input->post('lsyear');
                $data['msmonth'] = $this->input->post('msmonth');
                $data['msyear'] = $this->input->post("msyear");
                
                $data['emp_reemax']=$emp_reemax;
                $data['emp_reemin']=$emp_reemin;
                $data['sdate']=$sdate;
                $data['edate']=$edate;
//                echo '<pre>';print_r($data);die();
		$this->load->view('query/querybuilder_2',$data);
                }
                else{
                    $this->load->view('query/querybuilder');
                }
                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
		
	}
	 public function rep_field_list2()
	{
		
		$rno=$this->input->post('rep_no');
		$this->gdata['test']="list2";
		$temp2=json_decode($this->input->post('flist2'));
		
		$temp1=$this->input->post('flist');
	
		$temp3=array_merge($temp1,$temp2);
	
		$out="";
		foreach($temp3 as $arr) {
    	   $out .= ",".$arr;

           }
		   $out=ltrim($out,",");
		
		$out4="";
		foreach($temp2 as $arr) {
    	   $out4 .= ",".$arr;

           }
		   $out4=ltrim($out4,",");
		   $team1=$this->reports->team_list();
			$branch1=$this->reports->branch_list();
			$designation1=$this->reports->desig_list();
			$dept1=$this->reports->dept_list();
                        $proj1 = $this->reports->project_list();  
                        $team[0]='';
                        $branch[0]='';
                        $designation[0]='';
                        $dept[0]='';
                        $proj[0]='';
			foreach($team1 as $key=>$value)
			{
				$team[$value['reporting_no']]=$value['team'];
				
			}
			foreach($branch1 as $key=>$value)
			{
				$branch[$value['Id']]=$value['Location'];
				
			}
			 foreach($designation1 as $key=>$value)
			{
				$designation[$value['Id']]=$value['Design_name'];
			} 
                        
                         foreach($dept1 as $key=>$value)
			{
				$dept[$value['Id']]=$value['Dept_name'];
			}
                         foreach($proj1 as $key=>$value)
			{
				$proj[$value['Project_id']]=$value['Project_no'];
			}
			$ctc_range=array("1"=>"A","2"=>"B","3"=>"S","4"=>"Z");
			$gender=array("1"=>"MALE","2"=>"FEMALE");
			
			$bgroup=array("1"=> "A+ ve","2"=>"B+ ve","3"=>"O+ ve","4"=>"AB+ ve","5"=>"A1 +ve","6"=>"A2 +ve","7"=>"A1B +ve","8"=>"A2B +ve","9"=>"A- ve","10"=>"B- ve","11"=>"O- ve","12"=>"AB- ve","13"=>"A1- ve","14"=>"A2- ve","15"=>"A1B -ve","16"=>"A2B -ve","17"=>"B1 +ve");
			$shift=array("1"=>"Day","2"=>"General","3"=>"Semi","4"=>"Night");
                        $status=array("0"=>"Inactive","1"=>"Active","2"=>"Transferred");
			$qual1=array("1"=>"B.A","4"=>"B.B.A","5"=>"B.B.M","6"=>"B.C.A","24"=>"B.C.S","3"=>"B.Com","7"=>"B.E","10"=>"B.P.T","9"=>"B.Pham","2"=> "B.Sc","8"=>"B.Tech","11"=>"Diploma in");
			$process=array("1"=>"Data","2"=>"AR","3"=>"Coding","4"=>"HR","5"> "Network","6"=>"Software","7"=>"Litigation","8"=>"Finance","9"=> "Admin","10"=>"BD","11"=>"E-Pub" );
			$qual3=array("17"=>"M.B.A","19"=>"M.C.A","16"=>"M.Com","20"=>"M.E","18"=>"M.F.M","22"=>"M.Pham","15"=>"M.Sc","21"=>"M.Tech","23"=> "Others");
			$res=$this->reports->rep_field_list2($out,$out4);
                
			
		      $temp=$this->getKeys($res);
			 
			foreach($temp as $key => $value)
			{
				if($value=="Branch")
				$temp[$key]="Location";
			    if($value=="Emp_id")
				$temp[$key]="EMP.No";
			    if($value=="D_of_join")
				$temp[$key]="DOJ";
			    if($value=="Project_team")
				$temp[$key]="Team";
			    if($value=="employ_gross")
				$temp[$key]="Gross";
			    if($value=="employ_net")
				$temp[$key]="Net";
			}
			//need to add ctc range //
			
		   $data['keys']=$temp;
		  
		  foreach($res as $key=> $value)
		  {
			  
			   if(array_key_exists('Project_team',$value)){
				   if(isset($value['Project_team']))
					  $res[$key]['Project_team']=$team[$value['Project_team']];
			  }
			  if(array_key_exists('Branch',$value)){
				  if(isset($value['Branch']))
			  $res[$key]['Branch']=$branch[$value['Branch']];
			  }
			  if(array_key_exists('Design',$value)){
				  if(isset($value['Design']))
			  $res[$key]['Design']=$designation[$value['Design']];
			  }
                          if(array_key_exists('Process',$value)){
				  if(isset($value['Process']))
			  $res[$key]['Process']=$process[$value['Process']];
			  }
                          if(array_key_exists('Shift',$value)){
				  if(isset($value['Shift']))
			  $res[$key]['Shift']=$shift[$value['Shift']];
			  }
			  if(array_key_exists('Dept',$value)){
				  if(isset($value['Dept']))
			  $res[$key]['Dept']=$dept[$value['Dept']];
			  }
			  if(array_key_exists('Ctc_range',$value)){
				  if(isset($value['Ctc_range']))
			  $res[$key]['Ctc_range']=$ctc_range[$value['Ctc_range']];
			  }
			  if(array_key_exists('Gender',$value)){
				  if(isset($value['Gender']))
			  $res[$key]['Gender']=$gender[$value['Gender']];
			  }
			   if(array_key_exists('Blood_group',$value)){
				  if(isset($value['Blood_group']))
			  $res[$key]['Blood_group']=$bgroup[$value['Blood_group']];
			  }
			   if(array_key_exists('Qualification1',$value)){
				  if(isset($value['Qualification1']))
			  $res[$key]['Qualification1']=$qual1[$value['Qualification1']];
			  }
			   if(array_key_exists('Qualification3',$value)){
				  if(isset($value['Qualification3']))
			  $res[$key]['Qualification3']=$qual3[$value['Qualification3']];
			  }
                          if(array_key_exists('Status',$value)){
				  if(isset($value['Status']))
			  $res[$key]['Status']=$status[$value['Status']];
			  }
                          
                          if(array_key_exists('Project',$value)){
				  if(isset($value['Project']))
			  $res[$key]['Project']=$proj[$value['Project']];
			  }
			  
		  }
			//print_r($res);
			//die();
		  $data['result']=$res;
		  $data['rep_no']=$rno;
		  
	
//		$this->session->set_userdata('team', $data);
	
		  
		  $this->load->view('query/querybuilder_3',$data);
	
	}
        
    public function it_update_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
               $this->load->view('it/it_update_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
        
	public function it_update()
	{
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
                {
                     $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                     $this->form_validation->set_rules('emp_id', 'Employee name', 'required|trim|xss_clean');
                    if ($this->form_validation->run()){
                        $res=$this->reports->it2();	
                        $this->load->view('it/it_update_emp',$res);
                    }else{
                        $this->load->view('it/it_update_detail');
                    }
                }
                else{
                    $this->load->view('master/noaccess');
    //                redirect("master/noaccess");
                }
            }
           else{
               redirect("master/login");
           }
		
	}
	public function it2()
	{
//	  $res=$this->reports->it2();
           $this->load->view('it/it_update_detail');
	}
	
	public function payslip(){
		$res=$this->reports->payslip();
                if($res){
                    
                
		$data="";
		foreach($res as $key => $value)
		{
				
		$data.="<br><br><p> Payroll Details of ".$value['Emp_id']." </p></br><table border=1 ><tr><td colspan=5 align='center'>Allzone Management Solutions (P) Ltd</td></tr><tr><td colspan=5 align='center'>#85, 3rd Street, Kamdar Nagar, Nungambakkam, Chennai - 600034</td></tr><tr><td colspan=5 align='center'>Payslip for the month of ".date('M')."-".date('Y')."</td></tr>
		<tr><td>EMP CODE</td><td colspan=2>".$value['Emp_id']."</td><td>PF NO</td><td>".$value['employ_pf_no']."</td></tr>
		<tr><td>EMP NAME</td><td colspan=2>".$value['name']."</td><td>ESI NO</td><td>".$value['employ_esi_no']."</td></tr>
		<tr><td>Designation</td><td colspan=2>".$value['Designation']."</td><td>PAN NO</td><td>".$value['pan']."</td></tr>
		<tr><td>DOJ</td><td colspan=2>".$value['doj']."</td><td>Paid Days</td><td>".  (date('t') - $value['employ_lop'])."</td></tr>
		<tr><td>Bank Name</td><td colspan=2>".$value['bank_name']."</td><td>LOP Days</td><td>".$value['employ_lop']."</td></tr>
		<tr><td>Bank Account no</td><td colspan=2>".$value['employ_bank_ac_no']."</td></tr>
<tr><td>EARNINGS</td><td>AMOUNT</td><td>DEDUCTIONS</td><td>AMOUNT</td><td>ACCUMULATED</td></tr>
<tr><td>Basic</td><td>".  round($value['employ_basic_pay'])."</td><td>Provident Fund</td><td>".$value['employ_pf']."</td><td></td></tr>
<tr><td>HRA</td><td>".$value['employ_hra']."</td><td>Professional Tax</td><td>".$value['employ_pt']."</td><td></td></tr>
<tr><td>Conveyance</td><td>".$value['employ_cony_allow']."</td><td>Income Tax</td><td>".$value['employ_it']."</td><td></td></tr>
<tr><td>Allowance</td><td>".$value['employ_other_allow']."</td><td></td><td></td><td></td></tr>
<tr><td>SDA</td><td>".$value['employ_sda_allow']."</td><td></td><td></td><td></td></tr>
<tr><td>Attendence Incentive</td><td>".$value['attn_half_incent']."</td><td></td><td></td><td></td></tr>
<tr><td>Mobile Reimbursement</td><td>".$value['MobileReimnt']."</td><td></td><td></td><td></td></tr>
<tr><td>Referral Incentive</td><td>".$value['employ_reff_incent']."</td><td></td><td></td><td></td></tr>
<tr><td>HA</td><td>".$value['employ_ha_allow']."</td><td></td><td></td><td></td></tr>
<tr><td colspan=5></td></tr>
<tr><td>TOTAL EARNINGS</td><td>".$value['Total_Earning']."</td><td></td><td></td><td></td></tr>
<tr><td>NET PAY</td><td>".$value['employ_net']."</td><td></td><td></td><td></td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr><td>Employee Signature</td><td></td><td></td><td></td><td> Authorized Signatory</td></tr>
</table>";
		 
		 }
		 $fname="payslip_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
       }else{
           $this->load->view('emp_salary/nopayslip');
        }
	}
	
	public function Leave_salary_payslip(){
		//$res=$this->reports->payslip();
		$res=$this->reports->leave_salary();
		if($res){
		
		$data="";
		foreach($res as $key => $value)
		{
				
		$data.="</br></br><p> Leave Salary Payroll Details of ".$value['details']['Emp_id']." </p></br><table   border=1><tr><td colspan=5 align='center'>Allzone Management Solutions (P) Ltd</td></tr><tr><td colspan=5 align='center'>#85, 3rd Street, Kamdar Nagar, Nungambakkam, Chennai - 600034</td></tr><tr><td colspan=5 align='center'>Leave Salary Payslip for the Year ".(date('Y')-1)."</td></tr>
		<tr><td>EMP NO</td><td colspan=2>".$value['details']['Emp_id']."</td><td>PF NO</td><td>".$value['details']['employ_pf_no']."</td></tr>
		<tr><td>EMP NAME</td><td colspan=2>".$value['details']['Name']."</td><td>ESI NO</td><td>".$value['details']['employ_esi_no']."</td></tr>
		<tr><td>DOJ</td><td colspan=2>".$value['details']['D_of_join']."</td><td></td><td></td></tr>
<tr><td>EARNINGS</td><td>AMOUNT</td><td>DEDUCTIONS</td><td colspan=2>AMOUNT</td></tr>
<tr><td>Basic</td><td>".$value['tbasic']."</td><td>Provident Fund</td><td colspan=2>".$value['tepf_12']."</td></tr>
<tr><td>HRA</td><td>".$value['thra']."</td><td>Professional Tax</td><td colspan=2>".$value['ipt']."</td></tr>
<tr><td>Conveyance</td><td>".$value['tconv']."</td><td>Income Tax</td><td colspan=2>".$value['tit']."</td></tr>
<tr><td>Allowance</td><td>".$value['tallowance']."</td><td ></td><td colspan=2></td></tr><tr><td colspan=5></td></tr>
<tr><td>TOTAL EARNINGS</td><td>".$value['Total_Earning']."</td><td></td><td></td><td></td></tr>
<tr><td>NET PAY</td><td>".$value['tnet_pay']."</td><td></td><td></td><td></td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr><td>Employee Signature</td><td></td><td></td><td></td><td> Authorized Signatory</td></tr>
</table>";
		 
		 }
		 $fname="Leave_Salary_payslip_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
       }
       else{
           $this->load->view('emp_salary/nopayslip');
        }
	}

public function forms(){
		$res=$this->reports->forms();
		
		$data="";
		
                $month = $this->input->post('month');
                $year = $this->input->post('year');
                
		$data.="<table  border=1><tr><td colspan=13 align='center'><h1>FORM S for the Month of $month - $year</h1></td></tr><tr><td colspan=13 align='center'><h2>NOTICE OF DAILY HOURS OF WORK, REST INTERVAL, WEEKLY HOLIDAY, ETC.,</h2></td></tr><tr><td colspan=10 align='center'><h4><b>(See Sub Rule (4) of Rule 18 of the Tamil Nadu Shops and Establishments Rule, 1948)</b></h4></td><td colspan=3><b>1st of Every Month</b></td></tr><tr><td colspan=10 align='center'></td><td colspan=3><b>Date of Payment of Wages</b></td></tr>
		<tr><td colspan=5><b>Name & Address of the Establishment :</b></td><td colspan=7><b>Allzone Management Solutions P. Ltd.,</b></td><td></td></tr>
		<tr><td colspan=5></td><td colspan=7 Rowspan=2><b>No. 85, 3rd Street, Kamdar Nagar, Nungambakkam, 
Chennai - 600034 Ph: 044-4213 8535</b></td><td></td></tr><tr><td colspan=5></td></tr><tr><td colspan=5><b>Name of Employer / Authorised Person with full Residential Address :</b></td><td colspan=7><b>B. Nazeer Ahmed</b></td><td></td></tr><tr><td colspan=5></td><td colspan=7 rowspan=2><b>36, Chandra Banu Street,
Pudupet, Chennai - 600 002.</b></td><td></td></tr><tr><td colspan=5></td></tr>
		<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td><td>11</td><td>12</td><td>13</td></tr>
		<tr><td><b>S.No.</b></td><td><b>Name of the Person
Employed</b></td><td><b>Sex</b></td><td><b>Father's/ Husband Name</b></td><td><b>Designation</b></td><td><b>Empl.
No.</b></td><td><b>Date of Entry</b></td><td><b>Adult/
Adolescent</b></td><td><b>Time
Commence</b></td><td><b>Rest
Interval</b></td><td><b>Time at
which ended</b></td><td><b>Weekly
holiday</b></td><td><b>Wages</b></td></tr>";
$i=0;
 foreach($res as $key => $value)
		{ 
	$i++;	
		$data.="<tr><td>$i<td>".$value['Name']."</td><td>".$value['gender']."</td><td>".$value['Father_name']."</td><td>".$value['odesign']."</td><td>".$value['Id']."</td><td>".$value['D_of_join']."</td><td>Adult</td><td>9:00 AM & 9:00 PM</td><td>1:00 PM to 2:00 PM </td><td>6:00 PM & 6:00 AM</td><td>Sunday</td><td>".$value['Ctc']."</td></tr>";
		
		}
		

$data.="<tr colspan=13></tr>
</table>";
		 
		 //}
		 $fname="forms_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
	}
	
	
	public function formo(){
		$res=$this->reports->formo();
		
                $month = $this->input->post('month');
                $year = $this->input->post('year');
                
		$data="";
		 foreach($res as $key => $value)
		{
				
		$data.="<table border=1><tr><td colspan=2 align='center'><h2>FORM O</h2></td></tr><tr><td colspan=2><h3>HALF YEARLY RETURN FOR THE YEAR ENDING THE 31st ".date('M')."-".date('Y')." </h3></td></tr><tr><td>1. Name of the Establishment and Postal Address of the Information Technology Establishments.</td><td>ALLZONE MANAGEMENT SOLUTIONS Pvt. Ltd. No. 85, 3rd Main Road, Kamdar Nagar, Nungambakkam, Chennai - 600034 Ph: 044-42138535</td></tr><tr><td>2. Name and address of the Employer</td><td>B. Nazeer Ahmed 36, Chandra Banu Street, Chennai - 600 002.</td></tr>
		<tr><td>3. Nature of the Information Technology activity</td><td>Information Technology Enabled Services ( BPO)</td></tr>
		
  <tr><td>4. Number of persons employed <br>(i)Male <br> (ii) Female</td><td>".$value['Total']."<br>
".$value['male']."<br>
".$value['female']."</td></tr>
  <tr><td>5. Working Hours :
 Compliance of provisions relating to daily/ weekly working hours ( Sec.14)<br>
                                   Daily <br>

                                   Weekly <br>

                                   Hoilday <br>
</td><td><br>
8 Hour - 1 Hour Break <br>

48 Hours <br>

Sunday Holiday</td>
<tr><td>(b) Leave :
Wheather the persons employed are allowed leave with wages
</td><td>yes</td></tr>
  <tr><td>
6. Wheather a Notice in Form 'S' exhibited</td><td>yes</td></tr>
<tr><td>
7. Health and Safety :
Wheather precautionary measures against fire and other health and safety measures are provided as prescribed in sec.20 to 23</td><td>yes</td></tr>
<tr><td>
8. Wheather the following registers are maintained upto dated as prescribed in the Act/ Rules</td><td></td></tr>
<tr><td>
(i) Register of Fines
(ii) Register of Deduction for damages of loss
(iii) Register of advance
(iv) Register of Employment
(v) Register of hours of work
(vi) Register of holiday, leave granted, etc (Rules 16(1)
(vii) Register of services
(viii) Register o</td><td>yes</td></tr>
<tr><td>
9. Wheather permission for maintaining computerised and alternative forms are obtained. If yes give the no. and date of the order.</td><td>yes</td></tr>
<tr><td>
10. Wheather Name Board of the estt exhibited in Tamil as prescribed in Rule 15 of the TNSE Rules 1948.</td><td>yes</td></tr><tr></tr><tr></tr><tr></tr>
<tr><td></td><td>Signature of the Employer</td></tr><tr><td></td><td></td></tr>
<tr><td colspan=2 align='center'>CERTIFICATE</td></tr><tr><td colspan=2 rowspan=2>      Certified that during the half year ending 31st December 2013, We have complied with all the provisions under the Tamil Nadu shops and Establishments Act. 1947( Tamil Nadu ACT XXXVI of 1947) and the Tamil Nadu shops and Establishments Rules,1948 mad </br>The above certificate is issued with full knowledge of the statute. We are jointly and severally responsible for any information found incorrect subsequently and liable for prosecution under the provisions of the said Act and Rules made thereunder </td></tr>

<tr></tr><tr></tr>
<tr><td>Signature of the Manager</td><td>Signature of the Employer</td></tr><tr></tr><tr></tr><tr></tr><tr><td>Name                       : S. Eathish Kumar
</td><td>Name                       : B. Nazeer Ahmed</td></tr>
<tr><td>Designation               : Manager - Administration
</td><td>Designation               : Director</td></tr>
<tr></tr>
<tr><td>Office Seal:</td><td>Office Seal:</td></tr>
</table>";
		 
		 }
		 $fname="formo_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
	}
	
	public function form3(){
		$res=$this->reports->form3();
		
		$data="";
		/* foreach($res as $key => $value)
		{ */
		 $month = $this->input->post('month');
                $year = $this->input->post('year');
                
                
		$data.="<table border=1><tr><td colspan=4 align='center'><h3>FORM III    </h3></td></tr><tr><td colspan=4><h3> [Prescribed under rule 21(4)  </h3></td></tr>
		<tr><td colspan=4>ANNUAL RETURN</td></tr>
		<tr><td colspan=4>RETURN FOR THE YEAR ENDING THE 31ST DECEMBER $year</td></tr>
		<tr><td colspan=2>1. (a) Name of the Establishment and Postal Address </td><td colspan=2>ALLZONE MANAGEMENT SOLUTIONS Pvt. Ltd. 
No. 85, 3rd Main Road, Kamdar Nagar, Nungambakkam, Chennai - 600034 Ph: 044-42138535</td></tr>
<tr><td colspan=2>   (b) Name and residential address of the Owner / Contractor</td><td colspan=2>B. Nazeer Ahmed
36, Chandra Banu Street, </td></tr>
<tr><td colspan=2>   ©  Name and residential address of the Managing Agent / Director / partner in-charge of the day-to-day affairs of the establishment owned by a company, body corporate of associate</td><td colspan=2></td></tr>
<tr><td colspan=2>     (d) Name and residential address of the Manager / Agent, if any</td><td colspan=2></td></tr>
<tr><td colspan=2>2. Number of days worked during the year</td><td colspan=2>365-(4+4+5+4+4+5+4+4+5+4+4+5+10)</td></tr>
<tr><td colspan=2>*3. Number of man-days worked during the year</td><td colspan=2>".$res['temp']."</td></tr>
<tr><td colspan=2>4. Average daily number of persons employed during the year-            (i) Adults …                                  </td><td colspan=2>".round($res['temp']/2)."</td></tr>
<tr><td colspan=2>5. TOTAL wages paid in cash</td><td colspan=2>".round($res['total'])."</td></tr>
<tr><td colspan=2>6. TOTAL cash value of the wages paid in kind</td><td colspan=2></td></tr>
<tr><td colspan=2>7. Deductions :-     </td><td>Number of cases</td><td>Total Amount        Rs.                      P.</td></tr>
<tr><td colspan=2>     (a) Fines  </td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>     (b) Deductions for damage or loss </td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>        © Deductions for breach of contract </td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2> 8. Disbursement from fines :-</td><td>Purpose
</td><td>       Amount        Rs.                      P.</td></tr>
<tr><td colspan=2>   (a)</td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>    (b)</td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>(c)</td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>       (d)</td><td>Nil
</td><td>Nil</td></tr>
<tr><td colspan=2>9. BALANCE of fine fund in hand at the end of the year</td><td colspan=2>Nil</td></tr>
<tr></tr>
<tr></tr>
<tr><td  colspan=2></td><td align='right'>Signature</td><td></td></tr>
<tr><td colspan=2>Date :____________</td><td align='right'>Designation</td><td></td></tr>
		</table>";
		$fname="form3_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
	}
	
	public function form5(){
		$res=$this->reports->form5();
		
		$data="";
		 $month = $this->input->post('month');
                $year = $this->input->post('year');
		$data.="<table border=1><tr><td colspan=9 align='center'><h3>Form - 5  </h3></td></tr><tr><td colspan=9><h3> The Employees Provident Funds Scheme, 1652  </h3></td></tr>
		<tr><td colspan=9><h3> Paragraph 36(2) (a) and (b) </h3></td></tr>
		<tr><td colspan=9><h4> Returns of Additions and Deletions for the month of  January 2016  Part 'A'  Details of employees qualifying for membership of the fund  for the first time (along with form 2) </h4></td></tr>
		<tr><td colspan=9><h4> Code No. Of Factory/ Establishment: TN/VL/ 74670/ </h4></td></tr>
		<tr><td colspan=3><h4> Name and address of the Factory/ Establishment: </h4></td><td colspan=6>ALLZONE Management Solutions Private Limited</td></tr>
		<tr><td colspan=3></td><td colspan=6 rowspan=2>No: 3, First East Main Road, Gandhi Nagar East, 
Vellore - 632 006 Ph: 0416-2240333/ 2240332</td></tr><tr></tr>
		<tr><td>Sl.No</td><td>Account No:</td><td>Name of the Employee</td><td>Father's Name/ Husband Name</td><td>Age</td><td>Date of Birth</td><td>Sex</td><td>Date of Eligibility
 for the New Members, </td><td>Total period of Previous service</td></tr>";
		$x="";
		$i=1;
		  foreach($res as $key => $value)
		 { 
		 
		$x.="<tr><td>".$i."</td><td>".$value['acc_no']."</td><td>".$value['Name']."</td><td>".$value['Father_name']."</td><td>".$value['age']."</td><td>".$value['Dob']."</td><td>".$value['Sex']."</td><td>".$value['D_of_join']."</td><td>".$value['Total']."</td></tr>";
		$i++;
		 }
$data.=$x;		 
 $data.="
 <tr> <td>Place : </td><td colspan=5></td><td colspan=3>Signature of the employer/ Authorised officer</td></tr>  
  <tr> <td>Date : </td><td colspan=8></td></tr><tr></tr>  <tr><td colspan=6></td><td colspan=3>Stamp of the Factory/ Establishment</td></tr>
		</table>";
		//}
		$fname="form5_".date('m')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
	}

        
        
    public function master_salary_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('emp_salary/master_salary');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
	
	function master_salary(){
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
                {
                    
                    $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                    if ($this->form_validation->run()){
                    $res=$this->reports->master_salary();
                    
                    if($res){
                    
                    
                    $excel_data = "<table border=1>
<tr>
<th>Emp. No</th>
<th>Emp. Name</th>
<th> Basic </th>
<th>HRA</th>
<th>Conv</th>
<th>Allowance</th>
<th>Gross</th>
<th>Special Duty Allowance</th>
<th>Ref Incentive</th>
<th>Attendence incentive </th>
<th>Other Earning</th>
<th> Total Earnings </th>
<th>EPF 12%</th>
<th>ESI 1.75%</th>
<th>P.T</th>
<th>Income Tax</th>
<th>Other Deduction</th>
<th>Revised NET PAY</th>
<th>EPF 13.61%</th>
<th>ESI 4.75%</th>
<th>CTC</th>
<th>Holiday Allowance</th>
<th>D.i.m</th>
<th>LOP</th>
<th>Earned CTC</th>
<th>Revised Earned CTC</th>
<th>Category</th>
<th>Z1</th>
<th>Z2</th>
</tr>
";
   foreach($res as $row){
                    $excel_data.= "<tr>
<td>".$row['Emp_id']."</td>
<td>".$row['Name']."</td>
<td>".  round($row['basic'])."</td>
<td>".$row['hra']."</td>
<td>".$row['conv']."</td>
<td>".$row['allowance']."</td>
<td>".round($row['gross'])."</td>
<td>".  round($row['spd_allowance'])."</td>
<td>".round($row['ref_inc'])."</td>";
if(isset($row['half_attn_inc']) )
	 $excel_data.="<td>".  round($row['half_attn_inc'])."</td>";
 else
	 $excel_data.="<td>0</td>";
$excel_data.="<td></td>
<td>".$row['tot_ear']."</td>
<td>".round($row['epf_12_new'])."</td>";
if(isset($row['esi_175']) )
	 $excel_data.="<td>".  round($row['esi_175'])."</td>";
 else
	 $excel_data.="<td>0</td>";
 	 $excel_data.="<td>".$row['pt']."</td>
<td>".$row['it']."</td>
<td></td>
<td>".$row['rev_net_pay']."</td>
<td>".round($row['epf_1361n'])."</td>";

if(isset($row['esi_475']) )
	 $excel_data.="<td>".  round($row['esi_475'])."</td>";
 else
	 $excel_data.="<td>0</td>";
 		 $excel_data.="<td>".$row['Ctc']."</td>
<td>".$row['hda_allowance']."</td>
<td>".date('t')."</td>";

if(isset($row['lop']) )
	 $excel_data.="<td>".$row['lop']."</td>";
 else
	 $excel_data.="<td>0</td>";

	 $excel_data.="<td>".  round($row['ear_ctc'])."</td>
<td>".  round($row['rev_ear_ctc'])."</td>
<td>".  strtoupper($row['cat'])."</td>
<td>".$row['Zadditional1']."</td>
<td>".$row['Zadditional2']."</td>
</tr>";
//                    echo $excel_data;
                    }                 
             /*    foreach($res as $row){
                    $excel_data.= "<tr>
<td>".$row['Emp_id']."</td>
<td>".$row['Name']."</td>
<td>".$row['basic']."</td>
<td>".$row['hra']."</td>
<td>".$row['conv']."</td>
<td>".$row['allowance']."</td>
<td>".$row['gross']."</td>
<td>".$row['spd_allowance']."</td>
<td></td>
<td></td>
<td></td>
<td>".$row['tot_ear']."</td>
<td>".$row['epf_12_new']."</td>
<td>".array_key_exists('esi_175', $row)? $row['esi_175'] : ''."</td>
<td>".$row['pt']."</td>
<td>".$row['it']."</td>
<td></td>
<td>".$row['rev_net_pay']."</td>
<td>".$row['epf_1361n']."</td>
<td>".array_key_exists('esi_475',$row)?$row['esi_475'] : '' ."</td>
<td>".$row['Ctc']."</td>
<td>".$row['hda_allowance']."</td>
<td>".date('t')."</td>
<td>".array_key_exists('lop',$row)?$row['lop'] :''."</td>
<td>".$row['rev_ear_ctc']."</td>
<td>".$row['rev_ear_ctc']."</td>
<td>".$row['cat']."</td>
<td>".$row['Zadditional1']."</td>
<td>".$row['Zadditional2']."</td>
</tr>";
//                    echo $excel_data;
                    }*/
                    $excel_data.="</table>";
//                    echo $excel_data; die();
//                    print_r($res);
//                    die();
                    $fname="Master Salary - ".date('F')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $excel_data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
                    
                    }else{
                        $this->load->view('emp_salary/nosalary');
                    }
                    }else{
                        redirect('reports/master_salary_det');
                    }
                }
                else{
                    $this->load->view('master/noaccess');
    //                redirect("master/noaccess");
                }
            }
           else{
               redirect("master/login");
           }
		
		
		
	}
	function leave_salary(){
             if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
                {
                    
                    $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                    if ($this->form_validation->run()){
                        
		$res=$this->reports->leave_salary();
                if($res){
                
//               echo "<pre>"; print_r($res);
               $excel_data = "<table border='1'>
<tr>
<th>Emp. No</th>
<th>Emp. Name</th>
<th>IT</th>
<th>Earned CTC</th>
<th>NOD</th>
<th>Actual CTC</th>
<th>Leave Availed</th>
</tr>
";
               foreach($res as $row){
                   
                   $leave_taken = ($row['details']['Pl_credit']) - ($row['details']['rem_pl']);
                   
                   $excel_data.= "<tr>
<td>".$row['details']['Emp_id']."</td>
<td>".$row['details']['Name']."</td>
<td>".$row['tit']."</td>
<td>".$row['ectc']."</td>
<td>".$row['details']['rem_pl']."</td>
<td>".$row['tactc']."</td>
<td>".$leave_taken."</td>
</tr>";
               }
               
                $excel_data.="</table>";
//                echo $excel_data;die();
                $fname="Leave Salary - ".date('F')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $excel_data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); redirect('reports/leave_salary_det');
       
       }else{
                   $this->load->view('emp_salary/nosalary');
                }
       
       }else{
                        redirect('reports/leave_salary_det');
                    }
                }
                else{
                    $this->load->view('master/noaccess');
    //                redirect("master/noaccess");
                }
            }
           else{
               redirect("master/login");
           }
	
	}
	function leave_salary_yend(){
		$res=$this->reports->leave_salary_yend();
	
	}
	
	function array_replace_value(&$ar, $value, $replacement)
   {
	   
    if (($key = array_search($ar, $value)) !== FALSE) {
		
        $ar[$key] = $replacement;
    }
   }
	
	
	function rem_val(&$array, $key, $val) {
		
    foreach ($array as $id=> $item){
		
		
		  if (isset($item[$key]) && $item[$key] ==$val){
			unset($array[$id]);
    
		}
	}
    return false;
}

function getKeys($array)
{
    $result = array();
    foreach($array as $sub) {
        $result = array_merge($result, $sub);
    }        
    return array_keys($result);
}
    public function form_detail(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/form_details');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function form_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('frm', 'status', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                $status = $this->input->post('frm');
                if($status != 4 || $status != "4"){
                    $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');    
                }
                if ($this->form_validation->run())
                {                     
//                 echo"<pre>"; print_r($this->input->post());
                    if($status == 1 || $status == '1'){
                        $this->forms();
                    }else if($status == 2 || $status == '2'){
//                        $this->load->view('it/form_o');
                        $this->formo();
                    }else if($status == 3 || $status == '3'){
//                        $this->load->view('it/form_5');
                        $this->form5();
                    }
                    else{
//                        $this->load->view('it/form_III');
//                        $this->load->view('it/form_details');
                        $this->form3();
                    }
                    
                }else{                   
//                    echo validation_errors();
                     $this->load->view('it/form_details');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function leave_salary_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('emp_salary/leave_salary');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function payslip_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('emp_salary/payslip_det');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function payslip_record(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('payslip_type', 'status', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                $status = $this->input->post('payslip_type');
                if($status == 1 || $status == "1"){
                    $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');    
                }
                if ($this->form_validation->run())
                {                     
                    if($status == 1 || $status == '1'){

                        $this->payslip();
                    }
                    else{

//                        $this->Leave_salary_payslip();
                        $this->load->view('emp_salary/nopayslip');
                        
                    }
                    
                }else{                   
//                    echo validation_errors();
                      $this->load->view('emp_salary/payslip_det');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    
    public function nsa_salary_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('emp_salary/nsa_salary_det');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function nsa_salary(){
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'year', 'required|trim|xss_clean');
                 if ($this->form_validation->run()){
//                     print_r($this->input->post());  nsa_calc
                     $month = $this->input->post('month');
                     $year = $this->input->post('year');
                        $result =  $this->reports->nsa_salary();
                        if($result){
                            
                            $excel_data = " <b> Night Shift Allowance for the Month of $month - $year </b> <table border=1>                              
<tr>
<th>Emp. No</th>

<th> No of Days </th>
<th>Amount</th>
<th>Employee Contribution ESI</th>
<th> Net Allowance Amount Payable</th>
<th>Category</th>
</tr>
";
                            foreach($result as $row){
                    $excel_data.= "<tr>
<td>".$row['Emp.no']."</td>
<td>".$row['tnsa']."</td>
<td>".$row['Allowance']."</td>
<td>".$row['Emp_esi']."</td>
<td>".$row['net_pay']."</td>
<td>".$row['Categoty']."</td>
    ";
                            }
             $excel_data.="</table>";

                    $fname="Night Shift Allowance Salary - ".date('F')."-".date('Y').".xls";
		 write_file(FCPATH.'/assets/uploads/'.$fname, $excel_data, 'w+');
		 $data = file_get_contents(FCPATH."/assets/uploads/$fname"); // Read the file's contents
        $name = $fname;
       force_download($name, $data); 
                            
                        }else{
                            $this->load->view('emp_salary/nosalary');
                        }                     
                 }else{
                     $this->load->view('emp_salary/nsa_salary_det');
                 }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    /*
     *  Income tax update for slalry process
     */
    
    public function incometax_update_det(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/incometax_update_det');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function incometax_update(){
        if($this->auth->checku()){
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view')){
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month1', 'Month', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                 if ($this->form_validation->run()){
                     $data = $this->input->post();
                     $data['array' ]= $this->reports->incometax_update();
//                     echo "<pre>";print_r($data);die();
                     $this->load->view('it/incometax_update',$data);
                 }else{
                     $this->load->view('it/incometax_update_det');
                 }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function incometax_updatesal(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('Emp_id[]', 'Emp_id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('it_amount[]', 'IT Amount', 'required|trim|xss_clean');
                if ($this->form_validation->run()){
                    $this->reports->incometax_updatesal();
                    $this->load->view('it/incometax_update_det');
                }else{ 
                    $this->load->view('it/incometax_update_det');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
 
    }
}
