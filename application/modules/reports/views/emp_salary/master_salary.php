<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
         <style>
             .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a>Report</a>
                    </li>
                    <li>
                        <a> Master Salary </a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Master Salary  </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                              <form class="form-horizontal" id="master_sal" role="form" method="post" action="reports/master_salary">
                            <br>				
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Branch</label><span ></span>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group">
                                    <label class="control-label col-xs-2" for="month">Month</label>
                                    <div class="col-xs-3">
                                        <input type="text" readonly="" class="form-control" id="month" name="month" value="<?php echo date('F'); ?>" />
<!--                                        <select class="validate[required] form-control" id="month" name="month" data-placeholder="Select Month">
                                           <option value=""> </option>
                                            <option value ='13'> All Month </option>
                                            <option value ='1'> January </option>
                                            <option value ='2'> February </option>
                                            <option value ='3'> March </option>
                                            <option value ='4'> April </option>
                                            <option value ='5'> May </option>
                                            <option value ='6'> June </option>
                                            <option value ='7'> July </option>
                                            <option value ='8'> August </option>
                                            <option value ='9'> September </option>
                                            <option value ='10'> October </option>
                                            <option value ='11'> November </option>
                                            <option value ='12'> December </option>
                                        </select> -->

                                    </div>
                                </div>
                                     <div class="form-group">
                                         <?php 
                                            // $to_date = 26; 
                                           $to_date = date('d');
                                            ?>
                                            <div class="col-xs-3 col-sm-offset-3">
<!--                                                <input type="submit" <?php // if($to_date >= 25 && $to_date <= 31 ){ echo "disabled"; } ?> class="btn btn-primary" value="Generate Salary">-->
                                                 <input type="submit" <?php if($to_date < 26 ){ echo "disabled"; } ?> class="btn btn-primary" value="Generate Salary">
                                            </div>                                         
                                    </div>    
                                   <div class="form-group">
                                <div class="col-xs-12">
                                    <h5>Note : Master Salary can generate only in the following period 26th to last date of Every Month</h5>
                                </div>
                            </div>
                             </form> 
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
         $(document).ready(function(){
                $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#master_sal").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
// branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name
                   
//            $("#submit").click(function(){
//                    if ( $("#master_sal").validationEngine('validate') ) {
//                        return true;
//                    }                    
//                    return false;
//                });

                
            });    
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>