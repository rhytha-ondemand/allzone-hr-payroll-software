<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
    <style>
        .form-control[readonly]{
            cursor: text;
            background-color:#fff;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Income Tax</a>
                    </li>
                    <li>
                        <a >Update</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Income Tax Update</h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <br>
                            <form class="form-horizontal" role="form" id="itupdate" method="POST" action="reports/incometax_update">
                                <br>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2"  for="bname"> Branch</label><span ></span>
                                        <div class="col-xs-3">
                                            <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                                <option value=""></option>
                                                <option value="4">Chennai</option>
                                                <option value="3">Vellore</option>                                            
                                            </select>
                                        </div>
                                    </div>
                                
                                <div class="form-group" id="emp_no">
                                        <label class="control-label col-xs-2"  for="month"> Month</label><span ></span>
                                        <div class="col-xs-3">
                                             <input type="text" readonly="" value="<?php echo date('F'); ?>" class="validate[required] form-control" name="month" id="month" />
                                             <input type="hidden" value="<?php echo date('m'); ?>" name="month1" id="month1" />
                                        </div>
                                </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="year"> year</label>
                                        <div class="col-xs-3">
                                            <input type="text" readonly="" value="<?php echo date('Y'); ?>" class="validate[required] form-control" name="year" id="year" />                                               
                                        </div>                                    
                                    </div>
                                
                                    <div class="form-group">
                                        <div class="col-xs-3 col-sm-offset-3">
                                            <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                            </form>
                            <!--working content start-->
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
  <script>
            $(document).ready(function(){
                $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#itupdate").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#itupdate").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#itupdate").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
// branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name
            
//            $("#submit").click(function(){
//                    if ( $("#itupdate").validationEngine('validate') ) {
//                        return true;
//                    }                    
//                    return false;
//                });

                
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>