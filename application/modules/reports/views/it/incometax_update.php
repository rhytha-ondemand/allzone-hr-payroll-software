<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
    <style>
        .form-control[readonly]{
            cursor: text;
            background-color:#fff;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Income Tax</a>
                    </li>
                    <li>
                        <a >Update</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Income Tax Update for <?php echo $month .'  '. $year ?> </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <form class="form-horizontal" role="form" id="itupdate" method="POST" action="reports/incometax_updatesal">
                                
                                <table id="itupdte" class="table table-striped table-bordered bootstrap-datatable responsive">
                                    <thead>
                                        <tr>
                                            <th>S. No</th>
                                            <th>Emp.No</th>
                                            <th>Emp.Name</th>
                                            <th>Income Tax Amount</th>
                                            <!--<th>Total Deduction</th>-->
                                            <th>Net Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $flag = 1;
                                        if($array){
                                            $i = 1;
                                            $Net_amt =0;
                                            foreach ($array as $row){
                                                if(isset($row['It_amt']) && $row['It_amt'] != '' ){
                                                    $It_amt = $row['It_amt'];
                                                }else if(isset($row['employ_it']) && $row['employ_it'] != '' ){
                                                    $It_amt = $row['employ_it'];
                                                }else{
                                                    $It_amt = 0;
                                                }
                                                
                                                if(isset($row['Net_amt']) && $row['Net_amt'] != '' ){
                                                    $Net_amt = $row['Net_amt'];
                                                }else{
                                                    $Net_amt = $row['employ_net'];
                                                }
                                                
                                                echo "<tr>";
                                                echo "<input type='hidden' value='".$row['Id']."' name='Id[]'  /> ";
                                                echo "<input type='hidden' value='".$row['Emp_id']."' name='Emp_id[]'  /> ";
                                                echo "<td>". $i++ ."</td>";
                                                echo "<td>". $row['Emp_id'] ."</td>";
                                                echo "<td>". $row['Name'] ."</td>";
                                                echo "<td> <input type='text' class='validate[required,min[0],custom[number]]' name='it_amount[]' value='". $It_amt."' /> </td>";
//                                                echo "<td>  <input type='text' name='tot_deduct[]' value='0' /> </td>";
                                                echo "<td> <input type='text' readonly name='net_amount[]' value='". round($Net_amt) ."' /> </td>";
                                                echo "</tr>";
                                            }
                                        }else{
                                            $flag = 0;
                                            echo "<tr><td colspan='5'> Salary not yet generated for the given month </td></tr>";
                                        }

                                    ?>
                                    </tbody>
                                    
                                </table>
                                <?php
                                    if($flag){
                                        ?>                                
                                    <div class="form-group">
                                        <div class="col-xs-3 col-sm-offset-5">
                                            <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                <?php                                        
                                    }                                
                                ?>
                            </form>
                            <!--working content start-->
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
  <script>
            $(document).ready(function(){
                $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#itupdate").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
//                $("#itupdate").bind("jqv.form.validating", function(event){
//                    $("#hookError").css('display','none');
//                    $("#hookError").html("");
//                });
//                $("#itupdate").bind("jqv.form.result", function(event , errorFound){
//                    if(errorFound){ 
//                        $("#hookError").append("Please fill all required fields");
//                        $("#hookError").css('display','block');                        
//                    }
//                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });

            
//            $("#submit").click(function(){
//                    if ( $("#itupdate").validationEngine('validate') ) {
//                        return true;
//                    }                    
//                    return false;
//                });

                
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>