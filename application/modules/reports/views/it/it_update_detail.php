<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #emp_id_chosen{
                width:100%!important;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a>Report</a>
                    </li>
                    <li>
                        <a>Income Tax</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Income Tax Update </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <br>
                            <form class="form-horizontal" role="form" id="it_update" method="POST" action="reports/it_update">
                                <br>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2"  for="bname"> Branch</label><span ></span>
                                        <div class="col-xs-3">
                                            <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                                <option value=""></option>
                                                <option value="4">Chennai</option>
                                                <option value="3">Vellore</option>                                            
                                            </select>
                                        </div>
                                    </div>
                                
                                <div class="form-group" id="emp_no">
                                        <label class="control-label col-xs-2"  for="emp_id"> Employee No</label><span ></span>
                                        <div class="col-xs-3">
                                            <select id="emp_id" name="emp_id" data-placeholder="Select Employee" class="validate[required] form-control" >
                                                <option value=""></option>                                            
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">IT year</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" data-placeholder="Select Year" name="year" id="year" data-rel="chosen">
                                               <option value=""> </option> 
                                               <?php
                                                    $curr_year = date('Y');
                                                    for($i=2017;$i<=$curr_year+1;$i++ ){
                                                        echo "<option value='$i'> $i</option>" ;
                                                    }
                                               ?>
                                            </select> 
                                        </div>                                    
                                    </div>
                                
                                    <div class="form-group">
                                        <div class="col-xs-3 col-sm-offset-3">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
         <script>
            $(document).ready(function(){
                
            
             $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#it_update").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#it_update").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#it_update").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });

        // branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name
                $("#bname").change(function(){
                    var emp_list ="<option></option>";
                    $.ajax({
                        type:"POST",
                        url:"master/emp_id_list",
                        cache:false,
                        data:"bname="+$("#bname").val(),
                        dataType:"json",
                        async:false,
                        success:function(json){
                             $("#emp_id").find('option').remove();
                            if(json){                               
                                $.each(json,function(i,value){
                                    emp_list += "<option value="+value['Id']+">"+value['emp_name']+"</option>";
                                });
                                $("#emp_id").append(emp_list);                                
                            }
                            $("#emp_id").trigger("chosen:updated");
                        }
                    });
                });
         /*   $('#radioDiv input').on('change', function() {                
                if ($("#radioDiv input[type='radio']:checked").val() == 2) {                
                    $("#emp_no").css('display','block');                                                     
                }else{                                        
                    $("#emp_no").css('display','none');
                } 
            });*/
                
            $("#submit").click(function(){
                    if ( $("#it_update").validationEngine('validate') ) {
                        return true;
                    }                    
                    return false;
                });
                
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>