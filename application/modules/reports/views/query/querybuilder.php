<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
<style>
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        height:37px;
        padding:1px!important;
		
    }
    .default{
        height: 25px!important;
        padding-top: 2px!important
    }
    #emp_id_chosen{
        /*height:37px;*/
        max-width:200%!important;
        display:inline-block;
    }
    .bordering{
        display:none;
    }
    .form-control[readonly]{
        cursor: text;
        background-color:#fff;
    }
</style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
                 <?php $this->load->view('includes/sidebar.php');?>
		
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                        <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Query Builder</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Query Builder </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form class="form-horizontal" id="query_builder" role="form" action="reports/rep_field_list" method="post">
                                <br>
                                <br>
							<div class="form-group">
                                        <label class="control-label col-xs-2" for="inputSuccess4">Report</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required]  form-control" data-rel="chosen" id="rep_list" name="rep_list">
                                                <OPTION value=""> Select Report</OPTION>
						<!--<OPTION value="1">Master Salary Report</OPTION>-->
                                                <option value ='2' > Teamwise Report </option>
                                                <option value ='3'> Employee Personal Report </option>
                                                <!--<option value ='4'> Each Employee Report </option>-->
                                                <option value ='5'> Consolidate Referral Incentive </option>
                                                <option value ='6'> Appraisal/Refix Report </option>
                                                <!--<option value ='7'> Leave Salary creator </option>-->
                                                <!--<option value ='8'> Leave Salary Payslip</option>-->
                                                <option value ='9'> Leave Salary Summary</option>											<!--					     <option value ='10'> Leave Salary Bank Credit</option>
                                                <option value ='11'> Monthly Salary creator</option>-->
                                                <!--<option value ='12'>Monthly Salary Payslip </option>-->
                                                <option value ='13'> Monthly Salary Summary</option>
<!--						     <option value ='14'> Monthly Salary Bank Credit</option> 
                                                <option value ='15'> Pf Details</option>
                                                <option value ='16'> IT Update </option>
                                                <option value ='17'>IT Statement </option>
                                                <option value ='18'>IT Support </option> 
                                                <option value ='19'>IT List </option>-->
<!--						<option value ='20'> IT Details </option>-->

                                            </SELECT> 
															

                                        </div>
                                        <label class="control-label col-xs-2" for="inputSuccess4">Branch</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required]  form-control" id="branch" data-rel="chosen" name="branch">
                                                <option value="">Select Branch</option>
                                                <option value="Regular">Chennai</option>
                                                <option value="Contract">Vellore</option>
                                            </select>
                                        </div>
                                </div>
								
					<!-- Team wise report  Start -->
								<div class="bordering subcontainer" id="r2" style="margin: 0em " data-value="2" >
								<h1><span>Teamwise report</span></h1>
							<div class="row">
								<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Gender</label>
							  <!--<label class="radio-inline col-xs-2" for="inputSuccess4"><input class="" type="radio" name="gen" id="gen" value="1">All</label>-->
							   <label class="radio-inline col-xs-2" for="inputSuccess4"><input class="" type="radio" name="gen" id="gen" value="1">Male</label>
							   <label class=" radio-inline col-xs-2" for="inputSuccess4"><input class="" type="radio" name="gen" id="gen" value="2">Female</label>
                                </div>
								
								<div class="form-group col-xs-6" data-value="team_rp">
								 <label class="control-label col-xs-3" for="inputSuccess4">Report Team</label>
                                        <div class="col-xs-3">
                                            <select class="form-control" id="emp_rtteam" name="emp_rteam"	data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Team</option>
<!--                                                <option value="Regular">Chennai</option>
                                                <option value="Contract">Vellore</option>-->
                                            </select>
                                        </div>
								
								</div>
							</div>
							<div class="row">
								<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Designation</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_rtdesig" Name="emp_rdesig" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Designation</option>
                                               <!--<OPTION value="ALL">ALL</OPTION>-->
						<!--<option value ='125'> 1 </option>-->
						
                                            </select>
                                        </div>
							 
                                </div>
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Project</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_rprj" Name="emp_rtprj" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Project</option>
                                               <!--<OPTION value="ALL">ALL</OPTION>-->
						<!--<option value ='125'> 1 </option>-->
						
                                            </select>
                                        </div>
							 
                                </div>
						</div>
						<!--Department-->
						<div class="row">
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Department</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_rtdept" Name="emp_rtdept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Department</option>
                                               <!--<OPTION value="ALL">ALL</OPTION>-->
						<!--<option value ='125'> 1 </option>-->
						
                                            </select>
                                        </div>
							 
                                </div>
						
						</div>
						
						</div>
						
				<!-- Team wise report  End -->	
					

                <!--- Employee Personal report  Start -->					
				<div class="bordering subcontainer" style="margin: 0em " id="r3">
								<h1><span>Employee Personal report</span></h1>
                   <div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Status</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repstatus" name="emp_repstatus" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Status</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='1'> Active </option>
						<option value ='0'> Inactive </option>
						<option value ='2'> Transfer </option>
                                            </select>
                                        </div>
							 
                                </div>
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Gender</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repgender" Name="emp_repgender" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Gender</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='1'> Male </option>
						<option value ='2'>Female </option>
                                            </select>
                                        </div>
							 
                                </div>		
                    </div>	
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Designation</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repdesig" Name="emp_repdesig" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Designation</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Active </option>
						<option value ='3'> Inactive </option>
						<option value ='4'> Transfer </option>-->
                                            </select>
                                        </div>
							 
                                </div>
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Department</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repdept" Name="emp_repdept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Department</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Male </option>
						<option value ='3'>Female </option>-->
                                            </select>
                                        </div>
							 
                                </div>		
                    </div>	
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Team</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repteam" Name="emp_repteam" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Team</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Active </option>
						<option value ='3'> Inactive </option>
						<option value ='4'> Transfer </option>-->
                                            </select>
                                        </div>
							 
                                </div>
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Project</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_repprj" Name="emp_repprj" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Project</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Male </option>
						<option value ='3'>Female </option>-->
                                            </select>
                                        </div>
							 
                                </div>		
                    </div>	
				</div>	
				
				 <!--- Employee Personal report  End -->	
                              
				<!--- Each Employee report  Start -->					
				<div class="bordering subcontainer" style="margin: 0em " id="r4" >
								<h1><span>Each Employee report</span></h1>
                   <div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Status</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_reestatus" Name="emp_reestatus" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Status</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='1'> Active </option>
						<option value ='0'> Inactive </option>
						<option value ='2'> Transfer </option>
                                            </select>
                                        </div>
							 
                                </div>
						
                       <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Department</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_reedept" Name="emp_reedept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Department</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Male </option>
						<option value ='3'>Female </option>-->
                                            </select>
                                        </div>
							 
                                </div>
						
                    </div>	
					
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Team</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_reeteam" Name="emp_reeteam" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Team</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Active </option>
						<option value ='3'> Inactive </option>
						<option value ='4'> Transfer </option>-->
                                            </select>
                                        </div>
							 
                                </div>
						<div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Current Project</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_reecprj" Name="emp_reecprj" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Project</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Male </option>
						<option value ='3'>Female </option>-->
                                            </select>
                                        </div>
							 
                                </div>		
                    </div>	
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">No of Years</label>
							  <div class="col-xs-1 ">
                                           <select class="form-control " id="emp_reemin" Name="emp_reemin" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Min</option>
                                               <OPTION value="1">0</OPTION>
						<option value ='2'> 1 </option>
						<option value ='3'> 2 </option>
						<option value ='4'> 3 </option>
                                            </select>
                                        </div>
							     <div class="col-xs-1 col-sm-offset-2">
                                           <select class="form-control" id="emp_reemax" name="emp_reemax" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Max</option>
                                               <OPTION value="1">1</OPTION>
						<option value ='2'> 2 </option>
						<option value ='3'> 3 </option>
						<option value ='4'> 4 </option>
                                            </select>
                                        </div>
                                </div>
						

						
                    </div>	
					
				</div>	
				
				 <!--- Each Employee report  End -->	
				<!--- Consolidate Referral Incentive   Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r5">
								<h1><span>Consolidate Referral Incentive</span></h1>
                        <div class="row">								
                            <div class="form-group col-xs-6" data-value="team_rp">                      
                                <label class="control-label col-xs-3 " for="sdate">From</label>
                                <div class="col-xs-6 ">
                                    <input type="text" class=" form-control datepicker" readonly="" id="sdate" Name="sdate"  />
                                </div>
                            </div>
                            <div class="form-group col-xs-6" data-value="team_rp">                      
                                <label class="control-label col-xs-3 " for="inputSuccess4">To</label>
                                <div class="col-xs-6 ">
                                    <input type="text" class=" form-control datepicker" readonly="" id="edate" Name="edate" />
                                </div>
                            </div>
                        </div>
<!--					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">From</label>
							  <div class="col-xs-1 ">
                                           <select class=" form-control " id="emp_crmonth" Name="emp_crmonth" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Month</option>
                                               <OPTION value="1">JAN</OPTION>
						<option value ='2'> FEB </option>
						<option value ='3'> MAR </option>
						<option value ='4'> APR </option>
						<option value ='5'> MAY </option>
						<option value ='6'> JUNE </option>
						<option value ='7'> JULY </option>
						<option value ='8'> AUG </option>
						<option value ='9'> SEP </option>
						<option value ='10'> OCT </option>
						<option value ='11'> NOV </option>
						<option value ='12'> DEC </option>
                                            </select>
                                        </div>
							     <div class="col-xs-1 col-sm-offset-2">
                                           <select class="yea form-control" id="emp_cryear" Name="emp_cryear" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Year</option>
                                               <OPTION value="2016">2016</OPTION>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018 </option>
						<option value ='2019'> 2019 </option>
                                            </select>
                                        </div>
                                </div>
					                   								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">To</label>
							  <div class="col-xs-1 ">
                                           <select class="yea form-control " id="emp_crmonth2" Name="emp_crmonth2" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Month</option>
                                               <OPTION value="1">JAN</OPTION>
						<option value ='2'> FEB </option>
						<option value ='3'> MAR </option>
						<option value ='4'> APR </option>
						<option value ='5'> MAY </option>
						<option value ='6'> JUNE </option>
						<option value ='7'> JULY </option>
						<option value ='8'> AUG </option>
						<option value ='9'> SEP </option>
						<option value ='10'> OCT </option>
						<option value ='11'> NOV </option>
						<option value ='12'> DEC </option>
                                            </select>
                                        </div>
							     <div class="col-xs-1 col-sm-offset-2">
                                           <select class=" form-control " id="emp_cryear2" Name="emp_cryear2" data-rel="chosen" style="width: 90px;">
                                                <option value="">Select Year</option>
                                               <OPTION value="2016">2016</OPTION>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018 </option>
						<option value ='2019'> 2019 </option>
                                            </select>
                                        </div>
                                </div>
						

						
                    </div>-->
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Emp. Status</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_crstatus" Name="emp_crstatus" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Status</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='1'> Active </option>
						<option value ='0'> Inactive </option>
						<option value ='2'> Transfer </option>
                                            </select>
                                        </div>
							 
                                </div>
						
                    </div>	
					
				</div>	
				
				 <!--- Consolidate Referral Incentive  End -->			  
					
                <!---Appraisal/Refix Report   Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r6">
								<h1><span>Appraisal/Refix Report</span></h1>
                  				
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">Department</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_ardept" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Department</option>
<!--                                               <OPTION value="1">ALL</OPTION>
						<option value ='2'> Active </option>
						<option value ='3'> Inactive </option>
						<option value ='4'> Transfer </option>-->
                                            </select>
                                        </div>
							    
                        </div>
					                   								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">Team</label>
							 
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_arteam" Name="emp_arteam" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Team</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
<!--						<option value ='2'> Active </option>
						<option value ='3'> Inactive </option>
						<option value ='4'> Transfer </option>-->
                                            </select>
                                        </div>
							  
                     </div>
						

						
                    </div>
					<div class="row">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Emp. Status</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_arstatus" name="emp_arstatus" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Status</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='1'> Active </option>
						<option value ='0'> Inactive </option>
						<option value ='2'> Transfer </option>
                                            </select>
                                        </div>
							 
                                </div>
						
                    </div>	
					
				</div>	
				
				 <!--- Appraisal/Refix Report  End -->
                
                <!---Leave Salary Creator  Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r7">
								<h1><span>Leave Salary Creator</span></h1>
                  				
					<div class="row">								
				    <div class="form-group col-xs-6 center" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">Select Year</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_lscdept" Name="emp_lyear" data-rel="chosen" >
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016 </option>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018</option>
                                            </select>
                                        </div>
							    
                        </div>
					                   								
				   
						

						
                    </div>
					
					
				</div>	
				
				 <!--- Leave Salary Creator END -->				
				 
				 <!---Leave Salary Payslip  Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r8">
								<h1><span>Leave Salary Payslip</span></h1>
                  				
					<div class="row">								
				    <div class="form-group col-xs-12" data-value="team_rp">                      
                              <label class="control-label col-xs-2 " for="inputSuccess4">Department</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_ardept" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016 </option>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018</option>
                                            </select>
                                        </div>
						<label class="control-label col-xs-2" for="inputSuccess4">Employee Id</label>
                                        <div class="col-xs-3">
										 <select class="form-control" id="emp_lpempid" Name="emp_ardept" data-rel="chosen" >
                                                <option value="">Select ID</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> AMSC1 </option>
						<option value ='2017'>  AMSC2 </option>
						<option value ='2018'>  AMSC3</option>
                                            </select>
										</div>
							    
                     </div>
					
                    </div>
					
					
				</div>	
				
				 <!--- Leave Salary Payslip END -->	

  <!---Leave Salary Summary  Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r9">
								<h1><span>Leave Salary Summary</span></h1>
                  				
					<div class="row col-sm-offset-3">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-3 " for="inputSuccess4">Select Year</label>
							  <div class="col-xs-8">
                                           <select class="validate[required] form-control" id="emp_lsdept" Name="lsyear" data-rel="chosen" >
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016 </option>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018</option>
                                            </select>
                                        </div>
							    
                        </div>
					                   								
				   
						

						
                    </div>
					
					
				</div>	
				
				 <!--- Leave Salary Summary END -->	
				  <!---Monthly Salary Payslip Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r12">
								<h1><span>Monthly Salary Payslip</span></h1>
                  				
					<div class="row">								
				    <div class="form-group col-xs-12" data-value="team_rp">                      
                              <label class="control-label col-xs-2 " for="inputSuccess4">Employee Id</label>
							  <div class="col-xs-3">
                                           <select class="form-control" id="emp_ardept" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select ID</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> AMSC1 </option>
						<option value ='2017'> AMSC2</option>
						<option value ='2018'> AMSC3</option>
                                            </select>
                                        </div>
					</div>
					</div>
   					<div class="row">								
				    <div class="form-group col-xs-12" data-value="team_rp">	
						<label class="control-label col-xs-2" for="inputSuccess4">Pay Slip From</label>
                                        <div class="col-xs-3">
                                        <select class="form-control" id="emp_msm1" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Month</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<OPTION value="1">JAN</OPTION>
						<option value ='2'> FEB </option>
						<option value ='3'> MAR </option>
						<option value ='4'> APR </option>
						<option value ='5'> MAY </option>
						<option value ='6'> JUNE </option>
						<option value ='7'> JULY </option>
						<option value ='8'> AUG </option>
						<option value ='9'> SEP </option>
						<option value ='10'> OCT </option>
						<option value ='11'> NOV </option>
						<option value ='12'> DEC </option>
                                            </select>
										</div>
										  <div class="col-xs-4">
										 <select class="form-control" id="emp_msy1" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016</option>
						<option value ='2017'>  2017 </option>
						<option value ='2018'>  2018</option>
                                            </select>
										</div>
							    
                     </div>
					
                    </div>
					<div class="row">								
				    <div class="form-group col-xs-12" data-value="team_rp">	
						<label class="control-label col-xs-2" for="inputSuccess4">Pay Slip To</label>
                                        <div class="col-xs-3">
										 <select class="form-control" id="emp_msm2" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Month</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<OPTION value="1">JAN</OPTION>
						<option value ='2'> FEB </option>
						<option value ='3'> MAR </option>
						<option value ='4'> APR </option>
						<option value ='5'> MAY </option>
						<option value ='6'> JUNE </option>
						<option value ='7'> JULY </option>
						<option value ='8'> AUG </option>
						<option value ='9'> SEP </option>
						<option value ='10'> OCT </option>
						<option value ='11'> NOV </option>
						<option value ='12'> DEC </option>
                                            </select>
										</div>
										  <div class="col-xs-4">
										 <select class="form-control" id="emp_msy2" Name="emp_ardept" data-rel="chosen" style="width: 234px;">
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016</option>
						<option value ='2017'>  2017 </option>
						<option value ='2018'>  2018</option>
                                            </select>
										</div>
							    
                     </div>
					
                    </div>
					
				</div>	
				
				 <!--- Monthly Salary Payslip END -->
				 <!---Monthly Salary Summary  Start -->					
				<div class="bordering subcontainer" style="margin: 0em "  id="r13">
								<h1><span>Monthly Salary Summary</span></h1>
                  	<div class="row col-sm-offset-3">								
				    <div class="form-group col-xs-6" data-value="team_rp">                      
                              <label class="control-label col-xs-4 " for="inputSuccess4">Select Month</label>
							  <div class="col-xs-8">
                                           <select class="validate[required] form-control" id="msmonth" Name="msmonth" data-rel="chosen">
                                                <option value="">Select Month</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<OPTION value="1">JAN</OPTION>
						<option value ='2'> FEB </option>
						<option value ='3'> MAR </option>
						<option value ='4'> APR </option>
						<option value ='5'> MAY </option>
						<option value ='6'> JUNE </option>
						<option value ='7'> JULY </option>
						<option value ='8'> AUG </option>
						<option value ='9'> SEP </option>
						<option value ='10'> OCT </option>
						<option value ='11'> NOV </option>
						<option value ='12'> DEC </option>
                                            </select>
                                        </div>
							    
                        </div>
					                   								
				   
						

						
                    </div>			
					<div class="row col-sm-offset-3">								
				    <div class="form-group col-xs-6 " data-value="team_rp">                      
                              <label class="control-label col-xs-4 " for="inputSuccess4">Select Year</label>
							  <div class="col-xs-8">
                                           <select class="validate[required] form-control" id="msyear" Name="msyear" data-rel="chosen">
                                                <option value="">Select Year</option>
                                               <!--<OPTION value="1">ALL</OPTION>-->
						<option value ='2016'> 2016 </option>
						<option value ='2017'> 2017 </option>
						<option value ='2018'> 2018</option>
                                            </select>
                                        </div>
							    
                        </div>
					                   								
				   
						

						
                    </div>
					
					
				</div>	
				
				 <!--- Monthly Salary Summary END -->	
				 
				 
				</br>
				</br>			  
									<div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                        <input type="submit" class="btn btn-primary" value="Next">
                                    </div>
                                   </div>
							
                            </form>
									
                            <!--working area end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
          <?php $this->load->view('includes/footer.php');?>
        </div>
		<script>
		 toappend="";
		$(document).ready(function(){
                    $.validationEngine.defaults.scroll = false;
               $("#query_builder").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                    
                    $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                    
                 $("#sdate").datepicker({dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2015:+0",
                    onClose: function( selectedDate ) {
                        $( "#edate" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#edate").datepicker({dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2015:+0"});
                    
			url="master/location_all";
				 $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    //data: data,
                    async: false,
                    success: function(json) {
                        $('#branch').find('option').remove();
						if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#branch').append(toappend);
                            $("#branch").trigger('chosen:updated');
							
                        }
                    }            
                });
			
			
			$('.subcontainer').hide();
			$("#rep_list").on("change",function(){
				$(".chosen-container").each(function(){
	
        if($(this).width() == 0){
            $(this).width(235);
			$("#emp_cryear_chosen").width(90);
			$("#emp_crmonth_chosen").width(90);
			$("#emp_cryear2_chosen").width(90);
			$("#emp_crmonth2_chosen").width(90);
			$("#emp_min_chosen").width(90);
			$("#emp_max_chosen").width(90);
			$("#emp_reemin_chosen").width(90);
			$("#emp_reemax_chosen").width(90);
			
        }
    });
			var target = "#r" + $("#rep_list :selected").val();
			$('.subcontainer').not(target).hide();
			$(target).show();
			//	console.log($("#rep_list :selected").val());
			//console.log($("#team_wise").data('value'));
		});
		$("#branch").on("change",function(){
			toappend="";
			toappend_desig="";
			toappend_prj="";
			toappend_team="";
			loc=$("#branch :selected").val();
			data="bname="+loc;
		
			$deptsrc=$desigsrc=$prjsrc=$teamsrc="master/emp_dynamic";
			//Department
			$.ajax({
                    type: "POST",
                    url: $deptsrc,
                    cache: false,
					data:data,
                    dataType: "json",
                    //data: data,
                    async: false,
                    success: function(json) {
                        $('#emp_rtdept').find('option').remove();
						$('#emp_repdept').find('option').remove();
						$('#emp_reedept').find('option').remove();
						$('#emp_ardept').find('option').remove();
						//Designation
						 $('#emp_rtdesig').find('option').remove();
						$('#emp_repdesig').find('option').remove();
						
						//Projects
						$('#emp_rprj').find('option').remove();
						$('#emp_repprj').find('option').remove();
						$('#emp_reecprj').find('option').remove();
						
						//team
						$('#emp_rtteam').find('option').remove();
						$('#emp_repteam').find('option').remove();
						$('#emp_reeteam').find('option').remove();
						$('#emp_arteam').find('option').remove();
						
						if(json) {
							toappend+='<option value=""></option>';
                             $.each(json['dept'], function(i, value) {
								toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
                            });   
                            $('#emp_rtdept').append(toappend);
                            $("#emp_rtdept").trigger('chosen:updated');
							$('#emp_repdept').append(toappend);
							$('#emp_repdept').trigger('chosen:updated');
							$('#emp_reedept').append(toappend);
							$('#emp_reedept').trigger('chosen:updated');
							$('#emp_ardept').append(toappend);
							$('#emp_ardept').trigger('chosen:updated');
							//Designation
							toappend_desig+='<option value=""></option>';
                             $.each(json["design"], function(i, value) {
                                 toappend_desig+='<option value='+value['Id']+'>'+value['Design_name']+'</option>';
                                
                            });   
                            $('#emp_rtdesig').append(toappend_desig);
                            $("#emp_rtdesig").trigger('chosen:updated');
							$('#emp_repdesig').append(toappend_desig);
							$('#emp_repdesig').trigger('chosen:updated');
							
							//Projects
							toappend_prj+='<option value=""></option>';
                             $.each(json['project'], function(i, value) {
								toappend_prj+='<option value='+value['Project_id']+'>'+value['Project_name']+'</option>';
                                
                            });   
                            $('#emp_rprj').append(toappend_prj);
                            $("#emp_rprj").trigger('chosen:updated');
							$('#emp_repprj').append(toappend_prj);
							$('#emp_repprj').trigger('chosen:updated');
							$('#emp_reecprj').append(toappend_prj);
							$('#emp_reecprj').trigger('chosen:updated');
							
							//team
							toappend_team+='<option value=""></option>';
                             $.each(json['teamlist'], function(i, value) {
								toappend_team+='<option value='+value['reporting_no']+'>'+value['team']+'</option>';
                                
                            });   
                            $('#emp_rtteam').append(toappend_team);
                            $("#emp_rtteam").trigger('chosen:updated');
							$('#emp_repteam').append(toappend_team);
							$('#emp_repteam').trigger('chosen:updated');
							$('#emp_reeteam').append(toappend_team);
							$('#emp_reeteam').trigger('chosen:updated');
							$('#emp_arteam').append(toappend_team);
							$('#emp_arteam').trigger('chosen:updated');
							
						}
                    }            
                });
			
			
		});
		
	
		
		
		
		});
		</script>
		 <?php $this->load->view('includes/additional.php');?>
    </body>
</html>