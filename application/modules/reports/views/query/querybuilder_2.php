<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
<style>
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        height:37px;
        padding:1px!important;
		
    }
    .default{
        height: 25px!important;
        padding-top: 2px!important
    }
    #emp_id_chosen{
        /*height:37px;*/
        max-width:200%!important;
        display:inline-block;
    }
</style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
                 <?php $this->load->view('includes/sidebar.php');?>
		
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                        <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Query Builder</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Query Builder </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-1">
						 
                            <!--working content start-->
							<div class="row">
							
								
							
							<div class="form-group col-xs-6">  
							<form id="form1" class="form-horizontal" role="form" action="reports/rep_field_list2" method="post">                    
							<input type="hidden" name="rep_no" value="<?=$rep_no;?>">
                                                        <input type="hidden" name="branch" value="<?php echo $branch; ?>"/>
                                                        <input type="hidden" name="emp_rteam" value="<?php echo $emp_team; ?>"/>	
                                                        <input type="hidden" name="emp_rdesig" value="<?php echo $emp_desig; ?>"/>	
                                                        <input type="hidden" name="emp_rtprj" value="<?php echo $emp_prj; ?>"/>	
                                                        <input type="hidden" name="emp_rtdept" value="<?php echo $emp_dept; ?>"/>	
                                                        <input type="hidden" name="gen" value="<?php echo $emp_repgender; ?>"/>	
                                                        <input type="hidden" name="sdate" value="<?php echo $sdate; ?>"/>	
                                                        <input type="hidden" name="edate" value="<?php echo $edate; ?>"/>	
                                                        <input type="hidden" name="status" value="<?php echo $emp_status; ?>"/>	
                                                        <input type="hidden" name="max" value="<?php echo $emp_reemax; ?>"/>	
                                                        <input type="hidden" name="min" value="<?php echo $emp_reemin; ?>"/>
                                                        <input type="hidden" name="lyear" value="<?php echo $lyear; ?>"/>
                                                        <input type="hidden" name="msmonth" value="<?php echo $msmonth; ?>"/>
                                                        <input type="hidden" name="msyear" value="<?php echo $msyear; ?>"/>
							<div id="flist" class="selected">
							<?php
						
							if(isset($res) && is_array($res) && count($res)>0){
								
								?>
					<!--<input type="checkbox" value="0" id="selecctall" >All</br>-->			<?php
					
					foreach($res as $id=>$value){
						
						if(!in_array($id,$temp)){
					?>
					
					<input type="checkbox" name="flist[]" checked="checked"  disabled="disabled" value="<?=$id?>"><?=$value?></br>
					
					<?php 
					}
					} if(isset($temp) && is_array($temp) && count($temp)>0){ ?>
					<h4>Additional Fields</h4>
					<?php
					foreach($temp as $key=>$value){
						$tid=$value;
						$tvalue=$res[$value];?>
						<input class="item" type="checkbox" name="flist2[]" checked="checked"  
						value="<?=$tid?>" data-value=<?=$tid?>><?=$tvalue?></br>
					<?php }
					
					}
							}
					?>
							</div>
							<input type="hidden" name="flist2" value="">
							<!--<input class="btn btn-primary" type="button" id="test" value="Next">-->
							<input class="btn btn-primary" type="submit" id="test2" value="Next">
							</form>
							</div>
								<div class="form-group col-xs-6">                      
                              <label class="control-label col-xs-3" for="inputSuccess4">Tables</label>
							  <div class="col-xs-3">
                                           
											<select multiple="multiple" class="" id="emp_rtdesig" Name="emp_rdesig">
											
					<?php
					foreach($employee as $id=>$value){
					?>
					<option value="<?=$value['id'];?>" data-section="Employee"><?=$value['field'];?></option>
											
					<?php  }  
					foreach($personal as $id=>$value){
					?>
						<option value="<?=$value['id'];?>" data-section="personal"><?=$value['field'];?></option>					
						<?php  } ?>
 						</select>
                                        </div>
							 
                                </div>
							
							</div>
                            <!--working area end-->
							
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
          <?php $this->load->view('includes/footer.php');?>
        </div>
		<script>
		 toappend="";
		$(document).ready(function(){
			
			 var options = {
    startCollapsed: true
  };
			$("select").treeMultiselect(options);
			$("#test").on("click",function(){
				t="<input type=checkbox value=0>test";
				$("#flist").append(t);
				
			});
			
			$('#selecctall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('input[type=checkbox]').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('input[type=checkbox]').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
		
		$("#test2").on("click",function(event){
			event.preventDefault();
			var elems = [];
			if($('div.selected > .item')!=null)
			{
			    $.each( $('div.selected > .item'), function(index, value) 
				{
                	
				   if(typeof value != "undefined" || value)
				   {
					  if($(value).attr('type') == 'checkbox' && $(value).is(':checked'))
				        {
					   elems.push($(value).attr('data-value'));
						}
						else if($(value).is("div"))
						{
						elems.push($(value).attr('data-value'));	
						}
					   
                      
				    }
			    });
			
				$("input[name='flist2']").val(JSON.stringify(elems));
			   
			}
			$("input[name='flist[]']").removeAttr("disabled");
			$("#form1").submit();	
		});	
		
		});
		</script>
		 <?php $this->load->view('includes/additional.php');?>
 <link rel="stylesheet" href="assets/css/font-awesome.min.css">
 <link rel="stylesheet" href="assets/css/jquery.tree-multiselect.css">

<script type="text/javascript" src="assets/js/jquery.tree-multiselect.js"></script>



		 
		 
    </body>
</html>