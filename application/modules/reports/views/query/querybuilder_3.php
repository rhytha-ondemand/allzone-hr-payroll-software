<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
<style>
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        height:37px;
        padding:1px!important;
		
    }
	.dataTables_filter
	{
		display:none;
	}
    .default{
        height: 25px!important;
        padding-top: 2px!important
    }
    #emp_id_chosen{
        /*height:37px;*/
        max-width:200%!important;
        display:inline-block;
    }
</style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
                 <?php $this->load->view('includes/sidebar.php');?>
		
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                        <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Query Builder</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Query Builder View</h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
						 
                            <!--working content start-->
							<?php
                                                            $flag = 0;
                                                        ?>
							
							<table id="teamdetails" class="table table-striped table-bordered bootstrap-datatable datatable responsive" data-tableexport-display="always">
							<thead>
							<tr>
							
							<?php
                                $i=0;
							foreach($keys as $value){ 
                                                            $flag = 1;
							if($i>12){
							?>
							<th style="display:none;" data-tableexport-display="always"><?=$value?></th>
							<?php } else { ?>
							<th><?=$value?></th>
							<?php $i++; }} ?>
								</tr>
							</thead>
							<tbody>
							<?php for($i=0;$i<count($result);$i++){
								echo "<tr>";
								$temp=array_values($result[$i]);
								for($j=0;$j<count($temp);$j++){
									if($j>12) {
									?>
									<td style="display:none;" data-tableexport-display="always"><?=$temp[$j]?></td>
									<?php } else {  ?>
									<td><?=$temp[$j]?></td>
									
									<?php
									}
								}
								
							     echo "</tr>";
							 } ?>
							 </tbody>
								</table>
                            <?php 
                                if($flag == 0){
                                    echo " <div style='text-align:center;' > <h3>No data available </h3> </div>";
                                }else{
                            ?>
                            <div class="row">
                                <div class="col-sm-offset-5">
                                   <div class="col-xs-2">
							<input class="btn btn-primary" type="button" name="exp" id="exp" value="Export">
                                   </div>
                                </div>
                            </div>
                                <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
          <?php $this->load->view('includes/footer.php');?>
        </div>
		<script>
		$(document).ready(function(){
			$.fn.dataTableExt.sErrMode = 'throw';
                    $("#exp").on("click",function(){
			$("#removetr").remove();
                    $("thead").prepend("<tr style='display:none;' id='removetr' data-tableexport-display='always' ><th colspan=10 > Employee Report </th></tr>");
                    	 $("select option:last").after("<option value='500'>500</option>");
				
                        var select = $("select").val();
                        var page = $('.pagination .active a').text();
                        var select_last = $('select option:last-child').val();
                        $("select").val(select_last);
                        $("select").change();    
                        $('#teamdetails').tableExport({type:'excel',escape:'false'});
                        $("select").val(select);
                        $("select").change(); 
                        $("a:contains("+page+")").click();	
				
			});
			
			
		});
		</script>
		<?php $this->load->view('includes/additional.php');?> 
<script type="text/javascript" src="assets/js/html2canvas.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/tableExport.js"></script>

    </body>
</html>