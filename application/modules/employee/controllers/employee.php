<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class employee extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('auth');
        $this->load->model('employee/employee_model');	
    }
    
    public function index(){
        echo "employee";
    }
    /*
     *  Add new Employee
     */
    public function add(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->load->view('add_employee');
            }else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }else{
            redirect("master/login");
        }
    }
    
    public function employee_add(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_name', 'Employee Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('sex', 'Gender', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('dob', 'Date of Birth', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('doj', 'Date of Joining', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_pl', 'Paid Leave', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_desig', 'Designation', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_dept', 'Department', 'required|strip_tags|trim|xss_clean');
//                $this->form_validation->set_rules('emp_reportteam', 'Team', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_fname', 'Father name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('official_name', 'Official Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_type', 'Employee type', 'required|strip_tags|trim|xss_clean');
//                $this->form_validation->set_rules('office_email', 'Official Email', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
//                    $bname = $this->input->post('bname');	
//                    if($bname != '');{        
                        $res=$this->employee_model->emp_add();
                        $data['Id']=$res;
                        $this->load->view('add_emppersonal',$data);
//                    }else{
//                        redirect('employee/add');
//                    }                    
                }
                else {
                    $this->load->view('add_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function emp_personal(){
           echo '<pre>';
        print_r($this->input->post());
//        $this->load->view('add_emppersonal');
    }
//    public function employee_addpersonal(){
//           echo '<pre>';
//        print_r($this->input->post());
//    }
    
     function emp_ctc(){
      if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('cur_add1', 'Current Address1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_add2', 'Current Address2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_city', 'City', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_pincode', 'Pincode', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_state', 'State', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_country', 'Country', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_mobile', 'Mobile', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('personal_email', 'Personal email', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_add1', 'Address 1 ', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_add2', 'Address 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_city', 'City', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_pincode', 'Pincode', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_state', 'State', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_country', 'Country', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_mobile', 'Mobile', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('bgroup', 'Blood Group', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_qua', 'Qualification', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_name', 'Referece Name1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_add', 'Refference Address1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_mobile', 'Reference Mobile 1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_name', 'Reference Name 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_add', 'Reference Address 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_mobile', 'Reference Mobile 2', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
//                    $emp_id = $this->input->post('Emp_id');
//                   if($emp_id){
                        $result = $this->employee_model->emp_personal();
                        $result_ctc =$this->employee_model->modifyctc($result['Emp_id']);
                        if($result_ctc){
                            $data=$result_ctc[0];
                        }else{
                            $data=$result;
                        }
                        
                        $this->load->view('add_ctc',$data);
//                   }else{
//                       redirect('employee/add');
//                   }                   
                }
                else {
                    $data['Id']=  $this->input->post('Emp_id');
                    $this->load->view('add_emppersonal',$data);
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    function payroll(){
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('ctc', 'CTC Amount', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ctcr', 'CTC Range', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
//                    $emp_id = $this->input->post('Emp_id');
//                    if($emp_id){
                        $result = $this->employee_model->emp_ctc();        
                        $payroll_result = $this->employee_model->emp_payroll($result);             
                        $this->load->view('add_payroll',$payroll_result[0]);
//                    }else{
//                         redirect('employee/add');
//                    }                  
                }
                else {
                    $data['Emp_id']=  $this->input->post('Emp_id');
                    $this->load->view('add_ctc',$data);
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
     /*
     *  Transfer employee
     */
    public function transfer(){        
        if($this->auth->checku())
        {
            $this->load->view('transfer_employee');
        }
        else{
            redirect("master/login");
        }
    }
    
    public function emp_transfer(){
        $res=$this->employee_model->emp_transfer();
        echo $res;
    }


    /*
     * Team change
     */
    public function team(){
        if($this->auth->checku())
        {
            $this->load->view('team_change');
        }
        else{
            redirect("master/login");
        }
    }
    
    /*
     * Team list
     */
    public function team_list(){
        if($this->auth->checku())
        {
//            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
//            {
                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
                $this->form_validation->set_rules('report_team', 'team', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $data['bname']=$this->input->post('bname');
                    $data['rteam']=$this->input->post('report_team');
                    $this->load->view('teamchange_list',$data);                
                }
                else {
                    $this->load->view('team_change');
                }
//            }
//            else{
//                $this->load->view('master/noaccess');
////                redirect("master/noaccess");
//            }
        }
       else{
           redirect("master/login");
       }
        
    }
    
    public function emp_teamlist_all(){
		$res=$this->employee_model->emp_teamlist_all();
                 if($res == null){
                     echo json_encode(array("aaData"=>''));
                 }else{
                     echo json_encode(array("aaData"=>$res));
                 }
		


	}
public function updateteam(){
		
	$res=$this->employee_model->updateteam();
		 //
		 
		 $ut=$this->input->post(NULL, TRUE);
		 if($ut['columnId']==4){
			 
			echo $ut['value'];
		}
		else
	         echo json_encode(array("aaData"=>$res));

	}
    
    /*
     *  Modify the employee
     */
    public function modify(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->load->view('modify_employee');
            }else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }else{
            redirect("master/login");
        }

    }
    
    public function emp_modify(){

        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
//                    $bname = $this->input->post('bname');
//                    if($bname != ''){
                        $result = $this->employee_model->emp_modify();  
                        $result[0]['backbtn'] = $this->input->post('backbtn');
                        if(sizeof($result)>0){
                            $this->load->view('modify_employeedet',$result[0]);
                        }else{
                            $this->load->view('modify_employee');
                        }
//                    }else{
//                        redirect('employee/modify');
//                    }
                }
                else {
                    $this->load->view('modify_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
                
    }
   
    function modifyemp_personal(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
//                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_name', 'Employee Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('sex', 'Gender', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('dob', 'Date of Birth', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('doj', 'Date of Joining', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_pl', 'Paid Leave', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_desig', 'Designation', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_dept', 'Department', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_reportteam', 'Team', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_fname', 'Father name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('official_name', 'Official Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_type', 'Employee type', 'required|strip_tags|trim|xss_clean');
//                $this->form_validation->set_rules('office_email', 'Official Email', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('emp_id');
                    
                    $res=$this->employee_model->empmodify_update();   
                    $result = $this->employee_model->modifyemp_personal($res); 
                    $result[0]['backbtn'] = $this->input->post('backbtn');
                    if($result){
                         $this->load->view('modify_emppersonal',$result[0]);            
                    }else{
                        $result[0]['Emp_id'] = $emp_id;
                        $this->load->view('modify_emppersonal',$result[0]);            
                    }
                }
                else {
                    $this->load->view('modify_employee');                    
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
       
        
    }
    function modifyctc(){
        
        
      if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('cur_add1', 'Current Address1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_add2', 'Current Address2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_city', 'City', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_pincode', 'Pincode', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_state', 'State', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_country', 'Country', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('cur_mobile', 'Mobile', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('personal_email', 'Personal email', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_add1', 'Address 1 ', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_add2', 'Address 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_city', 'City', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_pincode', 'Pincode', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_state', 'State', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_country', 'Country', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('per_mobile', 'Mobile', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('bgroup', 'Blood Group', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_qua', 'Qualification', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_name', 'Referece Name1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_add', 'Refference Address1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref1_mobile', 'Reference Mobile 1', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_name', 'Reference Name 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_add', 'Reference Address 2', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ref2_mobile', 'Reference Mobile 2', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('Emp_id');

                        $res = $this->employee_model->emp_personalupdate();
                        $result =$this->employee_model->modifyctc($res);                        
                         if($result){
                             $result[0]['backbtn'] = $this->input->post('backbtn');                             
                            $this->load->view('modify_ctc',$result[0]);             
                         }else{
                            $result[0]['Emp_id'] = $emp_id;
                            $result[0]['backbtn'] = $this->input->post('backbtn');                            
                            $this->load->view('modify_ctc',$result[0]);  
                         }
                }
                else{
                    $this->load->view('modify_employee'); 
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
    }
    function modifypayroll(){
//        modifyctc_update

        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('ctc', 'CTC Amount', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('ctcr', 'CTC Range', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->employee_model->modifyctc_update();    
                    $payroll_result = $this->employee_model->emp_payroll($result); 
                    $payroll_result[0]['backbtn'] = $this->input->post('backbtn');
                    $this->load->view('modify_payroll',$payroll_result[0]);
                }
                else {
                    $this->load->view('modify_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }        
    }
    function emp_experience($backbtn=''){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $emp_id = $this->input->post('emp_id');

                if($emp_id != '')
                {
                   $result = $this->employee_model->emp_experience();
                   if(!$result){
                       $result['Emp_id'] = $emp_id;
                   }
                    if($backbtn){
                        $result['backbtn'] = $backbtn;
                     }else{
                         $result['backbtn'] = 0;
                     }
                    $this->load->view('emp_experience_edit',$result);
                }
                else {
                    $this->load->view('modify_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
    }
    
    function emp_experience_update(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
//                    echo"<pre>";print_r($this->input->post());
                     $this->employee_model->emp_experience_update();    
                    redirect('employee/modify'); 

                }else{
                    redirect('employee/modify');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       } 
        
        
    }
    
    function emp_experience_edit(){
        $data['total_year']="10";
        $data['total_month']="3";
        $data['mb_year']="1";
        $data['mb_month']="5";
        $data['records']=array(array("prev_cname"=>"Trydisc System","prev_caddr"=>"Chennai","prev_cdesig"=>"Support Executive","prev_ctc"=>"","prev_net"=>"","prev_service"=>"January 2009 to December 2010"),array("prev_cname"=>"Global Infoserver Limited","prev_caddr"=>"Chennai","prev_cdesig"=>"Executive Networking","prev_ctc"=>"","prev_net"=>"","prev_service"=>"January 2011 to December 2012"));
        $this->load->view('emp_experience_edit',$data);
    }
    
   public function trackingsheet() {
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {                
//                    echo"<pre>";print_r($this->input->post());die();
                     $result['array'] = $this->employee_model->trackingsheet();  
                     $result['Name'] = $result['array'][0]['Name'];
                     $result['Emp_id'] = $this->input->post('emp_id');
                     $backbtn = $this->input->post('backbtn');
                     if($backbtn){
                        $this->load->view('emp_trackingsheet_back',$result); 
                     }else{
                        $this->load->view('emp_trackingsheet',$result);
                     }
                }
                else {
                    $this->load->view('modify_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }    
    }
    public function trackingsheet_update(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('appr_refix', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {  
                    $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('desig', 'Designation', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('process', 'Process', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('date1', 'Date', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('date2', 'Next Appraisal Date ', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('ctc', 'CTC', 'required|trim|xss_clean');
                    if ($this->form_validation->run())
                    {                            
                        $result = $this->employee_model->trackingsheet_update(); 
                        
//                        echo $result;die();
//                        redirect('employee/modify');
                        $emp_id = $this->input->post('emp_id');
                        $this->emp_experience(1);
                    }else{                        
                        $result['array'] = $this->employee_model->trackingsheet();  
                        $result['Name'] = $result['array'][0]['Name'];
                        $result['Emp_id'] = $this->input->post('emp_id');
                        $this->load->view('emp_trackingsheet',$result);
                    }
                }
                else {
//                    redirect('employee/modify');
                    $emp_id = $this->input->post('emp_id');
                    $this->emp_experience();
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    public function trackingsheet_updateback(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {   
                $this->form_validation->set_rules('appr_refix', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('process', 'Process', 'required|trim|xss_clean');
                $this->form_validation->set_rules('desig', 'Designation', 'required|trim|xss_clean');
                $this->form_validation->set_rules('date1', 'Date', 'required|trim|xss_clean');
                $this->form_validation->set_rules('date2', 'Next Appraisal Date ', 'required|trim|xss_clean');
                $this->form_validation->set_rules('ctc', 'CTC', 'required|trim|xss_clean');
                $this->form_validation->set_rules('tracking_id', 'Tracking id', 'required|trim|xss_clean');
                if ($this->form_validation->run()){
                    $result = $this->employee_model->trackingsheet_updateback(); 
                    $this->emp_experience(1);
                }else{
//                    echo validation_errors();die();
//                    $emp_id = $this->input->post('emp_id');
                    $this->emp_experience();
                }
                
            }else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }else{
            redirect("master/login");
        }
    }
    
    public function referralby(){
        $result['remp_id'] = $this->input->get('remp_id');
        $result['rD_of_join'] = $this->input->get('rD_of_join');
        $array = $this->employee_model->referralby();
//        echo "<pre>"; print_r($array);die();
//        log_message('error',  json_encode($array));
        if($array){
            $result['array'] = $array;
            $this->load->view('refferalby_update',$result);    
        }else{
            $this->load->view('referralby',$result);
        }
    }
    
    
    public function refferal_update(){
       $result =  $this->employee_model->refferal_update(); 
       if($result){
           echo 1;
       }
       else{
           echo 0;
       }
    }
    
    public function refferal_add(){
       $result =  $this->employee_model->refferal_add(); 
       if($result){
           echo 1;
       }
       else{
           echo 0;
       }
    }
    /*
     * List screen
     */
    public function banklist(){
        $data['array'] = $this->employee_model->banklist();
        $this->load->view("bankac_list",$data);
    }
    public function panlist(){
        $data['array'] = $this->employee_model->panlist();
        $this->load->view("pancard_list",$data);
    }
    public function pflist(){
        $data['array'] = $this->employee_model->pflist();
        $this->load->view("pf_list",$data);
    }
    public function esilist(){
        $data['array'] = $this->employee_model->esilist();
        $this->load->view("esi_list",$data);
    }
    public function uanlist(){
        $data['array'] = $this->employee_model->uanlist();
        $this->load->view("uan_list",$data);
    }
    
    
    
    public function eadd(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('uemp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $id = $this->input->post('uemp_id');
                    $data = $this->employee_model->eadd();     
                    if($data){
                        $this->load->view('back_employeedet',$data[0]);
                    }else{
                        $this->load->view('add_employee');
                    }                    
                }else{
                    $this->load->view('add_employee');
                }
                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
        

    }
    public function bemp_personal(){
        
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
//                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
                $this->form_validation->set_rules('Id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_name', 'Employee Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('sex', 'Gender', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('dob', 'Date of Birth', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('doj', 'Date of Joining', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_pl', 'Paid Leave', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_desig', 'Designation', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_dept', 'Department', 'required|strip_tags|trim|xss_clean');
//                $this->form_validation->set_rules('emp_reportteam', 'Team', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_fname', 'Father name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('official_name', 'Official Name', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('emp_type', 'Employee type', 'required|strip_tags|trim|xss_clean');
//                $this->form_validation->set_rules('office_email', 'Official Email', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('Id');
                    $empp_id = $this->input->post('emp_id');
                    $res=$this->employee_model->empmodify_update();   
//                    $result = $this->employee_model->modifyemp_personal($res); 
                        $res1 = $this->employee_model->modifyemp_personal($empp_id); 
                        $result = $res1[0];
                        $result['Id'] = $emp_id;
                        $this->load->view('add_emppersonal',$result);            
                    
                }
                else {
                    $this->load->view('add_employee');                    
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    public function eemp_personal(){
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('uemp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('uemp_id');
                    $result = $this->employee_model->modifyemp_personal($emp_id); 
                    
                    if($result){
                         $this->load->view('add_emppersonal',$result[0]);            
                    }else{
                        $result[0]['Emp_id'] = $emp_id;
                        $this->load->view('add_emppersonal',$result[0]);            
                    }
                }
                else {
                    $this->load->view('modify_employee');                    
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function dummyemp1(){
       echo '<pre>';
        print_r($this->input->post()); 
    }
    
    public function modifyemp_personall(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('emp_id');
                    $result = $this->employee_model->modifyemp_personal($emp_id); 
                    $result[0]['backbtn'] = $this->input->post('backbtn');
                    if($result){
                         $this->load->view('modify_emppersonal',$result[0]);            
                    }else{
                        $result[0]['Emp_id'] = $emp_id;
                        $this->load->view('modify_emppersonal',$result[0]);            
                    }
                }
                else {
                    $this->load->view('modify_employee');                    
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function modify_ctcc(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('Emp_id');
                        
                        $result =$this->employee_model->modifyctc($emp_id);                        
                         if($result){                             
                             $result[0]['backbtn'] = $this->input->post('backbtn');
                            $this->load->view('modify_ctc',$result[0]);             
                         }else{                             
                            $result[0]['Emp_id'] = $emp_id;
                            $result[0]['backbtn'] = $this->input->post('backbtn');
                            $this->load->view('modify_ctc',$result[0]);  
                         }
                }
                else{
                    $this->load->view('modify_employee'); 
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function emp_ctcc(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('uemp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('branch', 'Branch', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $emp_id = $this->input->post('uemp_id');
                    $branch= $this->input->post('branch');
                    $result_ctc =$this->employee_model->modifyctc($emp_id);
                        if($result_ctc){
                            $data=$result_ctc[0];
                        }else{
                            $data=array('Emp_id'=>$emp_id,'Branch'=>$branch);
                        }
                        
                        $this->load->view('add_ctc',$data);
                }
                else {
                    $this->load->view('modify_employee');                    
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    function modifypayrolll(){
//        modifyctc_update

        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->input->post('Emp_id');   
                    $payroll_result = $this->employee_model->emp_payroll($result); 
                    $payroll_result[0]['backbtn'] = $this->input->post('backbtn');
                    $this->load->view('modify_payroll',$payroll_result[0]);
                }
                else {
                    $this->load->view('modify_employee');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }        
    }

  public function appraisal2(){
//        echo "<pre>";
//        print_r($this->input->post());
//        die();
        
        $emp_id = $this->input->post('emp_id');
        
        $Id = $this->input->post('Id');
        $appr_refix = $this->input->post('appr_refix');
        $date1 = $this->input->post('date1');
        $desig= $this->input->post('desig');
        $ctc = $this->input->post('ctc');
        $date2 = $this->input->post('date2');
        
        $uid = $this->session->userdata('uid');
        
        $count_insert = sizeof($appr_refix);
       if($count_insert >0){
           for($i=0;$i<$count_insert;$i++){
              
               $iId = $Id[$i];
               $iappr_refix = $appr_refix[$i];  
               if(trim($date1[$i]) != ''){
                   $idate1 = date('Y-m-d',  strtotime($date1[$i]));
               }else{
                    $idate1 = null;    
               }
               
               $idesig = $desig[$i];
               $ictc = $ctc[$i];
               if(trim($date2[$i]) != ''){
                   $idate2 = date('Y-m-d',  strtotime($date2[$i]));
               }else{
                   $idate2 = NULL;
               }
               
               
               if(trim($iappr_refix) != ''){
                    $exp_array=array("Emp_id"=>$emp_id, "Desic"=>$iappr_refix, "Date1"=>$idate1, "Desig"=>$idesig, "ctc"=>$ictc ,"Date2"=>$idate2  ,"Uid"=>$uid );
                    if(trim($iId) != '' ){
                        unset($exp_array['Emp_id']);
                        $this->db->where('Id',$iId);
                        $this->db->update('emp_appraisal',$exp_array);
                    }else{
                        $this->db->insert('emp_appraisal',$exp_array);
                    }
               }
               
           }
       }
       
       
       $this->emp_experience(1);
    }	
}

