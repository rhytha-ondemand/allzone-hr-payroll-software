<?php
class Employee_model extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
		$this->load->library('employee_lib',NULL,'emp');
		
    }
	function emp_add()
	{
            $result = $this->emp->emp_add();
		return $result;
	}
        function emp_personal(){
            $result = $this->emp->emp_personal();
            return $result;
        }
        function emp_ctc(){
            $result = $this->emp->emp_ctc();
            return $result;
        }
        function emp_payroll($emp_id){
            $result =  $this->emp->emp_payroll($emp_id);
            return $result;
        }
        function emp_modify(){
            $result = $this->emp->emp_modify();
            return $result;
        }
        function empmodify_update(){
            $result = $this->emp->empmodify_update();
            return $result;
        }
        function modifyemp_personal($emp_id){
            $result = $this->emp->modifyemp_personal($emp_id);
            return $result;
        }
        function emp_personalupdate(){
            $result = $this->emp->emp_personalupdate();
            return $result;
        }        
        function modifyctc($emp_id){
            $result = $this->emp->modifyctc($emp_id);
            return $result;
        }
         function modifyctc_update(){
            $result = $this->emp->modifyctc_update();
            return $result;
        } 
        function emp_teamlist_all(){
            $result = $this->emp->emp_teamlist_all();
            return $result;
        }
        function emp_transfer(){
            $result = $this->emp->emp_transfer();
            return $result;
        }
		function updateteam(){
		$res=$this->emp->updateteam();
		return $res;
    	}
		 function trackingsheet(){
            $res=$this->emp->trackingsheet();
            return $res;
        }
        function trackingsheet_update(){
            $res=$this->emp->trackingsheet_update();
            return $res;
        }
        function referralby(){            
            $res=$this->emp->referral_by();
            return $res;
        }
        
        function refferal_add(){
            $res=$this->emp->refferal_add();
            return $res;
        }
        
        function refferal_update(){
            $res=$this->emp->refferal_update();
            return $res;
        }
        
        function emp_experience(){
            $res=$this->emp->emp_experience();
            return $res;
        }
        
        function emp_experience_update(){
            $res=$this->emp->emp_experience_update();
            return $res;
        }
    public function banklist(){
        
        $bname = $this->input->get('bname');
        
        $sql = "select e.Emp_id, e.Name, DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join, e.Status,s.employ_bank_ac_no,s.bank_name from employee e,employ_salary s where e.Emp_id = s.Emp_id and e.Branch = '$bname' and e.Status=1 and s.Status = 0  group by e.Id ";
        
        $res=$this->db->query($sql);

        if($res->num_rows()>0){
            return $res->result_array();
        }
        else{
            return false;
        }        
    }
    public function panlist(){
        $bname = $this->input->get('bname');
        $sql = "select e.Emp_id, e.Name, DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join, e.Status,p.Pancard_no from employee e,emp_personal p where e.Emp_id = p.Emp_id and e.Branch = '$bname' and e.Status=1  group by e.Id ";
        
        $res=$this->db->query($sql);

        if($res->num_rows()>0){
            return $res->result_array();
        }
        else{
            return false;
        }
    }
    public function pflist(){
        $bname = $this->input->get('bname');
        $sql = "select e.Emp_id, e.Name, DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join, e.Status,s.employ_pf_no from employee e,employ_salary s where e.Emp_id = s.Emp_id and e.Branch = '$bname' and e.Status=1 and s.Status = 0 group by e.Id ";
        
        $res=$this->db->query($sql);

        if($res->num_rows()>0){
            return $res->result_array();
        }
        else{
            return false;
        }
    }
    public function esilist(){
        $bname = $this->input->get('bname');
        $sql = "select e.Emp_id, e.Name, DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join, e.Status,s.employ_esi_no from employee e,employ_salary s where e.Emp_id = s.Emp_id and e.Branch = '$bname' and e.Status=1 and s.Status = 0  group by e.Id ";
        
        $res=$this->db->query($sql);

        if($res->num_rows()>0){
            return $res->result_array();
        }
        else{
            return false;
        }
    }
    public function uanlist(){
        $bname = $this->input->get('bname');
        $sql = "select e.Emp_id, e.Name, DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join, e.Status,s.employ_uan_no from employee e,employ_salary s where e.Emp_id = s.Emp_id and e.Branch = '$bname' and e.Status=1 and s.Status = 0  group by e.Id ";
               
        $res=$this->db->query($sql);

        if($res->num_rows()>0){
            return $res->result_array();
        }
        else{
            return false;
        }
    }
    
    public function trackingsheet_updateback(){
        $res=$this->emp->trackingsheet_updateback();
        return $res;
    }
    public function eadd(){
        $res = $this->emp->eadd();
        return $res;
    }
}