<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
         <style>
            .dataTables_filter{
                display: none;
            }
            #desig_chosen{
                /*min-width: 150px!important;*/
                width: 100%!important;
            }
            #appr_refix_chosen{
                width: 100%!important;
            }
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
            #ui-datepicker-div{
                margin-left: -30px;
            }
        </style>

      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Tracking sheet</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employment Tracking sheet <?php if(isset($Emp_id)){ echo "- ". $Emp_id;} ?></h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form class="form-horizontal" id="emp_tracking" role="form" method="post" action="employee/appraisal2">
                                <input type="hidden" name="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id; } ?>" />    
                                <input type="hidden" name="process" id="process" />
                                <br>                  
                                 <div class="form-group">
                                     <div class="col-sm-offset-2" >
                                        <label class="control-label col-xs-1" for="name"> Name </label>
                                        <div class="col-xs-10">
                                             <label class="control-label" style="font-weight: normal;" for="name"><?php if(isset($Name)){ echo $Name; } ?></label>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered responsive">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <!--<th>Location</th>-->
                                            <th>Designation</th>
                                            <th>CTC</th>              
                                            <th>Appraisal Date</th>   
                                            <!--<th></th>-->
                                        </tr>
                                    </thead>                                    
                                    <tbody>
                            <?php 
                                $n=0;
                                $appraisal = 1;
                                $refixation = 1;
                                $others = 1;
                                $date_of_join = date('d-M-Y');
                                $Last_app_date = date('d-M-Y');
                                if($array){        
                                    $date_of_join = $array[0]['Date1'];
                                    $appr_refix_det = '';
//                                    echo count($array);
                                    foreach($array as $row){
                                        
                                        echo "<tr>";
                                        if($row['joining_id'] != $row['id']){
                                            if($row['Desic'] == 1 || $row['Desic'] == "1" ){
                                                $appr_refix_det = "Appraisal - ".$appraisal;
                                                $appraisal++;
                                            }else if($row['Desic'] == 2 || $row['Desic'] == "2" ){
                                                $appr_refix_det = "Refixation - ".$refixation;
                                                $refixation++;
                                            }else{
                                                $appr_refix_det = "Others - ".$others;
                                                $others++;
                                            }
                                        }else{
                                            $appr_refix_det = "At time of joining";
                                        }
                             /*           echo "<td>".$appr_refix_det."</td>";
                                        echo "<td>".$row['Date1']."</td>";
                                        echo "<td>".$row['Desig_name']."</td>";
                                        echo "<td>".$row['ctc']."</td>";                                        
                                        echo "<td>".$row['Date2']."</td>";
//                                        echo "<td></td>";
                                        $Last_app_date = $row['Date2'];
                                        echo "</tr>";    */
                                        
                                        
                                       $id = $row['id'];
                                        $Desic = $row['Desic'];
                                        $date1 = $row['Date1'];
                                        $desig_nme = $row['Desig_name'];
                                        $desgi = $row['Desig'];
                                        $ctc = $row['ctc'];
                                        $date2 = $row['Date2'];
                                        
                                        
                                        echo "<td><input type='hidden' name='Id[]' value='$id' />
<select id='appr_refix' name='appr_refix[]' class='appr_refix form-control' data-placeholder='Select' >
<option value='$Desic' selected > $appr_refix_det </option>
</select>
</td>";
echo "<td><input type='text' readonly='' value='$date1' class=' form-control datepicker' name='date1[]' id='date1$n'></td>";     
echo "<td>
<select id='desig' name='desig[]' class=' desig form-control' data-placeholder='Designation' >
<option value='$desgi'>$desig_nme</option>
</select>
</td>";
echo "<td><input type='text' value='$ctc' class='validate[min[1],custom[number]]] form-control' maxlength='8' name='ctc[]' data-errormessage-custom-error='Invalid CTC' id='ctc$n'></td>";
echo "<td><input type='text' readonly='' value='$date2' class='form-control datepicker' name='date2[]' id='date2$n'></td>";
echo "</tr>";
$n++;
                                    }
                                }
for($i=$n;$i<15;$i++){                                
echo "<tr>";
echo "<td><input type='hidden' name='Id[]' value=''  />
<select id='appr_refix' name='appr_refix[]' class='appr_refix form-control' data-placeholder='Select' >
<option value=''>Select Appraisal No</option></select>
</td>";
echo "<td><input type='text' readonly='' class=' form-control datepicker' name='date1[]' id='date1$i'></td>";     
echo "<td>
<select id='desig' name='desig[]' class=' desig form-control' data-placeholder='Designation' >
<option value=''></option>
</select>
</td>";
echo "<td><input type='text' class='validate[min[1],custom[number]]] form-control' maxlength='8' name='ctc[]' data-errormessage-custom-error='Invalid CTC' id='ctc$i'></td>";
echo "<td><input type='text' readonly='' class='form-control datepicker' name='date2[]' id='date2$i'></td>";
echo "</tr>";
}
                                ?>       
                                    </tbody>
                                </table>
                                
                                <div class="form-group">
                                    <div class="col-xs-1 col-sm-offset-2">
                                        <input type="button" id="backsubmit" class="btn btn-primary" value="Back">
                                    </div> 
                                        <div class="col-xs-3 col-sm-offset-2">
                                           <input type="submit" id="submit" class="btn btn-primary" value=" Submit ">
                                        </div>
                                    </div> 
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
  <form id="back_btn" method="post" action="employee/modifypayrolll" >
      <input type="hidden" name="Emp_id" id="Emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
      <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
  </form>
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#emp_tracking").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
               $("#emp_tracking").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#emp_tracking").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                
            $(".datepicker").datepicker({dateFormat: 'dd-M-yy',changeMonth: true,yearRange: "-20:+5", changeYear: true});    
        /*    $("#date1").datepicker({ dateFormat: 'dd-M-yy',changeMonth: true,yearRange: "-1:+5", changeYear: true,minDate:'<?php echo $date_of_join;?>',maxDate: '<?php // echo date('d-M-Y',strtotime(date("Y-m-d", strtotime($Last_app_date)) . " + 2 year"));?>',
            onClose: function( selectedDate ) {
                        $( "#date2" ).datepicker( "option", "minDate", selectedDate);
                    }
            });
            $("#date2").datepicker({ dateFormat: 'dd-M-yy',changeMonth: true,yearRange: "-1:+5", changeYear: true,minDate:'<?php echo $date_of_join;?>',maxDate: '<?php // echo date('d-M-Y',strtotime(date("Y-m-d", strtotime($Last_app_date)) . " + 2 year"));?>'});
          */  // branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/desig_list",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
//                        $('.desig').find('option').remove();
                        if(json) {
                             toappend='';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Design_name']+'</option>';  
                                
                            });   
                            $('.desig').append(toappend);
                            $(".desig").trigger('chosen:updated');
                        }
                    }
                });
                
          /*       $('.appr_refix').find('option').remove();*/
//                var appr_toappend ="<option value='1'>Appraisal - 1</option><option value='2'>Refixation - 1</option><option value='1'>Appraisal - 2</option><option value='2'>Refixation - 2</option><option value='1'>Appraisal - 3</option><option value='2'>Refixation - 3</option><option value='1'>Appraisal - 4</option><option value='2'>Refixation - 4</option><option value='1'>Appraisal - 5</option><option value='2'>Refixation - 5</option><option value='1'>Appraisal - 6</option><option value='2'>Refixation - 6</option><option value='1'>Appraisal - 7</option><option value='2'>Refixation - 7</option><option value='1'>Appraisal - 8</option><option value='2'>Refixation - 8</option><option value='1'>Appraisal - 9</option><option value='2'>Refixation - 9</option><option value='1'>Appraisal - 10</option><option value='2'>Refixation - 10</option>";
                var appr_toappend = "<option value='1'>Appraisal - 1</option><option value='2'>Refixation - 1</option><option value='3'>Others - 1</option><option value='1'>Appraisal - 2</option><option value='2'>Refixation - 2</option><option value='3'>Others - 2</option><option value='1'>Appraisal - 3</option><option value='2'>Refixation - 3</option><option value='3'>Others - 3</option><option value='1'>Appraisal - 4</option><option value='2'>Refixation - 4</option><option value='3'>Others - 4</option><option value='1'>Appraisal - 5</option><option value='2'>Refixation - 5</option><option value='3'>Others - 5</option><option value='1'>Appraisal - 6</option><option value='2'>Refixation - 6</option><option value='3'>Others - 6</option><option value='1'>Appraisal - 7</option><option value='2'>Refixation - 7</option><option value='3'>Others - 7</option><option value='1'>Appraisal - 8</option><option value='2'>Refixation - 8</option><option value='3'>Others - 8</option><option value='1'>Appraisal - 9</option><option value='2'>Refixation - 9</option><option value='3'>Others - 9</option><option value='1'>Appraisal - 10</option><option value='2'>Refixation - 10</option><option value='3'>Others - 10</option>";
                $('.appr_refix').append(appr_toappend);
                $(".appr_refix").trigger('chosen:updated');
                
                
        /*        $(".appr_refix").change(function (){
                    var process = $(".appr_refix option:selected").text();
                    $("#process").val(process);
                    
                });*/
// branch name
                $("#submit").click(function(){
                    if ( $("#emp_tracking").validationEngine('validate') ) {
                        return true;
                    }                    
                    return false;
                });
                
            $("#backsubmit").click(function(){
//                window.history.back();
                $("#back_btn").submit();
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>