<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
	<style>
            .dataTables_filter{
                display: none;
            }
            
        </style>  
      <?php // $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php // $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-12">
            <!-- content starts -->
<!--            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">ESI list</a>
                    </li>
                </ul>
            </div>-->
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employee ESI No. List </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                             <table id="bankaclist" class="table table-striped table-bordered datatable responsive">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Emp. No</th>
                                        <th>Emp. Name</th>
                                        <th>Date Of Joining</th>
                                        <th>ESI. No</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    if($array){
                                        $i=1;
                                        foreach($array as $row){
                                            echo "<tr>";
                                            echo "<td>".$i++."</td>";
                                            echo "<td>".$row['Emp_id']."</td>";
                                            echo "<td>".$row['Name']."</td>";
                                            echo "<td>".$row['D_of_join']."</td>";
                                            echo "<td>".$row['employ_esi_no']."</td>";                                            
                                            if($row['Status'] == 1){
                                                $status = "Active";
                                            }else{
                                                $status = "Inactive";
                                            }
                                            echo "<td>".$status."</td>";
                                            echo "</tr>";
                                        }
                                        
                                    }else{
                                        echo "<tr><td colspan='6' >No date available in table</td></tr>";
                                    }
                                    
                                    ?>
                                </tbody>
                            </table> 
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php // $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>