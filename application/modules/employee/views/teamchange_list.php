<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
	<script type="text/javascript" language="javascript" src="http://www.appelsiini.net/download/jquery.jeditable.mini.js"></script>
	<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/bower_components/datatables/media/js/jquery.dataTables.editable.js"></script>
	<script type="text/javascript" language="javascript" src="<?=base_url()?>assets/bower_components/datatables/media/js/jquery.validate.js"></script>
    <style>
        .dataTables_filter{
            display: none;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Employee List</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employee List </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
<form id="formAddNewRow" title="Add new record" style=
"display:none">
        <label for="id">Id</label><br />
	<input type="text" name="id" id="id" class="required" rel="0" />
        <br />
        <label for="emp_id">Employee Id</label><br />
	<input type="text" name="emp_id" id="emp_id" rel="1" />
        <br />
        <label for="emp_name">Employee Name</label><br />
	<input type="text"name="emp_name" id="emp_name" rel="2"/>
        <br />
        <label for="team_change">Team</label><br />
	<select name="team_change" id="team_change" rel="3">
                <option>edwin</option>
                <option>HR2</option>
                
        </select>
		<!--<input type="text" name="team_change" id="team_change" rel="1" />-->
        <br />
        <label for="shift_time">Shift time</label><br />
		<select name="shift_time" id="shift_time" rel="4" multiple="multiple">
                <option>Day</option>
                <option>Night</option>
                
        </select>
		<!--<input type="text" name="shift_time" id="shift_time" rel="1" />-->
		<!--<input type="text" name="shift_time" id="shift_time" rel="1" />-->
		<label for="process">Process</label><br />
		<!--<input type="text" name="process" id="process" rel="5" />-->
		<select name="process" id="process" rel="5" multiple="multiple">
                <option>Day</option>
                <option>Night</option>
                
        </select>
		<label for="tdate">Team Date</label><br />
		<!--<input type="text" name="process" id="process" rel="5" />-->
		<select name="tdate" id="tdate" rel="6" multiple="multiple">
                <option>Day</option>
                <option>Night</option>
                
        </select>
        <br />
</form>
                            <!--working content start-->
                            <br>
                            <br>
                            <form action="employee/teamchange_details" method="post" >
                            <table id="empteamlist" class="table table-striped table-bordered responsive">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Emp. No</th>
                                    <th>Emp. Name</th>
                                    <th>Team  Change</th>
                                    <th> Team Change Date </th>
                                    <th>Shift Time</th>
                                    <th> Process </th>
									
                                </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                                
                            </table>
                            <div class="add_delete_toolbar" style="display:none">
                                <input type="hidden" id="bname" name="bname" value="<?php echo $bname;?>" />
				<input type="hidden" id="rteam" name="rteam" value="<?=$rteam;?>" />
                            <div class="row">
                                <div class="col-sm-offset-4">
                                <div class="col-xs-2">
                                    <input type="submit" id="update" class="btn btn-primary" value="Update">
                                </div>
                                <div class="col-xs-3">
                                    <input type="button" id="report" class="btn btn-primary" value="Report">
                                </div>
                                </div>
                            </div>
                            </div>
                                
</form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            var process ="";
			var team=[];
			var shift=[];
			var process_det=[];
			 $.ajax({
                    type: "POST",
                    url: "master/team_info?bname="+$("#bname").val(),
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        
						
                        if(json) {
							
							for(var i in json)
                            
							team[i]=json[i];

                            
                        }
						
                    }            
                }); 
			$.ajax({
                    type: "POST",
                    url: "master/shift_info?bname="+$("#bname").val(),
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        
						
                        if(json) {
							
							for(var i in json)
                            
							shift[i]=json[i];

                            
                        }
						
                    }            
                }); 
				$.ajax({
                    type: "POST",
                    url: "master/process_info?bname="+$("#bname").val(),
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        
						
                        if(json) {
							
							for(var i in json)
                            
							process_det[i]=json[i];

                            
                        }
						
                    }            
                }); 
            
            var teams = "";
            var src="employee/emp_teamlist_all?bname="+$("#bname").val()+"&report_team="+$("#rteam").val();
             var oTable =  $('#empteamlist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "emp_id", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "emp_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "team_change", "sWidth": "125px", "bSortable": true },
						{ "mDataProp": "tdate", "sWidth": "125px", "bSortable": true },  
                        { "mDataProp": "shift_time", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "process", "sWidth": "125px", "bSortable": true }
                                              
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
//                            $('#empteamlist > tbody > tr ').each(function() {
//                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
//                                $(this ).find('a:nth-child(1)').on('click',edit);
//                                
//                            });	
                                callteamdet();
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
//                    $('#empteamlist > tbody > tr ').each(function() {
//                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
//                        $(this ).find('a:nth-child(1)').on('click',edit);
//                    });	
                        callteamdet();
                },

                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   $(nRow).find("td").eq(0).html(iDisplayIndex+1);
					$(nRow).find("td").eq(3).html(team[ aData['team_change']]);
					
					test = new Date(aData['tdate']);
					   if (isNaN(test )==false && aData['tdate']!=null)
						{
							var d=new Date(test );
							$(nRow).find("td").eq(4).html(test.getDate()+"-"+test.toDateString().substring(4, 7)+"-"+test.getFullYear());

						}
					
					$(nRow).find("td").eq(5).html(shift[ aData['shift_time']]);
					 $(nRow).find("td").eq(6).html(process_det[ aData['process']]);
					 			   
                    
					
                                        
                 }
            });
			var src2="master/team_info?bname="+$("#bname").val()+"&report_team="+$("#rteam").val();
			var src3="master/shift_info?bname="+$("#bname").val()+"&report_team="+$("#rteam").val();
			var src4="master/process_info?bname="+$("#bname").val()+"&report_team="+$("#rteam").val();
               $('#empteamlist').dataTable().makeEditable({
                        sUpdateURL: "master/UpdateData",
                        "aoColumns": [
                                    null,
									null,
									null,
									 {
                                        indicator: 'Saving Team...',
                                        tooltip: 'Click to edit team',
                                        type: 'select',
                                        onblur: 'cancel',
                                        submit:'Save',
                                        loadurl: src2,
										loadtype: 'POST',
										sUpdateURL:"employee/updateteam",
										sSuccessResponse: "IGNORE"
                                    },
									{
                                        //indicator: 'Saving Team change da...',
                                        //tooltip: 'Click to edit Teamd change',
                                        type: 'datepicker',
                                        //onblur: 'cancel',
                                        submit:'Save',
                                        //loadurl: src4,
										//loadtype: 'POST',
										sUpdateURL:"employee/updateteam",
										sSuccessResponse: "IGNORE"
                                    },
                                    {
                                        indicator: 'Saving Shift...',
                                        tooltip: 'Click to edit Shift time',
										//name: "activitydetail",
                                         type: 'select',
                                        //onblur: 'cancel',
										//dataProp: "4",
                                        submit:'Save',
										loadurl: src3,
										loadtype: 'POST',
										/*data: "{'':'Please select...', 'A':'A','B':'B','C':'C'}",*/
										 //ipOpts: getStateList(),
                                        /*fnOnCellUpdated: function(sStatus, sValue, settings){
                                            alert("(Cell Callback): Cell is updated with value " + sValue);
                                        }*/
										sUpdateURL:"employee/updateteam",
										sSuccessResponse: "IGNORE"
										/*sUpdateURL: function(value, settings){
										
                							//alert("Custom function for posting results");
											$.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": "<?=base_url()?>master/updatedata",
                        "data": "data="+value+"&data2="+settings,
						 "success": function (json) {
							 console.log(json);
						 }
											});
											
                							return value;

                						}*/
                                    },
									{
                                        indicator: 'Saving Process...',
                                        tooltip: 'Click to edit Process',
                                        type: 'select',
                                        onblur: 'cancel',
                                        submit:'Save',
                                        loadurl: src4,
										loadtype: 'POST',
										sUpdateURL:"employee/updateteam",
										sSuccessResponse: "IGNORE"
                                    },
									
                    ],
                      sAddURL: "AddData.php",
                    					sDeleteURL: "DeleteData.php",					
                });
            function callteamdet(){
            var toappend;
            $.ajax({
                    type: "POST",
                    url: "master/team_list?bname="+$("#bname").val(),
                    data :"bname="+$("#bname").val(),
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('.team').find('option').remove();
                        if(json) {
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['team']+'</option>';
                                
                            });   
                            $(".team").append(toappend);
                        }
                    }            
                }); 
            }
          });
        </script>
        <?php $this->load->view('includes/additional.php');?>
		<script type="text/javascript" language="javascript" src="assets/bower_components/datatables/media/js/jquery.jeditable.datepicker.js"></script>
    </body>
</html>