<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
       
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Payroll</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>  Modify Employee Payroll <?php if(isset($Emp_id)){ echo "- ". $Emp_id;} ?> </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
                         <div id="hookError" class="alert alert-danger" style="">  
                         </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                                <form class="form-horizontal" id="addemployee" method="post" action="employee/trackingsheet" role="form">
                                <br>
<!--                                 <div class="box col-md-12">
                                <div class="box-inner">-->
                            <div class="row">
                                <div class="bordering">
                                <h1><span>Official information</span></h1>
                                <div class="col-md-6">
                                    <div class="form-group">								
                                      <label class="control-label col-xs-4" for="emp_id">Employee ID</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" value="<?php if(isset($Emp_id)){echo $Emp_id;}?>" readonly name="emp_id" id="emp_id">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-xs-4" for="emp_desig">Designation</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" readonly name="emp_desig" value="<?php if(isset($Design)){echo $Design;}?>" id="emp_desig">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_bacno">Bank Account No</label>
                                        <div class="col-xs-7">
                                            <input type="text" id="emp_bacno" readonly name="emp_bacno"  class="form-control" value="<?php if(isset($employ_bank_ac_no)){echo $employ_bank_ac_no;}?>" placeholder="" >    					
                                            
<!--                                            <input  tabindex="1" type=radio CHECKED value="Active"  name='emp_bstatus' id='emp_bstatus' >Active&nbsp;&nbsp;
                                            <input type=radio tabindex="1" value="Inactive"  name='emp_bstatus' id='emp_bstatus'>Inactive&nbsp;&nbsp;									-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_bstatus">Bank Account Status</label>
                                        <div class="col-xs-7">
                                            <input  tabindex="1" type=radio checked value="Active"  name='emp_bstatus' id='emp_bstatus' >Active&nbsp;&nbsp;
                                            <input type=radio tabindex="1" value="Inactive"  name='emp_bstatus' id='emp_bstatus'>Inactive&nbsp;&nbsp;									
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_name">Employee Name</label>
                                        <div class="col-xs-7">
                                             <input type="text" class="form-control" readonly name="emp_name" id="emp_name" value="<?php if(isset($Name)){echo $Name;}?>" />
                                         </div>                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_doj">Date of Joining</label>
                                           <div class="col-xs-7">
                                                <input type="text" class="form-control" readonly name="emp_doj" id="emp_doj" value="<?php if(isset($D_of_join)){echo $D_of_join;}?>" />
                                            </div>                                        
                                    </div>
                                    <div class="form-group">
                                            <label class="control-label col-xs-4" for="emp_ctc">CTC</label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" readonly name="emp_ctc" id="emp_ctc" value="<?php if(isset($employ_rev_ear_ctc)){echo $employ_rev_ear_ctc;}?>" />											
                                            </div>                                        
                                    </div>
                                </div>
                                    <div class="form-group">                                            
                                            <div class="col-xs-7"></div>                                        
                                    </div> 
                                 </div>
                            </div>

<!--                                </div>
                                 </div>-->
<!--<hr>-->
<!--                                      <div class="box col-md-12">
				<div class="box-inner">				-->
                            <div class="row"> 
                                <div class="bordering">
                                <h1><span>Salary information</span></h1>
                                <div class="col-md-6">
                                    <div class="form-group">								
                                        <label class="control-label col-xs-4" for="emp_basicpay">Basic Pay</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_basicpay" id="emp_basicpay" value="<?php if(isset($employ_basic_pay)){echo $employ_basic_pay;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_hra">HRA</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_hra" id="emp_hra" value="<?php if(isset($employ_hra)){echo $employ_hra;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_cca">CCA</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_cca" id="emp_cca" value="<?php if(isset($employ_cca)){echo $employ_cca;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_callow">Conveyance Allow</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_callow" id="emp_callow" value="<?php if(isset($employ_cony_allow)){echo $employ_cony_allow;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_enallow">Entertainment Allow</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_enallow" id="emp_enallow" value="<?php if(isset($employ_enter_allow)){echo $employ_enter_allow;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_pincent">Performance Incentive </label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_pincent" id="emp_pincent" value="<?php if(isset($employ_perform_incen)){echo $employ_perform_incen;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_oallow">Other Allowance</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_oallow" id="emp_oallow" value="<?php if(isset($employ_other_allow)){echo $employ_other_allow;}?>" />
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <br><br><br><br> <br><br><br><br><br>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_grosssal">Gross Salary</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_grosssal" id="emp_grosssal" value="<?php if(isset($employ_gross)){echo $employ_gross;}?>" />
                                        </div>
                                    </div>
				</div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_pf">PF</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_pf" id="emp_pf" value="<?php if(isset($employ_pf)){echo $employ_pf;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_pfno">PF Number</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_pfno" id="emp_pfno" value="<?php if(isset($employ_pf_no)){echo $employ_pf_no;}?>" />
                                        </div>        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_esideduct">ESI</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_esideduct" id="emp_esideduct" value="<?php if(isset($employ_esi)){echo $employ_esi;}?>" />
                                        </div>         
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_esino">ESI Number</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_esino" id="emp_esino" value="<?php if(isset($employ_esi_no)){echo $employ_esi_no;}?>" />
                                        </div>      
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_ptax">Professional Tax</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_ptax" id="emp_ptax" value="<?php if(isset($employ_pt)){echo $employ_pt;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_intax">Income Tax</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_intax" id="emp_intax" value="<?php if(isset($employ_it)){echo $employ_it;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_insurance">General Insurance</label>
                                        <div class="col-xs-7">
                                            <input type="text"  readonly class="form-control" name="emp_insurance" id="emp_insurance" value="<?php if(isset($employ_gen_insure)){echo $employ_gen_insure;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_odeduct">Other Deduction</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_odeduct" id="emp_odeduct" value="<?php if(isset($employ_other_deduct)){echo $employ_other_deduct;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_tdeduct">Total Deduction</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_tdeduct" id="emp_tdeduct" value="<?php if(isset($tot_deduction)){echo $tot_deduction;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_empyrpf">Employer PF</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_empyrpf" id="emp_empyrpf" value="<?php if(isset($employer_pf)){echo $employer_pf;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_empyresi">Employer ESI</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_empyresi" id="emp_empyresi" value="<?php if(isset($employer_esi)){echo $employer_esi;}?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_netsal">Net Salary</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly class="form-control" name="emp_netsal" id="emp_netsal" value="<?php if(isset($employ_net)){echo $employ_net;}?>" />
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
                                <div class="form-group">
                                    <div class="col-xs-1 col-sm-offset-2">
                                        <input type="button" id="backsubmit" class="btn btn-primary" value="Back">
                                    </div> 
                                        <div class="col-xs-3 col-sm-offset-2">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                </div>
    </div>
                            </div>
<!--                            </div>
                        </div>-->
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
  <!--back Button-->
  <form id="back_btn" method="post" action="employee/modify_ctcc" >
      <input type="hidden" name="Emp_id" id="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
        <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />	  
  </form>
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
    <script>
        $(document).ready(function(){
            $("#backsubmit").click(function(){
                $("#back_btn").submit();
            });
        });
    </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>