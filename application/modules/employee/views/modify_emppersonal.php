<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Employee address</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>  Modify Employee Address  <?php if(isset($Emp_id)){ echo "- ". $Emp_id;} ?> </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
                         <div id="hookError" class="alert alert-danger" style="">  
                         </div>
                        <div class="box-content col-sm-offset-0" style="margin-left:10px;">
                            <!--working content start-->
                            <form class="form-horizontal" id="addemp_personal" method="post" action="employee/modifyctc" enctype="multipart/form-data"  role="form">
                                  <input type="hidden" name="Emp_id" id="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
                                  <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
                            <div class="row"> 
                                <div class="bordering">
                                    <h1><span>Personal information</span></h1>
                                <div class="col-md-5">
                                    <div class="form-group"><div class="col-xs-7"><h3> Current Address</h3> </div></div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_add1">Address 1</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="cur_add1" id="cur_add1" value="<?php if(isset($Temp_add1)){ echo $Temp_add1;} ?>" />
                                        </div>
                                     </div>                                    
                                    <div class="form-group">                                    
                                         <label class="control-label col-xs-4" for="cur_add2">Address 2</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="cur_add2" id="cur_add2" value="<?php if(isset($Temp_add2)){ echo $Temp_add2;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_add3">Address 3</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="cur_add3" id="cur_add3" value="<?php if(isset($Temp_add3)){ echo $Temp_add3;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_city">City</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="cur_city" id="cur_city" value="<?php if(isset($Temp_city)){ echo $Temp_city;} ?>" />
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_pincode">Pincode</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,maxSize[8],minSize[6],custom[integer]] form-control" data-errormessage-custom-error="Invalid pincode" name="cur_pincode" id="cur_pincode" value="<?php if(isset($Temp_pincode)){ echo $Temp_pincode;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_state">State</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" id="cur_state" name="cur_state" data-placeholder="Select state">
                                                <option value=""></option>
                                                <option value="2"  <?php if(isset($Temp_state) &&($Temp_state == 2)){ echo "selected";} ?>> Andhra Pradesh </option>
                                                <option value="3" <?php if(isset($Temp_state) &&($Temp_state == 3)){ echo "selected";} ?>> Arunachal Pradesh </option>
                                                <option value="4" <?php if(isset($Temp_state) &&($Temp_state == 4)){ echo "selected";} ?>> Assam </option>
                                                <option value="5" <?php if(isset($Temp_state) &&($Temp_state == 5)){ echo "selected";} ?>> Bihar </option>
                                                <option value="7" <?php if(isset($Temp_state) &&($Temp_state == 7)){ echo "selected";} ?>> Chattisgarh </option>
                                                <option value="11" <?php if(isset($Temp_state) &&($Temp_state == 11)){ echo "selected";} ?>> Goa </option>
                                                <option value="12" <?php if(isset($Temp_state) &&($Temp_state == 12)){ echo "selected";} ?>> Gujarat </option>
                                                <option value="13" <?php if(isset($Temp_state) &&($Temp_state == 13)){ echo "selected";} ?>> Haryana </option>
                                                <option value="14" <?php if(isset($Temp_state) &&($Temp_state == 14)){ echo "selected";} ?>> Himachal Pradesh </option>
                                                <option value="15" <?php if(isset($Temp_state) &&($Temp_state == 15)){ echo "selected";} ?>> Jammu and Kashmir </option>
                                                <option value="16" <?php if(isset($Temp_state) &&($Temp_state == 16)){ echo "selected";} ?>> Jharkhand </option>
                                                <option value="17" <?php if(isset($Temp_state) &&($Temp_state == 17)){ echo "selected";} ?>> Karnataka </option>
                                                <option value="18" <?php if(isset($Temp_state) &&($Temp_state == 18)){ echo "selected";} ?>> Kerala </option>
                                                <option value="20" <?php if(isset($Temp_state) &&($Temp_state == 20)){ echo "selected";} ?>> Madhya Pradesh </option>
                                                <option value="21" <?php if(isset($Temp_state) &&($Temp_state == 21)){ echo "selected";} ?>> Maharashtra </option>
                                                <option value="22" <?php if(isset($Temp_state) &&($Temp_state == 22)){ echo "selected";} ?>> Manipur </option>
                                                <option value="23" <?php if(isset($Temp_state) &&($Temp_state == 23)){ echo "selected";} ?>> Meghalaya </option>
                                                <option value="24" <?php if(isset($Temp_state) &&($Temp_state == 24)){ echo "selected";} ?>> Mizoram </option>
                                                <option value="25" <?php if(isset($Temp_state) &&($Temp_state == 25)){ echo "selected";} ?>> Nagaland </option>
                                                <option value="26" <?php if(isset($Temp_state) &&($Temp_state == 26)){ echo "selected";} ?>> Orissa </option>
                                                <option value="28" <?php if(isset($Temp_state) &&($Temp_state == 28)){ echo "selected";} ?>> Punjab </option>
                                                <option value="29" <?php if(isset($Temp_state) &&($Temp_state == 29)){ echo "selected";} ?>> Rajasthan </option>
                                                <option value="30" <?php if(isset($Temp_state) &&($Temp_state == 30)){ echo "selected";} ?>> Sikkim </option>
                                                <option value="31" <?php if(isset($Temp_state) &&($Temp_state == 31)){ echo "selected";} ?>> Tamil Nadu </option>
                                                <option value="32" <?php if(isset($Temp_state) &&($Temp_state == 32)){ echo "selected";} ?>> Tripura </option>
                                                <option value="34" <?php if(isset($Temp_state) &&($Temp_state == 34)){ echo "selected";} ?>> Uttarakhand </option>
                                                <option value="33" <?php if(isset($Temp_state) &&($Temp_state == 33)){ echo "selected";} ?>> Uttar Pradesh </option>
                                                <option value="35" <?php if(isset($Temp_state) &&($Temp_state == 35)){ echo "selected";} ?>> West Bengal </option>
                                                <option value="1" <?php if(isset($Temp_state) &&($Temp_state == 1)){ echo "selected";} ?>> Andaman and Nicobar </option>
                                                <option value="10" <?php if(isset($Temp_state) &&($Temp_state == 10)){ echo "selected";} ?>> Delhi </option>
                                                <option value="27" <?php if(isset($Temp_state) &&($Temp_state == 27)){ echo "selected";} ?>> Pondicherry </option>
                                                <option value="6" <?php if(isset($Temp_state) &&($Temp_state == 6)){ echo "selected";} ?>> Chandigarh </option>
                                                <option value="8" <?php if(isset($Temp_state) &&($Temp_state == 8)){ echo "selected";} ?>> Dadra and Nagar Haveli </option>
                                                <option value="9" <?php if(isset($Temp_state) &&($Temp_state == 9)){ echo "selected";} ?>> Daman and Diu </option>
                                                <option value="19" <?php if(isset($Temp_state) &&($Temp_state == 19)){ echo "selected";} ?>> Lakshadweep </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_country">Country</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="cur_country" id="cur_country" value="<?php if(isset($Temp_country)){ echo $Temp_country;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_mobile">Mobile No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,custom[phone],minSize[9],maxSize[15]] form-control" data-errormessage-custom-error="Invalid Mobile number" name="cur_mobile" id="cur_mobile" value="<?php if(isset($Temp_mobile)){ echo $Temp_mobile;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="cur_phone">Land Line No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[phone],minSize[9],maxSize[15]] form-control" data-errormessage-custom-error="Invalid landline number" name="cur_phone" id="cur_phone" value="<?php if(isset($Temp_phone)){ echo $Temp_phone;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="personal_email">Personal E-Mail ID</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,custom[email]] form-control datepicker" name="personal_email" id="personal_email" value="<?php if(isset($Perm_email)){ echo $Perm_email;} else{ echo "demo@allzonems.com";} ?>" />
                                        </div>
                                    </div>
                                    
                                    
                                </div>  
                                <!--<div class="col-xs-1"></div>-->    
                                    
                                <div class="col-md-5">
                                    <div class="form-group"><div class="col-xs-7"><h3> Permanent Address</h3> </div></div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_add1">Address 1</label>
                                       <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="per_add1" id="per_add1" value="<?php if(isset($Perm_add1)){ echo $Perm_add1;} ?>" />
                                        </div> 
                                    </div>
                                    <div class="form-group">                                    
                                        <label class="control-label col-xs-4" for="per_add2">Address 2</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="per_add2" id="per_add2" value="<?php if(isset($Perm_add2)){ echo $Perm_add2;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_add3">Address 3</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="per_add3" id="per_add3" value="<?php if(isset($Perm_add3)){ echo $Perm_add3;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_city">City</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="per_city" id="per_city" value="<?php if(isset($Perm_city)){ echo $Perm_city;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_pincode">Pincode</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,maxSize[8],minSize[6],custom[integer]] form-control" data-errormessage-custom-error="Invalid pincode" name="per_pincode" id="per_pincode" value="<?php if(isset($Perm_pincode)){ echo $Perm_pincode;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_state">State</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" id="per_state" name="per_state"  data-placeholder="Select state">
                                                <option value=""></option>
                                                <option value="2"  <?php if(isset($Perm_state) &&($Perm_state == 2)){ echo "selected";} ?>> Andhra Pradesh </option>
                                                <option value="3" <?php if(isset($Perm_state) &&($Perm_state == 3)){ echo "selected";} ?>> Arunachal Pradesh </option>
                                                <option value="4" <?php if(isset($Perm_state) &&($Perm_state == 4)){ echo "selected";} ?>> Assam </option>
                                                <option value="5" <?php if(isset($Perm_state) &&($Perm_state == 5)){ echo "selected";} ?>> Bihar </option>
                                                <option value="7" <?php if(isset($Perm_state) &&($Perm_state == 7)){ echo "selected";} ?>> Chattisgarh </option>
                                                <option value="11" <?php if(isset($Perm_state) &&($Perm_state == 11)){ echo "selected";} ?>> Goa </option>
                                                <option value="12" <?php if(isset($Perm_state) &&($Perm_state == 12)){ echo "selected";} ?>> Gujarat </option>
                                                <option value="13" <?php if(isset($Perm_state) &&($Perm_state == 13)){ echo "selected";} ?>> Haryana </option>
                                                <option value="14" <?php if(isset($Perm_state) &&($Perm_state == 14)){ echo "selected";} ?>> Himachal Pradesh </option>
                                                <option value="15" <?php if(isset($Perm_state) &&($Perm_state == 15)){ echo "selected";} ?>> Jammu and Kashmir </option>
                                                <option value="16" <?php if(isset($Perm_state) &&($Perm_state == 16)){ echo "selected";} ?>> Jharkhand </option>
                                                <option value="17" <?php if(isset($Perm_state) &&($Perm_state == 17)){ echo "selected";} ?>> Karnataka </option>
                                                <option value="18" <?php if(isset($Perm_state) &&($Perm_state == 18)){ echo "selected";} ?>> Kerala </option>
                                                <option value="20" <?php if(isset($Perm_state) &&($Perm_state == 20)){ echo "selected";} ?>> Madhya Pradesh </option>
                                                <option value="21" <?php if(isset($Perm_state) &&($Perm_state == 21)){ echo "selected";} ?>> Maharashtra </option>
                                                <option value="22" <?php if(isset($Perm_state) &&($Perm_state == 22)){ echo "selected";} ?>> Manipur </option>
                                                <option value="23" <?php if(isset($Perm_state) &&($Perm_state == 23)){ echo "selected";} ?>> Meghalaya </option>
                                                <option value="24" <?php if(isset($Perm_state) &&($Perm_state == 24)){ echo "selected";} ?>> Mizoram </option>
                                                <option value="25" <?php if(isset($Perm_state) &&($Perm_state == 25)){ echo "selected";} ?>> Nagaland </option>
                                                <option value="26" <?php if(isset($Perm_state) &&($Perm_state == 26)){ echo "selected";} ?>> Orissa </option>
                                                <option value="28" <?php if(isset($Perm_state) &&($Perm_state == 28)){ echo "selected";} ?>> Punjab </option>
                                                <option value="29" <?php if(isset($Perm_state) &&($Perm_state == 29)){ echo "selected";} ?>> Rajasthan </option>
                                                <option value="30" <?php if(isset($Perm_state) &&($Perm_state == 30)){ echo "selected";} ?>> Sikkim </option>
                                                <option value="31" <?php if(isset($Perm_state) &&($Perm_state == 31)){ echo "selected";} ?>> Tamil Nadu </option>
                                                <option value="32" <?php if(isset($Perm_state) &&($Perm_state == 32)){ echo "selected";} ?>> Tripura </option>
                                                <option value="34" <?php if(isset($Perm_state) &&($Perm_state == 34)){ echo "selected";} ?>> Uttarakhand </option>
                                                <option value="33" <?php if(isset($Perm_state) &&($Perm_state == 33)){ echo "selected";} ?>> Uttar Pradesh </option>
                                                <option value="35" <?php if(isset($Perm_state) &&($Perm_state == 35)){ echo "selected";} ?>> West Bengal </option>
                                                <option value="1" <?php if(isset($Perm_state) &&($Perm_state == 1)){ echo "selected";} ?>> Andaman and Nicobar </option>
                                                <option value="10" <?php if(isset($Perm_state) &&($Perm_state == 10)){ echo "selected";} ?>> Delhi </option>
                                                <option value="27" <?php if(isset($Perm_state) &&($Perm_state == 27)){ echo "selected";} ?>> Pondicherry </option>
                                                <option value="6" <?php if(isset($Perm_state) &&($Perm_state == 6)){ echo "selected";} ?>> Chandigarh </option>
                                                <option value="8" <?php if(isset($Perm_state) &&($Perm_state == 8)){ echo "selected";} ?>> Dadra and Nagar Haveli </option>
                                                <option value="9" <?php if(isset($Perm_state) &&($Perm_state == 9)){ echo "selected";} ?>> Daman and Diu </option>
                                                <option value="19" <?php if(isset($Perm_state) &&($Perm_state == 19)){ echo "selected";} ?>> Lakshadweep </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_country">Country</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="per_country" id="per_country" value="<?php if(isset($Perm_country)){ echo $Perm_country;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_mobile">Mobile No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,custom[phone],minSize[9],maxSize[15]]  form-control" data-errormessage-custom-error="Invalid mobile number" name="per_mobile" id="per_mobile" value="<?php if(isset($Perm_mobile)){ echo $Perm_mobile;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="per_phone">Land Line No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[phone],minSize[9],maxSize[15]]  form-control" data-errormessage-custom-error="Invalid landline number"  name="per_phone" id="per_phone" value="<?php if(isset($Perm_phone)){ echo $Perm_phone;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">                                        
                                        <label class="control-label col-xs-4" for="bgroup">Blood Group</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" name="bgroup" id="bgroup" data-placeholder="Select Group">
                                                <option value=""></option>
                                                <option value="1" <?php if(isset($Blood_group) &&($Blood_group == 1)){ echo "selected";} ?>> A+ ve </option>
                                                <option value="2" <?php if(isset($Blood_group) &&($Blood_group == 2)){ echo "selected";} ?>> B+ ve </option>
                                                <option value="3" <?php if(isset($Blood_group) &&($Blood_group == 3)){ echo "selected";} ?>> O+ ve </option>
                                                <option value="4" <?php if(isset($Blood_group) &&($Blood_group == 4)){ echo "selected";} ?>> AB+ ve </option>
                                                <option value="5" <?php if(isset($Blood_group) &&($Blood_group == 5)){ echo "selected";} ?>> A1 +ve </option>
                                                <option value="6" <?php if(isset($Blood_group) &&($Blood_group == 6)){ echo "selected";} ?>> A2 +ve </option>
                                                <option value="7" <?php if(isset($Blood_group) &&($Blood_group == 7)){ echo "selected";} ?>> A1B +ve </option>
                                                <option value="8" <?php if(isset($Blood_group) &&($Blood_group == 8)){ echo "selected";} ?>> A2B +ve </option>
                                                <option value="9" <?php if(isset($Blood_group) &&($Blood_group == 9)){ echo "selected";} ?>> A- ve </option>
                                                <option value="10" <?php if(isset($Blood_group) &&($Blood_group == 10)){ echo "selected";} ?>> B- ve </option>
                                                <option value="11" <?php if(isset($Blood_group) &&($Blood_group == 11)){ echo "selected";} ?>> O- ve </option>
                                                <option value="12" <?php if(isset($Blood_group) &&($Blood_group == 12)){ echo "selected";} ?>> AB- ve </option>
                                                <option value="13" <?php if(isset($Blood_group) &&($Blood_group == 13)){ echo "selected";} ?>> A1- ve </option>
                                                <option value="14" <?php if(isset($Blood_group) &&($Blood_group == 14)){ echo "selected";} ?>> A2- ve </option>
                                                <option value="15" <?php if(isset($Blood_group) &&($Blood_group == 15)){ echo "selected";} ?>> A1B -ve </option>
                                                <option value="16" <?php if(isset($Blood_group) &&($Blood_group == 16)){ echo "selected";} ?>> A2B -ve </option>
                                                <option value="17" <?php if(isset($Blood_group) &&($Blood_group == 17)){ echo "selected";} ?>> B1 +ve </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                     <div class="col-sm-offset-0">
                                        <div class="form-group col-xs-12" style="margin-left: 0px;">
                                            <div class="col-xs-2">
                                                <label class="control-label" for="emp_qua" style="width: 97%">Qualification</label>
                                            </div>
                                            <div class="col-xs-10">
                                                <div class="col-xs-3 radio">
                                                    <label><input  tabindex="1" type="radio" <?php if(isset($Qualification1) && (($Qualification1 == 25) || ($Qualification1 == 0)) ){ echo "checked";}  if(!isset($Qualification1)){ echo 'checked'; }  ?> value="25"  name='emp_qua' id='status2' > < 10th &nbsp;&nbsp;</label><br>
                                                    <label><input  tabindex="1" type="radio" <?php if(isset($Qualification1) &&($Qualification1 == 13)){ echo "checked";}?> value="13"  name='emp_qua' id='status1' > 10th(SSLC) &nbsp;&nbsp;</label>
                                                    <label><input  tabindex="1" type="radio" <?php if(isset($Qualification1) && ( ($Qualification1 == 12) || (($Qualification1 != 13) && ($Qualification1 != 25) ) )  ){ echo "checked";}?> value="12"  name='emp_qua' id='status'> 12th(HSC) &nbsp;&nbsp;</label>
                                                </div>
                                                <div class="col-xs-3">
                                                    <select class="form-control" name="emp_qua1" id="emp_qua1" data-placeholder='Select UG' data-rel="chosen">
                                                        <option value=""></option>
                                                        <option value="1" <?php if(isset($Qualification1) &&($Qualification1 == 1)){ echo "selected";} ?>> B.A </option>
                                                        <option value="4" <?php if(isset($Qualification1) &&($Qualification1 == 4)){ echo "selected";} ?>> B.B.A </option>
                                                        <option value="5" <?php if(isset($Qualification1) &&($Qualification1 == 5)){ echo "selected";} ?>> B.B.M </option>
                                                        <option value="6" <?php if(isset($Qualification1) &&($Qualification1 == 6)){ echo "selected";} ?>> B.C.A </option>
                                                        <option value="24" <?php if(isset($Qualification1) &&($Qualification1 == 24)){ echo "selected";} ?>> B.C.S </option>
                                                        <option value="3" <?php if(isset($Qualification1) &&($Qualification1 == 3)){ echo "selected";} ?>> B.Com </option>
                                                        <option value="7" <?php if(isset($Qualification1) &&($Qualification1 == 7)){ echo "selected";} ?>> B.E </option>
                                                        <option value="10" <?php if(isset($Qualification1) &&($Qualification1 == 10)){ echo "selected";} ?>> B.P.T </option>
                                                        <option value="9" <?php if(isset($Qualification1) &&($Qualification1 == 9)){ echo "selected";} ?>> B.Pham </option>
                                                        <option value="2" <?php if(isset($Qualification1) &&($Qualification1 == 2)){ echo "selected";} ?>> B.Sc </option>
                                                        <option value="8" <?php if(isset($Qualification1) &&($Qualification1 == 8)){ echo "selected";} ?>> B.Tech </option>
                                                        <option value="11" <?php if(isset($Qualification1) &&($Qualification1 == 11)){ echo "selected";} ?>> Diploma in </option>
                                                    </select>
                                                    <input type="text" class="form-control" name="emp_qua2" placeholder="Department"  id="emp_qua2" value="<?php if(isset($Qualification2)){ echo $Qualification2;} ?>" style="margin-top: 7px"/>
                                                </div>
                                                <div class="col-xs-3">
                                                    <select class="form-control" name="emp_qua3" id="emp_qua3" data-placeholder='Select PG ' data-rel="chosen">
                                                        <option value=""></option>                                                        
                                                        <option value="14" <?php if(isset($Qualification3) &&($Qualification3 == 14)){ echo "selected";} ?>> M.A </option>
                                                        <option value="17" <?php if(isset($Qualification3) &&($Qualification3 == 17)){ echo "selected";} ?>> M.B.A </option>
                                                        <option value="19" <?php if(isset($Qualification3) &&($Qualification3 == 19)){ echo "selected";} ?>> M.C.A </option>
                                                        <option value="16" <?php if(isset($Qualification3) &&($Qualification3 == 16)){ echo "selected";} ?>> M.Com </option>
                                                        <option value="20" <?php if(isset($Qualification3) &&($Qualification3 == 20)){ echo "selected";} ?>> M.E </option>
                                                        <option value="18" <?php if(isset($Qualification3) &&($Qualification3 == 18)){ echo "selected";} ?>> M.F.M </option>
                                                        <option value="22" <?php if(isset($Qualification3) &&($Qualification3 == 22)){ echo "selected";} ?>> M.Pham </option>
                                                        <option value="15" <?php if(isset($Qualification3) &&($Qualification3 == 15)){ echo "selected";} ?>> M.Sc </option>
                                                        <option value="21" <?php if(isset($Qualification3) &&($Qualification3 == 21)){ echo "selected";} ?>> M.Tech </option>
														
<option value="24" <?php if(isset($Qualification3) &&($Qualification3 == 24)){ echo "selected";} ?>>M.S.W</option>
<option value="25" <?php if(isset($Qualification3) &&($Qualification3 == 25)){ echo "selected";} ?>>M.L</option>
<option value="26" <?php if(isset($Qualification3) &&($Qualification3 == 26)){ echo "selected";} ?>>M.Phil</option>
<option value="27" <?php if(isset($Qualification3) &&($Qualification3 == 27)){ echo "selected";} ?>>Ph.D</option>
                     
					  <option value="23" <?php if(isset($Qualification3) &&($Qualification3 == 23)){ echo "selected";} ?>> Others </option>
					  
					  
					  
                                                    </select>
                                                    <input type="text" class="form-control" name="emp_qua4" placeholder="Department" id="emp_qua4" value="<?php if(isset($Qualification4)){ echo $Qualification4;} ?>" style="margin-top: 7px"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group col-xs-12" style="margin-left: 0px;">
                                            <label class="control-label col-xs-5" for="add_as_above"> Address as above                                       
                                                <input type="checkbox" class="" name="add_as_above" id="add_as_above"/> 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="bordering">
                                <h1><span>Reference details</span></h1>
                                <div class="col-md-5">
                                    <div class="form-group"><div class="col-xs-5"><h3> Reference 1 </h3> </div></div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="ref1_name">Name</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="ref1_name" id="ref1_name" value="<?php if(isset($Ref1_name)){ echo $Ref1_name;} ?>" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="control-label col-xs-4" for="ref1_add">Address</label>
                                        <div class="col-xs-7">                                
                                            <textarea class="validate[required]  autogrow form-control" id="ref1_add" name="ref1_add" > <?php if(isset($Ref1_addr)){ echo $Ref1_addr;} ?></textarea>			
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="control-label col-xs-4" for="ref1_mobile">Contact No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,custom[phone],minSize[10],maxSize[11]] form-control" data-errormessage-custom-error="Invalid Mobile number" name="ref1_mobile" id="ref1_mobile" value="<?php if(isset($Ref1_phone)){ echo $Ref1_phone;} ?>" />
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-xs-1"></div>-->   
                                <div class="col-md-5">
                                    <div class="form-group"><div class="col-xs-5"><h3> Reference 2 </h3> </div></div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-4" for="ref2_name">Name</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required] form-control" name="ref2_name" id="ref2_name" value="<?php if(isset($Ref2_name)){ echo $Ref2_name;} ?>" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="control-label col-xs-4" for="ref2_add">Address</label>
                                        <div class="col-xs-7">                                
                                            <textarea class="validate[required]  autogrow form-control"  id="ref2_add" name="ref2_add" ><?php if(isset($Ref2_addr)){ echo $Ref2_addr;} ?></textarea>			
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="control-label col-xs-4" for="ref2_mobile">Contact No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[required,custom[phone],minSize[10],maxSize[11]] form-control" data-errormessage-custom-error="Invalid Mobile number" name="ref2_mobile" id="ref2_mobile" value="<?php if(isset($Ref2_phone)){ echo $Ref2_phone;} ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3 col-sm-offset-5"></div>
                                </div>
                            </div>
                            </div>
                                <div class="form-group">
                                    <div class="col-xs-1 col-sm-offset-2">
                                        <input type="button" id="backsubmit" class="btn btn-primary" value="Back">
                                    </div> 
                                    <div class="col-xs-3 col-sm-offset-2">
                                        <input type="submit" id="submit" class="btn btn-primary" value="Modify Address">
                                     </div>
                                </div>
                            </form>	
                              
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
  
  <!--Back Button form-->
  <!--emp_modify-->
  <form id="back_btn" method="post" action="employee/emp_modify" >
      <input type="hidden" name="emp_id" id="bemp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
	<input type="hidden" name="bname" id="bbname" value="<?php if(isset($Branch)){ echo $Branch;} ?>" />  
        <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
  </form>
  
  <!--back Button form-->
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

    <hr>
    <?php $this->load->view('includes/footer.php');?>
    </div>
    <script>
        $(document).ready(function(){
            $("#addemp_personal").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#addemp_personal").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#addemp_personal").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
            });
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
//            add_as_above
            $('#add_as_above').click(function() {
                if ($(this).is(":checked")){
                    $("#per_add1").val($("#cur_add1").val());                    
                    $("#per_add2").val($("#cur_add2").val());
                    $("#per_add3").val($("#cur_add3").val());
                    $("#per_city").val($("#cur_city").val());
                    $("#per_state").val($("#cur_state").val());                    
                    $("#per_pincode").val($("#cur_pincode").val());
                    $("#per_country").val($("#cur_country").val());
                    $("#per_mobile").val($("#cur_mobile").val());
                    $("#per_phone").val($("#cur_phone").val());
                    $("#addemp_personal").validationEngine('hide');
                }else{
                    $("#per_add1").val('');                 
                    $("#per_add2").val('');
                    $("#per_add3").val('');
                    $("#per_city").val('');
                    $("#per_state").val('');
                    $("#per_pincode").val('');
                    $("#per_country").val('');
                    $("#per_mobile").val('');
                    $("#per_phone").val('');
                }
                $('#per_state').trigger('chosen:updated');
            });
            
            $("#backsubmit").click(function(){
                $("#addemp_personal").validationEngine('hide');
                $("#back_btn").submit();
            });
            
        });
    </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>