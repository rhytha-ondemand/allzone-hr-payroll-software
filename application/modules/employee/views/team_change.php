<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Team Change</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Team Change </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                                
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="teamchange" role="form" method="post" action="employee/team_list">
                                <br>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Select Team </label>
                                    <div class="col-xs-4">
                                        <select class="validate[required] form-control" data-placehoder="Select Team" id="report_team" name="report_team" data-rel="chosen" >
                                                                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#teamchange").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("select").chosen({disable_search_threshold: 10});
             $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            
            $("#bname").change(function(){
              var toappend;
                var url = "master/team_list";
                var data =  "bname=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#report_team').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['reporting_no']+'>'+value['team']+'</option>';                                
                            });   
                            $('#report_team').append(toappend);                           
                        }
                         $("#report_team").trigger('chosen:updated');
                    }            
                });                 
          });
          
           //                   Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();                      
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');                        
                        }
                    }            
                }); 
                          
//                Branch det
          
          
          
//          $("#submit").click(function(){
//                if($("#teamchange").validationEngine('validate') ) {   
//                    alert("Success");
//                }
//                return false;
//          });
                      
        });
        </script>
            
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>