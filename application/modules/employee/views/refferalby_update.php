<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #bname_chosen,#emp_id_chosen,#cadre_chosen{
                width: 100%!important;
            }
            .datepicker{
                 cursor: text!important;
                background-color:#fff!important;
            }
/*            .btn-group{
                display: none;
            }*/
        </style>
      <?php // $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php // $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-12">
            <!-- content starts -->
<!--            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Referral details</a>
                    </li>
                </ul>
            </div>-->
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Referral details </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="mreferral" method="post" action="" role="form">                                
                                <input type="hidden" name="remp_id" value="<?php echo $remp_id; ?>" />
<!--                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname"> Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" id="bname" name="bname" readonly="" value="<?php  ?>"  />
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>-->
<input type="hidden" name="id1" value="<?php if(isset($array[0]['Id'])){echo $array[0]['Id'];}?>" />
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="emp_id">Employee Id</label><span ></span>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" name="emp_id" id="emp_id" readonly="" value="<?php if(isset($array[0]['Emp_name'])){ echo $array[0]['Emp_name']; }  ?>"  />
<!--                                        <select name="emp_id" id="emp_id" data-placeholder="Select Employee ID" class="validate[required] form-control" >
                                            <option></option>
                                        </select>-->
                                    </div>
                                </div>
<!--
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="emp_name">Employee name</label><span ></span>
                                    <div class="col-xs-4">
                                        <input type="text" id="emp_name" name="emp_name" class="validate[required] form-control"/>                                     
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="b_rel">Blood relation</label><span ></span>
                                    <div class="col-xs-4">
                                        <!--<div class="radio">-->
                                            <label class="radio-inline">
                                                <input type="radio" name="b_rel" id="b_rel" value="1" disabled="" <?php if(isset($array[0]['blood_rel']) && $array[0]['blood_rel'] == 1 ){echo "checked";}?> /> Yes                                                
                                            </label>
<!--                                        </div>
                                        <div class="radio">-->
                                            <label class="radio-inline">
                                                <input type="radio" name="b_rel" id="b_rel2" value="0" disabled="" <?php if(isset($array[0]['blood_rel']) && $array[0]['blood_rel'] == 0 ){echo "checked";}?> /> No                                                
                                            </label>
                                        <!--</div>-->    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="cadre">Cadre</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="cadre" name="cadre" disabled="" data-placeholder="Select Cadre" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="1" data-amt="500" data-period="1" <?php if(isset($array[0]['Cadre']) && $array[0]['Cadre'] == 1 ){echo "selected";}?>  >Fresher</option>
                                            <option value="2" data-amt="10000" data-period="2" <?php if(isset($array[0]['Cadre']) && $array[0]['Cadre'] == 2 ){echo "selected";}?> >Experienced AR</option>   
                                            <option value="3" data-amt="5000" data-period="2" <?php if(isset($array[0]['Cadre']) && $array[0]['Cadre'] == 3 ){echo "selected";}?> >Experienced Data</option>
                                            <option value="4" data-amt="5000" data-period="1" <?php if(isset($array[0]['Cadre']) && $array[0]['Cadre'] == 4 ){echo "selected";}?> >Experienced Coder</option>                                    
                                        </select>
                                    </div>
                                </div>
                                <div id="cadre1">
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="amt">Amount</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="amt" readonly="" name="amt" class="validate[required] form-control" value="<?php if(isset($array[0]['Amt'])){echo $array[0]['Amt'];}?>"  />                                     
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="date">Date</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="date" name="date" readonly="" class="validate[required] form-control datepicker" value="<?php if(isset($array[0]['rdate'])){echo $array[0]['rdate'];}?>" />                                     
                                        </div>
                                    </div>
                                </div>
<?php if (count($array) == 2 ){ ?>
                                <div id="cadre2"  >
                                    <input type="hidden" name="id2" value="<?php if(isset($array[1]['Id'])){echo $array[1]['Id'];}?>" />
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="amt2">Amount 2</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="amt2" name="amt2" readonly="" class="validate[required] form-control" value="<?php if(isset($array[1]['Amt'])){echo $array[1]['Amt'];}?>" />                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="date2">Date 2</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="date2" name="date2" readonly="" class="validate[required] form-control datepicker" value="<?php if(isset($array[1]['rdate'])){echo $array[1]['rdate'];}?>" />                                     
                                        </div>
                                    </div>
                                </div>

    <?php 
}?>                                
                                <div class="form-group">
                                    <div class="col-sm-offset-4">
                                        <div class="col-xs-2">
                                            <input type="submit" id="submit" class="btn btn-primary fancybox-close" value="Submit">
                                        </div>
                                    </div>                                   
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php // $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){  
                
                $.validationEngine.defaults.scroll = false;
                $("#mreferral").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"                
                });
            
                $("select").chosen({disable_search_threshold: 10});    
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                $("#date").datepicker({ dateFormat: 'dd-M-yy',minDate: 0,changeMonth: true,changeYear: true,
                onClose: function( selectedDate ) {
                        $( "#date2" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#date2").datepicker({ dateFormat: 'dd-M-yy',minDate: 0,changeMonth: true,changeYear: true});

                
                $("#submit").click(function(){
//                     $('.fancybox-close').trigger('click');                     
                    if ( $("#mreferral").validationEngine('validate') ) {
                        $.ajax({
                            type: "POST",
                            url: 'employee/refferal_update',
                            cache: false,
                            dataType: "json",
                            data: $("#mreferral").serialize(),
                            async: false,
                            success: function(json) { 
                                if(json){
                                    $("#mreferral").trigger("reset");
                                    $("select").trigger('chosen:updated');
                                    parent.$.fancybox.close();
                                }
                                return true;
                            }
                        });
                    }
                    return false;
                });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>
