<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
    <style>
        .label_colen:before{
            content: " : ";
            margin-left: 5px;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Details</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employee other details <?php if(isset($Emp_id)){ echo "- ". $Emp_id;} ?></h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form class="form-horizontal" id="emp_exp" role="form" action="employee/emp_experience_update" method="post">
                                                               
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Total years of previous experience</label>
                                    <div class="col-xs-4">
                                        <div class="col-xs-4">    
                                            <select name="Texp_year" class="form-control" id="Texp_year" data-placeholder="Years">
                                                <option value=""></option>
                                                <option value="-1" <?php if(isset($Texp_year) &&($Texp_year == -1)){ echo "selected";} ?> >Fresher</option>
                                                <option value="0" <?php if(isset($Texp_year) &&($Texp_year == 0)){ echo "selected";} ?> >0</option>
                                                <?php
                                                    for($i=1;$i<15;$i++){
                                                        $select='';
                                                        if($i == $Texp_year){
                                                            $select = "selected";
                                                        }                                                       
                                                        echo '<option value="'.$i.'" '.$select.' >'.$i.'</option>';
                                                    }
                                                ?>
                                            </select> 
                                        </div><div class="col-xs-2" style="padding-left: 0px; padding-right: 0px; margin-top: 10px;">Years</div>
                                         <div class="col-xs-4"> 
                                            <select name="Texp_month" class="form-control" id="Texp_month" data-placeholder="Month">
                                                <option value=""></option>
                                                <option value="-1" <?php if(isset($Texp_month) &&($Texp_month == -1)){ echo "selected";} ?> >Fresher</option>
                                                <option value="0" <?php if(isset($Texp_month) &&($Texp_month == 0)){ echo "selected";} ?> >0</option>
                                                 <option value="1" <?php if(isset($Texp_month) &&($Texp_month == 1)){ echo "selected";} ?> >1</option>
                                                    <option value="2" <?php if(isset($Texp_month) &&($Texp_month == 2)){ echo "selected";} ?> >2</option>
                                                    <option value="3" <?php if(isset($Texp_month) &&($Texp_month == 3)){ echo "selected";} ?> >3</option>
                                                    <option value="4" <?php if(isset($Texp_month) &&($Texp_month == 4)){ echo "selected";} ?> >4</option>
                                                    <option value="5" <?php if(isset($Texp_month) &&($Texp_month == 5)){ echo "selected";} ?> >5</option>
                                                    <option value="6" <?php if(isset($Texp_month) &&($Texp_month == 6)){ echo "selected";} ?> >6</option>
                                                    <option value="7" <?php if(isset($Texp_month) &&($Texp_month == 7)){ echo "selected";} ?> >7</option>
                                                    <option value="8" <?php if(isset($Texp_month) &&($Texp_month == 8)){ echo "selected";} ?> >8</option>
                                                    <option value="9" <?php if(isset($Texp_month) &&($Texp_month == 9)){ echo "selected";} ?> >9</option>
                                                    <option value="10" <?php if(isset($Texp_month) &&($Texp_month == 10)){ echo "selected";} ?> >10</option>
                                                    <option value="11" <?php if(isset($Texp_month) &&($Texp_month ==11)){ echo "selected";} ?> >11</option>
                                                    <option value="12" <?php if(isset($Texp_month) &&($Texp_month == 12)){ echo "selected";} ?> >12</option>                                              
                                            </select> 
                                         </div><div class="col-xs-2" style="padding-left: 0px; padding-right: 0px; margin-top: 10px;">Months</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Relevant Experience</label>
                                    <div class="col-xs-4">
                                        <div class="col-xs-4">    
                                            <select name="Exp_MB_Year" class="form-control" id="Exp_MB_Year" data-placeholder="Years">
                                                <option value=""></option>
<option value="-1" <?php if(isset($Exp_MB_Year) &&($Exp_MB_Year == -1)){ echo "selected";} ?> >Fresher</option>
<option value="0" <?php if(isset($Exp_MB_Year) &&($Exp_MB_Year == 0)){ echo "selected";} ?> >0</option>
                                                <?php
                                                    for($i=1;$i<15;$i++){
                                                       $select='';
                                                        if($i == $Exp_MB_Year){
                                                            $select = "selected";
                                                        }                                                       
                                                        echo '<option value="'.$i.'" '.$select.' >'.$i.'</option>';
                                                    }
                                                ?>
                                            </select> 
                                        </div><div class="col-xs-2" style="padding-left: 0px; padding-right: 0px; margin-top: 10px;">Years</div>
                                         <div class="col-xs-4"> 
                                            <select name="Exp_MB_Month" class="form-control" id="Exp_MB_Month" data-placeholder="Month">
                                                <option value=""></option>
 <option value="-1" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == -1)){ echo "selected";} ?> >Fresher</option>
 <option value="0" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 0)){ echo "selected";} ?> >0</option>
                                                <option value="1" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 1)){ echo "selected";} ?> >1</option>
                                                <option value="2" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 2)){ echo "selected";} ?> >2</option>
                                                <option value="3" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 3)){ echo "selected";} ?> >3</option>
                                                <option value="4" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 4)){ echo "selected";} ?> >4</option>
                                                <option value="5" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 5)){ echo "selected";} ?> >5</option>
                                                <option value="6" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 6)){ echo "selected";} ?> >6</option>
                                                <option value="7" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 7)){ echo "selected";} ?> >7</option>
                                                <option value="8" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 8)){ echo "selected";} ?> >8</option>
                                                <option value="9" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 9)){ echo "selected";} ?> >9</option>
                                                <option value="10" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 10)){ echo "selected";} ?> >10</option>
                                                <option value="11" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month ==11)){ echo "selected";} ?> >11</option>
                                                <option value="12" <?php if(isset($Exp_MB_Month) &&($Exp_MB_Month == 12)){ echo "selected";} ?> >12</option>                                             
                                            </select> 
                                         </div><div class="col-xs-2" style="padding-left: 0px; padding-right: 0px; margin-top: 10px;">Months</div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable responsive">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Company name</th>
                                            <th>Address</th>
                                            <th>Designation</th>
                                            <th>CTC</th>
                                            <th>Net</th>
                                            <th>Period of service</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" name="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id; } ?>" />
                                    <?php
                                       $i=1;
                                       if(isset($records)){
                                        if($records){
                                         foreach($records as $row){
                                                 echo"<tr>";
                                                 echo "<td>". $i++ ." <input type='hidden' name='Id[]' value='".$row['Id']."'  /> </td>";
                                                 echo "<td><input type='text' class='form-control' name='Comp_name[]' id='prev_cname$i' value='".$row['Comp_name']."' maxlength='50' /></td>";
                                                 echo "<td><input type='text' class='form-control' name='Address[]' id='prev_caddr$i' value='".$row['Address']."' maxlength='50' /></td>";
                                                 echo "<td><input type='text' class='form-control' name='Design[]' id='prev_cdesig$i' value='".$row['Design']."' maxlength='30' /></td>";
                                                 echo "<td><input type='text' class='validate[min[0],custom[number]] form-control' name='Ctc[]' id='prev_ctc$i' value='".$row['Ctc']."' maxlength='7' /></td>";
                                                 echo "<td><input type='text' class='validate[min[0],custom[number]] form-control' name='Net[]' id='prev_net$i' value='".$row['Net']."' maxlength='7' /></td>";
                                                 echo "<td><input type='text' class='form-control' name='Period[]' id='prev_service$i' value='".$row['Period']."' maxlength='30' /></td>";
                                                 echo"</tr>";
                                         }  
                                        }
                                       }
                                        for($n=$i; $n<6;$n++){
                                                echo"<tr>";
                                                echo "<td>". $i++ ." <input type='hidden' name='Id[]' value=''  /> </td>";
                                                echo "<td><input type='text' class='form-control' name='Comp_name[]' id='prev_cname$i' maxlength='50'  /></td>";
                                                echo "<td><input type='text' class='form-control' name='Address[]' id='prev_caddr$i' maxlength='50' /></td>";
                                                echo "<td><input type='text' class='form-control' name='Design[]' id='prev_cdesig$i' maxlength='30' /></td>";
                                                echo "<td><input type='text' class='validate[min[0],custom[number]] form-control' name='Ctc[]' id='prev_ctc$i' maxlength='7' /></td>";
                                                echo "<td><input type='text' class='validate[min[0],custom[number]] form-control' name='Net[]' id='prev_net$i' maxlength='7' /></td>";
                                                echo "<td><input type='text' class='form-control' name='Period[]' id='prev_service$i' maxlength='30' /></td>";
                                                echo"</tr>";
                                        } 
                                   ?>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Year(s) at Allzone</label>
                                    <div class="col-xs-4">
<!--                                        <input type="text" class=" form-control" name="" maxlength='20' id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;" ><?php if(isset($totalin_allzone)){echo $totalin_allzone ;}?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Designation at the time of joining</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php if(isset($joining)){echo $joining;}?></label>
                                    </div>
                                </div>
<!--                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Previous Designation</label>
                                    <div class="col-xs-4">
                                        <input type="text" class=" form-control" name="" id="">
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php echo '';?></label>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Present Designation</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php if(isset($present)){echo $present;} ?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">CTC at the time of joining</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php if(isset($join_ctc)){echo "Rs. ".$join_ctc .'/-';} ?> </label>
                                    </div>
                                </div>
<!--                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Previous CTC</label>
                                    <div class="col-xs-4">
                                        <input type="text" class=" form-control" name="" id="">
                                        <label class="control-label label_colen" style="font-weight: normal;">Rs. /-</label>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Present CTC</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"> <?php if(isset($present_ctc)){echo "Rs. ".$present_ctc .'/-';}?> </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Re-Fixation</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php if(isset($refix)){echo $refix;}?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">No.of Appraisal</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;"><?php if(isset($appr)){echo ($appr - 1) ;}?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-1 col-sm-offset-2">
                                        <input type="button" id="backsubmit" class="btn btn-primary" value="Back">
                                    </div> 
                                    <div class="col-xs-3 col-sm-offset-2">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  <form id="back_btn" method="post" action="employee/trackingsheet" >
      <input type="hidden" name="emp_id" id="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
      <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
  </form>
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                $.validationEngine.defaults.scroll = false;
                $("#emp_exp").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("select").chosen({disable_search_threshold: 8});
                
                
                $("#Texp_year").change(function(){
//                    alert($(this).val());
                    if($(this).val() === -1 || $(this).val() === '-1' ){                        
                        $("#Texp_month").val('-1');
                        $("#Exp_MB_Year").val('-1');
                        $("#Exp_MB_Month").val('-1');
                        $("select").trigger("chosen:updated");
                    }
                });
                
                //            back buton

            $("#backsubmit").click(function(){
                $("#addemp_personal").validationEngine('hide');
                $("#back_btn").submit();
            });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>