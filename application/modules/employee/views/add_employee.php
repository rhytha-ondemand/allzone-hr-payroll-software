<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    
    <body>
        <style>
             .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">New</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add New Employee </h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                                
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="addemployee" method="post" action="employee/employee_add" enctype="multipart/form-data"  role="form">
                                <br>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="bname">Branch</label>
                                    <div class="col-xs-7">
                                        <select id="bname" name="bname" class="validate[required] form-control">
                                        <option value="">Select branch</option>
                                        <option value="4">Chennai</option>
                                        <option value="3">Vellore</option>                                            
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_id">Employee ID</label>
                                    <div class="col-xs-7">
                                        <input type="text" readonly="" class="form-control" name="emp_id" id="emp_id">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_name">Employee Name</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="validate[required] form-control" name="emp_name" id="emp_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="sex">Gender</label>
                                    <div class="col-xs-7">
                                        <div class="radio">
                                            <label><input type="radio" class="validate[required]" checked="" name="sex" value="1" id="sexm"> Male</label> <br>
                                            <label><input type="radio" class="validate[required]" name="sex" value="2" id="sexf"> Female </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="dob">Date Of Birth</label>
                                    <div class="col-xs-7">
                                        <input type="text" readonly="" class="validate[required] datepicker form-control" name="dob" placeholder="Select date" id="dob">
                                        
                                    </div>                                        
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="doj">Date Of Joining</label>
                                    <div class="col-xs-7">
                                        <input type="text" readonly="" class="validate[required] form-control datepicker" name="doj" placeholder="Select date"  id="doj">
                                    </div>                                        
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_pl">PL Credit</label>
                                        <div class="col-xs-7">
                                            <input type="text" name="emp_pl" id="emp_pl" class="validate[required,custom[integer],min[0],max[15]] form-control" data-errormessage-custom-error="PL should contain numeric value" >
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="designation">Designation</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" id="designation" name="emp_desig" data-rel="chosen" data-placeholder="Select Designation">
<!--                                                <option value="">Select Designation</option>
                                                <option value="56"> Accountant </option>
                                                <option value="31"> Admin Assistant </option>
                                                <option value="5"> Admin Executive </option>
                                                <option value="25"> AR Analyst </option>
                                                <option value="19"> AR Caller </option>
                                                <option value="60"> Assistant - Accounts </option>
                                                <option value="97"> Assistant General Manager </option>
                                                <option value="83"> Assistant Manager </option>
                                                <option value="75"> Assistant Manager - Operations </option>
                                                <option value="73"> Assistant Manager - Quality </option>
                                                <option value="76"> Assistant Manager - Recruitment </option>
                                                <option value="74"> Assistant Manager - Training </option>
                                                <option value="54"> Asst. Manager - Administration </option>
                                                <option value="17"> Asst. Network Engineer </option>
                                                <option value="121"> AVP Operations & Facility Head </option>
                                                <option value="26"> Business Development Executive </option>
                                                <option value="2"> CEO </option>
                                                <option value="104"> Chief Business Officer </option>
                                                <option value="3"> Chief Financial Officer </option>
                                                <option value="103"> Chief Operating Officer </option>
                                                <option value="89"> Client Service Executive </option>
                                                <option value="98"> Data Extraction Executive </option>
                                                <option value="45"> Deputy Manager </option>
                                                <option value="80"> Desktop Engineer </option>
                                                <option value="81"> Developer </option>
                                                <option value="1"> Director </option>
                                                <option value="16"> Executive </option>
                                                <option value="43"> Front Office Executive </option>
                                                <option value="90"> Graphic Artist </option>
                                                <option value="118"> Group Coordinator </option>
                                                <option value="78"> Hardware Engineer </option>
                                                <option value="71"> Head - Administration </option>
                                                <option value="22"> Head - Client Relations </option>
                                                <option value="21"> Head - Operations </option>
                                                <option value="35"> House Keeping </option>
                                                <option value="42"> HR Executive </option>
                                                <option value="100"> HR Recruiter </option> 
                                                <option value="122"> Indesign Paginator </option>
                                                <option value="117"> Junior Paginator </option>
                                                <option value="108"> Junior Process Associate </option>
                                                <option value="120"> Junior Quality Controller - Typesetting </option>
                                                <option value="57"> Junior Team Leader - Litigation </option>
                                                <option value="50"> Manager </option>
                                                <option value="59"> Manager - Accounts </option>
                                                <option value="65"> Manager - Administration </option>
                                                <option value="8"> Manager - Coding </option>
                                                <option value="110"> Manager - E Publishing </option>
                                                <option value="116"> Manager - Finance </option>
                                                <option value="4"> Manager - Human Resources </option>
                                                <option value="9"> Manager - Litigation </option>
                                                <option value="63"> Manager - Network Engineering </option>
                                                <option value="115"> Marketing Executive </option>
                                                <option value="23"> Medical Coder </option>
                                                <option value="27"> Network Engineer </option>
                                                <option value="29"> Office Assistant </option>
                                                <option value="111"> Operations Manager - e Publishing </option>
                                                <option value="66"> Others </option>
                                                <option value="92"> Paginator Technician </option>
                                                <option value="114"> Pantry Attender </option>
                                                <option value="119"> PF Revision </option>
                                                <option value="13"> Process Associate </option>
                                                <option value="53"> Process Associate - Litigation </option>
                                                <option value="6"> Project Manager </option>
                                                <option value="91"> Proof Reader </option>
                                                <option value="112"> Proof Reader / Technical Editor </option>
                                                <option value="48"> Quality Associate - Litigation </option>
                                                <option value="106"> Quality Controller - E Pub </option>
                                                <option value="96"> Quality Controller - Typesetting </option>
                                                <option value="40"> Quality Manager </option>
                                                <option value="34"> Recruitment Manager </option>
                                                <option value="85"> Senior Analyst </option>
                                                <option value="88"> Senior Associate </option>
                                                <option value="82"> Senior Associate - HR & Admin </option>
                                                <option value="102"> Senior Data Conversion Technician </option>
                                                <option value="107"> Senior HR Associate </option>
                                                <option value="87"> Senior HR Executive </option>
                                                <option value="58"> Senior HR/Admin Executive </option>
                                                <option value="51"> Senior Manager </option>
                                                <option value="113"> Senior Manager - Human Resources </option>
                                                <option value="101"> Senior Software Developer </option>
                                                <option value="61"> Senior Software Engineer </option>
                                                <option value="64"> Senior Supervisor </option>
                                                <option value="62"> Senior Team Leader - Medical Coding </option>
                                                <option value="36"> Software Developer </option>
                                                <option value="38"> Sr. Admin Executive </option>
                                                <option value="30"> Sr. AR Analyst </option>
                                                <option value="20"> Sr. AR Caller </option>
                                                <option value="52"> Sr. Coding Manager </option>
                                                <option value="32"> Sr. HR Executive </option>
                                                <option value="46"> Sr. Medical Coder </option>
                                                <option value="7"> Sr. Network Engineer </option>
                                                <option value="37"> Sr. Operations Manager </option>
                                                <option value="94"> Sr. Paginator </option>
                                                <option value="12"> Sr. Process Associate </option>
                                                <option value="69"> Sr. Process Associate - Data & Litigation </option>
                                                <option value="47"> Sr. Process Associate - Litigation </option>
                                                <option value="28"> Sr. Project Manager </option>
                                                <option value="10"> Sr. Team Leader </option>
                                                <option value="33"> Supervisor </option>
                                                <option value="67"> Supervisor - Administration </option>
                                                <option value="68"> Supervisor - Human Resources </option>
                                                <option value="72"> Supervisor - Networking </option>
                                                <option value="11"> Team Leader </option>
                                                <option value="105"> Team Leader - Typesetting </option>
                                                <option value="99"> Tele Caller </option>
                                                <option value="124"> Test </option>
                                                <option value="70"> Trainee </option>
                                                <option value="49"> Trainee - Admin </option>
                                                <option value="15"> Trainee - AR </option>
                                                <option value="24"> Trainee - Coder </option>
                                                <option value="55"> Trainee - HR </option>
                                                <option value="44"> Trainee - Litigation </option>
                                                <option value="41"> Trainee - Networking </option>
                                                <option value="14"> Trainee - Process Associate </option>
                                                <option value="18"> Trainee - Software Developer </option>
                                                <option value="86"> Trainee Analyst </option>
                                                <option value="95"> Trainee Business Development </option>
                                                <option value="77"> Trainee HR / Admin </option>
                                                <option value="84"> Trainee Marketing </option>
                                                <option value="93"> Trainee Proof Reader </option>
                                                <option value="109"> Trainee Tele Caller </option>
                                                <option value="79"> Trainer </option>
                                                <option value="39"> Training Manager </option>-->
                                            </select>
                                        </div>
                                        
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="department">Department</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" name="emp_dept" id="department" data-rel="chosen" data-placeholder="Select department">
<!--                                                <option value="">Select Department</option>
                                                <option value="8"> Admin </option>
                                                <option value="11"> Business Development </option>
                                                <option value="14"> Dept </option>
                                                <option value="17"> dept1 </option>
                                                <option value="12"> E - Publishing </option>
                                                <option value="7"> Finance </option>
                                                <option value="10"> House Keeping </option>
                                                <option value="3"> Human Resource </option>
                                                <option value="6"> Litigation </option>
                                                <option value="1"> Medical Billing </option>
                                                <option value="2"> Medical Coding </option>
                                                <option value="13"> Medical Transcription </option>
                                                <option value="4"> Network </option>
                                                <option value="9"> Others </option>
                                                <option value="5"> Software </option>-->
                                            </select>
                                        </div>
                                </div>
<!--                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="locations">Location</label>
                                        <div class="col-xs-7">
                                            <select class="form-control" name="emp_loc" id="locations" data-rel="chosen" data-placeholder='Select Location'>
                                                <option value=""></option>
                                                <option value="6"> Egmore </option>
                                                <option value="2"> Kodambakkam </option>
                                                <option value="5"> Mugappair </option>
                                                <option value="4"> Nungambakkam </option>
                                                <option value="3"> Vellore </option>
                                            </select>
                                        </div>
                                </div>-->
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="project">Project</label>
                                        <div class="col-xs-7">
                                            <select class="form-control" name="emp_proj" id="project" data-rel="chosen" data-placeholder="Select Project">
<!--                                                <option value="">Select Project</option>
                                                <option value="45"> Admin </option>
                                                <option value="47"> BD </option>
                                                <option value="25"> Coding </option>
                                                <option value="44"> HR </option>
                                                <option value="43"> Networking </option>
                                                <option value="50"> Others </option>
                                                <option value="1"> PRJ-01 </option>
                                                <option value="2"> PRJ-03 </option>
                                                <option value="3"> PRJ-07 </option>
                                                <option value="4"> PRJ-08 </option>
                                                <option value="30"> PRJ-12 </option>
                                                <option value="5"> PRJ-15 </option>
                                                <option value="26"> PRJ-17 </option>
                                                <option value="27"> PRJ-18 </option>
                                                <option value="16"> PRJ-19 </option>
                                                <option value="6"> PRJ-20 </option>
                                                <option value="7"> PRJ-21 </option>
                                                <option value="17"> PRJ-22 </option>
                                                <option value="8"> PRJ-23 </option>
                                                <option value="18"> PRJ-24 </option>
                                                <option value="19"> PRJ-25 </option>
                                                <option value="20"> PRJ-26 </option>
                                                <option value="42"> PRJ-27 </option>
                                                <option value="28"> PRJ-28 </option>
                                                <option value="12"> PRJ-29 </option>
                                                <option value="31"> PRJ-30 </option>
                                                <option value="32"> PRJ-33 </option>
                                                <option value="9"> PRJ-34 </option>
                                                <option value="22"> PRJ-35 </option>
                                                <option value="13"> PRJ-36 </option>
                                                <option value="23"> PRJ-37 </option>
                                                <option value="10"> PRJ-38 </option>
                                                <option value="14"> PRJ-39 </option>
                                                <option value="29"> PRJ-40 </option>
                                                <option value="24"> PRJ-41 </option>
                                                <option value="11"> PRJ-42 </option>
                                                <option value="15"> PRJ-43 </option>
                                                <option value="38"> PRJ-44 </option>
                                                <option value="36"> PRJ-45 </option>
                                                <option value="35"> PRJ-48 </option>
                                                <option value="39"> PRJ-49 </option>
                                                <option value="37"> PRJ-50 </option>
                                                <option value="41"> PRJ-51 </option>
                                                <option value="34"> PRJ-52 </option>
                                                <option value="40"> PRJ-53 </option>
                                                <option value="33"> PRJ-54 </option>
                                                <option value="52"> PRJ-55 </option>
                                                <option value="51"> PRJ-57 </option>
                                                <option value="21"> PRJ-58 </option>
                                                <option value="53"> PRJ-60 </option>
                                                <option value="48"> PRJ-61 </option>
                                                <option value="57"> PRJ-62 </option>
                                                <option value="54"> PRJ-63 </option>
                                                <option value="55"> PRJ-65 </option>
                                                <option value="49"> PRJ-66 </option>
                                                <option value="58"> PRJ-69 </option>
                                                <option value="56"> PRJ-70 </option>
                                                <option value="60"> PRJ-71 </option>
                                                <option value="59"> PRJ-74 </option>
                                                <option value="61"> PRJ-76 </option>
                                                <option value="62"> PRJ-77 </option>
                                                <option value="63"> PRJ-78 </option>
                                                <option value="46"> Software </option>-->
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="report_team">Team</label>
                                        <div class="col-xs-7">
                                            <select class="form-control" name="emp_reportteam" id="report_team" data-rel="chosen" data-placeholder="Select Team">
<!--                                                <option value="">Select Team </option>
                                                <option value="19"> John </option>
                                                <option value="8"> Coding </option>
                                                <option value="3"> Edwin </option>
                                                <option value="4"> John </option>
                                                <option value="19"> John </option>
                                                <option value="1"> Lionel </option>
                                                <option value="14"> Management </option>
                                                <option value="12"> Martin </option>
                                                <option value="2"> Naz </option>
                                                <option value="15"> Others </option>
                                                <option value="11"> Patrick </option>
                                                <option value="10"> Paula </option>
                                                <option value="9"> Peter </option>
                                                <option value="13"> Rajee </option>
                                                <option value="17"> Rajee </option>
                                                <option value="6"> Raymond </option>
                                                <option value="16"> Robert </option>
                                                <option value="7"> Ryan </option>
                                                <option value="19"> S </option>
                                                <option value="5"> Training </option>
                                                <option value="18"> Y.P. Satheesh Kumar </option>-->
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="shift_time">Shift Time</label>
                                        <div class="col-xs-7">
                                            <select class="form-control" id="shift_time" name="emp_stime" data-placeholder='Select Shift Time' data-rel="chosen">
                                                <option value=""></option>
                                                <option value="1">Day</option>
                                                <option value="2">General</option>
                                                <option value="3">Semi</option>
                                                <option value="4">Night</option>
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_nsa">Night Shift Eligibility</label>
                                        <div class="col-xs-7">
                                            <input type="Checkbox" checked value="1" name="emp_nsa" id="emp_nsa" class="form-control" />
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="inputSuccess4">Process</label>
                                        <div class="col-xs-7">
                                            <select class="form-control" name="emp_process" id="process" data-placeholder='Select Process' data-rel="chosen">
                                                <option value=""> </option>
                                                <option value="9"> Admin </option>
                                                <option value="2"> AR </option>
                                                <option value="10"> BD </option>
                                                <option value="3"> Coding </option>
                                                <option value="1"> Data </option>
                                                <option value="11"> E-Pub </option>
                                                <option value="8"> Finance </option>
                                                <option value="4"> HR </option>
                                                <option value="7"> Litigation </option>
                                                <option value="5"> Network </option>
                                                <option value="6"> Software </option>
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-4" for="emp_status">Employee Status</label>
                                        <div class="col-xs-7">
                                            <div class="radio">
                                                <label><input type="radio" class="validate[required]" name="status" checked value="1" id="emp_status"> Active</label> <br>
                                                <label><input type="radio"  class="validate[required]" name="status" value="0" id="emp_status2"> Inactive </label>
                                            </div>
                                        </div>
                                </div>
                                        
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_fname">Father Name</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="validate[required] form-control" name="emp_fname" id="emp_fname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_hname">Husband Name</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" name="emp_hname" id="emp_hname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_wname">Wife Name</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" name="emp_wname" id="emp_wname">
                                    </div>                                        
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="official_name">Official Name</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="validate[required] form-control" name="official_name" id="official_name">
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_type">Employee Type</label>
                                    <div class="col-xs-7">
                                        <select class="validate[required] form-control" name="emp_type" id="emp_type" data-placeholder='Select Employee type' data-rel="chosen">
                                            <option value=""></option>
                                            <option value="1">Regular</option>
                                            <option value="2">Contract</option>
                                        </select>
                                    </div>                                        
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="office_email">Official E-mail</label>
                                    <div class="col-xs-7">
                                        <input type="text" class="validate[custom[email]] form-control" value="demo@allzonems.com" name="office_email" id="office_email">
                                    </div>
                                        
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="emp_photo">Add Photo:</label>
                                    <div class="col-xs-7">
                                        <input type="file" name="emp_photo" class="validate[funcCall[ftype]]" id="emp_photo">
                                    </div>
                                        
                                </div>
                            </div>    
                            </div>
                                <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                           <input type="submit" id="submit" class="btn btn-primary" value="Add New">
                                        </div>
                                </div>
                            </form>
                            <!--working area end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New Employee added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert employee details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div> 
        <script>

        $(document).ready(function(){
//            $.validationEngine.defaults.scroll = false;
            $("#addemployee").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#addemployee").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#addemployee").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
            });            
            $( "#doj" ).datepicker({ dateFormat: 'dd-M-yy',minDate:-7,maxDate:1 });
            $( "#dob" ).datepicker({ dateFormat: 'dd-M-yy', changeMonth: true,yearRange: "-50:+0",
            changeYear: true });
            $("select").chosen({disable_search_threshold: 10});
            
            $("#shift_time").change(function(){
                if($(this).val() == 4 || $(this).val() == "4" ){
                    $("#emp_nsa").prop("checked", false);
                    $("#emp_nsa").prop("disabled", true);
//                    alert("Night Shift");
                }else{
                    $("#emp_nsa").prop("checked", true);                    
                    $("#emp_nsa").prop("disabled", false);
                }
            });
//                                Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det
        
            
            $("#bname").change(function(){
                var bname = $("#bname").val();
                var emp_id;
                var depttoappend = "";
                var desigtoappend = "";
                var prjttoappend = "";
                var repttoappend = "";
                var location_city='';
//                console.log(bname);
                    $.ajax({
                        type: "POST",
                        url: "master/emp_id_all",
                        cache: false,
                        dataType: "json",                    
                        async: false,
                        success: function(json) {
                             if(json) {
                                 $.each(json, function(i, value) {
                                     if(value['Id'] == bname){
//                                         $("#emp_id").val(value['Value']);
                                        emp_id = value['Value'];
                                     }                                     
                                 });  
                                 if(emp_id){
                                     $("#emp_id").val(emp_id);
                                 }else{ 
                                     $("#emp_id").val("AMS"); }                                 
                             }
                        }   
                    });
                    $.ajax({
                        type: "POST",
                        url: "master/emp_dynamic",
                        cache: false,
                        data:"bname="+bname,
                        dataType: "json",                    
                        async: false,
                        success: function(json) {
                            $('#designation').find('option').remove();
                            $('#department').find('option').remove();
                            $('#project').find('option').remove();
                            $('#report_team').find('option').remove();
//                            $('#locations').find('option').remove();
                            if(json){
                                depttoappend='<option value=""></option>';
                                $.each(json.dept, function(i, value) {
                                     depttoappend+='<option value='+value['Dept_iid']+'>'+value['Dept_name']+'</option>';                                
                                }); 
                                
                                desigtoappend='<option value=""></option>';
                                $.each(json.design, function(i, value) {
                                     desigtoappend+='<option value='+value['Desig_iid']+'>'+value['Design_name']+'</option>';                                
                                });
                                
                                prjttoappend='<option value=""></option>';
                                $.each(json.project, function(i, value) {
                                     prjttoappend+='<option value='+value['Project_iid']+'>'+value['Project_no']+'</option>';                                
                                });
                                
                                repttoappend='<option value=""></option>';
                                $.each(json.teamlist, function(i, value) {
                                     repttoappend+='<option value='+value['team_iid']+'>'+value['team']+'</option>';                                
                                });
                                
                        /*         location_city='<option value=""></option>';
                                $.each(json.location_city, function(i, value) {
                                     location_city+='<option value='+value['Id']+'>'+value['Location']+'</option>';                                
                                }); */
                                
                                $('#designation').append(desigtoappend); 
                                $('#department').append(depttoappend); 
                                $('#project').append(prjttoappend); 
                                $('#report_team').append(repttoappend); 
//                                $('#locations').append(location_city); 
                            }
                            $('#designation').trigger('chosen:updated');
                            $('#department').trigger('chosen:updated');
                            $('#project').trigger('chosen:updated');
                            $('#report_team').trigger('chosen:updated');    
//                            $('#locations').trigger('chosen:updated');    
                              
                    }   
                    });
            });
            
            
            $("#submit").click(function(){
                if ( $("#addemployee").validationEngine('validate') ) {
                   return true;
                }
                return false;
            });
            
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
          });
        function ftype(field, rules, i, options){
	
            var ext = $("#emp_photo").val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg']) === -1) {
                return "Invalid File type";                  		
            }
            if($("#emp_photo")[0].files[0].size>307200)
            {
                console.log($("#emp_photo")[0].files[0].size);
                return "File limit exceeded";
            }
        }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    
    </body>
</html>