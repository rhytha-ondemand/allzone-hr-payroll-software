<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">CTC</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>  Modify CTC <?php if(isset($Emp_id)){ echo "- ". $Emp_id;} ?></h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                         <div id="hookError" class="alert alert-danger" style="">  
                         </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                              <form class="form-horizontal" id="addempctc" method="post" action="employee/modifypayroll" enctype="multipart/form-data"  role="form">
                                <br>
                                <div class="row">
                                    <input type="hidden" name="Emp_id" id="emp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />
                                    <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
                                <div class="col-md-5">
                                    <div class="form-group">
                                          <label class="control-label col-xs-5" for="ctc">Gross</label>
                                            <div class="col-xs-7">
                                                <input type="text" class="validate[required,min[1],custom[number]] form-control" data-errormessage-custom-error="Invalid CTC" name="ctc" maxlength="8" id="ctc" value="<?php if(isset($employ_ctc)){ echo $employ_ctc;} ?>" />
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="ctcr">CTC Range</label>
                                        <div class="col-xs-7">
                                            <select class="validate[required] form-control" id="ctcr" name="ctcr" data-placeholder="Select CTC range">
                                                <option value=""></option>
                                                <option value="1" <?php if(isset($employ_ctc_range) &&($employ_ctc_range == 1)){ echo "selected";} ?>>A</option>
                                                <option value="2" <?php if(isset($employ_ctc_range) &&($employ_ctc_range == 2)){ echo "selected";} ?>>B</option>
                                                <!--<option value="C">C</option>-->
                                                <option value="3" <?php if(isset($employ_ctc_range) &&($employ_ctc_range == 3)){ echo "selected";} ?>>S</option>
                                                <!--<option value="X">X</option>-->
                                                <option value="4" <?php if(isset($employ_ctc_range) &&($employ_ctc_range == 4)){ echo "selected";} ?>>Z</option>
                                            </select>
                                            </div>
                                    </div>
                                    <div id="categoryz" style="<?php if(isset($employ_ctc_range) &&($employ_ctc_range == 4)){ echo "display:block";} else { echo "display:none";} ?>">
                                        <div class="form-group">
                                            <label class="control-label col-xs-5" for="emp_additional1">Additional 1</label>
                                            <div class="col-xs-7">
                                                <input type="text"  class="validate[required,min[1],custom[number]] form-control" data-errormessage-custom-error="Invalid Amount" maxlength="10" name="emp_additional1" id="emp_additional1" value="<?php if(isset($Zadditional1)){ echo $Zadditional1;} ?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-5" for="emp_additional2">Additional 2</label>
                                            <div class="col-xs-7">
                                                <input type="text"  class="validate[required,min[1],custom[number]] form-control" data-errormessage-custom-error="Invalid Amount" maxlength="10" name="emp_additional2" id="emp_additional2" value="<?php if(isset($Zadditional2)){ echo $Zadditional2;} ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="pf_no">PF No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="pf_no" id="pf_no" value="<?php if(isset($employ_pf_no)){ echo $employ_pf_no;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="pf_name">PF Nominee Name</label>
                                        <div class="col-xs-7">
                                            <input type="text" class=" form-control" name="pf_nname" id="pf_nname" value="<?php if(isset($Naminee_name)){ echo $Naminee_name;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="pf_nrel">PF Nominee Relation</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="pf_nrel" id="pf_nrel" value="<?php if(isset($Naminee_relation)){ echo $Naminee_relation;} ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="pf_nadd">PF Nominee Address</label>
                                        <div class="col-xs-7">
                                            <textarea type="text" class="form-control" id="pf_nadd" name="pf_nadd"> <?php if(isset($Naminee_addr)){ echo $Naminee_addr;} ?></textarea>                            
                                        </div>       
                                    </div>                                
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_esino">ESI No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[onlyLetterNumber],maxSize[25]] form-control" maxlength="25"  data-errormessage-custom-error="Invalid ESI number" name="emp_esino" id="emp_esino" value="<?php if(isset($employ_esi_no)){ echo $employ_esi_no;} ?>" />
                                        </div>					
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_pan">PAN</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[onlyLetterNumber],maxSize[25]] form-control" maxlength="25"  data-errormessage-custom-error="Invalid PAN number" name="emp_pan" id="emp_pan" value="<?php if(isset($Pancard_no)){ echo $Pancard_no;} ?>" />
                                        </div>										
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_panname">PAN Name</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="emp_panname" id="emp_panname" value="<?php if(isset($Pancard_name)){ echo $Pancard_name;} ?>" />
                                        </div>										
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_pancard">PAN Card PDF</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="emp_pancard" id="emp_pancard" value="<?php if(isset($Pancard_copy)){ echo $Pancard_copy;} ?>" />
                                            <input type="file" name="pancard" class="validate[funcCall[ftype1]]" id="pancard" />
                                        </div>										
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_pancimage">PAN Card Image</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="emp_pancimage" id="emp_pancimage"  value="<?php if(isset($Pancard_image)){ echo $Pancard_image;} ?>" />
                                            <input type="file" name="pancard_image" class="validate[funcCall[ftype2]]" id="pancardimg" />
                                        </div>										
                                    </div>
                                </div>
								
                                <div class="col-md-5">
                                   
                                    <div class="form-group">
                                         <label class="control-label col-xs-5" for="emp_bacname">Bank Operating Name</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="emp_bacname" id="emp_bacname" value="<?php if(isset($Bank_operating_name)){ echo $Bank_operating_name;} ?>" />
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_bname">Bank Name</label>
                                        <div class="col-xs-7">
                                             <select class="form-control" name="emp_bname" id="emp_bname" data-placeholder="Select Bank">
                                                <option value=""></option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_bbranch">Bank Branch</label>
                                        <div class="col-xs-7">
                                            <input type="text" readonly style=" cursor: text; background-color:#fff;" class="form-control" name="emp_bbranch" id="emp_bbranch" value="<?php if(isset($bbranch)){ echo $bbranch;} ?>" />
<!--                                             <select class="form-control" name="emp_bbranch" id="emp_bbranch" data-placeholder="Select Branch" >
                                                <option value=""></option>
                                                <option value="1" <?php // if(isset($bbranch) &&($bbranch == 1)){ echo "selected";} ?>>Chennai</option>
                                                <option value="2" <?php // if(isset($bbranch) &&($bbranch == 2)){ echo "selected";} ?>>Vellore</option>
                                            </select>-->
                                        </div>                                        
                                    </div>
                                    <div class="form-group">
                                         <label class="control-label col-xs-5" for="emp_bacno">Bank A/C No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[number],maxSize[20]] form-control" maxlength="20" data-errormessage-custom-error="Invalid Bank A/c Number " name="emp_bacno" id="emp_bacno" value="<?php if(isset($employ_bank_ac_no)){ echo $employ_bank_ac_no;} ?>" />
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                         <label class="control-label col-xs-5" for="emp_uan">UAN No</label>
                                        <div class="col-xs-7">
                                            <input type="text" class="validate[custom[onlyLetterNumber],maxSize[20]] form-control" maxlength="20"  data-errormessage-custom-error="Invalid UAN Number " name="emp_uan" id="emp_uan" value="<?php if(isset($employ_uan_no)){ echo $employ_uan_no;} ?>" />
                                        </div>                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-5" for="emp_uan_filename">UAN Image </label>
                                        <div class="col-xs-7">
                                            <input type="text" class="form-control" name="emp_uan_filename" id="emp_uan_filename"  value="<?php if(isset($uan_image)){ echo $uan_image;} ?>" />
                                            <input type="file" name="emp_uanfile" class="validate[funcCall[ftype3]]" id="emp_uanfile" />
                                        </div>										
                                    </div>
                                   <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Employee PF No List</label>-->
                                        <div class="col-xs-5"> 
                                            <a href="employee/pflist?bname=<?php echo $Branch;?>" class="fancybox fancybox.iframe"><strong>Employee PF No List</strong></a>
                                        </div>
                                            <div class="col-xs-7">
                                           </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Employee ESI No List</label>-->
                                        <div class="col-xs-5"><a href="employee/esilist?bname=<?php echo $Branch;?>" class="fancybox fancybox.iframe"><strong>Employee ESI No List</strong></a></div>
                                        <div class="col-xs-7">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Pan card List</label>-->
                                        <div class="col-xs-5"><a href="employee/uanlist?bname=<?php echo $Branch;?>" class="fancybox fancybox.iframe" ><strong>Employee UAN No List</strong></a></div>
                                            <div class="col-xs-7">
                                           </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Bank A/c No List</label>-->
                                        <div class="col-xs-5"><a href="employee/banklist?bname=<?php echo $Branch;?>" class="fancybox fancybox.iframe"><strong>Bank A/c No List</strong></a></div>
                                            <div class="col-xs-7">
                                              
                                           </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Pan card List</label>-->
                                        <div class="col-xs-5"><a href="employee/panlist?bname=<?php echo $Branch;?>" class="fancybox fancybox.iframe"><strong>Pan card List</strong></a></div>
                                            <div class="col-xs-7">
                                           </div>
                                    </div>
                                   <div class="form-group">
                                        <!--<label class="control-label col-xs-5" for="inputSuccess4">Referral By</label>-->
                                        <div class="col-xs-5"><a href="employee/referralby?remp_id=<?php echo $Emp_id;?>&rD_of_join=<?php echo $D_of_join;?>" class="fancybox fancybox.iframe" ><strong>Referred By</strong></a></div>
                                            <div class="col-xs-7">
                                           </div>
                                    </div>
                                </div>	
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-1 col-sm-offset-2">
                                        <input type="button" id="backsubmit" class="btn btn-primary" value="Back">
                                    </div> 
                                       <div class="col-xs-3 col-sm-offset-2">
                                           <input type="submit" id="submit" class="btn btn-primary" value="Modify CTC">
                                        </div>
                                </div>
                            </form>	
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
    <!--Back Button form-->
  <!--emp_modify-->
  <form id="back_btn" method="post" action="employee/modifyemp_personall" >
      <input type="hidden" name="emp_id" id="bemp_id" value="<?php if(isset($Emp_id)){ echo $Emp_id;} ?>" />	
      <input type="hidden" name="backbtn" id="rewind" value="<?php if(isset($backbtn)){ echo $backbtn;} ?>" />
  </form>
  
  <!--back Button form-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
            $("#addempctc").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#addempctc").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#addempctc").bind("jqv.form.result", function(event , errorFound){
                if(errorFound){ 
                    $("#hookError").append("Please fill all required fields");
                    $("#hookError").css('display','block');                        
                }
            });
            
            
            // Bank Details
            var bname = '<?php echo $Branch;?>';
            var toappend = "";
             var bselect ='';
             
             var branch_det = '<?php if(isset($bank_name)){ echo $bank_name; }else{echo '-1';}?>';
             $.ajax({
                    type: "POST",
                    url: "master/bank_list",
                    cache: false,
                    data:"bname="+bname,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#emp_bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 if( value['ba_name'] == branch_det){
                                     bselect = "selected";
                                 }else{
                                     bselect = "";
                                 }                                 
                                toappend+='<option value="'+value['ba_name']+'" data-value="'+value['ba_branch']+'" '+ bselect+' >'+value['ba_name']+'</option>';
                            }); 
                            
                            $('#emp_bname').append(toappend);
                            $("#emp_bname").trigger('chosen:updated');
                        }
                    }            
                }); 
            
            $("#emp_bname").change(function(){
                    var selected = $(this).find('option:selected');
                    var tot_value = selected.data('value'); 
                    $("#emp_bbranch").val(tot_value);
                });
            
            // Bank details
            
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            $("#ctcr").change(function(){
                var ctcr = $("#ctcr").val();
                if(ctcr == 4 || ctcr == '4'){
                    $("#categoryz").css("display","block");
                }else{
                    $("#categoryz").css("display","none");
                }
            });
            $('.fancybox').fancybox({
//                'width'  : 600,           // set the width
//                'height' : 500,
//                'autoSize' : false
            });
            
            
//            back buton

            $("#backsubmit").click(function(){
                $("#addemp_personal").validationEngine('hide');
                $("#back_btn").submit();
            });
            
        });
        function ftype1(field, rules, i, options){
	
            var ext = $("#pancard").val().split('.').pop().toLowerCase();            
            if($.inArray(ext, ['pdf']) === -1) {
                return "* Invalid file type";
            }
            if($("#pancard")[0].files[0].size>307200)
            {
                return "* File limit exceeded";
	    }
        }   
        function ftype2(field, rules, i, options){
	
            var ext = $("#pancardimg").val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg']) === -1) {
                return "* Invalid file type";
            }
            if($("#pancardimg")[0].files[0].size>307200)
            {
                return "* File limit exceeded";
	    }
        }
        function ftype3(field, rules, i, options){
	
            var ext = $("#emp_uanfile").val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg']) === -1) {
                return "* Invalid file type";
            }
            if($("#emp_uanfile")[0].files[0].size>307200)
            {
                return "* File limit exceeded";
	    }
        }
        </script>
        <?php $this->load->view('includes/additional.php');?>
        <script type="text/javascript" src="assets/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    </body>
</html>