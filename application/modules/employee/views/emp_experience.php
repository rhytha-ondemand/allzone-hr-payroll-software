<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .label_colen:before{
                content: " : ";
                margin-left: 5px;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Details</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employee other details </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form class="form-horizontal" id="emp_modify" role="form" method="post">
                                                               
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Total years of previous experience</label>
                                    <div class="col-xs-4">
                                        <label class="control-label label_colen" style="font-weight: normal;" ><?php echo $total_year.' years,  '.$total_month.' months';?></label>
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Experience in MB</label>
                                    <div class="col-xs-4">
                                        <label class="control-label label_colen" style="font-weight: normal;" ><?php echo $mb_year.' years,  '.$mb_month.' months';?> </label>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable responsive">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Company name</th>
                                            <th>Address</th>
                                            <th>Designation</th>
                                            <th>CTC</th>
                                            <th>Net</th>
                                            <th>Period of service</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=1;
                                        foreach($records as $row){
                                            echo"<tr>";
                                            echo "<td>". $i++ ."</td>";
                                            echo "<td>".$row['prev_cname']."</td>";
                                            echo "<td>".$row['prev_caddr']."</td>";
                                            echo "<td>".$row['prev_cdesig']."</td>";
                                            echo "<td>".$row['prev_ctc']."</td>";
                                            echo "<td>".$row['prev_net']."</td>";
                                            echo "<td>".$row['prev_service']."</td>";
                                            echo"</tr>";
                                        }                                            
                                       ?>                                     
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Year at Allzone</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;" >10years, 3 months and 1 days </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Designation at the time of joining</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Sr. Network Engineer</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Previous Designation</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Manager - Network engineer</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Present Designation</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Senior Manager</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">CTC at the time of joining</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Rs. 14000/- </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Previous CTC</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Rs. 95000/-</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Present CTC</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">Rs. 100000/- </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Re-Fixation</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">No</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">No.of Appraisal</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class=" form-control" name="" id="">-->
                                        <label class="control-label label_colen" style="font-weight: normal;">9</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                        <input type="submit" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                $('select').chosen();
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>