<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #bname_chosen,#emp_id_chosen,#cadre_chosen{
                width: 100%!important;
            }
             .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
/*            .btn-group{
                display: none;
            }*/
        </style>
      <?php // $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php // $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-12">
            <!-- content starts -->
<!--            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Referral details</a>
                    </li>
                </ul>
            </div>-->
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Referral details </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="mreferral" method="post" action="" role="form">                                
                                <input type="hidden" name="remp_id" value="<?php echo $remp_id; ?>" />
                                <input type="hidden" name="rD_of_join" id="rD_of_join" value="<?php echo $rD_of_join; ?>" />
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="emp_id">Employee Id</label><span ></span>
                                    <div class="col-xs-4">
                                        <select name="emp_id" id="emp_id" data-placeholder="Select Employee ID" class="validate[required] form-control" >
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
<!--
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="emp_name">Employee name</label><span ></span>
                                    <div class="col-xs-4">
                                        <input type="text" id="emp_name" name="emp_name" class="validate[required] form-control"/>                                     
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="b_rel">Blood relation</label><span ></span>
                                    <div class="col-xs-4" id="radioDiv">
                                        <!--<div class="radio">-->
                                            <label class="radio-inline">
                                                <input type="radio" name="b_rel" id="b_rel" value="1"> Yes                                                
                                            </label>
<!--                                        </div>
                                        <div class="radio">-->
                                            <label class="radio-inline">
                                                <input type="radio" name="b_rel" id="b_rel2" value="0" checked> No                                                
                                            </label>
                                        <!--</div>-->    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="cadre">Cadre</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="cadre" name="cadre" data-placeholder="Select Cadre" class="validate[required] form-control" >
                                            <option value=""></option>
<!--                                            <option value="1" data-amt="500" data-period="1">Fresher</option>
                                            <option value="2" data-amt="10000" data-period="2" >Experienced AR</option>   
                                            <option value="3" data-amt="5000" data-period="2" >Experienced Data</option>
                                            <option value="4" data-amt="5000" data-period="1" >Experienced Coder</option>                                    -->
                                        </select>
                                    </div>
                                </div>
                                <div id="cadre1">
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="amt">Amount</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="amt" name="amt" class="validate[required] form-control"/>                                     
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="date">Date</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="date" readonly="" name="date" class="validate[required] form-control datepicker"/>                                     
                                        </div>
                                    </div>
                                </div>
                                <div id="cadre2" style="display: none">
<!--                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="amt1">Amount 1</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="amt1" name="amt1" class="validate[required] form-control"/>                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="date1">Date 1</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="date1" name="date1" class="validate[required] form-control datepicker"/>                                     
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="amt2">Amount 2</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="amt2" name="amt2" class="validate[required] form-control"/>                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-3"  for="date2">Date 2</label><span ></span>
                                        <div class="col-xs-4">
                                            <input type="text" id="date2" readonly="" name="date2" class="validate[required] form-control datepicker"/>                                     
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-4">
                                        <div class="col-xs-2">
                                            <input type="submit" id="submit" class="btn btn-primary fancybox-close" value="Submit">
                                        </div>
                                    </div>                                   
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php // $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){  
                
                $.validationEngine.defaults.scroll = false;
                $("#mreferral").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"                
                });
            
                $("select").chosen({disable_search_threshold: 10});    
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
//                $(".datepicker").datepicker({ dateFormat: 'dd-M-yy',minDate: 0});
                $("#date").datepicker({ dateFormat: 'dd-M-yy',minDate: '<?php echo date('d-M-Y', strtotime($rD_of_join));?>',changeMonth: true,changeYear: true,
                    onClose: function( selectedDate ) {
                        $( "#date2" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#date2").datepicker({ dateFormat: 'dd-M-yy',minDate: 0,changeMonth: true,changeYear: true});
// branch name
                var toappend = '';                
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';                             
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name                
            $("#bname").change(function(){
                    var toappend; 
                    var empid = '<?php echo $remp_id; ?>';
                    var cadre_value = {'1':'Fresher','2':'Experienced AR','3':'Experienced Data','4':'Experienced Coder'};
                var url = "master/emp_id_list_act";
                var data =  "bname=" + $(this).val();   
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#emp_id').find('option').remove();
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 if(empid != value['Id']){
                                   toappend+='<option value='+value['Id']+'>'+value['emp_name']+'</option>';   
                                 }
                            });   
                            $('#emp_id').append(toappend);                            
                        }
//                        else{
//                            alert('error');
//                        }
                        $("#emp_id").trigger('chosen:updated');
                    }            
                }); 
                
                $.ajax({
                    type: "POST",
                    url: 'master/ref_incentive_list',
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#cadre').find('option').remove();
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['cadre']+' data-amt="'+value['Amount']+'" data-period="'+value['period']+'" >'+cadre_value[value['cadre']]+'</option>';                               
                            });   
                            $('#cadre').append(toappend);                            
                        }
//                        else{
//                            alert('error');
//                        }
                        $("#cadre").trigger('chosen:updated');
                    }            
                }); 
                
                });
                
                $('#radioDiv input').on('change', function() {
                    if($("#cadre").val() != ''){
                        reffer();
                    }
                });
                
                $("#cadre").change(function(){
                    reffer();
                });  
                
                function reffer(){
                    var cadre = $("#cadre").val();
                    var branch = $("#bname").val();
//                    var tot_amount = 5000;
                    var selected = $("#cadre").find('option:selected');
                    var tot_amount = selected.data('amt'); 
                    var amt_period = selected.data('period');
                   if ($("#radioDiv input[type='radio']:checked").val() == 1) {
                        $("#cadre2").css('display','none');
                        $("#amt").val(tot_amount);
                        $("#date").val("<?php echo date('d-M-Y', strtotime('+12 months', strtotime($rD_of_join)));?>");    
                        $("#amt2").val(''); 
                        $("#date2").val(''); 
                   }else{                    
                        if(amt_period === 1 || amt_period === "1"){                        
                            $("#cadre2").css('display','none');
    //                        $("#cadre1").css('display','block');
                            $("#amt").val(tot_amount);
                            $("#date").val("<?php echo date('d-M-Y', strtotime('+6 months', strtotime($rD_of_join)));?>");
                            $("#amt2").val(''); 
                            $("#date2").val(''); 
                        }else{
    //                        $("#cadre1").css('display','none');
                            var doj = $("#rD_of_join").val();
                            $("#cadre2").css('display','block');
                            $("#amt").val(tot_amount*(40/100));
                            $("#amt2").val(tot_amount*(60/100)); 
                            $("#date").val("<?php echo date('d-M-Y', strtotime('+2 months', strtotime($rD_of_join)));?>");
                            $("#date2").val("<?php echo date('d-M-Y', strtotime('+6 months', strtotime($rD_of_join)));?>");
                        }    
                    }
                }
                
                $("#submit").click(function(){
//                     $('.fancybox-close').trigger('click');                     
                    if ( $("#mreferral").validationEngine('validate') ) {
                        $.ajax({
                            type: "POST",
                            url: 'employee/refferal_add',
                            cache: false,
                            dataType: "json",
                            data: $("#mreferral").serialize(),
                            async: false,
                            success: function(json) { 
                                if(json){
                                    $("#mreferral").trigger("reset");
                                    $("select").trigger('chosen:updated');
                                    parent.$.fancybox.close();
                                }
                                return true;
                            }
                        });
                    }
                    return false;
                });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>
