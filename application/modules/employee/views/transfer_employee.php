<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
	<body>
        <style>
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Transfer Employee</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Employee Transfer </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                                
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="emp_tranfer" action="" role="form">
                                
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Employee No</label>
                                    <div class="col-xs-4">
                                        <select class="validate[required] form-control" data-placeholder="Select Employee number" id="emp_id" name="emp_id" >                                           
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname_transfer">Select transfer branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname_transfer" name="bname_transfer" data-placeholder="Select branch" class="validate[required,funcCall[notEqual]] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Transfer Date</label>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="Select Date" name="transfer_date" readonly class="validate[required]  form-control datepicker" id="datepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>

                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Employee transfer to new location successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update employee tranfer details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $( "#datepicker" ).datepicker({ dateFormat: 'dd-M-yy',minDate:0,changeMonth: true,changeYear: true,yearRange:"0:+1" });
             $("select").chosen({disable_search_threshold: 10});
            $.validationEngine.defaults.scroll = false;
            $("#emp_tranfer").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            
            $("#emp_tranfer").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            
            $("#emp_tranfer").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
            });
            
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            //                   Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        $('#bname_transfer').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $('#bname_transfer').append(toappend);
                            $("#bname").trigger('chosen:updated');
                            $("#bname_transfer").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det
            $("#submit").click(function(){
                if ( $("#emp_tranfer").validationEngine('validate') ) {                
                   var data = "bname=" + $("#bname").val()+"&emp_id="+$("#emp_id").val()+"&bname_transfer=" + $("#bname_transfer").val()+"&transfer_date="+$("#datepicker").val();  
                    $.ajax({
                        type: "POST",
                        url: "employee/emp_transfer",
                        data: data,
                        success: function (result) {
                            $("#emp_tranfer").trigger("reset");
                            $("select").trigger('chosen:updated');                            
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to transfer employee","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){                                
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                             
                        }
                    }); 
                    
                }
                return false;
            });
            
            $("#bname").change(function(){
                var toappend;              
                var url = "master/emp_id_list";
                var data =  "bname=" + $(this).val();   
//                $("#bname_transfer option[value='"+$(this).val()+"']").remove();
//                $("#bname_transfer").trigger('chosen:updated');
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#emp_id').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['emp_name']+'</option>';                                
                            });   
                            $('#emp_id').append(toappend);                            
                        }
//                        else{
//                            alert('error');
//                        }
                        $("#emp_id").trigger('chosen:updated');
                    }            
                });  
               
          });
          });
            function notEqual(field){
                if(field.val() == $('#bname').val()){
                    return "* Current branch and transfer branch must be different";
                }
            }
              
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>