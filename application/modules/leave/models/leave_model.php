<?php
class Leave_model extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
		$this->load->library('leave_lib',NULL,'lev');
		
    }
	function pl_details()
	{
		
		$x=$this->lev->pl_details();
		return $x;
		
	}
	function save_emp_leave()
	{
		$this->lev->save_emp_leave();
		
	}
	function leave_all()
	{
		$x=$this->lev->leave_all();
		return $x;
				
	}
	 function leave_c(){
		
		$x=$this->lev->leave_c();
		return $x;
	 }
	 function leave_card($emp_id){
		 $x=$this->lev->leave_card($emp_id);
		return $x;
	 }
	 function cofff()
	{
		$x=$this->lev->cofff();
		return $x;
	}
	  public function coff_overall(){
		 $x=$this->lev->coff_overall();
		 return $x;
	  }
          
          function coff_empdet(){
               $x=$this->lev->coff_empdet();
                return $x;
          }
          
            function coff_update(){
                $result = $this->lev->coff_update();
                return $result;
            }

            function coff_delete(){
                $result = $this->lev->coff_delete();
                return $result;
            }
            function credit_overall(){
                $x = $this->lev->credit_overall();
                return $x;
            }
            function credit_empdet(){
                $x= $this->lev->credit_empdet();
                return $x;
            }
            function credit_update(){
                $result = $this->lev->credit_update();
                return $result;
            }

            function credit_delete(){
                $result = $this->lev->credit_delete();
                return $result;
            }
          
          
	  public function monthlydet(){
		 $x=$this->lev->monthlydet();
		 return $x;
	  }
          public function leavecard_update(){
            $x=$this->lev->leavecard_update();
            return $x;
          }
          public function leavecard_u(){
            $x=$this->lev->leavecard_u();
            return $x;
          }
          public function leavecard_delete(){
              $x = $this->lev->leavecard_delete();
              return $x;
          }
          
    function sda_all(){
        $result = $this->lev->sda_all();
        return $result;
    }
    function sdansa_add(){
        $result = $this->lev->sdansa_add();
        return $result;
    }
    
    function sda_update(){
        $result = $this->lev->sda_update();
        return $result;
    }
    
    function sda_delete(){
        $result = $this->lev->sda_delete();
        return $result;
    }
     function nsa_all(){
        $result = $this->lev->nsa_all();
        return $result;
    }
    function nsa_emp_all(){
        $result = $this->lev->nsa_emp_all();
        return $result;
    }
    function nsa_update(){
        $result = $this->lev->nsa_update();
        return $result;
    }
    
    function nsa_delete(){
        $result = $this->lev->nsa_delete();
        return $result;
    }
    function ha_all(){
        $result = $this->lev->ha_all();
        return $result;
    }  
    function ha_update(){
        $result = $this->lev->ha_update();
        return $result;
    }
    
    function ha_delete(){
        $result = $this->lev->ha_delete();
        return $result;
    }
    function appraisal_consolidatedet(){
        $result = $this->lev->appraisal_consolidatedet();
        return $result;
    }
    
    function status_teamwisedetails(){
        $result = $this->lev->status_teamwisedetails();
        return $result;
    }
    function appraisal_individualdet(){
        $result = $this->lev->appraisal_individualdet();
        return $result;
    }
    function coffcr_detail(){
        $result = $this->lev->coffcr_detail();
        return $result;
    }
}