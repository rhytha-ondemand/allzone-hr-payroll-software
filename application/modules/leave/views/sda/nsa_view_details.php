<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
            <style>
        .dataTables_filter{
            display: none;
        }
    </style>  
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Night Shift Allowance</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Night Shift Allowance  From <?php echo $from_date." To ".$to_date;?> </h2> 
                            <div class="box-icon">
                              <!--   <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> -->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                              <!--   <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <br>
                            <table id="nsa_details" class="table table-striped table-bordered responsive">
                            <thead>
                            <tr>
                                    <!--<th>S.No</th>-->
                                    <th>Emp.No</th>
                                    <th>Emp.Name</th>
                                    <th>Designation</th>
                                    <th>Team</th>
                                    <th>No.of Days</th>
                              <!--      <th>Amount in Rs.</th> -->
                                    <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            </table>
                            <div class="row">
                                 <form class="form-horizontal" role="form" action="leave/nsa_completedetail" >
                                    <div class="form-group">
                                        <div class="col-xs-4 col-sm-offset-4">
                                            <!--<input type="submit" class="btn btn-primary" value="Export >>">-->
                                        </div>
                                    </div>
                                </form>
                                
                            </div>

                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<div style="display:none;">
    <form id="editform" method="post" action="leave/nsa_empdetails">
        <input type="hidden" id="emp_id" name="emp_id"/>
        <input type="hidden" id="nsa_id" name="nsa_id"/>
        <input type="hidden" id="emp_name" name="emp_name"/>
        <input type="hidden" id="from_date" name="from_date" value="<?php echo $from_date;?>"/>
        <input type="hidden" id="to_date" name="to_date" value="<?php echo $to_date;?>"/>
    </form>
</div>
        <hr>
        <?php $this->load->view('includes/footer.php');?>
    </div>
         <script>
        $(document).ready(function(){
            var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>View</a>";
            var data = "from_date=<?php echo $from_date;?>"+"&to_date=<?php echo $to_date;?>"+"&bname=<?php echo $bname;?>";
                var src = "leave/nsa_all?"+data;
                var oTable =  $('#nsa_details').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Emp_id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Emp_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "design", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Team", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "total_days", "sWidth": "125px", "bSortable": true },
//                        { "mDataProp": "Amount", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#nsa_details > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
//
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#nsa_details > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });	
					
                }
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
//                       $(nRow).find("td").eq(4).html(location[aData['bname']]);                  
//                }
            });
            function edit(){
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#nsa_id").val($(this ).closest('tr').attr('id'));
                        if(j===0){
                          $("#emp_id").val($(this).text());
                        }
                        if(j===1){
                          $("#emp_name").val($(this).text());
                        } 
                    });                        
                });
                $("#editform").submit();
                return false;
            }
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>