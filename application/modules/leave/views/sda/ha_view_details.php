<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">HA</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Holiday Allowance Details From <?php echo $from_date." To ".$to_date;?> </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/sda_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Allowance </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
		<br>
                            <br>
                            <form class="form-horizontal" role="form" action="leave/leave_card">
                                <table id="ha_details" class="table table-striped table-bordered responsive">
                                <thead>                                
                                <tr>
                                    <th>Emp. No</th>
                                    <th>Emp. Name</th>
                                    <th>Designation</th>
                                    <th>HA Date</th>
                                    <th>Action</th>                                    
                                </tr>                                
                                </table>
                                <div class="form-group">
                                    <div class="col-xs-4 col-sm-offset-4">
                                        <!--<input type="submit" class="btn btn-primary" value="Export To Excel >>">-->
                                    </div>									
                                </div>
                            </form>	
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Holiday Allowance details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while delete this record &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	 
<!--Edit dialog box-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Holiday Allowance</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="ha_id" id="ha_id">
                <input type="hidden" name="emp_id" id="emp_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="credit_adate">HA Worked date</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" readonly="" class="validate[required] form-control datepicker " id="ha_date" name="ha_date">
                        </div>
                    </div>
                    <br><br>

                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>
<!--Edit dialog box-->
<!--Delete dialog box start-->

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Delete</h3>
                </div>
                 <input type="hidden" name="ha_id" id="dha_id">
                <input type="hidden" name="emp_id" id="demp_id">
                <div class="modal-body">
                    <p>Are you want you delete this record ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" id="del_confirm" class="btn btn-primary" data-dismiss="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
<!--Delete dialog box end-->
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
//            var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a><a class='btn btn-danger' style='margin-left:5px;' href='#'><i class='glyphicon glyphicon-trash icon-white'></i>Delete</a>";
var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
            var data = "from_date=<?php echo $from_date;?>"+"&to_date=<?php echo $to_date;?>"+"&bname=<?php echo $bname;?>";
                var src = "leave/ha_all?"+data;    
            var oTable =  $('#ha_details').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Emp_id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Emp_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "design", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Hol_date", "sWidth": "125px", "bSortable": true },
//                        { "mDataProp": "bname", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#ha_details > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                $(this ).find('a:nth-child(2)').on('click',deleter);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#ha_details > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                        $(this ).find('a:nth-child(2)').on('click',deleter);
                    });	
					
                }
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
//                       $(nRow).find("td").eq(4).html(location[aData['bname']]);                  
//                }
            });
           function edit(){
                var myModal = $('#edit-modal');
                $("#formsave").validationEngine('hide');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#ha_id").val($(this ).closest('tr').attr('id'));
                        if(j===0){
                          $("#emp_id").val($(this).text());
                        }
                        if(j===3){
                          $("#ha_date").val($(this).text());
                        }
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
            
            function deleter(){
                 var myModal = $('#delete_modal');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#dha_id").val($(this ).closest('tr').attr('id'));
                        if(j===0){
                          $("#demp_id").val($(this).text());
                        }                        
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
             $(".datepicker").datepicker({dateFormat:"dd-M-yy",minDate:-10,maxDate:30});
             $("#del_confirm").click(function(){
                 $.ajax({
                     type:"POST",
                     url:"leave/ha_delete",
                     cache:false,
                     data:"Id="+$("#dha_id").val()+"&emp_id="+$("#demp_id").val(),
//                     dataType:"json",
                     async:false,
                     success:function(result){
                         $('#delete_modal').modal('hide');                         
                         if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                        }else if(result === 2 || result === "2"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                            $(".notyerror").click();
                        }else if(result === 3 || result === "3"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Sorry, Invalid Holiday Allowance detail","layout":"topCenter","type":"error"}');
                            $(".notyerror").click();
                        }else if(result === 4 || result === "4"){ 
                            $(".notyerror").attr("data-noty-options",'{"text":"Holiday Allowance details deleted successfully","layout":"topCenter","type":"success"}');
                            $(".notyerror").click();
                            oTable.fnDraw();
                            oTable.fnReloadAjax();
                        }else{
                            window.location.href = "master/login";
                        } 
                     }
                 });
             });
            $.validationEngine.defaults.scroll = false;
            $("#formsave").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
             $("#submit").click(function(){
                if($("#formsave").validationEngine('validate')){ 
                    $.ajax({
                        type: "POST",
                        url: "leave/ha_update",
                        data:"Id="+$("#ha_id").val()+"&emp_id="+$("#emp_id").val()+"&ha_date="+$("#ha_date").val(),
                        async:false,
                        success: function (result) {
                            $('#edit-modal').modal('hide');
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){ 
                                oTable.fnDraw();
                                oTable.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                           
                        }
                    }); 
                }
                return false;                    
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>