<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        height:37px;
        padding:1px!important;
    }
    .default{
        height: 25px!important;
        padding-top: 2px!important
    }
    #emp_id_chosen{
        /*height:37px;*/
        max-width:200%!important;
        display:inline-block;
    }
    .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>        
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Allowance </h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                         <br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="lsdansa" role="form" method="post" action="">
                                <br>                                
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="inputSuccess4">Allowance Type</label>
                                    <div class="col-xs-3 radio" id="radioDiv">
                                            <label><input tabindex="1" type=radio checked value="1"  name='type' id='type1'> SDA </label>
                                            <label style="margin-left: 3%" ><input type=radio tabindex="1" value="2"  name='type' id='type2'> NSA </label>
                                            <label style="margin-left: 3%" ><input type=radio tabindex="1" value="3"  name='type' id='type3'> HA </label>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="team">Select Team</label>
                                    <div class="col-xs-4">
                                        <select id="team" name="team" class="validate[required] form-control" data-rel="chosen" data-placeholder="Select Team">
                                            <option value=""></option>                                           
                                        </select> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_id">Employee ID</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class="validate[required]  form-control" id="inputSuccess4">-->
                                        <select id="emp_id" name="emp_id[]" multiple data-placeholder="Select Employee" class="validate[required] form-control" >
                                            <option value=""></option>
                                            
                                        </select>
                                        Note: Please select only 25 members.
                                    </div>
                                </div>                                
                                
                                <div id="sdadate" class="form-group">
                                    <label class="control-label col-xs-2" for="sda_date">SDA Date</label>
                                    <div class="col-xs-4">
                                        <input type="text" readonly class="validate[required]  form-control datepicker" id="sda_date" name="sda_date">
                                        
                                    </div>
                                </div>
                                <div id="nsadate" style="display:none;">
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="nsa_fdate">NSA From Date</label>
                                        <div class="col-xs-4">
                                            <input type="text" readonly class="validate[required]  form-control datepicker" id="nsa_fdate" name="nsa_fdate">

                                        </div>
                                    </div>
                                    <div  class="form-group">
                                        <label class="control-label col-xs-2" for="nsa_tdate">NSA To Date</label>
                                        <div class="col-xs-4">
                                            <input type="text" readonly class="validate[required] form-control datepicker" name="nsa_tdate" id="nsa_tdate">

                                        </div>
                                    </div>
                                </div>
                                <div id="hadate" style="display:none;">
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="ha_date">HA Date</label>
                                        <div class="col-xs-4">
                                            <input type="text" readonly class="validate[required]  form-control datepicker" id="ha_date" name="ha_date"/>

                                        </div>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <input type="submit"  id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Allowance details added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert leave details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
  
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
<script>
    $(document).ready(function(){
        
        $.validationEngine.defaults.scroll = false;
        $("#lsdansa").validationEngine({
            prettySelect:true,
            useSuffix: "_chosen", 
            maxErrorsPerField: 1,
            promptPosition : "inline"
        });
        $("#lsdansa").bind("jqv.form.validating", function(event){
            $("#hookError").css('display','none');
            $("#hookError").html("");
        });
        $("#lsdansa").bind("jqv.form.result", function(event , errorFound){
            if(errorFound){ 
                $("#hookError").append("Please fill all required fields");
                $("#hookError").css('display','block');
            }
        });            
        $( "select" ).change(function() {
            var err = '.'+this.id+'_chosenformError';
            $(err).remove();
        });      

        $("#sda_date").datepicker({minDate:-10,maxDate:10,dateFormat:"dd-M-yy"});
        $("#ha_date").datepicker({minDate:-10,maxDate:10,dateFormat:"dd-M-yy"});
        $( "#nsa_fdate" ).datepicker({
                dateFormat: 'dd-M-yy',minDate:-10,maxDate:10,
                onClose: function( selectedDate ) {
                $( "#nsa_tdate" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#nsa_tdate" ).datepicker({ dateFormat: 'dd-M-yy',minDate:-10,maxDate:30});//,changeMonth: true,changeYear: true });
        $("#bname").chosen({disable_search_threshold: 10});
        $("#team").chosen({disable_search_threshold: 10});
        $("#emp_id").chosen({max_selected_options : 25});
        
        $("#emp_id").change(function(){
                if($("#emp_id_chosen .chosen-choices li").size() === 3){
                    $("#emp_id_chosen").css('width','200%');
                }
                 if($("#emp_id_chosen .chosen-choices li").size() === 2){  
                    $("#emp_id_chosen").css('width','100%');
                }
            });
         $('#radioDiv input').on('change', function() {
            var teamm = $("#team").val();
            if(teamm != ''){
				 $("#emp_id").find('option').remove();
                 $("#emp_id_chosen").css('width','100%');
				 $("#emp_id").trigger("chosen:updated");
                team_change();
            }
            if ($("#radioDiv input[type='radio']:checked").val() == 1) {                
                $("#sdadate").css('display','block');
                $("#nsadate").css('display','none');
                $("#hadate").css('display','none');
            }else if ($("#radioDiv input[type='radio']:checked").val() == 2) {
                $("#sdadate").css('display','none');
                $("#nsadate").css('display','block');
                $("#hadate").css('display','none');                
            }else{                                        
                $("#sdadate").css('display','none');
                $("#nsadate").css('display','none');
                $("#hadate").css('display','block');                
            }                  
        });
        
        //           Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det
               $("#bname").change(function(){
                   $.ajax({
                       type: "POST",
                       url: "master/team_list",
                       cache: false,
                       data: "bname="+$("#bname").val(),
                       dataType: "json",
                       async: false,
                       success: function(json){
                           if(json){
                               $("#team").find('option').remove();
                               var teamlist = '<option value=""></option>';
                               $.each(json,function(i, value){
                                   teamlist+= '<option value='+value['reporting_no']+'>'+value['team']+'</option>';
                               });
                               $("#team").append(teamlist);
                               $("#team").trigger('chosen:updated');
                           }
                       }
                   });                   
               });
               var emp_team ='<option value=""></option>';
             
               $("#team").change(function(){
                  team_change();
               });
               function team_change(){
                    var team = $("#team").val();
                    var atype = $("#radioDiv input[type='radio']:checked").val();
                    var teamname = $("#team option:selected").text();
                    $.ajax({
                        type: "POST",
                        url: "master/emp_team",
                        cache: false,
                        data:"bname="+$("#bname").val()+"&team="+team+"&type="+atype,
                        dataType: "json",
                        async: false,
                        success:function(json){
                             $("#emp_id").find('option:not(:selected)').remove();   
                             if(json){
                                emp_team='';                                                        
                                 var opts =[];
                                $('#emp_id option').each(function() {
                                     opts.push($(this).val());
                                 });     
 //                                console.log(opts);
                                $.each(json,function(i,value){
                                     if($.inArray(value['Id'], opts) == -1){
                                         emp_team+= '<option value='+value['Id']+' >'+value['emp_name']+'</option>';
                                     }
                                });
                                $("#emp_id").append(emp_team);                               
                            }
                            $("#emp_id").trigger("chosen:updated");
                        }
                    });
               }
               $("#submit").click(function(){
                   if ( $("#lsdansa").validationEngine('validate'))  {
                       $.ajax({
                           type:"POST",
                           url:"leave/sdansa_add",                           
                           cache:false,
                           data:$("#lsdansa").serialize(),
//                           dataType:"json",
                           async:false,
                           success:function(result){
//                                $("#lcoffcr").trigger('reset');  
                                $('#bname').prop('selectedIndex',0);
                                $("#team").find('option').remove();
                                $("#emp_id").find('option').remove();
                                $("select").trigger('chosen:updated');  
                                $("#sda_date").val('');
                                $("#nsa_fdate").val('');
                                $("#nsa_tdate").val('');
                                $("#emp_id_chosen").css('width','100%');
                                $("#type1").trigger('click');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){                                
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }
                           }
                           
                       });
                   }
                   return false;
               });
    });
</script>          
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>