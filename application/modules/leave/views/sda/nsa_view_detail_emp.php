<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
            <style>
        .dataTables_filter{
            display: none;
        }
        .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
    </style>  
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Night Shift Allowance</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Night Shift Allowance  From <?php echo $from_date." To ".$to_date;?> </h2> 
                            <div class="box-icon">
                              <!--   <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> -->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                              <!--   <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/sda_add';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Allowance </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <br>
                            <div class="form-group row" style="margin-left:15%;color:#317eac;">
                                    <label class="control-label col-xs-2" for="inputSuccess4">Emp. No</label>
                                    <label class="control-label col-xs-10" for="inputSuccess4"><?php echo $emp_id; ?></label><br>
                                    
                                    <label class="control-label col-xs-2" for="inputSuccess4">Emp. Name </label>
                                    <label class="control-label col-xs-5" for="inputSuccess4"><?php echo $emp_name; ?></label>
                            </div>
                            <table id="nsa_details" class="table table-striped table-bordered responsive">
                            <thead>
                            <tr>
                                    <!--<th>S.No</th>-->
                                    <th>Date</th>
                                    <th>From date</th>
                                    <th>To date</th>
                                    <th>No.of Days</th>
                              <!--      <th>Amount in Rs.</th> -->
                                    <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            </table>
                            <div class="row">
                                 <form class="form-horizontal" role="form" action="leave/nsa_completedetail" >
                                    <div class="form-group">
                                        <div class="col-xs-4 col-sm-offset-4">
                                            <!--<input type="submit" class="btn btn-primary" value="Export >>">-->
                                        </div>
                                    </div>
                                </form>
                                
                            </div>

                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Night Shift Allowance details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while delete this record &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	 
<!--Edit dialog box-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Night Shift Allowance</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="nsa_id" id="nsa_id">
                <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id;?>">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="credit_adate">From date</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" readonly="" class="validate[required] form-control datepicker " id="nsa_fdate" name="nsa_fdate">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="credit_adate">To date</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" readonly="" class="validate[required] form-control datepicker " id="nsa_tdate" name="nsa_tdate">
                        </div>
                    </div>
                    <br><br>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>
<!--Edit dialog box-->
<!--Delete dialog box start-->

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Delete</h3>
                </div>
                 <input type="hidden" name="nsa_id" id="dnsa_id">
                 <input type="hidden" name="emp_id" id="demp_id" value="<?php echo $emp_id;?>">
                <div class="modal-body">
                    <p>Are you want you delete this record ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" id="del_confirm" class="btn btn-primary" data-dismiss="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
<!--Delete dialog box end-->
        <hr>
        <?php $this->load->view('includes/footer.php');?>
    </div>
         <script>
        $(document).ready(function(){
//            var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a><a class='btn btn-danger' style='margin-left:5px;' href='#'><i class='glyphicon glyphicon-trash icon-white'></i>Delete</a>";
var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
            var data = "emp_id=<?php echo $emp_id;?>"+"&from_date=<?php echo $from_date;?>"+"&to_date=<?php echo $to_date;?>";
                var src = "leave/nsa_emp_all?"+data;
                var oTable =  $('#nsa_details').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Entry_date", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Nsa_date", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Nsa_todate", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "No_of_days", "sWidth": "125px", "bSortable": true },
//                        { "mDataProp": "Amount", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#nsa_details > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                $(this ).find('a:nth-child(2)').on('click',deleter);                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#nsa_details > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                        $(this ).find('a:nth-child(2)').on('click',deleter);
                    });	
					
                }
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
//                       $(nRow).find("td").eq(4).html(location[aData['bname']]);                  
//                }
            });
            function edit(){
                var myModal = $('#edit-modal');
                $("#formsave").validationEngine('hide');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#nsa_id").val($(this ).closest('tr').attr('id'));
                        if(j===1){
                          $("#nsa_fdate").val($(this).text());
                        }
                        if(j===2){
                            var selected_date = $("#nsa_fdate").val();
                            $( "#nsa_tdate" ).datepicker( "option", "minDate", selected_date );
                          $("#nsa_tdate").val($(this).text());
                        }
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
            
            function deleter(){
                 var myModal = $('#delete_modal');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#dnsa_id").val($(this ).closest('tr').attr('id'));                                               
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
             $("#nsa_fdate").datepicker({dateFormat:"dd-M-yy",minDate:-10,maxDate:30,
             onClose: function( selectedDate ) {
                $( "#nsa_tdate" ).datepicker( "option", "minDate", selectedDate );
            }});
             $("#nsa_tdate").datepicker({dateFormat:"dd-M-yy",minDate:-10,maxDate:30});
             
             $("#del_confirm").click(function(){
                 $.ajax({
                     type:"POST",
                     url:"leave/nsa_delete",
                     cache:false,
                     data:"Id="+$("#dnsa_id").val()+"&emp_id="+$("#demp_id").val(),
//                     dataType:"json",
                     async:false,
                     success:function(result){
                         $('#delete_modal').modal('hide');
                         if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                        }else if(result === 2 || result === "2"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                            $(".notyerror").click();
                        }else if(result === 3 || result === "3"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Sorry, Invalid Night Shift Allowance  detail","layout":"topCenter","type":"error"}');
                            $(".notyerror").click();
                        }else if(result === 4 || result === "4"){ 
                            $(".notyerror").attr("data-noty-options",'{"text":"Night Shift Allowance details deleted successfully","layout":"topCenter","type":"success"}');
                            $(".notyerror").click();
                            oTable.fnDraw();
                            oTable.fnReloadAjax();
                        }else{
                            window.location.href = "master/login";
                        } 
                     }
                 });
             });
            $.validationEngine.defaults.scroll = false;
            $("#formsave").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
             $("#submit").click(function(){
                 if($("#formsave").validationEngine('validate')){ 
                    $.ajax({
                        type: "POST",
                        url: "leave/nsa_update",
                        data:"Id="+$("#nsa_id").val()+"&emp_id="+$("#emp_id").val()+"&nsa_fdate="+$("#nsa_fdate").val()+"&nsa_tdate="+$("#nsa_tdate").val(),
                        async:false,
                        success: function (result) {
                            $('#edit-modal').modal('hide');
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){ 
                                oTable.fnDraw();
                                oTable.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                           
                        }
                    }); 
                }
                    return false;
                });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>