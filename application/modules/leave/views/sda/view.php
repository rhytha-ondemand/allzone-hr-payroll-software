<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> View Allowance </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/sda_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Allowance </a>
                        </div>
						<br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="sdansaview" method="post" role="form" action="leave/allowance_details">
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="inputSuccess4">Allowance Type</label>
                                    <div class="col-xs-4 radio" id="radioDiv">
                                            <label><input tabindex="1" type=radio checked value="1"  name='type' id='type1'> SDA </label>
                                            <label style="margin-left: 3%" ><input type=radio tabindex="1" value="2"  name='type' id='type2'> NSA </label>
                                            <label style="margin-left: 3%" ><input type=radio tabindex="1" value="3"  name='type' id='type3'> HA </label>
                                        </div>
                                </div>
                                 <div class="form-group">
                                      <label class="control-label col-xs-2" for="from_date">From Date</label>
                                        <div class="col-xs-4">
                                            <input type="text" readonly class="validate[required] form-control datepicker" placeholder="Select date" id="from_date" name="from_date" />
                                        </div>
                                 </div>
                                <div class="form-group">
                                   <label class="control-label col-xs-2" for="to_date">To Date</label>
                                    <div class="col-xs-4">
                                        <input type="text" readonly class="validate[required]  form-control datepicker" placeholder="Select date" id="to_date" name="to_date" />
                                    </div>
                                </div>                                        
                                <div class="form-group">
                                    <div class="col-xs-3 col-sm-offset-3">
                                        <input type="submit" id="submit" class="btn btn-primary" value="View">
                                     </div>
                                </div>      
                                     
                               
                             </form>  
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
        <script>
         $(document).ready(function(){
             
            $.validationEngine.defaults.scroll = false;
            $("#sdansaview").validationEngine({
                prettySelect:true,
                useSuffix: "_chosen", 
                maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#sdansaview").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#sdansaview").bind("jqv.form.result", function(event , errorFound){
                if(errorFound){ 
                    $("#hookError").append("Please fill all required fields");
                    $("#hookError").css('display','block');
                }
            });              
            
                    //           Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det
            
            $( "#from_date" ).datepicker({ 
                dateFormat: 'dd-M-yy',changeMonth: true,changeYear: true,yearRange: "2015:+0",
                onClose: function( selectedDate ) {
                $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
            }
            });
            $( "#to_date" ).datepicker({ dateFormat: 'dd-M-yy',changeMonth: true,changeYear: true,yearRange: "2015:+0" });
            //$("select").chosen({disable_search_threshold: 10});
            $("#submit").click(function(){
                if ( $("#sdansaview").validationEngine('validate'))  {
                    return true;
                }
                return false;
            });
             $( "select" ).change(function() {
            var err = '.'+this.id+'_chosenformError';
            $(err).remove();
        });      

        $("#bname").chosen({disable_search_threshold: 10});
            
          });
          </script>
    </body>
</html>