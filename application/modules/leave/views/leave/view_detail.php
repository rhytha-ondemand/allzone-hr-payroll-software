<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
    <style>
	
        .dataTables_filter{
            display: none;
        }
        #bname_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> View Leave Card </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/leave_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Leave </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>                            
                            <?php
                            if($month == 13){
                            ?>
                            <h2 style="font-size: 20px;text-align:center">Leave details for the year of <?php echo $year; ?></h2>
                            <?php
                            }else{
                            ?>
                            <h2 style="font-size: 20px;text-align:center">Leave details for the month of <?php echo date('F', mktime(0, 0, 0, $month, 10)).','.$year; ?></h2>
                            <?php
                            }
                            ?>
                            <br>				
                            <table id="leavelist" class="table table-striped table-bordered responsive">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Emp. No</th>
                                        <th>Emp. Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                
                            </table>

                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<!--view card details-->
<div style="display:none;">
    <form id="editform" method="post" action="leave/leave_card">
        <input type="hidden" id="emp_id" name="emp_id"/>
        <input type="hidden" id="Id" name="Id"/>
		<input type="hidden" id="month" name="month" value="<?=$month?>"/>
		<input type="hidden" id="year" name="year" value="<?=$year?>"/>
    </form>
</div>
<!--view card details-->
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
             var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> View card</a>";
            data="bname=<?=$bname?>&month=<?=$month?>&year=<?=$year?>";
            src="leave/leave_all/?"+data;
                var oTable =  $('#leavelist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "emp_id", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "emp_name", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#leavelist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#leavelist > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });						
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                       $(nRow).find("td").eq(0).html(iDisplayIndex+1);                  
                }
            });
            function edit(){
//                var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            $("#Id").val($(this ).closest('tr').attr('id'));
//                            if(j===0){
//                              $("#Id").val($(this).text());
//                            } 
                            if(j===1){
                              $("#emp_id").val($(this).text());
                            }                            
                        });                        
                    });
                    $("#editform").submit();
            }
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>