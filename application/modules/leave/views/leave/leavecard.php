<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        </style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View Leave Card</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> View Leave Card </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/leave_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Leave </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
							<?php 
							/*echo $emp_id."<br>";
							echo $id."<br>";
							echo $month."<br>";
							echo $year."<br>";*/
							
							?>
                            <br>
                            <table class="table table-striped table-bordered responsive">
                                <thead>
                                <tr>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Emp No</th>
                                    <th rowspan="2">Date Of Joining</th>
                                    <th colspan="2" style="text-align: center">Paid</th>
                                </tr>
                                <tr>
                                    <th>Credit</th>
                                    <th>Availble</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php if(isset($emp_name)){ echo $emp_name; } ?></td>
                                        <td><?php if(isset($emp_id)){ echo $emp_id; } ?></td>
                                        <td><?php if(isset($doj)){ echo date("d-M-Y", strtotime($doj)); } ?></td>
                                        <td><?php if(isset($pl_cr)){ echo $pl_cr; } ?></td>
                                        <td><?php if(isset($pl_avail)){ echo $pl_avail; } ?></td>                                        
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-sm-offset-8" > <h5 style="font-weight: bolder; font-size: 15px;"> Note: T - Leave Applicable , F - Leave Not Applicable </h5>  </div>
                            <table id="leavecard" class="table table-striped table-bordered responsive">
                                <thead>
                                <tr>
                                    <th rowspan="2">S.NO</th>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">Leave Date</th>
                                    <th colspan="6" style="text-align: center">Leave details</th>
                                    <!--<th rowspan="2">C-Off Worked-Date</th>-->
                                    <!--<th rowspan="2">Remaining C-Off (Upto <?php echo date("d-M-Y"); ?>)</th>-->
                                    <th rowspan="2">Reason</th>
                                    <th rowspan="2">Action</th>
                                </tr>
                                <tr>
                                    <th>PL</th>
                                    <th>LOP</th>
                                    <th>PTR</th>
                                    <th>BVR</th>
                                    <th>ML</th>
                                    <th>C-Off</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                    
                            </table>
                            <div class="form-group">
                                <div class=" col-sm-offset-4">
                                    <!--<input type="submit" id="submit" class="btn btn-primary" value="Export to excel">-->                                 
                                    <!--<input type="submit" id="submit" class="btn btn-primary" value="Yearly">-->
                                 </div>
                            </div>
                            
                           <!--working content end-->
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Record deleted successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while delete this record &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	 
<!--Delete dialog box start-->

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Delete</h3>
                </div>
                <div class="modal-body">
                    <p>Are you want you delete this record ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" id="del_confirm" class="btn btn-primary" data-dismiss="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
   <div style="display:none;">
    <form id="deleteform" method="post" action="">
        <input type="hidden" id="demp_id" value="<?php echo $emp_id;?>" name="emp_id"/>
        <input type="hidden" id="del_id" name="Id"/>
    </form>
</div>
<!-- Delete dialog box End -->
<!--view card details-->
<div style="display:none;">
    <form id="editform" method="post" action="leave/leavecard_update">
        <input type="hidden" id="emp_id" value="<?php echo $emp_id;?>" name="emp_id"/>
        <input type="hidden" id="Id" name="Id"/>
        <input type="hidden" id="month" name="month" value="<?php echo $month;?>" />
        <input type="hidden" id="year" name="year" value="<?php echo $year;?>"/>        
    </form>
</div>
<!--view card details-->
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
//                var editbtn = "<a class='btn'><i class='glyphicon glyphicon-edit icon-white'></i></a><a class='btn' style='margin-left:15px;' href='#'><i class='glyphicon glyphicon-trash icon-white'></i></a>";
//                 var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a><a class='btn btn-danger' style='margin-left:5px;' href='#'><i class='glyphicon glyphicon-trash icon-white'></i>Delete</a>";
var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
				 src="leave/leave_c/?emp_id=<?=$emp_id?>&month=<?=$month?>&year=<?=$year?>";
                var oTable =  $('#leavecard').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "enter_date", "sWidth": "100px", "bSortable": true },
                        { "mDataProp": "leave_date", "sWidth": "100px", "bSortable": true },
                        { "mDataProp": "pl", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "lop", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "ptr", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "bvr", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "ml", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "coff", "sWidth": "50px", "bSortable": true },
//                        { "mDataProp": "coff_wdate", "sWidth": "100px", "bSortable": true },
//                        { "mDataProp": "coff_remain", "sWidth": "100px", "bSortable": true },
                        { "mDataProp": "reason", "sWidth": "100px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "170px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#leavecard > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
//                                $(this ).find('a:nth-child(2)').on('click',deleter);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#leavecard > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
//                        $(this ).find('a:nth-child(2)').on('click',deleter);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                       $(nRow).find("td").eq(0).html(iDisplayIndex+1);                  
                }
            });
            function edit(){
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                       $("#Id").val($(this ).closest('tr').attr('id'));
//                        if(j===0){
//                          $("#Id").val($(this).text());
//                        }                                                    
                    });                        
                });
                $("#editform").submit();
            }
            function deleter(){
                 var myModal = $('#delete_modal');
                var tr=$(this).closest('tr');
                $(this).closest('tr').each(function(  ) {
                $("td", this).each(function( j ) {
                    if(j===0){
                        $("#del_id").val($(this).text());
                    }                                                         
                });
            });
                myModal.modal({ show: true });
                return false;                
            }
            
            $("#del_confirm").click(function(){                
                $('#delete_modal').modal('hide');
                var url ="leave/leavecard_delete";
                var data = "Id="+$("#del_id").val()+"&emp_id="+$("#demp_id").val();
                $.ajax({
                    type:"POST",
                    url:url,
                    cache:false,
                    data:data,
//                    dataType:"JSON",
                    async:false,
                    success:function(result){
                       if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to delete leave details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant delete this leave detail","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){   
                                oTable.fnDraw();
                                oTable.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }
                    }
                });
            });
        });
            
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>