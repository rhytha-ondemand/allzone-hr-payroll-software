<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #bname_chosen,#emp_id_chosen,#cadre_chosen{
                width: 100%!important;
            }
/*            .btn-group{
                display: none;
            }*/
        </style>
      <?php // $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php // $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-12">
            <!-- content starts -->
<!--            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Employee</a>
                    </li>
                    <li>
                        <a href="#">Referral details</a>
                    </li>
                </ul>
            </div>-->
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Coff details </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <h4>Coff Leave details</h4>
                             <table class="table table-striped table-bordered ">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Worked Date</th>
                                    </tr>                                   
                                </thead>
                                <tbody>
                                     <?php
                                        $i=1;
                                    if($Coff != ''){
                                        foreach ($Coff as $row){
                                            echo "<tr>";
                                            echo "<td>". $i++ ."</td>";
                                            echo "<td>". $row['CoffworkedDate'] ."</td>";
                                            echo "</tr>";
                                        }
                                    }else{
                                       echo"<tr><td colspan='2'>Comp off Leave is not available</td></tr>";
                                   }
                                   ?>                                      
                                </tbody>
                            </table>
<!--                                           
                            <h4>Credit Leave details</h4>
                            <table class="table table-striped table-bordered ">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Availed Date</th>
                                    </tr>                                   
                                </thead>
                                <tbody>
                                   <?php
                                 /*  $i=1;
                                   if($Credit != ''){
                                        foreach ($Credit as $row){
                                            echo "<tr>";
                                             echo "<td>". $i++ ."</td>";
                                            echo "<td>". $row['CrLeaveDate'] ."</td>";
                                            echo "</tr>";
                                        }
                                   }else{
                                       echo"<tr><td colspan='2'>Credit Leave is not available</td></tr>";
                                   }*/
                                   ?>                                 
                                </tbody>
                            </table>-->
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->  
       <hr>
        <?php // $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){                
            
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>
