<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .chosen-container{
                width: 97%!important;
            }
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>   
      <?php $this->load->view('includes/topbar.php');
	  
	  ?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Add Leave</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Leave  </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form action="leave/save_emp_leave" id="leaveadd_det" method="post">
                                <table class="table table-striped table-bordered responsive">
                                    <thead>
                                        <tr>
                                            <th>Employee Name</th>
                                            <th>PL Credit</th>
                                            <th>PL Available</th>
                                            <th>Leave Type</th>
                                            <th>Date</th>
                                            <!--<th>To Date</th>-->
                                            <th>Reason</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <!--<tr>
                                        <td>B. NaZeer Ahmed</td>
					<td>0</td>
					<td>0</td>
					<td>
                                            <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >							
                                                <option value="4">PL</option>
                                                <option value="3">LOP</option>
                                                <option value="3">PTR</option> 
                                                <option value="3">BVR</option> 
                                                <option value="3">ML</option>                                            
                                            </select>
					</td>
					<td>
                                            <input type="text" class="validate[required] form-control" name="emp_id" id="emp_id">                       
					</td>
					<td>
                                            <input type="text" class="validate[required] form-control" name="emp_id" id="emp_id">
					</td>
					<td>
                                            <input type="text" class="validate[required]  form-control" name="emp_id" id="emp_id">
					</td>
				</tr> -->
				<?php foreach($emp as $key=>$val) { ?>
                                <tr id="<?= $val['id']; ?>">
				<td name='ename[]'><?= $val['Name']; ?><input type="hidden" name="uid[]" value="<?= $val['id']; ?>"><input type="hidden" name="emp_id[]" value="<?= $val['Emp_id']; ?>"></td>
				<td name='plc[]'><?= $val['Pl_credit']; ?></td>
                                <td name='pla[]'><?php if(($val['pl_available'])>=0){ echo $val['pl_available'];}else{echo 0;} ;?></td>
				<td>
                                    <select id="ltype<?= $val['id']; ?>" name="ltype[]" data-placeholder="Select Leave type" class="validate[required] form-control ltype" >							
                                        <!--<option value="">Select leave type</option>-->
                                       <?php if($val['coff_det'] > 0){ echo '<option value="7">Coff</option>'; }?> 
                                       <?php if($val['pl_available'] > 0){ echo '<option value="1">PL</option>'; }?> 
                                        <!--<option value="1">PL</option>-->
                                        <option value="2">LOP</option>
                                        <option value="3">PTR</option> 
                                        <option value="4">BVR</option> 
                                        <option value="5">ML</option> 
                                        <!--<option value="6">RH</option>-->                                         
                                    </select>
				</td>
				<td>                                  
                                    <input type="text" readonly="" value="<?php echo date('d-M-Y',strtotime("-1 days"));?>" class="validate[condRequired[ltype<?= $val['id']; ?>] form-control datepicker fdate" name="fdate[]" id="fdate<?= $val['id']; ?>" />                       
                                    <input type="text" style="margin-top:3px; display:none;" readonly="" class="form-control datepicker tdate" name="tdate[]" id="tdate<?= $val['id']; ?>" />
                                </td>
<!--                                <td>
                                    <input type="text" readonly="" class="form-control datepicker tdate" name="tdate[]" id="tdate<?= $val['id']; ?>">
                                </td>-->
                                <td>
                                    <input type="text"  class="validate[condRequired[ltype<?= $val['id']; ?>] form-control" maxlength="50" name="reasen[]" id="reasen<?= $val['id']; ?>">
                                </td>
                                <td>
                                    <a href='leave/coffcr_detail?emp_id=<?= $val['Emp_id']; ?>' target="_blank" class="fancybox fancybox.iframe" > View Coff Details </a> <br>
                                    <input type="button" style="margin-top:7px;" value="View Leave card" class="btn-primary lview" id="<?= $val['Emp_id']; ?>" />
                                </td>				
				</tr>
				<?php } ?>
                                    </tbody>
                                </table>
                                <div class="form-group row">
                                   <div class="col-xs-3 col-sm-offset-4">
                                       <input type="submit"  id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<div>
    <form action="leave/leave_card" id="lviewcard" method="post" target="_blank">
        <input type="hidden" name ="emp_id" id="lemp_id" />
        <input type="hidden" name="month" value="13" />
        <input type="hidden" name="year" value="<?php echo date('Y');?>" />
    </form>
</div>
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            
            $.validationEngine.defaults.scroll = false;
            $("#leaveadd_det").validationEngine({
                prettySelect:true,
                useSuffix: "_chosen", 
                maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#leaveadd_det").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#leaveadd_det").bind("jqv.form.result", function(event , errorFound){
                if(errorFound){ 
                    $("#hookError").append("Please fill all required fields");
                    $("#hookError").css('display','block');
                }
            });   
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            }); 
            $(".ltype").change(function(){
               var id=$(this ).closest('tr').attr('id');
               var date_id ='#tdate'+id;
//               console.log(date_id);
               if($(this).val() == 5 ){
                    $(date_id).css('display','block');
                    $(date_id).addClass('validate[required]');
                }else{
                    var tdate = "#tdate"+id;
//                    console.log(tdate);
                    $(tdate).css('display','none');
                    $(tdate).removeClass('validate[required]');
                    $(".tdate"+id+"formError").remove();
                }
               $( ".tdate" ).datepicker({ dateFormat: 'dd-M-yy',minDate:0 , maxDate:150,changeMonth: true,changeYear: true });
            });
            
            $( ".fdate" ).datepicker({ dateFormat: 'dd-M-yy',maxDate:20  });
            $( ".tdate" ).datepicker({ dateFormat: 'dd-M-yy',minDate:0, maxDate:150,changeMonth: true,changeYear: true });
            $('.fancybox').fancybox({
                'width'  : 600,           // set the width
                'height' : 500,
                'autoSize' : false

            });
            
            
            $(".lview").click(function(){
                var lev_empid = this.id;
                $("#lemp_id").val(lev_empid);
                $("#lviewcard").submit();
            });
            
            $("#submit").click(function(){
                if ( $("#leaveadd_det").validationEngine('validate') ) {
                 return true;   
                }
                return false;
            });
        });    
        </script>
        <?php $this->load->view('includes/additional.php');?>
         <script type="text/javascript" src="assets/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
        <style>
/*            .fancybox-lock .fancybox-overlay {
    overflow: auto;
    overflow-y: auto;
}*/
        </style>
    </body>
</html>