<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
<style>
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        height:37px;
        padding:1px!important;
    }
    .default{
        height: 25px!important;
        padding-top: 2px!important
    }
    #emp_id_chosen{
        /*height:37px;*/
        max-width:200%!important;
        display:inline-block;
    }
</style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Add</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Leave </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="leaveadd" role="form" method="post" action="leave/leave_adddetails">
                                
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_id">Employee ID</label>
                                    <div class="col-xs-4">                                        
                                        <select id="emp_id" multiple=""  name="emp_id[]" data-placeholder="Select Employee details" class="validate[required] form-control" >
                                            <option value=""></option>                                                                                       
                                        </select>
                                        Note: Please select 25 members only.
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <input type="submit"  id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                                
                                
                                
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Employee leave details added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
        <?php 
            if(isset($result)){
            ?>            
               alert("Leave Details added succesfully");
            <?php
            }
            ?>
            $.validationEngine.defaults.scroll = false;
            $("#leaveadd").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#leaveadd").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#leaveadd").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            $("#bname").chosen({disable_search_threshold: 10});
            $("#emp_id").chosen({max_selected_options : 25});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
 //                   Branch det       
             var toappend;
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();                      
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');                        
                        }
                    }            
                }); 
                          
//                Branch det            
            $("#bname").change(function(){
              var toappend;
                var url = "master/emp_id_list";
                var data =  "bname=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#emp_id').find('option').remove();
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['emp_name']+'</option>';                                
                            });   
                            $('#emp_id').append(toappend);                           
                        }
                         $("#emp_id").trigger('chosen:updated');
                         
                    }            
                });                 
          });
            $("#emp_id").change(function(){
                if($("#emp_id_chosen .chosen-choices li").size() === 3){
                    $("#emp_id_chosen").css('width','200%');
                }
                 if($("#emp_id_chosen .chosen-choices li").size() === 2){  
                    $("#emp_id_chosen").css('width','100%');
                }
            });
            
            
            $("#submit").click(function(){
                if ( $("#leaveadd").validationEngine('validate')) {
                    return true;
                }
                return false;
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>