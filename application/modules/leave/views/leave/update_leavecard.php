<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
<style>
 .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }   
</style>    
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Update leave Card</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Update leave Card</h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="leaveupdate" role="form" method="post" action="leave/leavecard">
                                
                                <br>
                                <input type="hidden" value="<?php echo $Id;?>" name="Id"/>
                                <input type="hidden" value="<?php echo $month;?>" name="month"/>
                                <input type="hidden" value="<?php echo $year;?>" name="year"/>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="date">Date</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text" readonly="" name="date" id="date" value="<?php echo $Leave_edate;?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_id">Employee No</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text" readonly="" name="emp_id" id="emp_id" value="<?php echo $Emp_id;?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_name">Employee name</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text" readonly="" name="emp_name" id="emp_name" value="<?php echo $emp_name;?>" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="type_leave">Type Of Leave</label>
                                    <div class="col-xs-4">
                                        <input type="hidden" name="type_leave" value="<?php if(isset($Leave_type)){echo $Leave_type;}?>"/>
                                        <select id="type_leave" name="type_leave" disabled="" data-placeholder="Select Leavetype" class="validate[required] form-control" >
                                            <option value=""></option>
<!--                                            <option value="pl" >PL</option>
                                            <option value="lop">LOP</option>
                                            <option value="ptr">PTR</option> 
                                            <option value="bvr">BVR</option> 
                                            <option value="ml">ML</option>   
                                            <option value="rh">RH</option>  
                                            <option value="coff">C-OFF</option>-->
                                            <option value="1" <?php if($Leave_type == 1 || $Leave_type == '1'){echo "selected";}?> >PL</option>
                                            <option value="2" <?php if($Leave_type == 2 || $Leave_type == '2'){echo "selected";}?> >LOP</option>
                                            <option value="3" <?php if($Leave_type == 3 || $Leave_type == '3'){echo "selected";}?> >PTR</option> 
                                            <option value="4" <?php if($Leave_type == 4 || $Leave_type == '4'){echo "selected";}?> >BVR</option> 
                                            <option value="5" <?php if($Leave_type == 5 || $Leave_type == '5'){echo "selected";}?> >ML</option>  
                                            <option value="7" <?php if($Leave_type == 7 || $Leave_type == '7'){echo "selected";}?> >Coff</option>
                                        </select>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="ldate">Leave Date</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text"  name="ldate" id="ldate" readonly="" value="<?php echo $Leave_sdate;?>" class="validate[required] form-control datepicker" />
                                    </div>
                                </div>
                                <div class="form-group" <?php if($Leave_type != 5 || $Leave_type != '5'){echo "style='display:none'";}?> >
                                    <label class="control-label col-xs-2" for="enddate">Leave End Date</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text"  name="enddate" id="enddate" readonly="" value="<?php echo $Leave_endate;?>" class="validate[required] form-control datepicker" />
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="reason">Reason</label>
                                    <div class="col-xs-4">                                        
                                        <input type="text"  name="reason" id="reason" maxlength="50" value="<?php echo $Leave_reason; ?>" class="validate[required] form-control" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <input type="submit"  id="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                                
                                
                                
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#leaveupdate").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#leaveupdate").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#leaveupdate").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
//            $( ".datepicker" ).datepicker({ dateFormat: 'dd-M-yy',minDate:-30 });
            
            $("#ldate").datepicker({dateFormat:"dd-M-yy",minDate:-30,maxDate:30,
                    onClose: function( selectedDate ) {
                        $( "#enddate" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#enddate").datepicker({dateFormat:"dd-M-yy", maxDate:150,changeMonth: true,changeYear: true });
            
            $("#submit").click(function(){
                if ( $("#leaveupdate").validationEngine('validate')) {
                    return true;
                }
                return false;
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>