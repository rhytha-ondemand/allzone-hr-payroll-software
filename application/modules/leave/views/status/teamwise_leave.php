<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
             .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Leave Status - Teamwise </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="lteamwise" role="form" action="leave/status_teamwisedetails" method="post">
                            <br>
                              <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                              <div class="form-group">
                                    <label class="control-label col-xs-2" for="team">Team</label>
                                    <div class="col-xs-3">
                                        <select class="validate[required] form-control" id="team" name="team" data-placeholder="Select Team">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
<!--                                 <div class="form-group">
                                        <label class="control-label col-xs-2" for="status">Status</label>
                                        <div class="col-xs-3 radio">
                                            <select class="validate[required] form-control" id="status" name="status" data-placeholder="Select status">
                                                <option value=''></option>
                                                <option value=All>All</option>
						<option value=Active>Active</option>
						<option value=Inactive>Inactive</option>
                                            </select>
                                            <label><input  tabindex="1" type=radio checked value="1"  name='status' id='status' /> Active &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <label><input type=radio tabindex="1" value="0"  name='status' id='status'/> Inactive &nbsp;&nbsp;</label>
                                        </div>
                                </div>-->
<!--                                 <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">Year</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" id="year" name="year" data-placeholder="Select Year">
                                                <option value=''></option>
                                                <option value=0>All Year </option>
						<?php 
//                                                $st_year = 2015;
//                                                $year = date("Y");
//                                                for($i= $st_year;$i<=$year+1;$i++){
//                                                    echo "<option value='".$i."'>".$i."</option>";
//                                                }                                               
                                            ?>
                                            </select>
                                        </div>
                                </div>-->
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="fdate">From Date</label>
                                        <div class="col-xs-3">
                                            <input class="validate[required] form-control datepicker" readonly id="fdate" name="fdate" placeholder="Select Date">
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="tdate">To Date</label>
                                        <div class="col-xs-3">
                                            <input class="validate[required] form-control datepicker" readonly id="tdate" name="tdate" placeholder="Select Date">
                                        </div>
                                </div>
                                        
                                    <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                           <input type="submit" id="submit" class="btn btn-primary" value="View">
                                        </div>
                                    </div>      
                                     
                                <!--</div>-->
                             </form>           
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#lteamwise").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#lteamwise").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#lteamwise").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
//            changeMonth: true,changeYear: true}
                $("#fdate").datepicker({dateFormat:"dd-M-yy",minDate:"01-Jan-2016",maxDate:0,changeMonth: true,changeYear: true,
                    onClose: function( selectedDate ) {
                        $( "#tdate" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#tdate").datepicker({dateFormat:"dd-M-yy",minDate:"01-Jan-2016",maxDate:0,changeMonth: true,changeYear: true});
            
            $("select").chosen({disable_search_threshold: 15});           
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });

//             Branch det       
            var toappend;
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();                      
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');                        
                        }
                    }            
                }); 
                          
//                Branch det  
            $("#bname").change(function(){
                var toappend;
                $.ajax({
                    type: "POST",
                    url: "master/team_list",
                    cache: false,
                    data:"bname="+$("#bname").val(),
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#team').find('option').remove();                      
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['reporting_no']+'>'+value['team']+'</option>';

                            });   
                            $('#team').append(toappend);
                            $("#team").trigger('chosen:updated');                        
                        }
                    }            
                }); 
            });    

            $("#submit").click(function(){
                if ( $("#lteamwise").validationEngine('validate')) {
                    return true;
                }
                return false;
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>