<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Monthly view</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Leave status for the month of <?php echo date('F', mktime(0, 0, 0, $month, 10))."  ".$year ?> </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" >
                                <br>	
                                <br>	
                                <!--<p style="text-align: center; font-size: medium; ">Leave status for the month of June 2012</p>-->        
                                <table class="table table-striped table-bordered responsive">
                                <thead>
                                <tr>
                                    <th rowspan="2">S.No</th>
                                    <th rowspan="2">Emp.ID</th>
                                    <th rowspan="2">Employee Name</th>
                                    <th style="text-align: center" colspan="7">No Of Days</th>
                                    <th rowspan="2">Action</th>
                                </tr>
                                <tr>
                                    <th>LOP</th>
                                    <th>PL</th>
                                    <th>Bal PL</th>
                                    <th>PTR</th>
                                    <th>BVR</th>
                                    <th>ML</th>
                                    <th>TOT</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $slno=1;
                                    $bal_pl=0;
                                       if($array != ''){
                                            foreach($array as $row){
                                            if(($row['bal_pl'])>=0){
                                                $bal_pl = $row['bal_pl'];
                                            }else{
                                                $bal_pl = 0;
                                            }
                                            echo "<tr>";
                                            echo "<td>".$slno++."</td>";
                                            echo "<td>".$row['emp_id']."</td>";
                                            echo "<td>".$row['emp_name']."</td>";
                                            echo "<td>".$row['lop']."</td>";
                                            echo "<td>".$row['pl']."</td>";
                                            echo "<td>".$bal_pl."</td>";
                                            echo "<td>".$row['ptr']."</td>";
                                            echo "<td>".$row['bvr']."</td>";
                                            echo "<td>".$row['ml']."</td>";
                                            $total = $row['lop']+$row['pl']+$row['ptr']+$row['bvr']+$row['ml'];
                                            echo "<td>".$total."</td>";
                                            echo "<td><a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>View card</a></td>";
                                            echo "</tr>";                                            
                                        }
                                       }else{
                                            echo "<tr><td colspan='11' >No record found this table</td></tr>";
                                       }
                                    ?>
                                </tbody>			
                            </table>
							
<!--                            <div class="form-group">
                                <div class="col-xs-4 col-sm-offset-4">
                                    <input type="button" class="btn btn-primary" value="Export Report">
                                </div>
                            </div>-->
                        </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<!--view card details-->
<div style="display:none;">
    <form id="editform" method="post" action="leave/leave_card">
        <input type="hidden" id="emp_id" name="emp_id"/>
		<input type="hidden" id="month" name="month" value="<?=$month?>"/>
		<input type="hidden" id="year" name="year" value="<?=$year?>"/>
        <input type="hidden" id="Id" name="Id"/>
    </form>
</div>
<!--view card details-->
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $("select").chosen({disable_search_threshold: 15}); 
            
            $(".edit").click(function(){
                $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
//                            if(j===0){
//                              $("#Id").val($(this).text());
//                            } 
                            if(j===1){
                              $("#emp_id").val($(this).text());
                            }                            
                        });                        
                    });
                    $("#editform").submit();
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>