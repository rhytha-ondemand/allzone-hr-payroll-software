<?php error_reporting(0);?>
<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Teamwise leave report</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Total Leave Taken by Team members </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                              <table class="table table-striped table-bordered responsive">
                                <thead>	
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Emp.No</th>
                                    <th>Emp.Name</th>
                                    <th>Team</th>
                                    <th> Jan </th>
                                    <th> Feb </th>
                                    <th> Mar </th>
                                    <th> Apr </th>
                                    <th> May </th>
                                    <th> Jun  </th>
                                    <th> Jul </th>
                                    <th> Aug </th>
                                    <th> Sep </th>
                                    <th> Oct  </th>
                                    <th> Nov </th>
                                    <th> Dec </th>
                                    <th>Total</th>
                                    <!--<th>Remark</th>-->									
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i=0;
                                        if($array){
                                            foreach($array as $key => $row){
                                                echo "<tr>";
                                                $r = '-';
                                                echo "<td>".++$i."</td>";
                                                echo "<td>".$key."</td>";
                                                echo "<td>".$row['Emp_name']."</td>";
                                                echo "<td>".$row['Team']."</td>";
                                                echo "<td>".$row['JAN']."</td>";
                                                
                                                if($row['FEB'] > 28){
                                                    echo "<td colspan=2 style='text-align: center;'>". ($row['FEB']+$row['MAR']) ."</td>";    
                                                }else{
                                                    echo "<td>".$row['FEB']."</td>";
                                                    echo "<td>".$row['MAR']."</td>";
                                                }
                                                
//                                                echo "<td>".$row['FEB']."</td>";
//                                                echo "<td>".$row['MAR']."</td>";
                                                echo "<td>".$row['APR']."</td>";
                                                echo "<td>".$row['MAY']."</td>";
                                                echo "<td>".$row['JUN']."</td>";
                                                echo "<td>".$row['JUL']."</td>";
                                                echo "<td>".$row['AUG']."</td>";
                                                echo "<td>".$row['SEP']."</td>";
                                                echo "<td>".$row['OCT']."</td>";
                                                echo "<td>".$row['NOV']."</td>";
                                                echo "<td>".$row['DEC']."</td>";
                                                echo "<td>".$row['TOTAL']."</td>";
//                                                echo "<td>".$r."</td>";                                            
                                                echo "</tr>";
                                            }
                                        
                                        }
                                        else{
                                            echo "<tr><td colspan='17' >No record found this table</td></tr>";
                                        }
                                        
                                    ?>
                                </tbody>
                              </table>
                            <div class="row">
                            <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-5">
                                       <!--<input type="button" class="btn btn-primary" value="Export">-->
                                    </div>
                                </div>
                            </div>
								
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>