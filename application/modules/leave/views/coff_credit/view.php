<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #emp_id_chosen{
                width: 100%!important;
            }
            .chosen-container{
                width: 100%!important;
            }
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style> 
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">C-Off</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>View Compensation Leave</h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/coff_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Coff / Credit </a>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="lcoffcrview" role="form" method="post" action="leave/coff_details">
                               <br>
                                <div class="form-group">
                                    <div class="col-xs-2"><label class="control-label"  for="bname">Select Branch</label><span class="req"></span></div>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-2"><label class="control-label" for="loption">Option</label><span class="req"></span></div>
                                    <div class="col-xs-3 radio">
                                        <label><input  tabindex="1" type=radio checked  value="1"  name='loption' id='loption1'> C-Off </label>
                                        <label style="margin-left: 3%;"><input type=radio tabindex="1" value="2"  name='loption' id='loption2'> Credit </label>
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label class="control-label col-xs-2"  for="fdate">From Date</label><span></span>
                                    <div class="col-xs-4">
                                        <input type="text" readonly="" id="fdate" name="fdate" class="validate[required] form-control datepicker"/>                                     
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label class="control-label col-xs-2"  for="tdate">To Date</label><span></span>
                                    <div class="col-xs-4">
                                        <input type="text" readonly="" id="tdate" name="tdate" class="validate[required] form-control datepicker"/>                                     
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-2"><label class="control-label" for="inputSuccess4">Type</label><span class="req"></span></div>
                                    <div class="col-xs-3 radio" id="radioDiv">
                                        <label><input type="radio" checked="" name="ltype"  value="1">Over all</label>
                                        <label style="margin-left: 3%;"><input type="radio" name="ltype" value="2">Employee </label><span></span>
                                    </div>
                                </div>
                               <div id="emp_det" style="display: none">
                                    <div class="form-group">
                                        <div class="col-xs-2"><label class="control-label"  for="emp_id"> Select Employee ID</label><span class="req"></span></div>
                                        <div class="col-xs-4">
                                            <select id="emp_id" name="emp_id" data-placeholder="Select Employee" class="validate[required] form-control" >
                                                <option value=""></option>                                            
                                            </select>
                                        </div>
                                    </div>
                                   <input type="hidden" id="emp_name" name="emp_name" value="" />
                                   
<!--                                   <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">Year</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" id="year" name="year" data-placeholder="Select Year">
                                                <option value=''></option>
                                                <option value=0>All Year </option>
						<?php 
//                                                $st_year = 2015;
//                                                $year = date("Y");
//                                                for($i= $st_year;$i<=$year+1;$i++){
//                                                    echo "<option value='".$i."'>".$i."</option>";
//                                                }                                               
                                            ?>
                                            </select>
                                        </div>
                                </div>-->
                                   
                                </div>   
                                <br>
                                <div class="form-group">
                                    <div class="col-xs-3 "></div>
                                       <div class="col-xs-3 ">
                                           <input type="submit"  id="submit" class="btn btn-primary" value="View">
                                        </div>
                                </div>   
                             </form>

                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                $.validationEngine.defaults.scroll = false;
            $("#lcoffcrview").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#lcoffcrview").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#lcoffcrview").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
             $("#fdate").datepicker({
                dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2015:+0",
                onClose: function( selectedDate ) {
                    $( "#tdate" ).datepicker( "option", "minDate", selectedDate );
                  }
            });
            $("#tdate").datepicker({dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange: "2015:+0"});
//                $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
                $("select").chosen({disable_search_threshold: 10});
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                }); 
                $('#radioDiv input').on('change', function() {
                    if ($("#radioDiv input[type='radio']:checked").val() == 2) {
                        $("#emp_det").css('display','block');
                    }else{
                        $("#emp_det").css('display','none');
                    }
                });
                
//         Branch det       
            var toappend;
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();                      
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');                        
                        }
                    }            
                }); 
                          
//                Branch det   
                $("#bname").change(function(){
                    var emp_list ="<option></option>";
                    $.ajax({
                        type:"POST",
                        url:"master/emp_id_list",
                        cache:false,
                        data:"bname="+$("#bname").val(),
                        dataType:"json",
                        async:false,
                        success:function(json){
                            $("#emp_id").find('option').remove();
                            if(json){
                                
                                $.each(json,function(i,value){
                                    emp_list += "<option value="+value['Id']+">"+value['emp_name']+"</option>";
                                });
                                $("#emp_id").append(emp_list);
                                
                            }
                            $("#emp_id").trigger("chosen:updated");
                        }
                    });
                });
                $("#emp_id").change(function(){
                    var selected = $(this).find('option:selected');                    
                    $("#emp_name").val(selected.text());
                });
                $("#submit").click(function(){
                    if ( $("#lcoffcrview").validationEngine('validate') )  {
//                        alert("success");
                       return true;     
                    }
                    return false;
                });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>