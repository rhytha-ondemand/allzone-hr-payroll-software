<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
    <style>
        .dataTables_filter{
            display: none;
        }
        .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
    </style>  
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Compensation Leave </h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <!--<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>-->
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'leave/coff_add';?>" class="btn btn-success btn-xs pull-right" > <i class="glyphicon glyphicon-plus"></i> Add Coff / Credit </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                              <br>
                            <br>
<!--                            <div class="form-group">
                                    <label class="control-label col-xs-5" for="inputSuccess4">J. Satish Kumar - Emp. No 2 </label>
                            </div>-->
                            <table class="table table-striped table-bordered responsive" id="coff_list">
                                <thead>
                                <tr>
                                    <!--<th>Sl.No</th>-->
                                    <th>Emp.No</th>                                    
                                    <th>Emp. Name</th>
                                    <th>Date</th>
                                    <th>Date Worked</th>
                                    <th>Leave Availed </th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Coff details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while delete this record &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	 
<!--Edit dialog box-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Compensation Details</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="coff_id" id="coff_id">
                <input type="hidden" name="emp_id" id="emp_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="coff_wdate">Coff Worked date</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" readonly="" class="validate[required] form-control datepicker " id="coff_wdate" name="coff_wdate">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="coff_adate">Coff Availed Date</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" readonly="" class="validate[required] form-control datepicker" id="coff_adate" name="coff_adate">
                        </div>
                    </div>
                    <br><br>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>
<!--Edit dialog box-->
<!--Delete dialog box start-->

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Delete</h3>
                </div>
                 <input type="hidden" name="coff_id" id="dcoff_id">
                <input type="hidden" name="emp_id" id="demp_id">
                <div class="modal-body">
                    <p>Are you want you delete this record ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" id="del_confirm" class="btn btn-primary" data-dismiss="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
<!--   <div style="display:none;">
    <form id="deleteform" method="post" action="">
        <input type="hidden" id="demp_id" value="<?php // echo $emp_id;?>" name="emp_id"/>
        <input type="hidden" id="del_id" name="Id"/>
    </form>
</div>-->
<!-- Delete dialog box End -->
<!--view card details-->
<!--<div style="display:none;">
    <form id="editform" method="post" action="leave/coff_update">
        <input type="hidden" id="emp_id" value="<?php // echo $emp_id;?>" name="emp_id"/>
        <input type="hidden" id="Id" name="Id"/>
    </form>
</div>-->
<!--view card details-->

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
//                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a><a class='btn btn-danger' style='margin-left:5px;' href='#'><i class='glyphicon glyphicon-trash icon-white'></i>Delete</a>";
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                src="leave/coff_overall?bname=<?=$bname;?>"+"&fdate=<?php echo $fdate;?>"+"&tdate=<?php echo $tdate;?>";
                var sno = 1;
                var oTable =  $('#coff_list').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
//                        { "sDefaultContent": sno, "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Emp_id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Emp_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "CoffEntryDate", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "CoffworkedDate", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "CoffAvailedDate", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "150px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#coff_list > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                $(this ).find('a:nth-child(2)').on('click',deleter);                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#coff_list > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                        $(this ).find('a:nth-child(2)').on('click',deleter);
                    });	
					
                },
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
//                       $(nRow).find("td").eq(0).html(iDisplayIndex+1);                  
//                }
            });
            function edit(){
                var myModal = $('#edit-modal');
                $("#formsave").validationEngine('hide');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#coff_id").val($(this ).closest('tr').attr('id'));
                        if(j===0){
                          $("#emp_id").val($(this).text());
                        }
                        if(j===3){
                          $("#coff_wdate").val($(this).text());
                        }
                        if(j===4){
                            var selectedDate = $("#coff_wdate").val();
                          $( "#coff_adate" ).datepicker( "option", "minDate", selectedDate );
                          $("#coff_adate").val($(this).text());
                        }
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
            
            function deleter(){
                 var myModal = $('#delete_modal');
//                 var tr=$(this).closest('tr');
                 $(this).closest('tr').each(function(  ) {
                    $("td", this).each(function( j ) {
                        $("#dcoff_id").val($(this ).closest('tr').attr('id'));
                        if(j===0){
                          $("#demp_id").val($(this).text());
                        }                        
                    });                        
                });
                myModal.modal({ show: true });
                return false;
            }
//            var mindate =-2;
             $("#coff_wdate").datepicker({
                 dateFormat:"dd-M-yy", minDate :-10,maxDate:2,
                    onClose: function( selectedDate ) {
                    $( "#coff_adate" ).datepicker( "option", "minDate", selectedDate );
                  }
             });
             $("#coff_adate").datepicker({dateFormat:"dd-M-yy",maxDate:30});
             $("#del_confirm").click(function(){
                 $.ajax({
                     type:"POST",
                     url:"leave/coff_delete",
                     cache:false,
                     data:"Id="+$("#dcoff_id").val()+"&emp_id="+$("#demp_id").val(),
//                     dataType:"json",
                     async:false,
                     success:function(result){
                         $('#delete_modal').modal('hide');
                         if(result === 1 || result === "1"){
                            $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to delete details","layout":"topCenter","type":"warning"}');
                            $(".notyerror").click();
                        }else if(result === 2 || result === "2"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                            $(".notyerror").click();
                        }else if(result === 3 || result === "3"){
                            $(".notyerror").attr("data-noty-options",'{"text":"Sorry, Invalid coff details","layout":"topCenter","type":"error"}');
                              $(".notyerror").click();
                        }else if(result === 4 || result === "4"){  
                            $(".notyerror").attr("data-noty-options",'{"text":"Coff details deleted successfully","layout":"topCenter","type":"success"}');
                            $(".notyerror").click();
                            oTable.fnDraw();
                            oTable.fnReloadAjax();                                 
                        }else{
                            window.location.href = "master/login";
                        }
                     }
                 });
             });
                          
             $.validationEngine.defaults.scroll = false;
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submit").click(function(){
                    if ( $("#formsave").validationEngine('validate')) {
//                        var data = $("#formsave").serialize();
                        $.ajax({
                            type: "POST",
                            url: "leave/coff_update",
                            data:"Id="+$("#coff_id").val()+"&emp_id="+$("#emp_id").val()+"&coff_wdate="+$("#coff_wdate").val()+"&coff_adate="+$("#coff_adate").val(),
                            async:false,
                            success: function (result) {
                                $('#edit-modal').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTable.fnDraw();
                                    oTable.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });            
                    }
                    return false;
                });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>