<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .label-normal:before{
                content: " : ";
                padding-right: 5px;
            }
            .label-normal{
                font-weight: normal;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Appraisal</a>
                    </li>
                    <li>
                        <a href="#">Individual</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Appraisal Leave Details </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
<!--                            <form class="form-horizontal" role="form">-->							
                            <br>
                                <div class="row">
                                    <div class="bordering">
                                    <h1><span>Personal information</span></h1>
                                    <div class="col-xs-4 col-sm-offset-1">
                                        <div class="form-group">
                                            <label class="control-label col-xs-4" for="dot">Emp. No</label>
                                            <div class="col-xs-5">
                                                <label class=" label-normal" name="" id=""><?php echo $array[0]['Emp_id']; ?></label>
                                              </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-4" for="dot">DOJ</label>
                                            <div class="col-xs-5">
                                                <label class=" label-normal" name="" id=""><?php echo $array[0]['D_of_join']; ?></label>
                                              </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-4" for="dot">Team</label>
                                            <div class="col-xs-7">
                                                <label class=" label-normal" name="" id=""><?php echo $array[0]['Team']; ?></label>
                                              </div>
                                         </div>
                                    </div>
                                    
                                    <div class="col-xs-5">
                                        <div class="form-group">
                                            <label class="control-label col-xs-5" for="dot">Emp. Name</label>
                                            <div class="col-xs-5">
                                                <label class="label-normal" name="" id=""><?php echo $array[0]['Emp_name']; ?></label>
                                              </div>
                                         </div>
                                        <div class="form-group">
                                            <label class="control-label col-xs-5" for="dot">Designation</label>
                                            <div class="col-xs-7">
                                                <label class="label-normal" name="" id=""><?php echo $array[0]['Design_name']; ?></label>
                                              </div>
                                         </div>
<!--                                        <div class="form-group">
                                            <label class="control-label col-xs-5" for="dot">Appraisal period</label>
                                            <div class="col-xs-5">
                                                <label class="label-normal" name="" id="">From : <?php // echo $from_period; ?> <p style="padding-left: 12px;"> To : <?php // echo $to_period; ?> </p></label>
                                              </div>
                                         </div>-->
                                    </div>
                                     <div class="row">
                                        
                                    </div> 
                                </div>
                                   
                            </div>
                            <br>
                            <table class="table table-striped table-bordered responsive">
                            <thead>
                                <tr>
                                    <th>Month & Year</th>	
                                    <th>LOP</th>
                                    <th>PL</th>
                                    <th>PTR</th>
                                    <th>BVR</th>
                                    <th>ML</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $lop= 0;
                                $pl= 0;
                                $ptr= 0;
                                $bvr= 0;
                                $ml= 0;
                                $total_all= 0;
//                                $total= 0;
                                if($array != 0){
                                foreach($array as $row){
                                    echo "<tr>";
                                    echo "<td>".'January'."</td>";
                                    echo "<td>".$row['jan_lop']."</td>";
                                    echo "<td>".$row['jan_pl']."</td>";
                                    echo "<td>".$row['jan_ptr']."</td>";
                                    echo "<td>".$row['jan_bvr']."</td>";
                                    echo "<td>".$row['jan_ml']."</td>";                                    
                                    echo "<td>".$row['jan_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'February'."</td>";
                                    echo "<td>".$row['feb_lop']."</td>";
                                    echo "<td>".$row['feb_pl']."</td>";
                                    echo "<td>".$row['feb_ptr']."</td>";
                                    echo "<td>".$row['feb_bvr']."</td>";
                                    echo "<td>".$row['feb_ml']."</td>";                                    
                                    echo "<td>".$row['feb_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'March'."</td>";
                                    echo "<td>".$row['mar_lop']."</td>";
                                    echo "<td>".$row['mar_pl']."</td>";
                                    echo "<td>".$row['mar_ptr']."</td>";
                                    echo "<td>".$row['mar_bvr']."</td>";
                                    echo "<td>".$row['mar_ml']."</td>";                                    
                                    echo "<td>".$row['mar_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'April'."</td>";
                                    echo "<td>".$row['apr_lop']."</td>";
                                    echo "<td>".$row['apr_pl']."</td>";
                                    echo "<td>".$row['apr_ptr']."</td>";
                                    echo "<td>".$row['apr_bvr']."</td>";
                                    echo "<td>".$row['apr_ml']."</td>";                                    
                                    echo "<td>".$row['apr_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'May'."</td>";
                                    echo "<td>".$row['may_lop']."</td>";
                                    echo "<td>".$row['may_pl']."</td>";
                                    echo "<td>".$row['may_ptr']."</td>";
                                    echo "<td>".$row['may_bvr']."</td>";
                                    echo "<td>".$row['may_ml']."</td>";                                    
                                    echo "<td>".$row['may_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'June'."</td>";
                                    echo "<td>".$row['jun_lop']."</td>";
                                    echo "<td>".$row['jun_pl']."</td>";
                                    echo "<td>".$row['jun_ptr']."</td>";
                                    echo "<td>".$row['jun_bvr']."</td>";
                                    echo "<td>".$row['jun_ml']."</td>";                                    
                                    echo "<td>".$row['jun_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'July'."</td>";
                                    echo "<td>".$row['jul_lop']."</td>";
                                    echo "<td>".$row['jul_pl']."</td>";
                                    echo "<td>".$row['jul_ptr']."</td>";
                                    echo "<td>".$row['jul_bvr']."</td>";
                                    echo "<td>".$row['jul_ml']."</td>";                                    
                                    echo "<td>".$row['jul_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'August'."</td>";
                                    echo "<td>".$row['aug_lop']."</td>";
                                    echo "<td>".$row['aug_pl']."</td>";
                                    echo "<td>".$row['aug_ptr']."</td>";
                                    echo "<td>".$row['aug_bvr']."</td>";
                                    echo "<td>".$row['aug_ml']."</td>";                                    
                                    echo "<td>".$row['aug_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'September'."</td>";
                                    echo "<td>".$row['sep_lop']."</td>";
                                    echo "<td>".$row['sep_pl']."</td>";
                                    echo "<td>".$row['sep_ptr']."</td>";
                                    echo "<td>".$row['sep_bvr']."</td>";
                                    echo "<td>".$row['sep_ml']."</td>";                                    
                                    echo "<td>".$row['sep_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'October'."</td>";
                                    echo "<td>".$row['oct_lop']."</td>";
                                    echo "<td>".$row['oct_pl']."</td>";
                                    echo "<td>".$row['oct_ptr']."</td>";
                                    echo "<td>".$row['oct_bvr']."</td>";
                                    echo "<td>".$row['oct_ml']."</td>";                                    
                                    echo "<td>".$row['oct_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'November'."</td>";
                                    echo "<td>".$row['nov_lop']."</td>";
                                    echo "<td>".$row['nov_pl']."</td>";
                                    echo "<td>".$row['nov_ptr']."</td>";
                                    echo "<td>".$row['nov_bvr']."</td>";
                                    echo "<td>".$row['nov_ml']."</td>";                                    
                                    echo "<td>".$row['nov_total']."</td>";   
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td>".'December'."</td>";
                                    echo "<td>".$row['dec_lop']."</td>";
                                    echo "<td>".$row['dec_pl']."</td>";
                                    echo "<td>".$row['dec_ptr']."</td>";
                                    echo "<td>".$row['dec_bvr']."</td>";
                                    echo "<td>".$row['dec_ml']."</td>";                                    
                                    echo "<td>".$row['dec_total']."</td>";   
                                    echo "</tr>";
                                }
                                }
//                                echo "<tr>";
//                                echo "<td>Total</td>";
//                                echo "<td>".$lop."</td>";
//                                echo "<td>".$pl."</td>";
//                                echo "<td>".$ptr."</td>";
//                                echo "<td>".$bvr."</td>";
//                                echo "<td>".$ml."</td>";
//                                echo "<td>".$total_all."</td>";
//                                echo "</tr>";
                            ?>
                            </tbody>
                            </table>
                                <div class="form-group row">
                                    <div class="col-xs-4 col-sm-offset-4">
                                        <!--<input type="submit" class="btn btn-primary" value="Export >>">-->
                                    </div>
                                </div>
                            <!--</form>-->
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>