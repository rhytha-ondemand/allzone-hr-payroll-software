<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Appraisal Leave </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                         <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <?php 
                            if(isset($error)){
                                echo '<div class="alert alert-danger" style="text-align:center">'.$error.'</div>';
                            }
                            ?>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="appindividual" role="form" method="post" action="leave/appraisal_individualdet">
                                <br>                                
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_id">Emp. No</label>
                                    <div class="col-xs-4">
                                      <select class="validate[required]  form-control" name="emp_id" id="emp_id" data-placeholder="Select Employee No">
                                          <option value=""></option>
                                      </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">Year</label>
                                        <div class="col-xs-4">
                                            <select class="validate[required] form-control" id="year" name="year" data-placeholder="Select Year">
                                                <option value=''></option>
                                                <!--<option value=0>All Year </option>-->
						<?php 
                                                $st_year = 2015;
                                                $year = date("Y");
                                                for($i= $st_year;$i<=$year+1;$i++){
                                                    echo "<option value='".$i."'>".$i."</option>";
                                                }                                               
                                            ?>
                                            </select>
                                        </div>
                                </div>
<!--                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="dof">From Date</label>
                                      <div class="col-xs-4">
                                          <input type="text" readonly="" class="validate[required] form-control datepicker" placeholder="Select date" name="dof" id="dof">
                                      </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-xs-2" for="dot">To Date</label>
                                    <div class="col-xs-4">
                                        <input type="text" readonly="" class="validate[required] form-control datepicker" placeholder="Select date" name="dot" id="dot">
                                      </div>
                                 </div>-->
                                        
                                <div class="form-group">
                                    <div class="col-xs-3 col-sm-offset-3">
                                        <input type="submit" id="submit" class="btn btn-primary" value="View">
                                     </div>
                                </div>      
                                     
                                
                             </form>  
                            <!--working content end-->
                        </div>
                           
                        </div>
                    </div>
                <!--</div>-->
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
         <script>
        $(document).ready(function(){
            
            $.validationEngine.defaults.scroll = false;
            $("#appindividual").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#appindividual").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#appindividual").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            
            
            $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
//            $( "#dot" ).datepicker({ dateFormat: 'yy-mm-dd' });
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            //                   Branch det       
            var toappend;
            $.ajax({
                type: "POST",
                url: "master/location_all",
                cache: false,
                dataType: "json",                    
                async: false,
                success: function(json) {
                    $('#bname').find('option').remove();                      
                    if(json) {
                         toappend+='<option value=""></option>';
                         $.each(json, function(i, value) {
                             toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';

                        });   
                        $('#bname').append(toappend);
                        $("#bname").trigger('chosen:updated');                        
                    }
                }            
            }); 
                          
//                Branch det  
            $("#bname").change(function(){
                var toappend;
                  var url = "master/emp_id_list";
                  var data =  "bname=" + $(this).val();                
                  $.ajax({
                      type: "POST",
                      url: url,
                      cache: false,
                      dataType: "json",
                      data: data,
                      async: false,
                      success: function(json) {
                          $('#emp_id').find('option').remove();
                          if(json) {
                               toappend ='<option value=""></option>';
                               $.each(json, function(i, value) {
                                   toappend+='<option value='+value['Id']+'>'+value['emp_name']+'</option>';                                
                              });   
                              $('#emp_id').append(toappend);                           
                          }
                           $("#emp_id").trigger('chosen:updated');

                      }            
                  });                 
                    });

            $("#submit").click(function(){
                if ( $("#appindividual").validationEngine('validate')) {
                    return true;
                }
                return false;
            });
          });
        </script>
    </body>
</html>