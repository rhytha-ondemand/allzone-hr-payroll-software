<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View Consolidate</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Appraisal Leave Details - Consolidate </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <br>
                            <table class="table table-striped table-bordered responsive">
                                <thead>
                                <tr>
                                    <th rowspan="2">Sl.No</th>
                                    <th rowspan="2">Emp.No</th>
                                    <th rowspan="2">Emp.Name</th>
                                    <th rowspan="2">DOJ</th>
                                    <th rowspan="2">Designation</th>
                                    <!--<th rowspan="2">Appraisal Period</th>-->
                                    <th rowspan="2">Team</th>
                                    <th colspan="6" style="text-align: center">Leave Details</th>									
                                </tr>
                                <tr>
                                    <th>LOP</th>
                                    <th>PL</th>
                                    <th>PTR</th>
                                    <th>BVR</th>
                                    <th>ML</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    if($array == 0){
                                        echo "<tr><td colspan='12'>No data available in table</td></tr>";
                                    }
                                    else{
                                        foreach($array as $row){
                                            echo "<tr>";
                                            echo "<td>".$i++."</td>";
                                            echo "<td>".$row['emp_id']."</td>";
                                            echo "<td>".$row['emp_name']."</td>";
                                            echo "<td>".$row['doj']."</td>";
                                            echo "<td>".$row['design']."</td>";
//                                            echo "<td>".$row['app_period']."</td>";
                                            echo "<td>".$row['Team']."</td>";
                                            echo "<td>".$row['lop']."</td>";
                                            echo "<td>".$row['pl']."</td>";
                                            echo "<td>".$row['ptr']."</td>";
                                            echo "<td>".$row['bvr']."</td>";
                                            echo "<td>".$row['ml']."</td>";
//                                            $total = $row['lop']+$row['pl']+$row['ptr']+$row['bvr']+$row['ml'];
                                            echo "<td>".$row['total']."</td>";
                                            echo "</tr>";
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>
