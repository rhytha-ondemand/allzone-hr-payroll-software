<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">View</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Appraisal Leave </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" method="post" id="lconsolidate" action="leave/appraisal_consolidatedet">
                                <br>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="department">Department</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" name="department" id="department" data-placeholder="Select Department">
                                                <option selected=""></option>
                                                
                                            </select>
                                        </div>
                                 </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="team">Team</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" name="team" id="team" data-placeholder="Select Team">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                </div>
                                 <div class="form-group">
                                        <label class="control-label col-xs-2" for="project">Current Project</label>
                                        <div class="col-xs-3">
                                            <select class="form-control" id="project" name="project" data-placeholder="Select Project">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                </div>
                                
                                                                
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="designation">Designation</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" id="designation" name="designation" data-placeholder="Select Designation">
                                                <option value=""></option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="control-label col-xs-2" for="month">Month </label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" id="month" name="month" data-placeholder="Select Month">
                                                 <option value=""> </option>
                                                 <option  value= 1>Jan</option>
                                                 <option  value= 2>Feb</option>
                                                 <option  value= 3>Mar</option>
                                                 <option  value= 4>Apr</option>
                                                 <option  value= 5>May</option>
                                                 <option  value= 6>Jun</option>
                                                 <option  value= 7>Jul</option>
                                                 <option  value= 8>Aug</option>
                                                 <option  value= 9>Sep</option>
                                                 <option  value= 10>Oct</option>
                                                 <option  value= 11>Nov</option>
                                                 <option  value= 12>Dec</option>
                                            </select>                                             
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">Year </label>
                                        <div class="col-xs-3">
                                           <select class="validate[required] form-control" id="year" name="year" data-placeholder="Select Year">
                                             <option value=''></option>
                                             <?php 
                                                $st_year = 2015;
                                                $year = date("Y");
                                                for($i= $st_year;$i<=$year+1;$i++){
                                                    echo "<option value='".$i."'>".$i."</option>";
                                                }                                               
                                            ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                           <input type="submit" id="submit" class="btn btn-primary" value="View">
                                        </div>
                                    </div>
                              </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#lconsolidate").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#lconsolidate").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#lconsolidate").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            
            $("select").chosen({disable_search_threshold: 15});           
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
//             Branch det       
            var toappend;
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();                      
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');                        
                        }
                    }            
                }); 
                          
//                Branch det   
            

            $("#bname").change(function(){
                var toappend;
                var data =  "bname=" + $(this).val();                
                var depttoappend = "";
                var desigtoappend = "";
                var prjttoappend = "";
                var repttoappend = "";
                    $.ajax({
                        type: "POST",
                        data:data,
                        url: "master/emp_dynamic",
                        cache: false,
                        dataType: "json",                    
                        async: false,
                        success: function(json) {
                            $('#designation').find('option').remove();
                            $('#department').find('option').remove();
                            $('#project').find('option').remove();
                            $('#team').find('option').remove();
                            if(json){
                                depttoappend='<option value=""></option>';
                                $.each(json.dept, function(i, value) {
                                     depttoappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';                                
                                }); 
                                
                                desigtoappend='<option value=""></option>';
                                $.each(json.design, function(i, value) {
                                     desigtoappend+='<option value='+value['Id']+'>'+value['Design_name']+'</option>';                                
                                });
                                
                                prjttoappend='<option value=""></option>';
                                $.each(json.project, function(i, value) {
                                     prjttoappend+='<option value='+value['Project_Id']+'>'+value['Project_no']+'</option>';                                
                                });
                                
                                repttoappend='<option value=""></option>';
                                $.each(json.teamlist, function(i, value) {
                                     repttoappend+='<option value='+value['reporting_no']+'>'+value['team']+'</option>';                                
                                });
                                
                                $('#designation').append(desigtoappend); 
                                $('#department').append(depttoappend); 
                                $('#project').append(prjttoappend); 
                                $('#team').append(repttoappend); 
                            }
                            $('#designation').trigger('chosen:updated');
                            $('#department').trigger('chosen:updated');
                            $('#project').trigger('chosen:updated');
                            $('#team').trigger('chosen:updated');                             
                              
                    }   
                    });
                
            });
            $("#submit").click(function(){
                if ( $("#lconsolidate").validationEngine('validate')) {
                    return true;
                }
                return false;
            });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>