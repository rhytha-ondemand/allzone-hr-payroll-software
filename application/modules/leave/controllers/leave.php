<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of leave
 *
 */
class leave extends CI_Controller{
    //put your code here
    
    public function __construct() {
        parent::__construct();
        $this->load->model("leave/leave_model","leave");
        $this->load->library('auth');
        //$this->load->library('employee_lib',NULL,'emp');
        //$this->load->helper('cookie');
    }

    /*
     * Leave > Leave > add
     *  Add Leave 
     */    
    public function leave_add(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->load->view('leave/leave/add');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
    }
    
	public function save_emp_leave(){

            $emp=$this->input->post('uid');
            if($emp != ''){
            if($this->auth->checku())
            {
                $result = $this->leave->save_emp_leave();
                if($result){                       
                    $data['result'] = '1';
                    $this->load->view('leave/leave/add',$data);
                }else{
                     $data['result'] = '2';
                    $this->load->view('leave/leave/add',$data);
                }
            }
            else
                redirect("master/login");
            }
            else{
//            $this->load->view('leave/leave/add');
            redirect('leave/leave_add');
        }
		
    }
    /*
     * Leave > Leave > add
     *  Add Leave 
     */    
    public function leave_adddetails()
    {
        $emp_id=$this->input->post('emp_id');
        if($emp_id != ''){
        //$this->load->view('leave/leave/add_details');
		if($this->auth->checku())
		{
                    if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
                        $data=$this->leave->pl_details();
//                        echo '<pre>';print_r($data);die();
                        $this->load->view('leave/leave/add_details',$data);
                    }
                    else
                        $this->load->view('master/noaccess');
		}
		else{
		    redirect ('master/login');
                }
        }
        else{
//            $this->load->view('leave/leave/add');
            redirect('leave/leave_add');
        }
    }
    
    
    /*
     * Leave > Leave > view
     * View leave details
     */
    public function leave_view(){
        if($this->auth->checku())
        {
            $this->load->view('leave/leave/view');
        }
       else{
           redirect("master/login");
       }
    }
    
    public function leave_viewdetail(){
        if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('month', 'Month', 'required|strip_tags|trim|xss_clean');
            $this->form_validation->set_rules('year', 'Year', 'required|strip_tags|trim|xss_clean');
            if ($this->form_validation->run())
            {
                $data['bname']=$this->input->post('bname');
                $data['month']=$this->input->post('month');
                $data['year']=$this->input->post('year');
                $this->load->view('leave/leave/view_detail',$data);
            }
            else {
                $this->load->view('leave/leave/view');
            }

        }
       else{
           redirect("master/login");
       }
        
    }
    public function leave_all() {
		$data=$this->leave->leave_all();                
		echo json_encode($data);
       /* $array=array(array("Id"=>1,"emp_id"=>"AMSC1","emp_name"=>"Mohammed"),array("Id"=>2,"emp_id"=>"AMSC2","emp_name"=>"Mohammed"));
        echo json_encode(array("aaData"=>$array));*/
    }
    
    public function leave_viewcard(){
        
//        redirect("leave/leave_card");
//        $this->leave_card();
    }


    /*
     * Leave > Leave > view -> view card
     * Leave card 
     */
    
    public function leave_card(){
        if($this->auth->checku())
        {
            $emp_id=$this->input->post('emp_id');
            $data['emp_id'] = $emp_id;
            if($emp_id != ''){
                    $x=$this->leave->leave_card($data['emp_id']);		
                    $data['emp_name']=$x[0]['Name'];
                    $data['doj']=$x[0]['D_of_join'];
                    $data['pl_cr']=$x[0]['Pl_credit'];
                    $data['pl_avail']=$x[0]['pl_available'];;
                    $data['id']=$this->input->post('Id');
                    $data['month']=$this->input->post('month');
                    $data['year']=$this->input->post('year');
                            if($data['emp_id']=='' && $data['month']=='' &&$data['year']=='')
                            {
                                    $data['emp_id']=$this->input->get('emp_id');
                                    $data['month']=$this->input->get('month');
                            $data['year']=$this->input->get('year');
                            }

                    $this->load->view('leave/leave/leavecard',$data);
            }else{
                $this->load->view('leave/leave/view');            
            }
        }
       else{
           redirect("master/login");
       }
    }
    public function leave_c(){
		
		$x=$this->leave->leave_c();
		//echo json_encode(array("aaData"=>$x));
		echo json_encode($x);
        /*$array=array(array("Id"=>1,"enter_date"=>"2016-01-01","leave_date"=>"2015-12-25","pl"=>"1","lop"=>"","ptr"=>"","bvr"=>"","ml"=>"","coff"=>"","coff_wdate"=>"","coff_remain"=>"","reason"=>"Went to exam"),array("Id"=>2,"enter_date"=>"2016-02-01","leave_date"=>"2016-01-25","pl"=>"","lop"=>"","ptr"=>"","bvr"=>"","ml"=>"","coff"=>"1","coff_wdate"=>"2016-01-20","coff_remain"=>"1","reason"=>"Exam"),array("Id"=>2,"enter_date"=>"2016-02-01","leave_date"=>"2016-01-26","pl"=>"","lop"=>"","ptr"=>"","bvr"=>"","ml"=>"","coff"=>"1","coff_wdate"=>"2016-01-21","coff_remain"=>"","reason"=>"Exam"));
        echo json_encode(array("aaData"=>$array));*/
    }
    public function leavecard_update(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                
                if ($this->form_validation->run())
                {
                    $emp_id=  $this->input->post('emp_id');        
                    $month =  $this->input->post('month');
                    $year = $this->input->post('year');        
                    $data['Id']=  $this->input->post('Id');

                    $data['emp_id']=  $emp_id;
                    $data['month']= $month;
                    $data['year']=  $year;
//                    if($emp_id !=''){            
                        $result=$this->leave->leavecard_update(); 
                        if($result){
                            $result[0]['month']= $month;
                            $result[0]['year']= $year;
                            $this->load->view("leave/leave/update_leavecard",$result[0]);
                        }else{
                           $this->load->view('leave/leave/leavecard',$data); 
                        }
//                    }else{
//                         $this->load->view('leave/leave/view');
//                    }  
                }
                else {
                    $this->load->view('leave/leave/view');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
              
    }
    public function leavecard(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'ID', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee ID', 'required|trim|xss_clean');
                $this->form_validation->set_rules('reason', 'Reason', 'required|trim|xss_clean');
                $this->form_validation->set_rules('ldate', 'Leave Date', 'required|trim|xss_clean');
                $this->form_validation->set_rules('type_leave', 'Leave Type', 'required|trim|xss_clean');
                if ($this->form_validation->run()){
                    $emp_id=$this->input->post('emp_id');
                    $data['emp_id'] = $emp_id;
//                    if($emp_id != ''){
                            $xupdate = $this->leave->leavecard_u();
                            $x=$this->leave->leave_card($data['emp_id']);		
                            $data['emp_name']=$x[0]['Name'];
                            $data['doj']=$x[0]['D_of_join'];
                            $data['pl_cr']=$x[0]['Pl_credit'];
                            $data['pl_avail']=$x[0]['pl_available'];;
                            $data['id']=$this->input->post('Id');
                            $data['month']=$this->input->post('month');
                            $data['year']=$this->input->post('year');
                                    if($data['emp_id']=='' && $data['month']=='' &&$data['year']=='')
                                    {
                                            $data['emp_id']=$this->input->get('emp_id');
                                            $data['month']=$this->input->get('month');
                                    $data['year']=$this->input->get('year');
                                    }

                            $this->load->view('leave/leave/leavecard',$data);
//                    }else{
//                        $this->load->view('leave/leave/view');            
//                    }
                } 
                else{
                        $this->load->view('leave/leave/view');            
                    }
            }else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
    }
    public function leavecard_delete(){
        
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $res = $this->leave->leavecard_delete();
                    if($res){
                        echo 4;
                    }else{
                        echo 3;
                    }
                }
                else {
                    echo 2;
                }
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
    } 
    
    public function coffcr_detail(){
        $data = $this->leave->coffcr_detail();        
        $this->load->view('leave/leave/coffcr_details',$data);
    }
    
    
    /*
     * Leave > c_off > add
     */
    public function coff_add(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->load->view('leave/coff_credit/add');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
    }
    public function coff_view(){
        if($this->auth->checku())
        {
            $this->load->view('leave/coff_credit/view');	
        }
       else{
           redirect("master/login");
       }
        
    }
    public function cofff()
    {        
	if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('coff_wdate', 'Coff worked Date', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('cr_adate', 'Credit Availed Date', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('cr_wdate', 'Credit worked Date', 'required|trim|xss_clean');
                $this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean');
                $lstatus = $this->input->post('status');
                if($lstatus == 1 || $lstatus == '1'){
                    $this->form_validation->set_rules('coff_wdate', 'Coff worked Date', 'required|trim|xss_clean');                    
                }else  if($lstatus == 2 || $lstatus == '2'){
                    $this->form_validation->set_rules('cr_adate', 'Credit Availed Date', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('cr_wdate', 'Credit worked Date', 'required|trim|xss_clean');                    
                }
                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $data=$this->leave->cofff();	
                    echo 4; 
                }
                else {
                    echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
        else{
            echo 0;
        }
		
    }
    /*
     * Leave > c_off > Add -> add details
     */
    public function coff_details(){
	if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('loption', 'Leave Option', 'required|strip_tags|trim|xss_clean');
            $this->form_validation->set_rules('ltype', 'Leave Type', 'required|strip_tags|trim|xss_clean');
            $this->form_validation->set_rules('fdate', 'From date ', 'required|trim|xss_clean');
            $this->form_validation->set_rules('tdate', 'To date', 'required|trim|xss_clean');
            if ($this->form_validation->run())
            {
                    $loption = $this->input->post('loption');
                    $ltype = $this->input->post('ltype');
                    $bname=$this->input->post('bname');	
                    $fdate = $this->input->post('fdate');
                    $tdate = $this->input->post('tdate');
                    if($loption == 1){
                        if($ltype == 1){
                            $data['bname']=$bname;
                            $data['fdate']=$fdate;
                            $data['tdate']=$tdate;
                            $this->load->view('leave/coff_credit/coff_view_details',$data);
                        }else{                          
                            $data['emp_id']=$this->input->post('emp_id');
                            $string = $this->input->post('emp_name');
                            $regex = '#[^()]*\((([^()]+|(?R))*)\)[^()]*#';
                            $replacement = '\1';                                
                            $emp_name = preg_replace($regex, $replacement, $string);  
                            $data['emp_name']=  $emp_name;
                            $data['bname']=$bname;
                            $data['fdate']=$fdate;
                            $data['tdate']=$tdate;
                            $this->load->view('leave/coff_credit/emp_coff_view_details',$data);
                        }
                    }else if($loption == 2) {
                        if($ltype == 1){
                            $data['bname']=$bname;
                            $data['fdate']=$fdate;
                            $data['tdate']=$tdate;
                           $this->load->view('leave/coff_credit/credit_view_details',$data);
                        }else{
                            $fdate = $this->input->post('fdate');
                            $tdate = $this->input->post('tdate');

                            $data['emp_id']=  $this->input->post('emp_id');
                            $string = $this->input->post('emp_name');
                            $regex = '#[^()]*\((([^()]+|(?R))*)\)[^()]*#';
                            $replacement = '\1';                                
                            $emp_name = preg_replace($regex, $replacement, $string);  
                            $data['emp_name']=  $emp_name;
                            $data['bname']=$bname;
                            $data['fdate']=$fdate;
                            $data['tdate']=$tdate;
                            $this->load->view('leave/coff_credit/emp_credit_view_details',$data);
                        }
                    }else{
                        $this->load->view('leave/coff_credit/view');
                    }
            }
            else{
               $this->load->view('leave/coff_credit/view');
            }

        }
       else{
           redirect("master/login");
       }
	   
        
    }
    function coff_delete(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->coff_delete();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }  
                }
                else {
                        echo 2;
                }
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
       
    }
    function coff_update(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('coff_wdate', 'Coff Worked date', 'required|trim|xss_clean');
                $this->form_validation->set_rules('coff_adate', 'Coff Avail date', 'required|trim|xss_clean');                
                if ($this->form_validation->run())
                {
                    $result = $this->leave->coff_update();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }
                }
                else {
                        echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
        
    }
    function credit_delete(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->credit_delete();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }  
                }
                else {
                        echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
        
    }
    function credit_update(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {                
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('credit_wdate', 'Credit Worked date', 'required|trim|xss_clean');
                $this->form_validation->set_rules('credit_adate', 'Credit Avail date', 'required|trim|xss_clean');                
                if ($this->form_validation->run())
                {
                    $result = $this->leave->credit_update();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }   
                }
                else {
                        echo 2;
                }
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
    }
    
/*    Id
        Emp_id
        CoffEntryDate
        CoffworkedDate
        CoffAvailedDate   */
    public function coff_overall(){
		
		$data=$this->leave->coff_overall();
		echo json_encode($data);
        /*$array=array(array("DT_RowId"=>10,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>""),array("DT_RowId"=>2,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>"2015-02-02"),array("DT_RowId"=>3,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>"2015-02-02"));
        echo json_encode(array("aaData"=>$array));*/
    }
    public function coff_empdet(){
//        $array=array(array("DT_RowId"=>10,"Id"=>"1","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>""),array("DT_RowId"=>2,"Id"=>"1","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>"2015-02-02"),array("DT_RowId"=>3,"Id"=>"1","CoffEntryDate"=>"2015-02-02","CoffworkedDate"=>"2015-02-02","CoffAvailedDate"=>"2015-02-02"));
        $array=$this->leave->coff_empdet();
        echo json_encode($array);
    }
    public function credit_overall(){
        
        $array =  $this->leave->credit_overall();
        echo json_encode($array);
//        $array=array(array("DT_RowId"=>10,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-06","CrLeaveworkedDate"=>""),array("DT_RowId"=>2,"Id"=>"1","Emp_id"=>"AMSC2","Emp_name"=>"B. Nazeer Ahmed","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-22","CrLeaveworkedDate"=>""),array("DT_RowId"=>3,"Id"=>"1","Emp_id"=>"AMSC3","Emp_name"=>"B. Nazeer Ahmed","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-16","CrLeaveworkedDate"=>""));
//        echo json_encode(array("aaData"=>$array));
    }
    public function credit_empdet(){
        
        $array = $this->leave->credit_empdet();
        echo json_encode($array);
//        $array=array(array("DT_RowId"=>10,"Id"=>"1","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-06","CrLeaveworkedDate"=>""),array("DT_RowId"=>2,"Id"=>"1","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-22","CrLeaveworkedDate"=>""),array("DT_RowId"=>3,"Id"=>"1","CrLeaveEntryDate"=>"2015-02-02","CrLeaveDate"=>"2015-02-16","CrLeaveworkedDate"=>""));
//        echo json_encode(array("aaData"=>$array));
    }
    
    /*
     * Leave > Credit Leave > Add
     */
    public function creditleave_add(){
        $this->load->view('leave/credit_leave/add');
    }
     /*
     * Leave > c_off > Add -> add details
     */
    public function creditleave_adddetails(){
        $this->load->view('leave/credit_leave/add_details');
    }
    
    /*
     * Leave > Credit Leave > view
     */
    public function creditleave_view(){
        $this->load->view('leave/credit_leave/view');
    }
    
    /*
     * Leave > Credit Leave > view -> details
     */
    public function creditleave_details(){
        $this->load->view('leave/credit_leave/details');
    }
    
    /*
     * Leave > Status > Monthly salary(current month leave status)
     */
    public function leave_status(){
        $this->load->view('leave/status/current_month');
    }
    
    /*
     * Leave > status > Monthly
     */
    public function status_monthly(){
        if($this->auth->checku())
        {
            $this->load->view('leave/status/monthly');
        }else{
           redirect("master/login");
       }
    }
    
   public function status_monthlydet(){
       if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
            $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
            if ($this->form_validation->run())
            {
                $month = $this->input->post('month');    
                $bname = $this->input->post('bname');    
                $year = $this->input->post('year');  
//                if($bname !=''){
                     $data['month']=$month ;
                     $data['year']= $year;
                     $array = $this->leave->monthlydet();	
                     if($array[0]['emp_id'] != ''){
                         $data['array']= $array;
                     }else{
                         $data['array']= '';
                     }	
                     $this->load->view('leave/status/monthly_details',$data);
//                 }else{
//                     $this->load->view('leave/status/monthly');
//                 }
            }
            else {
                $this->load->view('leave/status/monthly');
            }
            
        }else{
           redirect("master/login");
        }
        
    }
    
    /*
     * Leave > Status > Teamwise
     */
    public function status_teamwise(){
        if($this->auth->checku())
        {
            $this->load->view('leave/status/teamwise_leave');
        }else{
           redirect("master/login");
       }
        
    }
    
    /*
     * Leave > Status > Teamwise
     
    public function status_teamwisedetails(){

        $bname = $this->input->post('bname');
        $team = $this->input->post('team');
        $year = $this->input->post('year');
        
        $data['bname']=$bname;
        $data['team']=$team;
        $data['year']=$year;
//        $data['array']=array(array("emp_id"=>"AMSC1","emp_name"=>"B. Nazeer Ahmed","team"=>"Management","remarks"=>""),array("emp_id"=>"AMSC2","emp_name"=>"J. Sathish Kumar","team"=>"Management","remarks"=>""),array("emp_id"=>"AMSC3","emp_name"=>"W. thomas Lionel Vijayakumar","team"=>"Management","remarks"=>""),array("emp_id"=>"AMSC4","emp_name"=>"K. Shanthi","designation"=>"Accountant","team"=>"Management","remarks"=>""));
        //$this->load->view('leave/status/teamwise_leavedet',$data);
		$array = $this->leave->status_teamwisedetails();
    }*/
    public function status_teamwisedetails(){
        if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
            $this->form_validation->set_rules('fdate', 'From date', 'required|trim|xss_clean');
            $this->form_validation->set_rules('tdate', 'To date', 'required|trim|xss_clean');
            if ($this->form_validation->run())
            {
                $bname = $this->input->post('bname');
                $team = $this->input->post('team');
//                $year = $this->input->post('year');
//                $fdate = $this->input->post('fdate');
//                $tdate = $this->input->post('tdate');
                $data['bname']=$bname;
                
//                $data['year']=$year;

                $array = $this->leave->status_teamwisedetails();    
//                print_r($array);die();
                if($array){
                    $data['array']= $array;
//                    $data['team']= "of ".$array['Team'];
                }else{
                    $data['array']= 0;
//                    $data['team']='';
                }
                $this->load->view('leave/status/teamwise_leavedet',$data);

            }
            else {
                $this->load->view('leave/status/teamwise_leave');
            }
        }
       else{
           redirect("master/login");
       }
                
    }
    
    
    /*
     * Leave > Appraisal > Individual
     */
    public function appraisal_individual(){
        if($this->auth->checku())
        {
            $this->load->view('leave/appraisal/individual');	
        }
       else{
           redirect("master/login");
       }
        
    }
    
    public function appraisal_individualdet(){
        if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('emp_id', 'Employee ID', 'required|trim|xss_clean');
            $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
            if ($this->form_validation->run())
            {
//                $emp_id = $this->input->post('emp_id');        
//                if($emp_id != ''){
                    $array = $this->leave->appraisal_individualdet();
                    if($array){
                        $data['array']=array($array);
                        $this->load->view('leave/appraisal/individual_details',$data);
                    }else{
                        $data['emp_id']=  $this->input->post();
                        $data["error"]="No Records found";
                        $this->load->view('leave/appraisal/individual',$data);       
                    }
//                }else{
//                    redirect('leave/appraisal_individual');
//                }
            }
            else {
                $this->load->view('leave/appraisal/individual');
            }

        }
       else{
           redirect("master/login");
       }

                
  
    }

    /*
     * Leave > Appraisal > Individual
     */
    public function appraisal_consolidate(){
        if($this->auth->checku())
        {
            $this->load->view('leave/appraisal/consolidate');	
        }
       else{
           redirect("master/login");
       }
        
    }
    
    public function appraisal_consolidatedet(){

        if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
            $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
            $this->form_validation->set_rules('department', 'Department', 'required|trim|xss_clean');
            $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('project', 'Project', 'required|trim|xss_clean');
            $this->form_validation->set_rules('designation', 'Designation', 'required|trim|xss_clean');
            if ($this->form_validation->run())
            {
//                $bname = $this->input->post('bname');
//                if($bname != ''){
                $result = $this->leave->appraisal_consolidatedet();
                if($result){
                    $data['array']=$result;
                }else{
                    $data['array']='0';
                }
                $this->load->view('leave/appraisal/consolidate_details',$data);        
//            }else{
//                redirect('leave/appraisal_consolidate');
//            }

            }
            else {
               $this->load->view('leave/appraisal/consolidate');	
            }

        }
       else{
           redirect("master/login");
       }          

        
    }
    
    /*
     *  Leave > SDA > Add
     */
    public function sda_add(){        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->load->view('leave/sda/add');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    public function sda_details(){
        $this->load->view('leave/sda/add_details');
    }
    public function sdansa_add(){
        
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
                $this->form_validation->set_rules('type', 'Type', 'required|trim|xss_clean');
                $type = $this->input->post('type');
                if($type == 1 || $type == '1'){
                  $this->form_validation->set_rules('sda_date', 'SDA Date', 'required|trim|xss_clean');  
                }else if($type == 2 || $type == '2'){
                    $this->form_validation->set_rules('nsa_fdate', 'NSA From Date', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('nsa_tdate', 'NSA To date', 'required|trim|xss_clean');                      
                }else if($type == 3 || $type == '3'){
                    $this->form_validation->set_rules('ha_date', 'Ha Date', 'required|trim|xss_clean');
                }
                
//                $this->form_validation->set_rules('sda_date', 'SDA Date', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('nsa_fdate', 'NSA From Date', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('nsa_tdate', 'NSA To date', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('ha_date', 'Ha Date', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $array = $this->leave->sdansa_add();
                    if($array){
                        echo 4;
                    } else{
                        echo 3;
                    }
                }
                else {
                    echo 2;
                }
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
        
        
    }
    /*
     *  Leave > SDA > View
     */
 public function sda_view(){
        if($this->auth->checku())
        {
            $this->load->view('leave/sda/view');
        }
        else{
            redirect("master/login");
        }
       
    }
    public function allowance_details(){   
        if($this->auth->checku())
        {
            $this->form_validation->set_rules('bname', 'Branch', 'required|trim|xss_clean');
            $this->form_validation->set_rules('type', 'Type', 'required|trim|xss_clean');
            $this->form_validation->set_rules('from_date', 'From date', 'required|trim|xss_clean');
            $this->form_validation->set_rules('to_date', 'To date', 'required|trim|xss_clean');
            if ($this->form_validation->run()){                
                $allow_type = $this->input->post('type');
                $from_date = $this->input->post('from_date');
                $to_date = $this->input->post('to_date');
                $bname = $this->input->post('bname');
                
                $data['from_date']=$from_date;
                $data['to_date']=$to_date;
                $data['bname']=$bname;
                if($allow_type == 1){
                    $this->load->view('leave/sda/sda_view_details',$data);
                }else if($allow_type == 2){
                    $this->load->view('leave/sda/nsa_view_details',$data);
                }
                else if($allow_type == 3){
                    $this->load->view('leave/sda/ha_view_details',$data);
                }
                else{
                    $this->load->view('leave/sda/view');
                }
                
            }else{
                $this->load->view('leave/sda/view');
            }
        }
        else{
            redirect("master/login");
        }
    }
    public function allow(){
        $data['from_date']="04-02-2015";
        $data['to_date']="04-02-2015";
        $this->load->view('leave/sda/nsa_view_details',$data);
    }
    public function sda_all(){
        
        $array = $this->leave->sda_all();
        if($array == null){
            $array = '';
        }        
//           $array=array(array("DT_RowId"=>10,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","desig"=>"mgmt","Sda_date"=>"2015-02-02"),array("DT_RowId"=>2,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","desig"=>"Mgmt","Sda_date"=>"2015-02-02"),array("DT_RowId"=>3,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","desig"=>"Mgmt","Sda_date"=>"2015-02-02"));
         echo json_encode(array("aaData"=>$array));
    }
    
    public function nsa_all(){
//        $array=array(array("DT_RowId"=>10,"Id"=>"1","Emp_id"=>"AMSC1","Emp_name"=>"B. Nazeer Ahmed","desig"=>"Mgmt","Nsa_date"=>"2015-02-02","Team"=>"Management","No_of_days"=>"10","Amount"=>""),array("DT_RowId"=>20,"Id"=>"1","Emp_id"=>"AMSC2","Emp_name"=>"Sathish Kumar","desig"=>"Mgmt","Nsa_date"=>"2015-02-02","Team"=>"Management","No_of_days"=>"10","Amount"=>""),array("DT_RowId"=>30,"Id"=>"1","Emp_id"=>"AMSC3","Emp_name"=>"Thomas","desig"=>"Mgtm","Nsa_date"=>"2015-02-02","Team"=>"Mgmt","No_of_days"=>"10","Amount"=>""));
        
        $array = $this->leave->nsa_all();
        if($array == null){
            $array = '';
        }
        echo json_encode(array("aaData"=>$array));
    }
    public function nsa_emp_all(){
//        $array=array(array("DT_RowId"=>10,"Id"=>"1","Entry_date"=>"2015-02-08","Nsa_date"=>"2015-02-07","No_of_days"=>"1","Amount"=>""));
        $array = $this->leave->nsa_emp_all();
        if($array == null){
            $array = '';
        }
        echo json_encode(array("aaData"=>$array));
    }
    public function sda_update(){
        
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('sda_date', 'SDA DATE', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->sda_update();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }
                }
                else {
                    echo 2;
                }                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
               
    }
    public function sda_delete(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->sda_delete();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }  
                }
                else {
                        echo 2;
                }                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
        
    }
    public function nsa_update(){
       
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('nsa_fdate', 'NSA FROM DATE', 'required|trim|xss_clean');
                $this->form_validation->set_rules('nsa_tdate', 'NSA TO DATE', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->nsa_update();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }
                }
                else {
                    echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
    }
    public function nsa_delete(){
       if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->nsa_delete();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }  
                }
                else {
                        echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       } 
        
    }
    public function nsa_empdetails(){
//        echo"";
//        print_r($this->input->post());
        $data['from_date']=$this->input->post('from_date');
        $data['to_date']=$this->input->post('to_date');
        $data['emp_id']=$this->input->post('emp_id');
        $data["emp_name"]=$this->input->post('emp_name');
        $this->load->view('leave/sda/nsa_view_detail_emp',$data);
    }
    
    
    
    /*
     *  Hoilday Allowances
     */
    public function ha_update(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('ha_date', 'HA DATE', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->ha_update();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }
                }
                else {
                    echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
    }
    public function ha_delete(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
            {
                $this->form_validation->set_rules('Id', 'Id', 'required|trim|xss_clean');
                $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result = $this->leave->ha_delete();
                    if($result){
                        echo 4;
                    }else{
                        echo 3;
                    }  
                }
                else {
                        echo 2;
                }
                
            }
            else{
                echo 1;
            }
        }
       else{
           echo 0;
       }
        
    }
    public function ha_all(){
        $array = $this->leave->ha_all();
        if($array == null){
            $array = '';
        }
        echo json_encode(array("aaData"=>$array));
    }
    
    
    /*
     *  Leave > NSA > Add
     */
    public function nsa_add(){
        $this->load->view('leave/nsa/add');
    }
    public function nsa_adddet(){
        $this->load->view('leave/nsa/add_details');
    }
    /*
     *  Leave > NSA > View
     */
    public function nsa_view(){
        $this->load->view('leave/nsa/view');
    }
    
    /*
     * Leave > NSA > view -> details
     */
    public function nsa_details(){
        $this->load->view('leave/nsa/view_details');
    }
    public function nsa_completedetail(){
        $this->load->view('leave/nsa/view_details2');
    }
    
	
	
	
	/* additional */
	public function date_range(){
		
		$d1='2016-01-13';
		$d2='2016-01-31';
		 $result = $this->leave->date_range($d2,$d1);
		 echo "<pre>";
		 print_r($result);
	}
	
	
}
