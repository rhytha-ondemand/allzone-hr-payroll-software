<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of report_model
 *
 * @author RHYTHA-JRPHP
 */
class report_model extends CI_Model {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        $this->load->library('report_lib',NULL,'rep');
    }
    
    public function birthday_list(){            
        $result = $this->rep->birthday_list();
        return $result;
    }
    
    public function appraisal_list(){
        $result = $this->rep->appraisal_list();
        return $result;
    }
    
    public function confirmation_list(){
        $result = $this->rep->confirmation_list();
        return $result;
    }
    public function entry_list(){
        $result = $this->rep->entry_list();
        return $result;
    }
    public function exit_list(){
        $result = $this->rep->exit_list();
        return $result;
    }
    public function employee_report_list(){
        $result = $this->rep->employee_report_list();
        return $result;
    }
    
    public function sda_list(){
        $result = $this->rep->sda_list();
        return $result;
    }
    
    public function nsa_list(){         
        $result = $this->rep->nsa_list();       
        return $result;
    }
    
    public function ha_list(){
        $result = $this->rep->ha_list();
        return $result;
    }


    public function it_update_detail(){
        $result = $this->rep->it_update_detail();
        return $result;
    }
}
