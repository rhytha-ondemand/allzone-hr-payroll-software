<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of report
 *
 * @author RHYTHA-JRPHP
 */
class report extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("report/report_model",'report');
        $this->load->library('auth');
    }
    /*
     *  Birthday
     */
    public function birthday(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('hr/birthday_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    /*
     * 
     */
    public function birthday_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {                
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('dept', 'Department', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
                
                if ($this->form_validation->run())
                {
                    $result['month']=  $this->input->post('month');
                    
                    $array = $this->report->birthday_list();
                    
                    $result['array'] = $array;
//                    echo "<pre>";print_r($array);die();
                    $this->load->view('hr/birthday_list',$result);
                }else{                   
                    $this->load->view('hr/birthday_detail');
                }
                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    
    
    
    /*
     *  Appraisal
     */
    
    public function appraisal(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('hr/appraisal_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function appraisal_list(){
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('dept', 'Department', 'required|trim|xss_clean');                
//                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('desig', 'Designation', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('project', 'Project', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Appraisal Month', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Appraisal year', 'required|trim|xss_clean');
                
                if ($this->form_validation->run())
                {
                    $result['month']=  $this->input->post('month');
                    
                    $array = $this->report->appraisal_list();
                    
                    $result['array'] = $array;
//                    echo "<pre>";print_r($array);die();
                    $this->load->view('hr/appraisal_list',$result);
                }else{     
                    $this->load->view('hr/appraisal_detail');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    
    /*
     *  Cnnfirmation screen
     */
    public function confirmation(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('hr/confirmation_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    /*
     * 
     */
    
    public function confirmation_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {                
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('dept', 'Department', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
                
                if ($this->form_validation->run())
                {
                    $result['month']=  $this->input->post('month');
                    
                    $array = $this->report->confirmation_list();
                    
                    $result['array'] = $array;
//                    echo "<pre>";print_r($array);die();
                    $this->load->view('hr/confirmation_list',$result);
                }else{                   
                    $this->load->view('hr/confirmation_detail');
                }
                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
    }
    public function entry_exit(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('hr/entry_exit_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function entry_exit_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {                
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('dept', 'Department', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('project', 'Project', 'required|trim|xss_clean');
                $this->form_validation->set_rules('from_period', 'From Period', 'required|trim|xss_clean');
                $this->form_validation->set_rules('to_period', 'To Period', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {
                    $result['from_date']= $this->input->post('from_period');
                    $result['to_date']= $this->input->post('to_period');
                     $status=  $this->input->post('status');
                     if($status == 1 || $status == "1" ){                        
                         
                        $array = $this->report->entry_list();
                        $result['array'] = $array;                        
//                        echo "<pre>";print_r($array);die();
                        $this->load->view('hr/entry_list',$result);
                     }
                     else {                         
                        $array = $this->report->exit_list();
                        $result['array'] = $array;                        
//                        echo "<pre>";print_r($array);die();
                        $this->load->view('hr/exit_list',$result);
                     }
                }else{                   
//                    echo validation_errors();
                    $this->load->view('hr/entry_exit_detail');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
    }
    public function employee_report(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('hr/employee_report_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
     public function employee_report_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {                
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('dept', 'Department', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('team', 'Team', 'required|trim|xss_clean');
//                $this->form_validation->set_rules('project', 'Project', 'required|trim|xss_clean');
                $this->form_validation->set_rules('minimum', 'Project', 'required|trim|xss_clean');
                $this->form_validation->set_rules('maximum', 'Project', 'required|trim|xss_clean');
                if ($this->form_validation->run())
                {                     
                    $array = $this->report->employee_report_list();
                    $result['array'] = $array;                        
                    $this->load->view('hr/employee_report_list',$result);                     
                }else{                   
                    $this->load->view('hr/employee_report_detail');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
        else{
           redirect("master/login");
       }
    }
    
    public function allowance_details(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('salary/allowance_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
        
        
    }
    
    public function allowance_list(){
         if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {                
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('status', 'status', 'required|trim|xss_clean');
                $this->form_validation->set_rules('from_period', 'From period', 'required|trim|xss_clean');
                $this->form_validation->set_rules('to_period', 'To period', 'required|trim|xss_clean');
                $status = $this->input->post('status');
             /*   if($status == 2 || $status == "2"){
                    $this->form_validation->set_rules('bank_name', 'Bank name', 'required|trim|xss_clean');    
                }*/
                if ($this->form_validation->run())
                {                     
//                 echo"<pre>"; print_r($this->input->post());
                    if($status == 1 || $status == '1'){
                        $array = $this->report->sda_list();
                        
                         $result['array']=$array;
                        $this->load->view('salary/sda_list',$result);
                    }else if($status == 2 || $status == '2'){
                        $array =  $this->report->nsa_list();                        
//                        echo"<pre>";print_r($array);die();
                        
                        $result['array']=$array;
                        $this->load->view('salary/nsa_list',$result);
                    }else{
                        $array = $this->report->ha_list();
//                        echo"<pre>";print_r($array);die();
                        
                        $result['array']=$array;
                        $this->load->view('salary/ha_list',$result);
                    }
                    
                }else{                   
//                    echo validation_errors();
                    $this->load->view('salary/allowance_detail');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
        else{
           redirect("master/login");
       }
    }
    
    public function form_detail(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/form_details');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function form_list(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('frm', 'status', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                $status = $this->input->post('frm');
                if($status != 4 || $status != "4"){
                    $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');    
                }
                if ($this->form_validation->run())
                {                     
//                 echo"<pre>"; print_r($this->input->post());
                    if($status == 1 || $status == '1'){
                        $this->load->view('it/form_s');
                    }else if($status == 2 || $status == '2'){
                        $this->load->view('it/form_o');
                    }else if($status == 3 || $status == '3'){
                        $this->load->view('it/form_5');
                    }
                    else{
                        $this->load->view('it/form_III');
                    }
                    
                }else{                   
//                    echo validation_errors();
                    $this->load->view('salary/allowance_detail');
                }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    
    public function prof_tax(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/prof_tax_det');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function proftax_create(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('month', 'Month', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                 if ($this->form_validation->run()){
//                     echo '<pre>';print_r($this->input->post());
                     $this->load->view('it/prof_tax_list');  
                 }else{
                    $this->load->view('it/prof_tax_det');     
                 }                
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }


    public function it_create(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/it_create_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }

    public function it_details(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->load->view('it/it_update_detail');
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
    public function it_update_detail(){
        if($this->auth->checku())
        {
            if ($this->acl->has_permission(strtolower( __CLASS__), 'view'))
            {
                $this->form_validation->set_rules('bname', 'Branch name', 'required|trim|xss_clean');
                $this->form_validation->set_rules('operation', 'Operation', 'required|trim|xss_clean');
                $this->form_validation->set_rules('year', 'Year', 'required|trim|xss_clean');
                $operation = $this->input->post('operation');
                if($operation == 2 || $operation == "2"){
                    $this->form_validation->set_rules('emp_id', 'Employee Id', 'required|trim|xss_clean');
                }

                if ($this->form_validation->run())
                { 
//                    echo '<pre>';print_r($this->input->post());
                    if($operation == 1 || $operation == "1"){
                        echo "Created";
                    }else if($operation == 2 || $operation == "2") {
//                        $result = $this->report->it_update_detail();
                        $this->load->view('it/it_update_emp');
                    }else{
                        $this->load->view('it/it_update_detail');
                    }                    

                    
                }else{
                    $this->load->view('it/it_update_detail');
                }
            }
            else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
        }
       else{
           redirect("master/login");
       }
    }
    
}
