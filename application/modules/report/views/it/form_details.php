<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >Forms</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Form </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" id="form_details" method="post" action="report/form_list">                                
                                <br>                                
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" data-rel="chosen"  class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                
                                
                                <div class="spacer30"></div>
                                    <div class="form-group">
                                            <label class="control-label col-xs-2"  for="inlineRadioOptions">Form Type</label><span ></span>
                                            <div class="col-xs-5" id="radioDiv">
                                            <label class="radio-inline">
                                                <input type="radio" name="frm" checked="" id="frm1" value="1"> Form S
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="frm" id="frm2" value="2"> Form O
                                            </label>
                                             <label class="radio-inline">
                                                <input type="radio" name="frm"  id="frm3" value="3"> Form 5
                                            </label>
                                             <label class="radio-inline">
                                                <input type="radio" name="frm"  id="frm4" value="4"> Form III
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="month">Select Month</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" data-rel="chosen" id="month" name="month"  data-placeholder="Select Month">
                                                <option value=""></option>
                                                <option value ='1'> January </option>
                                                <option value ='2'> February  </option>
                                                <option value ='3'> March </option>
                                                <option value ='4'> April </option>
                                                <option value ='5'> May </option>
                                                <option value ='6'> June </option>
                                                <option value ='7'> July </option>
                                                <option value ='8'> Auguest </option>
                                                <option value ='9'> September </option>
                                                <option value ='10'> October </option>
                                                <option value ='11'> November </option>
                                                <option value ='12'> December </option>
                                            </select> 
                                        </div>
                                    </div>
								
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="year">Select Year</label>
                                        <div class="col-xs-3">
                                            <select class="validate[required] form-control" name="year" id="year" data-rel="chosen" data-placeholder="Select Year">
                                                <option value=""></option>
                                                <?php
                                                    $curr_year = date('Y');
                                                    for($i=2014;$i<=$curr_year;$i++){
                                                        echo "<option value =$i>$i</option>";
                                                    }
                                                
                                                ?>
                                                    
                                                
                                            </select> 


                                        </div>
                                    </div>
									
                                     <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                           <input type="submit" id="submit" class="btn btn-primary" value="Submit>>">
                                        </div>
                                    </div>    
                             </form> 	
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#form_details").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#form_details").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#form_details").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                
        // branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name
            $('#radioDiv input').on('change', function() {
                
                if ($("#radioDiv input[type='radio']:checked").val() == 1) {                
                    $("#month").prop("disabled",false);                                              
                }else if ($("#radioDiv input[type='radio']:checked").val() == 2) {                                        
                    $("#month").prop("disabled",false);
                }else if ($("#radioDiv input[type='radio']:checked").val() == 3) {                                        
                    $("#month").prop("disabled",false);
                }else if ($("#radioDiv input[type='radio']:checked").val() == 4) {                                        
                    $("#month").prop("disabled",true);
                }   
//                alert($("#radioDiv input[type='radio']:checked").val());
                $("#month").trigger('chosen:updated');
            });
                
            $("#submit").click(function(){
                    if ( $("#form_details").validationEngine('validate') ) {
                        return true;
                    }                    
                    return false;
                });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>