<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            label{
                /*font-weight: normal;*/
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >IT Update</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> IT Update </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <form class="form-horizontal" role="form" id="itupdate" method="post" action="report/it2">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="salary">Salary received from Allzone Management Solutions P Ltd</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" id="salary" name="salary" class="validate[min[1],custom[number]] form-control" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="bonus">Bonus / Incentive</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" id="bonus" name="bonus" class="validate[min[1],custom[number]] form-control" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="inputSuccess4">Add Mobile Allowance</label>
                                        </td>
                                        <td>
                                            <label><input  tabindex="1" type=radio checked value="1"  name='mallow_status' id='status1' > Yes &nbsp;&nbsp;</label>
                                            <label><input type=radio tabindex="1" value="2"  name='mallow_status' id='status'> No &nbsp;&nbsp;</label>                                             
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="mobile_allow" id="mobile_allow" class="validate[min[1],custom[number]] form-control" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="inputSuccess4" Style="color:blue;">Less: Exempt U/s. 10</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="convey">Conveyance</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="convey" id="convey" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><label class="control-label" for="convey" Style="color:black;">HRA Exemption</label></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="actual_hra">Least of the below is exempt-<br>Actual HRA Received</label>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="actual_hra" id="actual_hra" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="basic_50p">50% of Basic Pay</label>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="basic_50p" id="basic_50p" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="rent_paid">Rent Paid</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="rent_paid" id="rent_paid" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="sal_10p">Less:10% of Salary</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="sal_10p1" id="sal_10p1" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td>
                                            <input type="text" value="" name="sal_10p2" id="sal_10p2" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td>
                                            <input type="text" value="" name="sal_10p3" id="sal_10p3" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="medical_exp">Medical Exemption</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="medical_exp" id="medical_exp" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="medical_exp_final" id="medical_exp_final" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="hra_exp_final" id="hra_exp_final" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="less_ptax">Less:Professional Tax</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="less_ptax" id="less_ptax" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>                                            
                                            <label class="control-label" for="taxable_salary" Style="color:black;">TAXABLE SALARY</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="taxable_salary" id="taxable_salary" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="taxable_salary" Style="color:black;">INCOME FROM HOUSE PROPERTY</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="rent_ownhouse">Rent From Own House 8,000*12</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="rent_ownhouse" id="rent_ownhouse" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="less_municipai_tax">Less:Municipal Taxs</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="less_municipai_tax" id="less_municipai_tax" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="net_annual_val">Net Annual Value</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="net_annual_val" id="net_annual_val" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="taxable_salary" Style="color:black;">Less : Deduction U/s 24</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="repair_30nav">Repair 30% of NAV</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="repair_30nav" id="repair_30nav" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="interest_loan">Interest on Loan</label>
                                        </td>
                                        <td>
                                            <input type="text" value="" name="interest_loan" id="interest_loan" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="total_house_property" id="total_house_property" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="taxable_salary" Style="color:blue;">Less : Deduction U/s 80C</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="pf">PF</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="pf" id="pf" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <label class="control-label" for="principle_house_loan">PRINCIPAL OF HOUSING LOAN</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="principle_house_loan" id="principle_house_loan" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="lic">LIC - I</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="lic" id="lic" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="less_deduct_80d" Style="color:blue;">Less: Deduction U/s. 80D</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="less_deduct_80d" id="less_deduct_80d" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="medi_insurance">MEDICLAIM INSURANCE</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="medi_insurance" id="medi_insurance" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="less_deduct_80ccf" Style="color:blue;">Less: Deduction U/s. 80CCF</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="less_deduct_80ccf" id="less_deduct_80ccf" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="infrastuct_bond">INFRASTRUCTURE BOND</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="infrastuct_bond" id="infrastuct_bond" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="less_deduct_80g" Style="color:blue;">Less: Deduction U/s. 80G</label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="less_deduct_80g" id="less_deduct_80g" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="longterm_invest">LONG TERM INVESTMENT</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="longterm_invest" id="longterm_invest" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="inputSuccess4"></label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="taxable_income1" id="taxable_income1" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="control-label" for="inputSuccess4"></label>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="taxable_income2" id="taxable_income1" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                             <label class="control-label" for="taxable_income" Style="color:black;">TAXABLE INCOME</label>                                            
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="taxable_income" id="taxable_income1" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="inputSuccess4" Style="color:black;">TAX CALC</label>                                            
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <label class="control-label" for="tax_there">Tax There on</label>                                            
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="tax_there" id="tax_there" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                             <label class="control-label" for="add_cess">Add Cess:3%</label>                                            
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="add_cess" id="add_cess" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                             <label class="control-label" for="total_tax" Style="color:black;">Total Tax</label>                                            
                                        </td>
                                        <td></td>
                                        <td>
                                            <input type="text" value="" name="total_tax" id="total_tax" class="validate[min[1],custom[number]] form-control" >
                                        </td>
                                    </tr>
                                </table>
                                
                                <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                           <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                            </div>
                                    </div> 
                            </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false; 
                $("#itupda4te").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
        });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>