<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
                display: none;
            }
            
        </style> 
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >Entry & Exit</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Entry List From <?php echo $from_date;?> To <?php echo $to_date;?> </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br><br>
                            <table id="entry_list" class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Emp.No</th>
                                        <th>Emp.Name</th>
                                        <th>Designation</th>
                                        <th>Department</th>
                                        <th>Date Of Joining</th>
                                        <th>Team</th>
                                        <th>Project</th>
                                        <!--<th>Remarks</th>-->
                                        <th>CTC</th>
    <!--                                    <th>Gross</th>
                                        <th>Net</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $excel = 1;
                                    if($array){
                                        $i=1;
                                            foreach($array as $row){
                                                echo "<tr>";
                                                echo "<td>".$i++."</td>";
                                                echo "<td>".$row['Emp_id']."</td>";
                                                echo "<td>".$row['Name']."</td>";
                                                echo "<td>".$row['Design']."</td>";
                                                echo "<td>".$row['Dept']."</td>";
                                                echo "<td>".$row['D_of_join']."</td>";
                                                echo "<td>".$row['Project_team']."</td>";
                                                echo "<td>".$row['Project']."</td>";
                                                echo "<td>".$row['ctc']."</td>";
                                                echo "</tr>";
                                            }
                                        
                                    }else{
                                        $excel = 0;
                                        echo "<tr><td colspan='9'>No data available in table</td></tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php 
                                if($excel){
                                   ?>
                            
                            <div class="row">
                                <div class="col-sm-offset-5">
                                   <div class="col-xs-2">
                                       <input type="submit" id="submit" class="btn btn-primary" value="Export">
                                       <!--<input type="submit" id="submit" class="btn btn-success" value="Export">-->
                                    </div>                                    
                                </div>
                            </div>
                            <?php 
                                }
                            ?>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                $.fn.dataTableExt.sErrMode = 'throw';
                $("#submit").click(function(){
                    $("#removetr").remove();
                    $("thead").prepend("<tr style='display:none;' id='removetr' data-tableexport-display='always' ><th colspan=7 > Entry List From <?php echo $from_date;?> To <?php echo $to_date;?> </th></tr>");
                    $("select option:last").after("<option value='500'>500</option>");
                     var select = $("select").val();
                    var page = $('.pagination .active a').text();
                    var select_last = $('select option:last-child').val();
                    $("select").val(select_last);
                    $("select").change();    
                    $('#entry_list').tableExport({type:'excel',escape:'false'});
                    $("select").val(select);
                    $("select").change(); 
                    $("a:contains("+page+")").click();
                });
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
        <script type="text/javascript" src="assets/js/html2canvas.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/tableExport.js"></script>
    </body>
</html>