<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
                display: none;
            }
            
        </style> 
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >Employee report</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>Employee  report </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="employee_report" class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                <thead>
                                    <tr>
                                        <th> S.No </th>
                                        <th> Emp. No </th>
                                        <th> Emp. Name </th>
                                        <th> Total Years of Exp </th>
                                        <th> Relevant experience </th>
                                        <th> Years @ Allzone </th>
                                        <th> Joining Designation </th>
                                        <th> Present Designation </th>
                                        <th> Joining CTC </th>
                                        <th> Present CTC </th>
                                        <th> No Of Appraisal </th>
                                        <th> Re-Fixation </th>
                                        <th> Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $excel = 1;
                                    // echo'<pre>'; print_r($array);
                                        if($array != ''){
                                            $i=1;
                                                foreach($array as $row){
                                                    echo "<tr>";
                                                    echo "<td>".$i++."</td>";
                                                    echo "<td>".$row['Emp_id']."</td>";
                                                    echo "<td>".$row['Name']."</td>";
                                                    if($row['total'] == '-1 years, -1 Months' || $row['total'] == '' ){
                                                        $total = 'Fresher';
                                                    }else{
                                                        $total = str_replace('-1', '0', $row['total']);
//                                                        $total = $row['total'];
                                                    }
                                                    echo "<td>".$total."</td>";
                                                    
                                                    // if($row['mb'] == '-1 years, -1 Months' || $row['mb'] == '' ){
													if($row['mb'] == '-1 Year(s), -1 Month(s)' || $row['mb'] == '' ){
                                                        $mb = 'Fresher';
                                                    }else{
                                                        $mb = str_replace('-1', '0', $row['mb']);
//                                                        $mb = $row['mb'];
                                                    }
                                                    
                                                    echo "<td>".$mb."</td>";

//                                                    echo "<td>".$row['D_of_join']."</td>";
                                                    echo "<td>".$row['totalin_allzone']."</td>";
                                                    echo "<td>".$row['joining']."</td>";
                                                    echo "<td>".$row['present']."</td>";
                                                    echo "<td>".$row['join_ctc']."</td>";
                                                    echo "<td>".$row['present_ctc']."</td>";
                                                    echo "<td>".($row['appr'] - 1) ."</td>";
                                                    echo "<td>".$row['refix']."</td>";
                                                    echo "<td>".$row['status']."</td>";
                                                    echo "</tr>";
                                                }

                                        }else{
                                            $excel = 0;
                                            echo "<tr><td colspan='14'>No data available in table</td></tr>";
                                        }                                    
                                    ?>
                                </tbody>
                            </table>
                            <?php 
                                if($excel){
                                   ?>
                            <div class="row">
                                <div class="col-sm-offset-5">
                                   <div class="col-xs-2">
                                       <input type="submit" id="submit" class="btn btn-primary" value="Export">
                                       <!--<input type="submit" id="submit" class="btn btn-success" value="Export">-->
                                    </div>                                    
                                </div>
                            </div>
                            <?php 
                                }
                            ?>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
           $(document).ready(function(){
                $.fn.dataTableExt.sErrMode = 'throw';
                $("#submit").click(function(){
                    $("#removetr").remove();
                    $("thead").prepend("<tr style='display:none;' id='removetr' data-tableexport-display='always' ><th colspan=7 >Employee  report </th></tr>");
                    
                    $("select option:last").after("<option value='2000'>2000</option>");
                        var select = $("select").val();
                        var page = $('.pagination .active a').text();
                        var select_last = $('select option:last-child').val();
                        $("select").val(select_last);
                        $("select").change();    
                        $('#employee_report').tableExport({type:'excel',escape:'false'});
                        $("select").val(select);
                        $("select").change(); 
                        $("a:contains("+page+")").click();
                });
            }); 
        </script>
        <?php $this->load->view('includes/additional.php');?>
        <script type="text/javascript" src="assets/js/html2canvas.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/tableExport.js"></script>
    </body>
</html>