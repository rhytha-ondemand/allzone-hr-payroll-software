<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >Confirmation</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Confirmation </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                         <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-1">
                        <!--working content start-->
                        <form class="form-horizontal" id="confirmation" role="form" method="post" action="report/confirmation_list">
                            <br>				
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname"> Branch</label><span ></span>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-xs-2" for="dept">Department</label>
                                        <div class="col-xs-3">
                                            <select class="form-control" id="dept" name="dept" data-placeholder="Select Department">
                                                <option value=""></option>
                                                <!--<option value="ALL">ALL</option>-->

                                            </select> 
                                        </div>
                                </div>
                                 <div class="form-group">
                                        <label class="control-label col-xs-2" for="team">Team</label>
                                        <div class="col-xs-3">
                                            <select class="form-control" name="team" id="team" data-placeholder="Select Team">
                                                <option value=""></option>
                                                <!--<option value="ALL">ALL</option>-->
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="month">Month</label>
                                    <div class="col-xs-3">
                                        <select class="validate[required] form-control" id="month" name="month" data-placeholder="Select Month">
                                           <option value=""> </option>
                                            <option value ='13'> All Month </option>
                                            <option value ='1'> January </option>
                                            <option value ='2'> February </option>
                                            <option value ='3'> March </option>
                                            <option value ='4'> April </option>
                                            <option value ='5'> May </option>
                                            <option value ='6'> June </option>
                                            <option value ='7'> July </option>
                                            <option value ='8'> August </option>
                                            <option value ='9'> September </option>
                                            <option value ='10'> October </option>
                                            <option value ='11'> November </option>
                                            <option value ='12'> December </option>
                                        </select> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="year">Year</label>
                                    <div class="col-xs-3">
                                        <select class="validate[required] form-control" data-placeholder="Select Year" name="year" id="year" data-rel="chosen">
                                           <option value=""> </option> 
                                           <?php
                                                $curr_year = date('Y')+1;
                                                for($i=2010;$i<=$curr_year;$i++ ){
                                                    echo "<option value='$i'> $i</option>" ;
                                                }
                                           ?>
                                        </select> 
                                    </div>                                    
                                </div>
                                     <div class="form-group">
                                       <div class="col-xs-3 col-sm-offset-3">
                                            <input type="submit" class="btn btn-primary" value="View">
                                        </div>
                                    </div>    
                                   
                             </form>     
                        <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
          <script>
            $(document).ready(function(){
                $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#confirmation").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#confirmation").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#confirmation").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
// branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name
            $("#bname").change(function(){
//                Department
                var dept_toappend;
                var url = "master/dept_list";
                var data =  "bname=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#dept').find('option').remove();
                        if(json) {
                             dept_toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 dept_toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
                            });   
                            $('#dept').append(dept_toappend);
                            $("#dept").trigger('chosen:updated');
                        }
                    }            
                });
            
//                Team
                var team_toappend;
                var url = "master/team_list";                             
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#team').find('option').remove();
                        if(json) {
                             team_toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 team_toappend+='<option value='+value['reporting_no']+'>'+value['team']+'</option>';
                                
                            });   
                            $('#team').append(team_toappend);
                            $("#team").trigger('chosen:updated');
                        }
                    }            
                });
            });
            
//            $("#submit").click(function(){
//                    if ( $("#confirmation").validationEngine('validate') ) {
//                        return true;
//                    }                    
//                    return false;
//                });

                
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>