<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            #bank_name_chosen{
                width:100%!important;
            }
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a >Report</a>
                    </li>
                    <li>
                        <a >Allowance</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Allowance Report </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">  
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" id="allowance_report" method="POST" action="report/allowance_list">                                
                                <br>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2"  for="bname"> Branch</label><span ></span>
                                        <div class="col-xs-3">
                                            <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control">
                                                <option value=""></option>
                                                <option value="4">Chennai</option>
                                                <option value="3">Vellore</option>                                            
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2"  for="bname">Allowance Type</label><span ></span>
                                        <div class="col-xs-3 radio" id="radioDiv">
                                            <label><input  tabindex="1" type=radio checked value="1"  name='status' id='status1' > SDA &nbsp;&nbsp;</label>
                                            <label><input type=radio tabindex="1" value="2"  name='status' id='status'> NSA &nbsp;&nbsp;</label>
                                            <label><input type=radio tabindex="1" value="3"  name='status' id='status'> HA &nbsp;&nbsp;</label>
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="from_period">From Period </label>
                                        <div class="col-xs-3">									
                                            <input type="text" readonly="" class="validate[required] form-control datepicker" id="from_period" name="from_period" />											
                                        </div>							
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-2" for="to_period">To Period</label>
                                        <div class="col-xs-3">									
                                            <input type="text" readonly="" class="validate[required] form-control datepicker" id="to_period" name="to_period" />											
                                        </div>							
                                    </div>
<!--                                <div class="form-group" id="nsa_bank" style="display: none;">
                                        <label class="control-label col-xs-2" for="bank_name">Bank </label>
                                        <div class="col-xs-3">									
                                            <select id="bank_name" name="bank_name" data-placeholder="Select Bank" class="validate[required] form-control">
                                                <option value=""></option>
                                                                                         
                                            </select>									
                                        </div>							
                                    </div>-->
                                    <div class="form-group">
                                        <div class="col-xs-3 col-sm-offset-3">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>	
                                </form>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                
            
             $("select").chosen({disable_search_threshold: 13});
                $.validationEngine.defaults.scroll = false; 
                $("#allowance_report").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#allowance_report").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#allowance_report").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                $("#from_period").datepicker({dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange:"2015:+0",
                    onClose: function( selectedDate ) {
                        $( "#to_period" ).datepicker( "option", "minDate", selectedDate);
                    }
                });
                $("#to_period").datepicker({dateFormat:"dd-M-yy",changeMonth: true,changeYear: true,yearRange:"2015:+0"});
        // branch name
                var toappend = '';
                $.ajax({
                    type:"POST",
                    url: "master/location_all",
                    cache:false,
                    dataType:"json",
                    async:false,
                    success: function(json){
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }
                });
// branch name

            $("#bname").change(function(){
                var bank_toappend = '<option value=""></option>';
                var data="bname="+$(this).val();
                $.ajax({
                    type:"Post",
                    url:"master/bank_list",
                    cache:false,
                    data:data,
                    dataType:"json",
                    success:function(result){
                        $("#bank_name").find('option').remove();
                        if(result){
                            $.each(result,function(i,value){
                                bank_toappend+='<option value="'+value['id']+'" >'+value['ba_name']+'</option>';
                            });                            
                        }
                        $('#bank_name').append(bank_toappend);
                        $("#bank_name").trigger('chosen:updated');
                    }
                });
            });

            $('#radioDiv input').on('change', function() {
                
                if ($("#radioDiv input[type='radio']:checked").val() == 2) {                
                    $("#nsa_bank").css('display','block');                                                     
                }else{                                        
                    $("#nsa_bank").css('display','none');
                }                  
            });
                
            $("#submit").click(function(){
                    if ( $("#allowance_report").validationEngine('validate') ) {
                        return true;
                    }                    
                    return false;
                });
                
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>