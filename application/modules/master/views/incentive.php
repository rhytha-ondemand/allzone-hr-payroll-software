<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Referral Incentive</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Referral Incentive </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" id="mincentive" method="post" action="master/ref_incentive_view" >
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="cadre">Cadre</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class="validate[required] form-control" name="" id="">-->
                                        <select id="cadre" name="cadre" data-placeholder="Select Cadre" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="1">Fresher</option>
                                            <option value="2">Experienced AR</option>   
                                            <option value="3">Experienced Data</option>
                                            <option value="4">Experienced Coder</option>                                    
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="amt">Enter amount</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required,custom[integer],min[1]] form-control" maxlength="5" data-errormessage-value-missing="* This field is required"  data-errormessage-custom-error="* Please enter valid amount." data-errormessage="* Please enter valid amount."  name="amt" id="amt">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="duration">Select duration</label>
                                    <div class="col-xs-4">
                                        <select id="duration" name="duration" data-placeholder="Select Duration" class="validate[required] form-control" >
                                            <option value=""></option>
<!--                                            <option value="1">1 month</option>
                                            <option value="2">2 month</option>-->
                                            <option value="3">3 month</option>
<!--                                            <option value="4">4 month</option>
                                            <option value="5">5 month</option>-->
                                            <option value="6">6 month</option>
<!--                                            <option value="7">7 month</option>
                                            <option value="8">8 month</option>
                                            <option value="9">9 month</option>
                                            <option value="10">10 month</option>
                                            <option value="11">11 month</option>-->
                                            <option value="12">12 month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="period">No of Period</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class="validate[required] form-control" name="" id="">-->
                                        <select id="period" name="period" data-placeholder="Select Period" class="validate[required] form-control">
                                            <option value=""></option>
                                            <option value="1"> 1 </option>
                                            <option value="2"> 2 </option>   
                                        </select>
<!--                                         <div class="radio">
                                            <label class="radio-inline">
                                                <input type="radio" name="period" id="period" value="1" checked > 1                                                
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label class="radio-inline">
                                                <input type="radio" name="period" id="period" value="2" > 2                                                
                                            </label>
                                        </div>-->
                                    </div>
                                </div>
                                
                                <!-- Need a big space between the previous row and the next one, so use one of the bigger spacers -->
                                <div class="spacer30"></div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3">
                                   <div class="col-xs-2">
                                       <input type="submit" id="submit" class="btn btn-success" value="Add Incentive">
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="submit" id="list" class="btn btn-primary" value="List Incentive">
                                    </div>
                                    </div>
                                </div>
                            </form> 
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New incentive added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert loation details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#mincentive").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"                
            });
            $("#mincentive").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#mincentive").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            $("select").chosen({disable_search_threshold: 10});
            
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            
            $('#list').on("click", function () {
                $("#mincentive").validationEngine('detach');
            });            
//                   Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det          
                          
            $("#submit").click(function(){
                if ( $("#mincentive").validationEngine('validate') )  {
                    if(checkIncent()){
                        var data = "bname="+$("#bname").val()+"&cadre="+$("#cadre").val()+"&amt="+$("#amt").val()+"&duration="+$("#duration").val()+"&period="+$("#period").val();
                        $.ajax({
                            type: "POST",
                            url: "master/ref_incentive_add",
                            data: data,
                            success: function (result) {
                                $("#mincentive").trigger('reset');
//                                $('#prj_dept').find('option').remove();
                                $("select").trigger('chosen:updated');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){                                
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                            
                            }
                        }); 
                    }                    
                }
                return false;
            });

          });
          function checkIncent(){
              var url = "master/ref_incentive_check";
                var data = "bname="+$("#bname").val()+"&cadre="+$("#cadre").val();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#cadre option:selected").text()+' is already exist with '+$("#bname option:selected").text()+' branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".cadre_chosenformError").remove();
                    $("#cadre_chosen").after('<div class="cadre_chosenformError parentFormmincentive formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
          }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>