<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Designation</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Designation </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            
                        </div>
                        <br>
                             <div id="hookError" class="alert alert-danger" style="">
                                 
                             </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="mdesignation" method="post" action="master/desig_view_all" role="form">
                                <br>                               
<!--                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>-->
                                <input type="hidden" id="bname" name="bname" value="4" />
                                <div class="spacer30"></div>
                                    <div class="form-group">
                                            <label class="control-label col-xs-3"  for="inlineRadioOptions">Select Team</label><span ></span>
                                            <div class="col-xs-5" id="radioDiv">
                                            <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" checked="" id="team1" value="1"> Operations
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" id="team2" value="2"> Support
                                            </label>
                                             <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" id="team3" value="3"> Others
                                            </label>
                                        </div>
                                    </div>
                                	
					<!-- Need a big space between the previous row and the next one, so use one of the bigger spacers -->
                                <div class="spacer30"></div>
					  
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="category">Select Category</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="category" name="category" class="validate[required] form-control" data-placeholder="Select Category" >
                                            <option value=""></option>
                                            <option value='4'>Medical Billing</option>
                                            <option value='5'>Medical Coding</option>
                                            <option value='6'>E Publishing</option>
                                        </select>
                                    </div>

                                </div>     
                                <div class="spacer30"></div>
			       <div class="form-group" id="sub_category">
                                    <label class="control-label col-xs-3" for="desig_type">Sub Category</label>
                                    <div class="col-xs-4" >
                                        <!--<input type="text" class="validate[required] form-control" id="desig_type" name="desig_type"/>-->
                                        <select id="desig_type" name="desig_type" class="validate[required] form-control" data-placeholder="Select Sub Category" >
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="spacer30"></div>
			       <div class="form-group">
                                    <label class="control-label col-xs-3" for="desig_name">Enter Designation Name</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" id="desig_name" name="desig_name"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-3">
                                        <div class="col-xs-2 " style="margin-right: 12px;">
                                            <input type="submit" id="submit" class="btn btn-success" value="Add Designation">
                                         </div>
                                         <div class="col-xs-3">
                                             <input type="submit" id="list" class="btn btn-primary" value="List Designation">
                                         </div>
                                    </div>
                                </div>
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New designation added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert designation details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#mdesignation").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#mdesignation").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#mdesignation").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            $("select").chosen({disable_search_threshold: 10});
            
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            
            $("#category").change(function(){
                var toappend_des ="<option value =''></option>";
                $('#desig_type').find('option').remove();
               if($("#category").val() == 4 ){                  
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="14">Data</option><option value="15">AR</option>';
               }else if($("#category").val() == 8 ){                    
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="18">Admin</option><option value="19">House Keeping</option>';
               }else if($("#category").val() == 11 ){                    
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="16">E Publishing</option><option value="17">Healthcare</option>';
               }else{
                    $("#sub_category").css("display","none");   
               }
                $('#desig_type').append(toappend_des);
                $("#desig_type").trigger('chosen:updated');  
            });
             $('#radioDiv input').on('change', function() {
                 var toappend ="<option value =''></option>";
                    if ($("#radioDiv input[type='radio']:checked").val() == 1) {
                        $('#category').find('option').remove();
                        toappend +='<option value="4">Medical Billing</option><option value="5">Medical Coding</option><option value="6">E Publishing</option>';
                    }else if($("#radioDiv input[type='radio']:checked").val() == 2) {
                        $('#category').find('option').remove();
                        toappend +='<option value="7">Human Resources</option><option value="8">Administration</option><option value="9">Technical</option><option value="10">Finance</option><option value="11">Business Development / Marketing</option>';
                    }else{
                        $('#category').find('option').remove();
                        toappend +='<option value="12">Management</option><option value="13">Others</option>';
                    }
                    $('#category').append(toappend);
                    $("#category").trigger('chosen:updated');  
                });
                
            $("#submit").click(function(){
                if ( ($("#mdesignation").validationEngine('validate')) && (checkDesignation()) ) {
                    var data = $("#mdesignation").serialize();
                    $.ajax({
                        type: "POST",
                        url: "master/desig_add",
                        data: data,
                        success: function (result) {
                            
                            
                            $(".desig_nameformError").remove();
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){
                                $("#category").val('');
                                $("#desig_name").val('');
                                $("#desig_type").val('');
                                $("#team1").trigger('click');
                                $("select").trigger('chosen:updated');
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                         
                            
                            
                        }
                    }); 
                }
                return false;
            });
            
            $('#list').on("click", function () {
                $("#mdesignation").validationEngine('detach');
            });
          });
          
          function checkDesignation(field, rules, i, options){
                var url = "master/desig_check";
                var data = $("#mdesignation").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#desig_name").val()+' is already exist with same designation category.';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".desig_nameformError").remove();
                    $("#desig_name").after('<div class="desig_nameformError parentFormmdesignation formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>