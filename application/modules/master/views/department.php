<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Department</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i>Add Department</h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                                
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
			   <br>
                           <form class="form-horizontal" id="mdepartment" action="master/dept_view_all" method="post" role="form">
                               
<!--                               <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>-->
                            <input type="hidden" id="bname" name="bname" value="4" />
                                <div class="spacer30"></div>
			        <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Enter Department Name</label>
                                    <div class="col-xs-4">
                                        <input type="text" name="dname" class="validate[required] form-control" id="dept_name">
                                    </div>
                                </div>
                                <!-- Need a big space between the previous row and the next one, so use one of the bigger spacers -->
                                <div class="spacer30"></div>
                                    <div class="form-group">
                                            <label class="control-label col-xs-3"  for="inlineRadioOptions">Select Team</label><span ></span>
					 <div class="col-xs-5">
                                            <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" checked="" id="team1" value="1"> Operations
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" id="team2" value="2"> Support
                                            </label>
                                             <label class="radio-inline">
                                                <input type="radio" name="team" class="validate[required]" id="team3" value="3"> Others
                                            </label>
                                        </div>
                                    </div>
                                <div class="spacer30"></div>
                                
				    <div class="form-group">
                                        <div class="col-sm-offset-3">
                                        <div class="col-xs-2" style="margin-right: 12px;">
                                            <input type="submit" id="adddept" class="btn btn-success" value="Add Department">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="submit" id="list" class="btn btn-primary" value="List Department">
                                        </div>
                                        </div>
				    </div>
                             </form> 
                            <!--working area end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New department added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert department details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
            <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#mdepartment").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#mdepartment").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#mdepartment").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
            });
            
            $("select").chosen({disable_search_threshold: 10});
            
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            
            $("#adddept").click(function(){
                if ( $("#mdepartment").validationEngine('validate') && (checkDepartment()) )  {
                    var data = $("#mdepartment").serialize();
                    $.ajax({
                        type: "POST",
                        url: "master/dept_add",
                        data: data,
                        success: function (result) {
                            $("#mdepartment").trigger('reset');
                            $("select").trigger('chosen:updated');
                           $(".dept_nameformError").remove();
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){                                
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                             
                        }
                    }); 
                }
                return false;
            });
            $('#list').on("click", function () {
                $("#mdepartment").validationEngine('detach');
              });
              
              return false;
            });
            function checkDepartment(){
                var url = "master/dept_check";
                var data = $("#mdepartment").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#dept_name").val()+' is already exist.';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".dept_nameformError").remove();
                    $("#dept_name").after('<div class="dept_nameformError parentFormmdepartment formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
    </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>