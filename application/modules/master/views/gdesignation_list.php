<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
            display: none;
        }
        #desig_type_chosen,#desig_typev_chosen,#bname_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Designation</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Designation </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <!--for chennai-->
                            <br>
                            <table id="designlistc" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Designation Name</th>
                                    <th>Designation Type</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
							<!--for vellore-->
                              <br>
                            <table id="designlistv" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Designation Name</th>
                                    <th>Designation Type</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->
<div class="modal fade" id="desgedit-modalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Designation</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavec" action="">
                <input type="hidden" name="desig_id" id="desig_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                          
							 <input type=text class="validate[required] form-control " id="bnamec" name="bnamec" readonly>
							 <input type="hidden" name="bname" value=4>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Designation name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="desig_name" name="desig_name">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Designation Type</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="desig_type" name="desig_type" class="validate[required] form-control" >
                                <option value="1">Data</option>
                                <option value="2">AR</option>
                                <option value="3">Support</option>
                            </select>
                        </div>
                    </div>
                </div>
                 <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitc" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Designation details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update designation details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
      

 <!--for vellore-->
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

<!--Modal dialog box Edit start-->
<div class="modal fade" id="desgedit-modalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Designation</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavev" action="">
                <input type="hidden" name="desig_id" id="desig_idv">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                           
							<input type=text class="validate[required] form-control " id="bnamev" name="bnamev" readonly>
							<input type=hidden id="bname" name="bname" value=3>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Designation name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="desig_namev" name="desig_name">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Designation Type</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="desig_typev" name="desig_type" class="validate[required] form-control" >
                                <option value="1">Data</option>
                                <option value="2">AR</option>
                                <option value="3">Support</option>
                            </select>
                        </div>
                    </div>
                </div>
                 <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitv" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Designation details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update designation details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
	  
	  
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                var oTablec = $('#designlistc').dataTable( {
                    "bProcessing": true,
                    "sAjaxSource": "master/desig_all_c",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true,                     
                    "bJQueryUI": false ,    
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Design_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Design_type", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#designlistc > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editc);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#designlistc > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editc);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    if(aData['Design_type']=="1" || aData['Design_type']== 1 ){
                        $(nRow).find("td").eq(2).html('Data');
                    }else if(aData['Design_type']=="2" || aData['Design_type']== 2 ){
                        $(nRow).find("td").eq(2).html('AR');
                    }else if(aData['Design_type']=="3" || aData['Design_type']== 3 ){
                        $(nRow).find("td").eq(2).html('Support');
                    }
                    $(nRow).find("td").eq(3).html(location[aData['Loc_id']]);
                }
            });
			<!-- for vellore -->
			 var oTablev = $('#designlistv').dataTable( {
                    "bProcessing": true,
                    "sAjaxSource": "master/desig_all_v",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true,                     
                    "bJQueryUI": false ,    
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Design_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Design_type", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#designlistv > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editv);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#designlistv > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editv);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) { 

                    
					
				
                    if(aData['Design_type']=="1" || aData['Design_type']== 1 ){
                        $(nRow).find("td").eq(2).html('Data');
                    }else if(aData['Design_type']=="2" || aData['Design_type']== 2 ){
                        $(nRow).find("td").eq(2).html('AR');
                    }else if(aData['Design_type']=="3" || aData['Design_type']== 3 ){
                        $(nRow).find("td").eq(2).html('Support');
                    }
                    $(nRow).find("td").eq(3).html(location[aData['Loc_id']]);
                }
            });
               $.validationEngine.defaults.scroll = false;
               $("#formsavec").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitc").click(function(){
                    if ( ($("#formsavec").validationEngine('validate')) && (checkDesignationc()) ) {
//                        alert("Department details updated successfully");
                        var data = $("#formsavec").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/desig_save",
                            data: data,
                            success: function (result) {
                                $('#desgedit-modalc').modal('hide');
                                 
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTablec.fnDraw();
									oTablec.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function editc() {
                    $('#formsavec').validationEngine('hide');
                    $(".desig_nameformError").remove();
                    var myModal = $('#desgedit-modalc');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#desig_id").val($(this).text());
                            }
                            if(j===1){
                                $("#desig_name").val($(this).text());
                            }
                            if(j===2){
//                                $("#dept_team").val($(this).text());
                                var desig_type = $(this).text();
                                
                                $('#desig_type option:contains('+desig_type+')').prop("selected", "selected");
                                $('#desig_type').trigger('chosen:updated');
                            }
                            if(j===3){
								$("#bnamec").val($(this).text());
//                                $("#location").val($(this).text());
/*                                var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
                                $('#bname').trigger('chosen:updated');*/
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
               
                <!-- for vellore -->
               $.validationEngine.defaults.scroll = false;
               $("#formsavev").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitv").click(function(){
                    if ( ($("#formsavev").validationEngine('validate')) && (checkDesignationv()) ) {
//                        alert("Department details updated successfully");
                        var data = $("#formsavev").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/desig_save",
                            data: data,
                            success: function (result) {
                                $('#desgedit-modalv').modal('hide');
                                 
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTablev.fnDraw();
									oTablev.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function editv() {
                    $('#formsavev').validationEngine('hide');
                    $(".desig_nameformError").remove();
                    var myModal = $('#desgedit-modalv');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#desig_idv").val($(this).text());
                            }
                            if(j===1){
                                $("#desig_namev").val($(this).text());
                            }
                            if(j===2){
//                                $("#dept_team").val($(this).text());
                                var desig_type = $(this).text();
                                
                                $('#desig_typev option:contains('+desig_type+')').prop("selected", "selected");
                                $('#desig_typev').trigger('chosen:updated');
                            }
                            if(j===3){
                                $("#bnamev").val($(this).text());
                               /* var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
                                $('#bname').trigger('chosen:updated');*/
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }			   
                
            });
            function checkDesignationc(){
                var url = "master/desig_check";
                var data = $("#formsavec").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#desig_name").val()+' is already exist with same designation type and branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".desig_nameformError").remove();
                    $("#desig_name").after('<div class="desig_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
			function checkDesignationv(){
				
                var url = "master/desig_check";
                var data = $("#formsavev").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#desig_name").val()+' is already exist with same designation type and branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".desig_nameformError").remove();
                    $("#desig_namev").after('<div class="desig_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>