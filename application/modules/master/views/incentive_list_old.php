<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
            display: none;
        }
        #cadre_chosen,#bname_chosen,#duration_chosen,#period_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Referral Incentive</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Referral Incentive </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="incentivelist" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Branch</th>
                                    <th>Category</th>
                                    <th>Amount</th>
                                    <th>Duration(in months)</th>
                                    <th>Period</th>                                    
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                  
                                </tbody>
                                
                            </table>
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<!--Modal dialog box Edit start-->
<div class="modal fade" id="incetiveedit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Referral Incentive</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="id" id="incent_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                <option value=""></option>
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>                                            
                            </select>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="cadre">Cadre</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="cadre" name="cadre" data-placeholder="Select Cadre" class="validate[required] form-control" >
                                <option value=""></option>
                                <option value="1">Fresher</option>
                                <option value="2">Experienced AR</option>   
                                <option value="3">Experienced Data</option>
                                <option value="4">Experienced Coder</option>                                    
                            </select>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="amt">Amount</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" name="amt" id="amt" class="validate[required,custom[integer],min[1]] form-control" maxlength="5" data-errormessage-value-missing="* This field is required"  data-errormessage-custom-error="* Please enter valid amount." data-errormessage="* Please enter valid amount." >
                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="duration">Duration</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="duration" name="duration" data-placeholder="Select Duration" class="validate[required] form-control" >
                                <option value=""></option>
<!--                                 <option value="1">1 month</option>
                                <option value="2">2 month</option>-->
                                <option value="3">3 month</option>
<!--                                  <option value="4">4 month</option>
                                <option value="5">5 month</option>-->
                                <option value="6">6 month</option>
<!--                                  <option value="7">7 month</option>
                                <option value="8">8 month</option>
                                <option value="9">9 month</option>
                                <option value="10">10 month</option>
                                <option value="11">11 month</option>-->
                                <option value="12">12 month</option>
                            </select>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="period">Period</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="period" name="period" data-placeholder="Select Period" class="validate[required] form-control" >
                                <option value=""></option>
                                <option value="1">1 </option>
                                <option value="2">2 </option>   
                                <option value="3">3 </option>
                                <option value="4">4 </option>                                    
                            </select>
                        </div>
                    </div>
                    <br><br>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Incentive details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update Incentive details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){    
                
                 var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                var oTable =  $('#incentivelist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": "master/ref_incentive_all",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "bname", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "cadre", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Amount", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "duration", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "period", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#incentivelist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#incentivelist > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });	
					
                }
            });
                
            function edit(){
                $('#formsave').validationEngine('hide');
                    $(".cadre_chosenformError").remove();
                    var myModal = $('#incetiveedit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#incent_id").val($(this).text());
                            }
                            if(j===1){
                                $("#bname").val($(this).text());
                            }
                            if(j===2){
                                 $("#cadre").val($(this).text());
                            }
                            if(j===3){                                                             
                                 $("#amt").val($(this).text());                                 
                            } 
                            if(j===4){
                                $("#duration").val($(this).text());
                            }
                            if(j===5){
                                $("#period").val($(this).text());
                            }
                        });
                        $("select").trigger('chosen:updated');
                        myModal.modal({ show: true });
                        return false;
                    });
            }
                
                
               $.validationEngine.defaults.scroll = false;
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });               
                $("select").chosen({disable_search_threshold: 10}); 
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
                
                $("#submit").click(function(){
                    if ( $("#formsave").validationEngine('validate')) {
                        if(checkIncent()){
                        var data = "id="+$("#incent_id").val()+"&bname="+$("#bname").val()+"&cadre="+$("#cadre").val()+"&amt="+$("#amt").val()+"&duration="+$("#duration").val()+"&period="+$("#period").val();
                        $.ajax({
                            type: "POST",
                            url: "master/ref_incentive_save",
                            data: data,
                            success: function (result) {
                                $('#incetiveedit-modal').modal('hide');
                                $("#formsave").trigger('reset');
                                $("select").trigger('chosen:updated');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTable.fnDraw();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }
                }

                    return false;
                });
                
                
            });
            
            function checkIncent(){
              var url = "master/ref_incentive_check";
                var data = "bname="+$("#bname").val()+"&cadre="+$("#cadre").val();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#cadre option:selected").text()+' is already exist with '+$("#bname option:selected").text()+' branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".cadre_chosenformError").remove();
                    $("#cadre_chosen").after('<div class="cadre_chosenformError parentFormmincentive formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
          }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>