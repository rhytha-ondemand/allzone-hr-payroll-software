<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
                display: none;
            }
        </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Location-city</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> City list details </h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                              <table id="citylist" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Branch</th>
                                    <th>City</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                                
                            </table>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<!--Modal dialog box Edit start-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">City detail</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="city_id" id="city_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">
                                <option value="">Select branch</option>
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>   
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">City</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="validate[required] form-control" name="loc_city" id="loc_city">
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New City Update successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert City details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>
      
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                var oTable =   $('#citylist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": "master/location_city_all",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "bname", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "city", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#citylist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                    },
                    "fnDrawCallback": function( oSettings ) {
                        $('#citylist > tbody > tr ').each(function() {
                            $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                            $(this ).find('a:nth-child(1)').on('click',edit);
                        });	

                    },
                    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                        $(nRow).find("td").eq(1).html(location[aData['bname']]);
                    }
                });
                function edit(){
                    $('#formsave').validationEngine('hide');
                    var myModal = $('#edit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#city_id").val($(this).text());
                            }
                            if(j===1){
                                var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
//                                callDepartment($("#bname").val());
                                $('#bname').trigger('chosen:updated');
                            }
                            if(j===2){
                                $("#loc_city").val($(this).text());
                            }
                            
                        });
                        myModal.modal({ show: true });
                        return false;
                    });

                }

                $.validationEngine.defaults.scroll = false;
                $("#formsave").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                
                $("#submit").click(function(){
                     if ( $("#formsave").validationEngine('validate')){
                          var data ="city_id = "+$("#city_id").val()+"bname = "+$("#bname").val()+"&loc_city = "+$("#loc_city").val();
                        $.ajax({
                            type: "POST",
                            url: "master/location_city_save",
                            data: data,
                            success: function (result) {
//                                $("#mcity").trigger('reset');
//                                $("select").trigger('chosen:updated');                                
                                if(result === 1 || result === "1"){
                                    $('#edit-modal').modal('hide');
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $('#edit-modal').modal('hide');
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $('#edit-modal').modal('hide');
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){  
                                    $('#edit-modal').modal('hide');
                                    $(".notysuccess").click();    
                                }else if(result === 5 || result === "5"){                                
                                    $(".loc_cityformError").remove();
                                    var msg ='* '+$("#loc_city").val()+' is already exist with '+$("#bname option:selected").text()+' branch';
                                    $("#loc_city").after('<div class="loc_cityformError parentFormmcity formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                                }else{
                                    window.location.href = "master/login";
                                }

                            }
                        });
                     }
                    return false;
                });
            
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>