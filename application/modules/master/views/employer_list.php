<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        #bname_chosen{
            width:100%!important;
        }
        .modal-dialog{
            width:1000px!important;
        }
         table{
            width: 100%; word-wrap:break-word;table-layout: fixed; 
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Employer</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Employer </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'master/employer';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Employer  </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="emprlist" class="table table-striped table-bordered bootstrap-datatable responsive">
                                <thead>
                                <tr>                                    
                                    <th>Company Name</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>Address 3</th>
                                    <th>PF Establishment No </th>
                                    <th>ESI No</th>
                                    <th>TIN No</th>
                                    <th>PAN No</th>
                                    <th>PTA No </th>
                                    <th>Branch</th>                                    
                                    <th>Action</th>
                                    <!--<th>Location</th>-->
                                </tr>
                                </thead>
                                
                            </table>                              	
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

   <!--Modal dialog box Edit start-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Department</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="empr_id" id="empr_id" />
                
<!--                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>
                        </div>
                    </div>
                </div>-->
<div class="row">
    <div class="col-xs-12">
            <div class="col-xs-6">
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Branch</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text"  readonly="" class="validate[required] form-control " id="bname" name="bname">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Company Name</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" name="comp_name" id="comp_name" value="Allzone" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Address 1</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" name="addr1" id="addr1" maxlength="50" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Address 2</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="addr2" name="addr2" maxlength="50" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Address 3</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="addr3" name="addr3" maxlength="50" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">PF Establishment No </label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="pf_no" name="pf_no" maxlength="20" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">ESI No </label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="esi_no" name="esi_no" maxlength="20" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">TIN No</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="tin_no" name="tin_no" maxlength="20" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">PAN No</label>
                        </div>
                        <div class="col-xs-7">
                            <input type="text" class="validate[required] form-control" id="pan_no" name="pan_no" maxlength="20" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">PTA No</label>
                        </div>
                        <div class="col-xs-7">
                           <input type="text" class="validate[required] form-control" id="pta_no" name="pta_no" maxlength="20" />
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
</div>
<br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            
    </form>
        </div>
    </div>
</div>
</div>

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Company details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update department details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
 
    <!--Modal dialog box Edit end-->

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> Edit </a>";
                 var oTable = $('#emprlist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": "master/employer_all",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
//                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Comp_name", "sWidth": "100px", "bSortable": true },
                        { "mDataProp": "Addr1", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Addr2", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Addr3", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Pf_no", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Esi_no", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Tin_no", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Pan_no", "sWidth": "75px", "bSortable": true },                        
                        { "mDataProp": "PTA_no", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Branch_name", "sWidth": "75px", "bSortable": true },                        
//                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "75px", "bSortable": false }
//                        { "mDataProp": "Branch", "sWidth": "75px", "bSortable": true,"bVisible":false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#emprlist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                    },
                    "fnDrawCallback": function( oSettings ) {
                        $('#emprlist > tbody > tr ').each(function() {
                            $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                            $(this ).find('a:nth-child(1)').on('click',edit);
                        });	

                    },
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
//                    
//
//                }                
                });
                
                function edit(){
//                    $('#formsave').validationEngine('hide');
//                    $(".dept_nameformError").remove();
                    var myModal = $('#edit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("#empr_id").val($(this ).closest('tr').attr('id'));
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#comp_name").val($(this).text());
                            }
                            if(j===1){
                                $("#addr1").val($(this).text());
                            }
                            if(j===2){
                               $("#addr2").val($(this).text());
                            }
                            if(j===3){
                               $("#addr3").val($(this).text());
                            }
                            if(j===4){
                               $("#pf_no").val($(this).text());
                            }
                            if(j===5){
                               $("#esi_no").val($(this).text());
                            }
                            if(j===6){
                               $("#tin_no").val($(this).text());
                            }
                            if(j===7){
                               $("#pan_no").val($(this).text());
                            }
                            if(j===8){
                               $("#pta_no").val($(this).text());
                            }
                            if(j===9){
                               $("#bname").val($(this).text());
                            }

                        });
                        myModal.modal({ show: true });
                        return false;
                    })

                }
                
                $.validationEngine.defaults.scroll = false; 
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                
                $("#submit").click(function(){
                  if ( $("#formsave").validationEngine('validate') ) {
                        var data = $("#formsave").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/employer_save",
                            data: data,
                            success: function (result) {
                                $('#edit-modal').modal('hide');                                
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){   
                                oTable.fnDraw();
                                oTable.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
//                                window.location.href = "master/login";
                            }                            
                            }
                        });
            
                    }
                    return false;
                });
                
                
                
            
            });
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>