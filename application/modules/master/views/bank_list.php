<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
            display: none;
        }
        #bname_chosen{
            width:100%!important;
        }
        table{
            width: 100%; word-wrap:break-word;table-layout: fixed; 
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Bank</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Bank </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'master/bank';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Bank  </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                                <div class="box-content col-sm-offset-0">
                                    <div class="form-group col-xs-11 ">
                                        <label class="control-label col-xs-2"  for="bname">Select Branch</label><span ></span>
                                        <div class="col-xs-3" style="margin-top:-10px;">
                                            <select id="bnamech" name="bnamech" data-placeholder="Select branch" class="validate[required] form-control" >
                                                <option value=""></option>
                                               <!-- <option value="4">Chennai</option>
                                                <option value="3">Vellore</option> -->                                            
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <table id="banklistch" class="table table-striped table-bordered bootstrap-datatable responsive">                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Bank name </th>
                                    <th>Bank branch</th>
                                    <th>Address</th>
                                    <th>Company branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                                                      
                                </tbody>                                
                            </table>
                              <!--for vellore-->
                            <br>
                                <div class="box-content col-sm-offset-0">
                                    <div class="form-group col-xs-11 ">
                                        <label class="control-label col-xs-2"  for="bname">Select Branch</label><span ></span>
                                        <div class="col-xs-3" style="margin-top:-10px;">
                                            <select id="bnameve" name="bnameve" data-placeholder="Select branch" class="validate[required] form-control" >
                                                <option value=""></option>
                                              <!--  <option value="4">Chennai</option>
                                                <option value="3">Vellore</option>  -->                                          
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </br>
							</br>
                            <table id="banklistve" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Bank name </th>
                                    <th>Bank branch</th>
                                    <th>Address</th>
                                    <th>Company branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
							</br>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  <!--- for chennai -->
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<!--Modal dialog box Edit start-->
<div class="modal fade" id="bankedit-modalch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Bank Details</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavech" action="">
                <input type="hidden" name="id" id="ba_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_name">Company Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <!--<select id="bname" name="bnamech" data-placeholder="Select branch" class="validate[required] form-control" >
                                <option value=""></option>
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>                                            
                            </select>-->
							<input type=text class="validate[required] form-control " id="bnamec" name="bnamec" readonly/>
							<input type=hidden name="bname" id="bname1" value="4"/>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_name">Bank name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ba_name" name="ba_name">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_branch">Bank branch</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ba_branch" name="ba_branch">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_add">Address</label>
                        </div>
                        <div class="col-xs-6">                            
                            <textarea class="validate[required] form-control" name="ba_add" id="ba_add"> </textarea>
                        </div>
                    </div>                    
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitch" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Bank details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update bank details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
<!-- for vellore -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="bankedit-modalve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Bank Details</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsaveve" action="">
                <input type="hidden" name="id" id="ba_idv">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_name">Company Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <!--<select id="bname" name="bnameve" data-placeholder="Select branch" class="validate[required] form-control" >
                                <option value=""></option>
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>                                            
                            </select>-->
							<input type=text class="validate[required] form-control " id="bnamev" name="bnamev" readonly/>
							<input type=hidden name="bname" id="bname2" value="3"/>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_name">Bank name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ba_namev" name="ba_name">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_branch">Bank branch</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ba_branchv" name="ba_branch">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="ba_add">Address</label>
                        </div>
                        <div class="col-xs-6">                            
                            <textarea class="validate[required] form-control" name="ba_add" id="ba_addv"> </textarea>
                        </div>
                    </div>                    
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitve" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<!--<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Bank details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update bank details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	-->

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
                <script>
            $(document).ready(function(){
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Nungambakkam','2'=>"Kodambakkam"))?> ;
				var  oTablech,oTableve;
                //           Branch det       
             var toappend = "";
			 var locations=[];
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('select[name="bnamech"]').find('option').remove();
						 $('select[name="bnameve"]').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
								 		 
								 locations[value['Id']]=value['Location'];
                                
                            });   
                            $('select[name="bnamech"]').append(toappend);
                            $('select[name="bnamech"]').trigger('chosen:updated');
							$('select[name="bnameve"]').append(toappend);
                            $('select[name="bnameve"]').trigger('chosen:updated');
							
                        }
                    }            
                }); 
                          
//                Branch det  

              var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> Edit </a>";
				
				
				
				$("#bnamech").change(function(){
				/*if($(this).val()==4){
					fchennai($(this).val());
					
				}
				else if($(this).val()==3)
				{
					 fchennai($(this).val());
				}*/
				if($(this).val()!=''){
					fchennai($(this).val());
				}
				else
				 {
					if(typeof oTablec!=='undefined')
					 oTablec.fnClearTable();
				 if(typeof oTablev!=='undefined')
					 oTablev.fnClearTable();
				 }
				$("#bname1").val($(this).val());
					});
				$("#bnameve").change(function(){
				/*if($(this).val()==4)
					fvellore($(this).val());
				else if($(this).val()==3){
					 fvellore($(this).val());
				}*/
				if($(this).val()!=''){
//					fchennai($(this).val());
                                    fvellore($(this).val());
				}
				else
				 {
				if(typeof oTablec!=='undefined')
					 oTablec.fnClearTable();
				 if(typeof oTablev!=='undefined')
					 oTablev.fnClearTable();
				 }
				  $("#bname2").val($(this).val());
					}); 
               
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
				function fchennai(loc){
					
					src="master/bank_list_det";
					src=src+"/?bname="+loc;
                oTablech =  $('#banklistch').dataTable( {
                    "bProcessing": true,

                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "ba_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "ba_branch", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "ba_add", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "loc_id", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#banklistch > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editc);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#banklistch > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editc);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                       $(nRow).find("td").eq(4).html(locations[aData['loc_id']]);                  
                }
            });
                }
			function fvellore(loc){
					
					src="master/bank_list_det/?bname="+loc;
                    oTableve =  $('#banklistve').dataTable( {
                    "bProcessing": true,

                    "sAjaxSource":src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "ba_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "ba_branch", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "ba_add", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "loc_id", "sWidth": "125px", "bSortable": true },
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
						
                            if ( json.sError ){
						          oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#banklistve > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editv);
                                
                            });	
                        }
						
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#banklistve > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editv);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                       $(nRow).find("td").eq(4).html(locations[aData['loc_id']]);                  
                }
            });
			}					
            function editc(){
                $('#formsavech').validationEngine('hide');
                    $(".reporting_nameformError").remove();
                    var myModal = $('#bankedit-modalch');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#ba_id").val($(this).text());
                            }
                            if(j===1){
                                $("#ba_name").val($(this).text());
                            }
                            if(j===2){
                                 $("#ba_branch").val($(this).text());
                            }
                            if(j===3){                                                             
                                 $("#ba_add").val($(this).text());                                 
                            }
                            if(j===4){                                                             
                                 $("#bnamec").val($(this).text());   
                                  //var branch = $(this).text();
 /*                               $('#bnamech option:contains('+branch+')').prop("selected", "selected");
                                 $("#bnamech").trigger("chosen:updated");*/
                            } 
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
            }
			 function editv(){
                $('#formsaveve').validationEngine('hide');
                    $(".reporting_nameformError").remove();
                    var myModal = $('#bankedit-modalve');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#ba_idv").val($(this).text());
                            }
                            if(j===1){
                                $("#ba_namev").val($(this).text());
                            }
                            if(j===2){
                                 $("#ba_branchv").val($(this).text());
                            }
                            if(j===3){                                                             
                                 $("#ba_addv").val($(this).text());                                 
                            }
                            if(j===4){                                                             
                                 $("#bnamev").val($(this).text());   
                                  //var branch = $(this).text();
 /*                               $('#bnamech option:contains('+branch+')').prop("selected", "selected");
                                 $("#bnamech").trigger("chosen:updated");*/
                            } 
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
            }
            $.validationEngine.defaults.scroll = false;
               $("#formsavech").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                
                $("select").chosen({disable_search_threshold: 10});                 
              
                 $("#submitch").click(function(){
                    if ( $("#formsavech").validationEngine('validate')) {
                        if(checkBank()){
                        var data = "id="+$("#ba_id").val()+"&bname="+$("#bname1").val()+"&ba_name="+$("#ba_name").val()+"&ba_branch="+$("#ba_branch").val()+"&ba_add="+$("#ba_add").val();
                        $.ajax({
                            type: "POST",
                            url: "master/bank_save",
                            data: data,
                            success: function (result) {
                                $('#bankedit-modalch').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTablech.fnDraw();
									oTablech.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }
                }

                    return false;
                });
                
				$.validationEngine.defaults.scroll = false;
               $("#formsaveve").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                
                //$("select").chosen({disable_search_threshold: 10});                 
              
                 $("#submitve").click(function(){
                    if ( $("#formsaveve").validationEngine('validate')) {
                        if(checkBank2()){
                        var data = "id="+$("#ba_idv").val()+"&bname="+$("#bname2").val()+"&ba_name="+$("#ba_namev").val()+"&ba_branch="+$("#ba_branchv").val()+"&ba_add="+$("#ba_addv").val();
                        $.ajax({
                            type: "POST",
                            url: "master/bank_save",
                            data: data,
                            success: function (result) {
                                $('#bankedit-modalve').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTableve.fnDraw();
									oTableve.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }
                }

                    return false;
                });
				
            });
           function checkBank(){
              var url = "master/bank_check";
                var data = "id="+$("#ba_id").val()+"&bname="+$("#bname1").val()+"&ba_name="+$("#ba_name").val()+"&ba_branch="+$("#ba_branch").val()+"&ba_add="+$("#ba_add").val();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#ba_name").val()+' is already exist with same branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".ba_nameformError").remove();
                    $("#ba_name").after('<div class="ba_nameformError parentFormformsavech formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
          }
          function checkBank2(){
              var url = "master/bank_check";
                var data = "id="+$("#ba_idv").val()+"&bname="+$("#bname2").val()+"&ba_name="+$("#ba_namev").val()+"&ba_branch="+$("#ba_branchv").val()+"&ba_add="+$("#ba_addv").val();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#ba_namev").val()+' is already exist with same branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".ba_namevformError").remove();
                    $("#ba_namev").after('<div class="ba_namevformError parentFormformsaveve formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
          }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>