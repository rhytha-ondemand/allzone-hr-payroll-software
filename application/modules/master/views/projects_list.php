<!DOCTYPE html>
<html lang="en">
<?php 
$CI =& get_instance();
$CI->load->library('employee_lib',NULL,'emp');
?>
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        #prj_deptc_chosen,#prj_deptv_chosen,#bname_chosen{
            width:100%!important;
        }
        #errorc,#errorv{
            color:#b94a48; 
            font-weight:bold;
            text-align: center;    
            display: none;
            margin: 4px;
            padding: 8px;
        }
         table{
            width: 100%; word-wrap:break-word;table-layout: fixed; 
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Project</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Projects </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                             <a href="<?php echo base_url().'master/projects';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Project  </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
<!--for chennai-->
                            <br>
                            <div class="box-content col-sm-offset-0">
                                <div class="form-group col-xs-11 ">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-3" style="margin-top:-10px;">
                                        <select id="bnamech" name="bnamech" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table id="projectlist1" class="table table-striped table-bordered bootstrap-datatable responsive">                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Project ID</th>
                                    <th>Project Name</th>
                                    <th>Department</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                    <!--for vellore-->
                        <br>
                        <div class="box-content col-sm-offset-0">
                            <div class="form-group col-xs-11 ">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-3" style="margin-top:-10px;">
                                        <select id="bnameve" name="bnameve" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                        </div>
                            <br>
                            <table id="projectlist2" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Project ID</th>
                                    <th>Project Name</th>
                                    <th>Department</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--for chennai-->
    <div class="modal fade" id="myModalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="repedit-modalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Project</h4>
        </div>
        <div class="modal-body">
            <div id="errorc" style="display:none"> </div>
	    <form method="post" id="formsavec" action="">
                <input type="hidden" name="prj_idd" id="prj_iddc">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <!--<select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>-->
							<input type=text class="validate[required] form-control " id="bnamec" name="bnamec" readonly/>
							<input type=hidden name="bname" id="bname1" value="4"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Project id</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="prj_idc" name="prj_id"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Project name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="prj_namec" name="prj_name"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="prj_deptc" name="prj_dept" class="validate[required] form-control">                                
                                    
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitc" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<!--<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Project details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update project details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	-->

<!--foc vellore-->
    <div class="modal fade" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="repedit-modalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Project</h4>
        </div>
        <div class="modal-body">
            <div id="errorv" style="display:none"> </div>
	    <form method="post" id="formsavev" action="">
                <input type="hidden" name="prj_idd" id="prj_iddv">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <!--<select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>-->
                                            <input type=text class="validate[required] form-control " id="bnamev" name="bnamev" readonly/>
                                            <input type=hidden name="bname" id="bname2" value="3"/>

                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Project id</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="prj_idv" name="prj_id"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Project name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="prj_namev" name="prj_name"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="prj_deptv" name="prj_dept" class="validate[required] form-control">                                
                                    
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitv" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Project details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update project details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
                <script>
            $(document).ready(function(){
                   var cdata="<?php echo ($CI->emp->getcook()); ?>";
				
//                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
            var locations=[];	
               /* var department = <?php echo json_encode(array('8'=> 'Admin','11'=> 'Business Development','12'=> 'E - Publishing','7'=> 'Finance','10'=> 'House Keeping','3'=> 'Human Resource','6'=> 'Litigation','1'=> 'Medical Billing','2'=> 'Medical Coding','13'=> 'Medical Transcription','4'=> 'Network','9'=> 'Others'))?> ;*/
			   
			   url="master/location_all";
				 $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                   
                    async: false,
                    success: function(json) {
                        $('#bnamech').find('option').remove();
						$('#bnameve').find('option').remove();
                        if(json) {
                             toappend = '<option value=""></option>';
                             $.each(json, function(i, value) {
                                 locations[value['Id']]=value['Location'];
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bnamech').append(toappend);
                            $("#bnamech").trigger('chosen:updated');
                            $('#bnameve').append(toappend);
                            $("#bnameve").trigger('chosen:updated');
                        }
                    }            
                });
				
			   
			   
			   
			      data="bname=4";
				  var cdept=new Array();
				  var cdeptjson="";
				 var vdept=new Array();
				  var vdeptjson="";
				  var toappend='<option value=""></option>';
			     $.ajax({
                            type: "POST",
                            url: "master/dept_list_c",
                            data: data,
							dataType: 'json',
                            success: function (result) {
								cdeptjson=result;
								$.each( result, function( key, value ) {
									temp="e"+value['Id'];
									cdept[temp]=value['Dept_name'];
								toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
								});
								 $('#prj_deptc').append(toappend);
                               $("#prj_deptc").trigger('chosen:updated');
							   $('#prj_deptv').append(toappend);
                               $("#prj_deptv").trigger('chosen:updated');
							}
							});
					toappend='<option value=""></option>';		
				  
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> Edit </a>";
				
				var  oTablec,oTablev;
				
                        $("#bnamech").change(function(){
//				if($(this).val()==4){
//					fchennai($(this).val());
//					
//				}
//				else if($(this).val()==3)
//				{
//					 fchennai($(this).val());
//				}
//				else
//				 {
//					if(typeof oTablec!=='undefined')
//					 oTablec.fnClearTable();
//				 if(typeof oTablev!=='undefined')
//					 oTablev.fnClearTable();
//				 }
                                fchennai($(this).val());
				$("#bname1").val($(this).val());
                        });
                        $("#bnameve").change(function(){
//				if($(this).val()==4)
//					fvellore($(this).val());
//				else if($(this).val()==3){
//					 fvellore($(this).val());
//				}
//				else
//				 {
//				if(typeof oTablec!=='undefined')
//					 oTablec.fnClearTable();
//				 if(typeof oTablev!=='undefined')
//					 oTablev.fnClearTable();
//				 }
                                fvellore($(this).val());
                                $("#bname2").val($(this).val());
                        });
				
				function fchennai(loc){
					
					src="master/projects_list_det";
					src=src+"/?bname="+loc;
				
				
                oTablec = $('#projectlist1').dataTable( {

                    "bProcessing": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Project_id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Project_no", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Project_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Project_dept", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#projectlist1 > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editc);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#projectlist1 > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editc);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    
                   if(typeof cdept["e"+aData['Project_dept']]!=='undefined'){
                        $(nRow).find("td").eq(3).html(cdept["e"+aData['Project_dept']]);
                    }
                    else{
                        $(nRow).find("td").eq(3).html("unknown");
                    }                    
                     $(nRow).find("td").eq(4).html(locations[aData['Loc_id']]); 
                }
            });
				}
               
//			   <!--  for vellore -->
			   function fvellore(loc){
					
					src="master/projects_list_det/?bname="+loc;
			    oTablev = $('#projectlist2').dataTable( {
                    "bProcessing": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Project_id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Project_no", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Project_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Project_dept", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#projectlist2 > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editv);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#projectlist2 > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editv);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    
                   if(typeof cdept["e"+aData['Project_dept']]!=='undefined'){
                        $(nRow).find("td").eq(3).html(cdept["e"+aData['Project_dept']]);
                    }
                    else{
                        $(nRow).find("td").eq(3).html("unknown");
                    }                    
                     $(nRow).find("td").eq(4).html(locations[aData['Loc_id']]); 
                }
            });
			   }
//                <!-- chennai --> 

               $.validationEngine.defaults.scroll = false;
               $("#formsavec").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitc").click(function(){
                    if ( $("#formsavec").validationEngine('validate') && (checkProjectc()) ) {
                        var data = $("#formsavec").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/projects_save",
                            data: data,
                            success: function (result) {
                                $('#repedit-modalc').modal('hide');
                                if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){  
                                oTablec.fnDraw();
				oTablec.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                            
                            }
                        });
            
                    }else{
                        
                    }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function editc() {
					 
					 console.log("cdata=".cdata);
					 
					 $.each( cdeptjson, function( key, value ) {
						if(cdata!='')
							if(cdata==value['Id'])
								console.log("matched");
						
					});

                    $('#formsavec').validationEngine('hide');
//                    $(".prj_idformError").remove();
                    $("#errorc").css('display','none'); 
                    var myModal = $('#repedit-modalc');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#prj_iddc").val($(this).text());
                            }
                            if(j===1){
                                $("#prj_idc").val($(this).text());
                            }
                            if(j===2){
                                $("#prj_namec").val($(this).text());
                            }
                            
                            if(j===3){
//                                $("#location").val($(this).text());
                                var department = $(this).text();
								
                                $('#prj_deptc option:contains('+department+')').prop("selected", "selected");
                                $('#prj_deptc').trigger('chosen:updated');
                                
                            }
                            if(j===4){
                                $("#bnamec").val($(this).text());
                             
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                $("#bname").change(function(){
                callDepartment($(this).val());
            });
                <!-- for vellore -->
				  $.validationEngine.defaults.scroll = false;
               $("#formsavev").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitv").click(function(){
                    if ( $("#formsavev").validationEngine('validate') && (checkProject()) ) {

                        var data = $("#formsavev").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/projects_save",
                            data: data,
                            success: function (result) {
                                $('#repedit-modalv').modal('hide');
                                if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){  
                                oTablev.fnDraw();
				oTablev.fnReloadAjax();
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                            
                            }
                        });
            
                    }else{
                        
                    }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function editv() {
                    $('#formsavev').validationEngine('hide');
//                    $(".prj_idformError").remove();
                    $("#errorv").css('display','none'); 
                    var myModal = $('#repedit-modalv');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#prj_iddv").val($(this).text());
                            }
                            if(j===1){
                                $("#prj_idv").val($(this).text());
                            }
                            if(j===2){
                                $("#prj_namev").val($(this).text());
                            }
                            
                            if(j===3){
//                                $("#location").val($(this).text());
                                var department = $(this).text();
								
                                $('#prj_deptv option:contains('+department+')').prop("selected", "selected");
                                $('#prj_deptv').trigger('chosen:updated');
                                
                            }
                            if(j===4){
                                $("#bnamev").val($(this).text());
                              
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                $("#bname").change(function(){
                callDepartment($(this).val());
            });
				
            });
            function callDepartment(brachname){
              var toappend;
                var url = "master/dept_list";
                var data =  "bname=" + brachname;                
               
            }
            function checkProject(field, rules, i, options){
                var url = "master/prjid_check";
                var data = $("#formsavev").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = ' Project ID or Project name is already exist with same department and branch';  
//                            msg = $("#prj_idv").val()+' or '+ $("#prj_namev").val()+' is already exist with same department and branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
//                    $(".prj_idvformError").remove();
//                    $("#prj_idv").after('<div class="prj_idvformError parentFormformsavev formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                        $("#errorv").html('');
                        $("#errorv").html(msg);
                        $("#errorv").css('display','block');
                        return false;
                }
                else{
                    $("#errorv").append('');
                    $("#errorv").css('display','none'); 
                }
                return true;
            }
            function checkProjectc(field, rules, i, options){
                var url = "master/prjid_check";
                var data = $("#formsavec").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = ' Project ID or Project name is already exist with same department and branch';                            
//                            msg = $("#prj_idc").val()+' or '+ $("#prj_namec").val()+' is already exist with same department and branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $("#errorc").html('');
//                    $(".prj_idcformError").remove();
//                    $("#prj_idc").after('<div class="prj_idcformError parentFormformsavec formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                        $("#errorc").html(msg);
                        $("#errorc").css('display','block');
                        return false;
                }
                else{
                    $("#errorc").append('');
                        $("#errorc").css('display','none'); 
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>