<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Add Employer</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Employer</h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" role="form" id="branch_emp" action="master/employer_view_all" method="POST">
                                <br>
                              <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Branch</label><span ></span>
                                    <div class="col-xs-3">
                                        <select id="bname" name="bname" data-placeholder="Select branch" data-rel="chosen"  class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="comp_name">Company Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" name="comp_name" id="comp_name" value="Allzone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="addr1">Address 1</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" name="addr1" id="addr1" maxlength="50" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="addr2">Address 2</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="addr2" name="addr2" maxlength="50" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="addr3">Address 3</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="addr3" name="addr3" maxlength="50" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="pf_no">PF Establishment No </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="pf_no" name="pf_no" maxlength="20" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="esi_no">ESI No </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="esi_no" name="esi_no" maxlength="20" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="tin_no">TIN No </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="tin_no" name="tin_no" maxlength="20" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="pan_no">PAN No </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="pan_no" name="pan_no" maxlength="20" />
                                    </div>
                                </div>
<!--                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="location">Location</label>
                                    <div class="col-xs-3">
                                        <select class="validate[required] form-control" data-rel="chosen" id="location" name="location" data-placeholder="Select Location">
                                                <option value=""></option>
                                            </select> 
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="pta_no">Professional Tax Assessment (PTA) No </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="validate[required] form-control" id="pta_no" name="pta_no" maxlength="20" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3">
                                        <div class="col-xs-2">
                                            <input type="submit" id="submit" class="btn btn-success" value="Add Employer">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="submit" id="list" class="btn btn-primary" value="List Employer">
                                        </div>
                                    </div>
                                </div>
                                   
                             </form>   
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New Company details added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert team details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
<script>
        $(document).ready(function(){
                
                $.validationEngine.defaults.scroll = false;
            $("#branch_emp").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#branch_emp").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#branch_emp").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            $("select").chosen({disable_search_threshold: 10});
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            $('#list').on("click", function () {
                $("#branch_emp").validationEngine('detach');
              });
//          Branch det  
            var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend ='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
            
//       Branch det    
            $("#bname").change(function(){
                var toappend_loc ='<option value=""></option>';
                $.ajax({
                    type:"POST",
                    url:"master/location_city_all",
                    data:"bname="+$("#bname").val(),
                    cache:false,
                    dataType:"json",
                    async:false,
                    success:function(json){
                        $('#location').find('option').remove();
                        if(json){
                            $.each(json,function(i,value){
                                toappend_loc+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                            });
                            $('#location').append(toappend_loc);
                            $("#location").trigger('chosen:updated');
                        }
                    }
                });
            });
            
            $("#submit").click(function(){
                if ( $("#branch_emp").validationEngine('validate') )  {
                   if(emp_check()){
                        var data = $("#branch_emp").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/employer_add",
                            data: "bname="+$("#bname").val()+"&comp_name="+$("#comp_name").val()+"&addr1="+$("#addr1").val()+"&addr2="+$("#addr2").val()+"&addr3="+$("#addr3").val()+"&pf_no="+$("#pf_no").val()+"&esi_no="+$("#esi_no").val()+"&tin_no="+$("#tin_no").val()+"&pan_no="+$("#pan_no").val()+"&location="+$("#location").val()+"&pta_no="+$("#pta_no").val(),
                            success: function (result) {
                                $("#branch_emp").trigger('reset');
//                                $('#prj_dept').find('option').remove();
                                $("select").trigger('chosen:updated');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){                                
                                    $(".notysuccess").click();    
                                }else{
//                                    window.location.href = "master/login";
                                }                            
                            }
                        }); 
                    }
                }               
                return false;
            });
            
            function emp_check(){
                var url = "master/employer_check";
                var data = $("#branch_emp").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#bname option:selected").text()+' branch details is already exist, Please add to different branch ';                            
                        }
                    }            
                });  
                 $("#hookError").html("");
                if(msg != undefined) {
//                    $(".bname_chosenformError").remove();
//                    $("#bname_chosen").after('<div class="bname_chosenformError parentFormbranch_emp formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    $("#hookError").append(msg);
                    $("#hookError").css('display','block');
                    return false;
                }
                $("#hookError").css('display','none');
                return true;
            }
            
            
        });
</script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>