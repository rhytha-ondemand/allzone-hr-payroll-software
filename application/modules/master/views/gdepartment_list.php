<!DOCTYPE html>
<?php 
$CI =& get_instance();
$CI->load->library('employee_lib',NULL,'emp');
?>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
 
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        #bname_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Department</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Department </h2>
                            <div class="box-icon">
                                <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                    <br>
                            <table id="deptlistch" class="table table-striped table-bordered bootstrap-datatable responsive">

                                <thead>
                                <tr>
                                    <th>Sl.No</th>

                                    <th>Department Name</th>
                                    <th>Department type</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                           <!--working content end-->
                        </div>
						  <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="deptlistve" class="table table-striped table-bordered bootstrap-datatable responsive">
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Department Name</th>
                                    <th>Department type</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
    <!-- for chennai -->
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	


    <!--Modal dialog box Edit start-->
<div class="modal fade" id="deptedit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Department</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="did" id="dept_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <!--<div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>
                        </div>-->
						<div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="bnamec" name="bnamec" readonly>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="dept_name" name="dname">
                        </div>
                    </div>
					<input type="hidden" name="bname" value="4">
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Team</label>
                        </div>
                        <div class="col-xs-6">
                            <label class="radio-inline">
                                <input type="radio" name="team" class="validate[required]" id="team1" value="1"> Production
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="team" class="validate[required]" id="team2" value="2"> Support
                            </label>
                        </div>
                    </div>
                </div>
                <br>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>
<!--Modal dialog box Edit end-->

    

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Department details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update department details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
  <!---  for vellore -->

  <!--Modal dialog box start-->

    <div class="modal fade" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	


    <!--Modal dialog box Edit start-->
<div class="modal fade" id="deptedit-modalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Department</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavev" action="">
                <input type="hidden" name="did" id="dept_idv">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <!--<div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>
                        </div>-->
						<div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="bnamev" name="bnamev" readonly>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="dept_namev" name="dname">
                        </div>
                    </div>
					<input type="hidden" name="bname" value="3">
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Team</label>
                        </div>
                        <div class="col-xs-6">
                            <label class="radio-inline">
                                <input type="radio" name="team" class="validate[required]" id="team1v" value="1"> Production
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="team" class="validate[required]" id="team2v" value="2"> Support
                            </label>
                        </div>
                    </div>
                </div>
                <br>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submitv" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>
<!--Modal dialog box Edit end-->

    

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Department details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update department details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>

       <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
              
            $(document).ready(function(){
 var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
				
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                 var oTablech = $('#deptlistch').dataTable( {
					 "aaSorting": [ ], 
                     "sAjaxSource": "master/dept_all_c",
					 "bProcessing": true,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Dept_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Dept_type", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#deptlistch > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#deptlistch > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });
               	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) { 
 
         
	
				
                    if(aData['Dept_type']=="1" || aData['Dept_type']== 1 ){
                        $(nRow).find("td").eq(2).html('Production');
                    }else if(aData['Dept_type']=="2" || aData['Dept_type']== 2 ){
                        $(nRow).find("td").eq(2).html('Support');
                    }
                    $(nRow).find("td").eq(3).html(location[aData['Loc_id']]);
					//$(nRow).find("td").eq(3).html(aData['Loc_id']);
					
					
                }
              });
                 

				/* for vellore   */
			var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
				
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
                 var oTableve = $('#deptlistve').dataTable( {
					 "aaSorting": [ ], 
                    "sAjaxSource": "master/dept_all_v",
					  "bProcessing": true,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
					
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Dept_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Dept_type", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
					
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#deptlistve > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editv);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#deptlistve > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editv);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                 
                    if(aData['Dept_type']=="1" || aData['Dept_type']== 1 ){
                        $(nRow).find("td").eq(2).html('Production');
                    }else if(aData['Dept_type']=="2" || aData['Dept_type']== 2 ){
                        $(nRow).find("td").eq(2).html('Support');
                    }
                   
				    $(nRow).find("td").eq(3).html(location[aData['Loc_id']]);
					
                }
                
            });   
                
				
                
            /* ============================ for chennai */
			$.validationEngine.defaults.scroll = false; 
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submit").click(function(e){

					 var data = $("#formsave").serialize();
					if(checkDepartment()){
                    if ( $("#formsave").validationEngine('validate')  ) {
						                      
						   $.ajax({
                            type: "POST",
							dataType: 'json',
                            url: "<?=base_url();?>master/dept_save",
                            data: data,
                            success: function (result) {
                                $('#deptedit-modal').modal('hide');
                               
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){  

							    oTablech.fnDraw(true);
								oTablech.fnReloadAjax();
								 
								$(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                            
                            }
                        });
                          
                    }
					}
					else
						return false;
					
                   				    	
					
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                function edit() {
					
                    $('#formsave').validationEngine('hide');
                    $(".dept_nameformError").remove();
                    var myModal = $('#deptedit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#dept_id").val($(this).text());
                            }
                            if(j===1){
                                $("#dept_name").val($(this).text());
                            }
                            if(j===2){
//                                $("#dept_team").val($(this).text());
                                var team_val = $(this).text();
                                var team ='';
                                if(team_val == 'Production'){
                                    team = 1;
                                }else if(team_val == 'Support'){
                                    team = 2;
                                }
                                $("input[name=team][value="+team+"]").prop("checked",true);
                            }
                            if(j===3){
                                $("#bnamec").val($(this).text());
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                
				
				<!--- for vellore -->
			/* ============================ for chennai */
			$.validationEngine.defaults.scroll = false; 
               $("#formsavev").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitv").click(function(e){
					 var data = $("#formsavev").serialize();
					if(checkDepartmentv()){
                    if ( $("#formsavev").validationEngine('validate')  ) {
						                      
						   $.ajax({
                            type: "POST",
							dataType: 'json',
                            url: "<?=base_url();?>master/dept_save",
                            data: data,
                            success: function (result) {
                                $('#deptedit-modalv').modal('hide');
                               
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){  
							    oTableve.fnDraw(true);
								oTableve.fnReloadAjax();
								 
								$(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            }                            
                            }
                        });
                          
                    }
					}
					else
						return false;
					
                   				    	
					
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                function editv() {
                    $('#formsavev').validationEngine('hide');
                    $(".dept_nameformError").remove();
                    var myModalv = $('#deptedit-modalv');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#dept_idv").val($(this).text());
                            }
                            if(j===1){
                                $("#dept_namev").val($(this).text());
                            }
                            if(j===2){
//                                $("#dept_team").val($(this).text());
                                var team_val = $(this).text();
                                var team ='';
                                if(team_val == 'Production'){
                                    team = 1;
                                }else if(team_val == 'Support'){
                                    team = 2;
                                }
                                $("input[name=team][value="+team+"]").prop("checked",true);
                            }
                            if(j===3){
                                $("#bnamev").val($(this).text());
                                
                            }
                        });
                        myModalv.modal({ show: true });
                        return false;
                    });
                }
				
				
            });
            function checkDepartment(){
                var url = "master/dept_check";
                var data = $("#formsave").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#dept_name").val()+' is already exist with same branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".dept_nameformError").remove();
                    $("#dept_name").after('<div class="dept_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
			
			
                
            
            function checkDepartmentv(){
                var url = "master/dept_check";
                var data = $("#formsavev").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#dept_name").val()+' is already exist with same branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".dept_nameformError").remove();
                    $("#dept_name").after('<div class="dept_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
			
			function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
		
        var c = ca[i];
		
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function updateDatabase(dataTable) {
$.ajax({
type: "POST",
url: "<?=base_url()?>master/updateDatabase",
cache: false,
dataType: "json",
data:{dataTable: dataTable},
success: function(html){
//success
}
});
}
        </script>
        
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>