<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Projects</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Projects </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                            <form class="form-horizontal" id="mproject" method="post" action="master/projects_view_all" role="form">
                                
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                 <div class="spacer30"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Enter Project Id</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" name="prj_id" id="prj_id">
                                    </div>
                                </div>
                                <!-- Need a big space between the previous row and the next one, so use one of the bigger spacers -->
                                <div class="spacer30"></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="inputSuccess5">Enter Project Name</label><span ></span>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" name="prj_name" id="project_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3"  for="inputSuccess5">Project Department</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="prj_dept" name="prj_dept" data-placeholder="Select department" class="validate[required] form-control">
                                            <option value=""></option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="spacer30"></div>
					  
                                <div class="form-group">
                                    <div class="col-sm-offset-3">
                                        <div class=" col-xs-2">
                                            <input type="submit" id="submit" class="btn btn-success" value="Add Project">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="submit" id="list" class="btn btn-primary" value="List Projects">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New project added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert project details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#mproject").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
            $("#mproject").bind("jqv.form.validating", function(event){
                $("#hookError").css('display','none');
                $("#hookError").html("");
            });
            $("#mproject").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');
                    }
            });
            
            $("select").chosen({disable_search_threshold: 10});
           
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            $("#submit").click(function(){
                    if ( $("#mproject").validationEngine('validate') && (checkProject()) )  {
                    var data = $("#mproject").serialize();
                    $.ajax({
                        type: "POST",
                        url: "master/projects_add",
                        data: data,
                        success: function (result) {
                            $("#mproject").trigger('reset');
                            $('#prj_dept').find('option').remove();
                            $("select").trigger('chosen:updated');
                            $(".prj_idformError").remove();
                            if(result === 1 || result === "1"){
                                $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                $(".notyerror").click();
                            }else if(result === 2 || result === "2"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                $(".notyerror").click();
                            }else if(result === 3 || result === "3"){
                                $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                $(".notyerror").click();
                            }else if(result === 4 || result === "4"){                                
                                $(".notysuccess").click();    
                            }else{
                                window.location.href = "master/login";
                            } 
                            
                        }
                    }); 
                }
                return false;
            });
            $('#list').on("click", function () {
                $("#mproject").validationEngine('detach');
              });
          });
          //                                Branch det       
             var toappend = "";
            $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                          
//                Branch det
          $("#bname").change(function(){
              var toappend;
                var url = "master/dept_list";
                var data =  "bname=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#prj_dept').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
                            });   
                            $('#prj_dept').append(toappend);
                            $("#prj_dept").trigger('chosen:updated');
                        }
                    }            
                });  
               
          });
          
          function checkProject(field, rules, i, options){
                var url = "master/prjid_check";
                var data = $("#mproject").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
//                            msg = $("#prj_id").val()+' is already exist with same department and branch';                            
                            msg = 'Project ID or Project name is already exist with same department and branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
//                    $(".prj_idformError").remove();
//                    $("#prj_id").after('<div class="prj_idformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                      $("#hookError").html('');
                        $("#hookError").html(msg);
                        $("#hookError").css('display','block');
                        return false;
                }
                else{
                    $("#hookError").append('');
                    $("#hookError").css('display','none'); 
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>