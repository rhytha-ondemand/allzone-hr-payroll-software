<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
            .dataTables_filter{
            display: none;
        }
        #desig_type_chosen,#category_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Designation</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Designation </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                            <a href="<?php echo base_url().'master/designation';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Designation  </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="designlist" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Designation Name</th>
                                    <th>Team</th>
                                    <th>Category</th>
                                    <th>Designation Type</th>
                                    <!--<th>Branch</th>-->
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

<!--Modal dialog box Edit start-->
<div class="modal fade" id="desgedit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Designation</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="desig_id" id="desig_id">
                <input type="hidden" name="bname" id="bname" value="4">
<!--                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>
                        </div>
                    </div>
                </div>-->
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Team</label>
                        </div>
                        <div class="col-xs-6" id="radioDiv">
                            <!--<div class="form-group">-->
                            <label class="radio-inline">
                                <input type="radio" name="team" class="" id="team1" value="1"> Operations
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="team" class="" id="team2" value="2"> Support
                            </label>
                            <label class="radio-inline">
                                 <input type="radio" name="team" class="validate[required]" id="team3" value="3"> Others
                            </label>
                                        
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Category</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="category" name="category" class="validate[required] form-control" data-placeholder="Select Category" >
                                <option value=""></option>
                                <option value='4'>Medical Billing</option>
                                <option value='5'>Medical Coding</option>
                                <option value='6'>E Publishing</option>
                            </select>
                        </div>
                    </div>
                </div><br>
                <div class="row" id="sub_category">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Sub Category</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="desig_type" name="desig_type" class="validate[required] form-control" data-placeholder="Select Sub Category" >
                                    <option value=""></option>
                                </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Designation name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="desig_name" name="desig_name">
                        </div>
                    </div>
                </div>
                 <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->

<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Designation details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update designation details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
      
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
                
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
                var desig_type = <?php echo json_encode(array('14'=> 'Data','15'=> 'AR','16'=> 'E Publishing','17'=> 'Healthcare','18'=> 'Admin','19'=> 'House Keeping')) ?>;
                var cate = <?php echo json_encode(array('4'=> 'Medical Billing','5'=> 'Medical Coding','6'=> 'E Publishing','7'=> 'Human Resources','8'=> 'Administration','9'=> 'Technical','10'=> 'Finance','11'=> 'Business Development / Marketing','12'=> 'Management','13'=> 'Others')) ?>;
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> Edit </a>";
                var oTable = $('#designlist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": "master/desig_all",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true,                     
                    "bJQueryUI": false ,    
                    "aoColumns": [
                        { "mDataProp": "Id", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "Design_name", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "team", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "category", "sWidth": "75px", "bSortable": true },
                        { "mDataProp": "Design_type", "sWidth": "75px", "bSortable": true },
//                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#designlist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#designlist > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    if(aData['team']=="1" || aData['team']== 1 ){
                        $(nRow).find("td").eq(2).html('Operations');
                    }else if(aData['team']=="2" || aData['team']== 2 ){
                        $(nRow).find("td").eq(2).html('Support');
                    }else if(aData['team']=="3" || aData['team']== 3 ){
                        $(nRow).find("td").eq(2).html('Others');
                    }
                    $(nRow).find("td").eq(3).html(cate[aData['category']]);
                    $(nRow).find("td").eq(4).html(desig_type[aData['Design_type']]);
                }
            });
               $.validationEngine.defaults.scroll = false;
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submit").click(function(){
                    if ( ($("#formsave").validationEngine('validate')) && (checkDesignation()) ) {
//                        alert("Department details updated successfully");
                        var data = $("#formsave").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/desig_save",
                            data: data,
                            success: function (result) {
                                $('#desgedit-modal').modal('hide');
                                 
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to update details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTable.fnDraw();
                                     oTable.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }

                    return false;
                });
                
//                $("select").chosen({disable_search_threshold: 10});
                $("#category").chosen({disable_search_threshold: 10});
                $("#desig_type").chosen({disable_search_threshold: 10});
                 function edit() {
                    $('#formsave').validationEngine('hide');
                    $(".desig_nameformError").remove();
                    var myModal = $('#desgedit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#desig_id").val($(this).text());
                            }
                            if(j===1){
                                $("#desig_name").val($(this).text());
                            }
                            if(j===2){
                                var team_val = $(this).text();
                                var team =1;
                                if(team_val == 'Operations'){
                                    team = 1;
                                }else if(team_val == 'Support'){
                                    team = 2;
                                }else if(team_val == 'Others'){
                                    team = 3;
                                }
//                                $("input[name=team][value="+team+"]").prop("checked",true);
                                $("input[name=team][value="+team+"]").trigger('click');
                            }
                            if(j===3){
                                var categoryn = $(this).text();
                                $('#category option:contains('+categoryn+')').prop("selected", "selected");
                                $('#category').trigger('chosen:updated');
                                $("#category").trigger('change')
                                
                            }
                            if(j===4){
                                var sub_categoryn = $(this).text();
                                $('#desig_type option:contains('+sub_categoryn+')').prop("selected", "selected");
//                                $('#category option:contains('+categoryn+')').trigger('click');
                                $('#desig_type').trigger('chosen:updated');
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
               $("#category").change(function(){
                var toappend_des ="<option value =''></option>";
                $('#desig_type').find('option').remove();
               if($("#category").val() == 4 ){                  
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="14">Data</option><option value="15">AR</option>';
               }else if($("#category").val() == 8 ){                    
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="18">Admin</option><option value="19">House Keeping</option>';
               }else if($("#category").val() == 11 ){                    
                   $("#sub_category").css("display","block");   
                    toappend_des +='<option value="16">E Publishing</option><option value="17">Healthcare</option>';
               }else{
                    $("#sub_category").css("display","none");   
               }
                $('#desig_type').append(toappend_des);
                $("#desig_type").trigger('chosen:updated');  
            });
             $('#radioDiv input').on('change', function() {
                 var toappend ="<option value =''></option>";
                    if ($("#radioDiv input[type='radio']:checked").val() == 1) {
                        $('#category').find('option').remove();
                        toappend +='<option value="4">Medical Billing</option><option value="5">Medical Coding</option><option value="6">E Publishing</option>';
                    }else if($("#radioDiv input[type='radio']:checked").val() == 2) {
                        $('#category').find('option').remove();
                        toappend +='<option value="7">Human Resources</option><option value="8">Administration</option><option value="9">Technical</option><option value="10">Finance</option><option value="11">Business Development / Marketing</option>';
                    }else{
                        $('#category').find('option').remove();
                        toappend +='<option value="12">Management</option><option value="13">Others</option>';
                    }
                    $('#category').append(toappend);
                    $("#category").trigger('chosen:updated');  
                }); 
                
            });
            function checkDesignation(){
                var url = "master/desig_check";
                var data = $("#formsave").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#desig_name").val()+' is already exist with same designation type';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".desig_nameformError").remove();
                    $("#desig_name").after('<div class="desig_nameformError parentFormformsave formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>