<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
         #prj_dept_chosen,#prj_deptc_chosen,#prj_deptv_chosen,#bname_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Teams</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Name </h2>
                            <div class="box-icon">
<!--                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                            </div>
                             <a href="<?php echo base_url().'master/team';?>" class="btn btn-success btn-xs pull-right"> <i class="glyphicon glyphicon-plus"></i> Add Team  </a>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
<div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bnamech" name="bnamech" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                            <br>
                            <table id="teamlist1" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Team</th>                                    
                                    <th>Immediate Reporting person</th>
                                    <th>Final Reporting person</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                             <br>
							 <div class="form-group">
                                    <label class="control-label col-xs-3"  for="bname">Select Branch</label><span ></span>
                                    <div class="col-xs-4">
                                        <select id="bnameve" name="bnameve" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
							 <br>
                            <table id="teamlist2" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Team</th>                                    
                                    <th>Immediate Reporting person</th>
                                    <th>Final Reporting person</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->
 <!--for chennai-->
    <div class="modal fade" id="myModalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="repedit-modalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Team</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavec" action="">
                <input type="hidden" name="rep_id" id="rep_idc">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <!--<select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>-->
							  <input type=text class="validate[required] form-control " id="bnamec" name="bnamec" readonly />
							  <input type=hidden name="bname" id="bname1" value=4/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Reporting person name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="reporting_namec" name="team"/>
                        </div>
                    </div>
                </div>
                <!--<br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="prj_deptc" name="prj_dept" class="validate[required] form-control">                                
                                  
                            </select>
                        </div>
                    </div>
                </div>
                <br>-->
				<br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Immediate Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ireporting_namec" name="i_rep_person"/>
                        </div>
                    </div>
                </div>
                <br>
			     <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Final Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="freporting_namec" name="f_rep_person"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitc" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Team details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update team details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	


<!--Modal dialog box start-->
    <!--for chennai-->
    <div class="modal fade" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="repedit-modalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Team</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsavev" action="">
                <input type="hidden" name="rep_id" id="rep_idv">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                           <!-- <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>-->
							<input type=text class="validate[required] form-control " id="bnamev" name="bnamev" readonly />
							<input type=hidden name="bname" id="bname2" value="3" />

                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Reporting person name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="reporting_namev" name="team"/>
                        </div>
                    </div>
                </div>
                <br>
                <!--<div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Department</label>

                        </div>
                        <div class="col-xs-6">
                            <select id="prj_deptv" name="prj_dept" class="validate[required] form-control">                                
                                  
                            </select>

                        </div>
                    </div>
                </div>
                <br>-->
				<br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Immediate Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="ireporting_namev" name="i_rep_person"/>




                        </div>
                    </div>
                </div>
                <br>
			     <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Final Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="freporting_namev" name="f_rep_person"/>





                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitv" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Team details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update team details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
                <script>
            $(document).ready(function(){
                
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
				url="master/location_all";
				 $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    //data: data,
                    async: false,
                    success: function(json) {
                        $('#bnamech').find('option').remove();
						$('#bnameve').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';
                                
                            });   
                            $('#bnamech').append(toappend);
                            $("#bnamech").trigger('chosen:updated');
							$('#bnameve').append(toappend);
                            $("#bnameve").trigger('chosen:updated');
                        }
                    }            
                });
				
               /* var department = <?php echo json_encode(array('8'=> 'Admin','11'=> 'Business Development','12'=> 'E - Publishing','7'=> 'Finance','10'=> 'House Keeping','3'=> 'Human Resource','6'=> 'Litigation','1'=> 'Medical Billing','2'=> 'Medical Coding','13'=> 'Medical Transcription','4'=> 'Network','9'=> 'Others'))?> ;*/
			   data="bname=4";
				  var cdept=new Array();
				  var vdept=new Array();
				  var toappend='<option value=""></option>';
			     $.ajax({
                            type: "POST",
                            url: "master/dept_list_c",
                            data: data,
							dataType: 'json',
                            success: function (result) {
								$.each( result, function( key, value ) {
									temp="e"+value['Id'];
									cdept[temp]=value['Dept_name'];
								toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
								});
								 $('#prj_deptc').append(toappend);
                               $("#prj_deptc").trigger('chosen:updated');
							   $('#prj_deptv').append(toappend);
                               $("#prj_deptv").trigger('chosen:updated');
							}
							});
				   /*data="bname=3";
				   toappend='<option value=""></option>';
				 $.ajax({
                            type: "POST",
                            url: "master/dept_list_v",
                            data: data,
							dataType: 'json',
                            success: function (result) {
								
								
								$.each( result, function( key, value ) {
									temp="e"+value['Id'];
									vdept[temp]=value['Dept_name'];
									toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
									
								});
								
								$('#prj_deptv').append(toappend);
                               $("#prj_deptv").trigger('chosen:updated');
							}
							});*/
			   
			   
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i> Edit </a>";
				<!-- for chennai -->
				 var oTablec,oTablev;
				 
				$("#bnamech").change(function(){
				if($(this).val()==4){
					fchennai($(this).val());
					
				}
				else if($(this).val()==3)
				{
					 fchennai($(this).val());
				}
				else
				 {
					if(typeof oTablec!=='undefined')
					 oTablec.fnClearTable();
				 if(typeof oTablev!=='undefined')
					 oTablev.fnClearTable();
				 }
				$("#bname1").val($(this).val());
					});
				$("#bnameve").change(function(){
				if($(this).val()==4)
					fvellore($(this).val());
				else if($(this).val()==3){
					 fvellore($(this).val());
				}
				else
				 {
				if(typeof oTablec!=='undefined')
					 oTablec.fnClearTable();
				 if(typeof oTablev!=='undefined')
					 oTablev.fnClearTable();
				 }
				  $("#bname2").val($(this).val());
					});
				function fchennai(loc){
					/*if(loc==3)
						src="master/team_all_v";
					 else if(loc==4)
						src="master/team_all_c";*/
					src="master/team_view_list";
					src=src+"/?bname="+loc;
					
              oTablec =  $('#teamlist1').dataTable( {
                    "bProcessing": true,
                    //"bServerSide": true,
                    "sAjaxSource": src,
					"bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "reporting_no", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "team", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "i_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "f_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                         
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#teamlist1 > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editc);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#teamlist1 > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editc);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    
                    //$(nRow).find("td").eq(2).html(department[aData['Dept_id']]);
//					if(typeof cdept["e"+aData['Dept_id']]!=='undefined')
//					  $(nRow).find("td").eq(2).html(cdept["e"+aData['Dept_id']]);
//				    else
//						$(nRow).find("td").eq(2).html("unknown");
					
                    $(nRow).find("td").eq(4).html(location[aData['Loc_id']]);
                }
            });
			}
			
                <!-- for vellore -->
				function fvellore(loc){
					/*if(loc==3)
						src="master/team_all_v";
					else if(loc==4)
						src="master/team_all_c";*/
					src="master/team_view_list/?bname="+loc;
			 oTablev =  $('#teamlist2').dataTable( {
                    "bProcessing": true,
                    //"bServerSide": true,
                    "sAjaxSource": src,
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "reporting_no", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "team", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "i_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "f_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                         
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#teamlist2 > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',editv);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#teamlist2 > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',editv);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    
                    //$(nRow).find("td").eq(2).html(department[aData['Dept_id']]);
//					console.log(aData['Dept_id']);
//					if(typeof vdept["e"+aData['Dept_id']]!=='undefined')
//					  $(nRow).find("td").eq(2).html(vdept["e"+aData['Dept_id']]);
//				    else
//						$(nRow).find("td").eq(2).html("unknown");
//					
                    $(nRow).find("td").eq(4).html(location[aData['Loc_id']]);
                }
            });	
			
			}
				
			<!-- for chennai -->
               $.validationEngine.defaults.scroll = false;
               $("#formsavec").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitc").click(function(){
                    if ( ($("#formsavec").validationEngine('validate')) && (checkTeamc())  ) {

//                        alert("Department details updated successfully");
                        var data = $("#formsavec").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/team_save",
                            data: data,
                            success: function (result) {
                                $('#repedit-modalc').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
								if(typeof oTablec!=='undefined'){
                                    oTablec.fnDraw();
									oTablec.fnReloadAjax();
								}
								else if(typeof oTablev!=='undefined')
								{
									 oTablev.fnDraw();
									 oTablev.fnReloadAjax();
								}

                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }else{
                        
                    }


                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function editc() {
					 console.log("calling");

                    $('#formsavec').validationEngine('hide');
                    $(".reporting_nameformError").remove();
                    var myModal = $('#repedit-modalc');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#rep_idc").val($(this).text());
                            }
                            if(j===1){
                                $("#reporting_namec").val($(this).text());
                            }
                           if(j===2){
                                $("#ireporting_namec").val($(this).text());
//                                var department = $(this).text();
//                                $('#prj_deptv option:contains('+department+')').prop("selected", "selected");
//                                $('#prj_deptv').trigger('chosen:updated');

                                
                            }
                             if(j===3){
                                $("#freporting_namec").val($(this).text());
                            }
                            if(j===4){
                                $("#bnamec").val($(this).text());
 /*                               var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
                                callDepartment($("#bname").val());
                                $('#bname').trigger('chosen:updated');   */                            
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                $("#bname").change(function(){
                callDepartment($(this).val());
            });
               
               <!-- for vellore -->
               $("#formsavev").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submitv").click(function(){
                    if ( ($("#formsavev").validationEngine('validate')) && (checkTeamv())  ) {
//                        alert("Department details updated successfully");
                        var data = $("#formsavev").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/team_save",
                            data: data,
                            success: function (result) {
                                $('#repedit-modalv').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTablev.fnDraw();
									oTablev.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }else{
                        
                    }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});




                
                 function editv() {
                    $('#formsavev').validationEngine('hide');
                    $(".reporting_nameformError").remove();
                    var myModal = $('#repedit-modalv');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#rep_idv").val($(this).text());
                            }
                            if(j===1){
                                $("#reporting_namev").val($(this).text());
                            }
                            if(j===2){
                                $("#ireporting_namev").val($(this).text());
//                                var department = $(this).text();
//                                $('#prj_deptv option:contains('+department+')').prop("selected", "selected");
//                                $('#prj_deptv').trigger('chosen:updated');

                                
                            }
                             if(j===3){
                                $("#freporting_namev").val($(this).text());
                            }
                            if(j===4){
                               $("#bnamev").val($(this).text());
  /*                              var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
                                callDepartment($("#bname").val());
                                $('#bname').trigger('chosen:updated'); */                              
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                $("#bname").change(function(){
                callDepartment($(this).val());
            });
			   
            });
            
            function callDepartment(brachname){
              var toappend;
                var url = "master/dept_list";
                var data =  "bname=" + brachname;                
               /* $.ajax({

                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#prj_dept').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
                            });   
                            $('#prj_dept').append(toappend);
                            $("#prj_dept").trigger('chosen:updated');
                        }
                    }            
                }); */
         
            }
            function checkTeamc(field, rules, i, options){
                var url = "master/team_check";
                var data = $("#formsavec").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                           /* msg = $("#reporting_name").val()+' is already exist with same department and branch';*/                            
						   msg = $("#reporting_namec").val()+' is already exist with same  branch';
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".reporting_nameformError").remove();
                    $("#reporting_namec").after('<div class="reporting_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;

            }
			function checkTeamv(field, rules, i, options){
                var url = "master/team_check";
                var data = $("#formsavev").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                           /* msg = $("#reporting_name").val()+' is already exist with same department and branch'; */
                             msg = $("#reporting_namev").val()+' is already exist with same  branch';                           
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".reporting_nameformError").remove();
                    $("#reporting_namev").after('<div class="reporting_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>