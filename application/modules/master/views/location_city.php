<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">Location-city</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add city detail </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>
                        <div class="box-content col-sm-offset-1">
                            <!--working content start-->
                           <form class="form-horizontal" role="form" id="mcity" method="post" action="master/location_citylist" >
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" class="validate[required] form-control">
                                        <option value="">Select branch</option>
                                        <option value="4">Chennai</option>
                                        <option value="3">Vellore</option>   
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="inputSuccess4">Enter Location Name</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="validate[required] form-control" name="loc_city" id="loc_city">
                                    </div>
                                </div>
                                <!-- Need a big space between the previous row and the next one, so use one of the bigger spacers -->
                                <div class="spacer30"></div>
                                <div class="form-group">
                                   <div class="col-sm-offset-1 col-xs-2">
                                       <input type="submit" id="submit" class="btn btn-primary" value="Add Location">
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" id="list" class="btn btn-primary" value="List Locations">
                                    </div>
                                </div>
                            </form>    
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;New City added successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while insert City details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>
       
        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
        <script>
            $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
                $("#mcity").validationEngine({
                    prettySelect:true,
                    useSuffix: "_chosen", 
                    maxErrorsPerField: 1,
                    promptPosition : "inline"
                });
                $("#mcity").bind("jqv.form.validating", function(event){
                    $("#hookError").css('display','none');
                    $("#hookError").html("");
                });
                $("#mcity").bind("jqv.form.result", function(event , errorFound){
                    if(errorFound){ 
                        $("#hookError").append("Please fill all required fields");
                        $("#hookError").css('display','block');                        
                    }
                });
                
                $("select").chosen({disable_search_threshold: 10});
           
                $( "select" ).change(function() {
                    var err = '.'+this.id+'_chosenformError';
                    $(err).remove();
                });
            
                $('#list').on("click", function () {
                    $("#mcity").validationEngine('detach');
                });
                
                var toappend = "";
                $.ajax({
                    type: "POST",
                    url: "master/location_all",
                    cache: false,
                    dataType: "json",                    
                    async: false,
                    success: function(json) {
                        $('#bname').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Location']+'</option>';

                            });   
                            $('#bname').append(toappend);
                            $("#bname").trigger('chosen:updated');
                        }
                    }            
                }); 
                
                $("#submit").click(function(){
                     if ( $("#mcity").validationEngine('validate') ) {
                          var data ="bname = "+$("#bname").val()+"&loc_city = "+$("#loc_city").val();
                        $.ajax({
                            type: "POST",
                            url: "master/location_city_add",
                            data: data,
                            success: function (result) {
//                                $("#mcity").trigger('reset');
//                                $("select").trigger('chosen:updated');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){                                
                                    $(".notysuccess").click();    
                                }else if(result === 5 || result === "5"){                                
                                    $(".loc_cityformError").remove();
                                    var msg ='* '+$("#loc_city").val()+' is already exist with '+$("#bname option:selected").text()+' branch';
                                    $("#loc_city").after('<div class="loc_cityformError parentFormmcity formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                                }else{
                                    window.location.href = "master/login";
                                }

                            }
                        });
                     }
                   return false; 
                });
            });

        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>