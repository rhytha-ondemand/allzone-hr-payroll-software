<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('includes/header.php');?>
    <body>
        <style>
        .dataTables_filter{
            display: none;
        }
        #i_rep_person_chosen,#f_rep_person_chosen,#bname_chosen{
            width:100%!important;
        }
    </style>
      <?php $this->load->view('includes/topbar.php');?>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
            <?php $this->load->view('includes/sidebar.php');?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Master</a>
                    </li>
                    <li>
                        <a href="#">View Teams</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> List of Name </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content col-sm-offset-0">
                            <!--working content start-->
                            <br>
                            <table id="teamlist" class="table table-striped table-bordered bootstrap-datatable responsive">
                                
                                <thead>
                                <tr>
                                    <th>Sl.No</th>
                                    <th>Team</th>                                    
                                    <th>Immediate Reporting person</th>
                                    <th>Final Reporting person</th>
                                    <th>Branch</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                
                            </table>
                              
                           <!--working content end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End -->
<!--Modal dialog box Edit start-->
<div class="modal fade" id="repedit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Team</h4>
        </div>
        <div class="modal-body">
	    <form method="post" id="formsave" action="">
                <input type="hidden" name="rep_id" id="rep_id">
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="bname">Branch</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="bname" name="bname" class="validate[required] form-control">                                
                                <option value="4">Chennai</option>
                                <option value="3">Vellore</option>  
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="team">Reporting person name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type=text class="validate[required] form-control " id="team" name="team"/>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Immediate Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="i_rep_person" name="i_rep_person" data-placeholder="Select Immediate Reporting Person" class="form-control">                                
                                <option value=""></option>                                            
                                <option value="1">person</option>  
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group ">
                        <div class="col-xs-4">
                            <label for="heading">Final Reporting person</label>
                        </div>
                        <div class="col-xs-6">
                            <select id="f_rep_person" name="f_rep_person" data-placeholder="Select Final Reporting Person" class="validate[required] form-control">                                
                                <option value=""></option>   
                                <option value="3">person</option>
                                <option value="4"></option>                                
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
    
        </div>
    </div>
</div>
</div>


    <!--Modal dialog box Edit end-->
<button class="btn btn-primary noty notysuccess" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Team details updated successfully &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;success&quot;}"></button>	
<button class="btn btn-primary noty notyerror" style="display:none;" data-noty-options="{&quot;text&quot;:&quot;Error while update team details &quot;,&quot;layout&quot;:&quot;topCenter&quot;,&quot;type&quot;:&quot;error&quot;}"></button>	

        <hr>
        <?php $this->load->view('includes/footer.php');?>
        </div>
                <script>
            $(document).ready(function(){
                
                var location = <?php echo json_encode(array('3' => 'Vellore','4' => 'Chennai'))?> ;
                var department = <?php echo json_encode(array('8'=> 'Admin','11'=> 'Business Development','12'=> 'E - Publishing','7'=> 'Finance','10'=> 'House Keeping','3'=> 'Human Resource','6'=> 'Litigation','1'=> 'Medical Billing','2'=> 'Medical Coding','13'=> 'Medical Transcription','4'=> 'Network','9'=> 'Others'))?> ;
                var editbtn = "<a class='btn btn-info edit'><i class='glyphicon glyphicon-edit icon-white'></i>Edit</a>";
              var oTable =  $('#teamlist').dataTable( {
                    "bProcessing": true,
//                    "bServerSide": true,
                    "sAjaxSource": "master/team_all",
                    "bDestroy" : true,
                    "bAutoWidth": false,
                    "sPaginationType": "bootstrap",
                    "bFilter": true,
                    "bInfo": true, 
                    "bJQueryUI": false ,  
                    "aoColumns": [
                        { "mDataProp": "reporting_no", "sWidth": "50px", "bSortable": true },
                        { "mDataProp": "team", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "i_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "f_rep_person", "sWidth": "125px", "bSortable": true },
                        { "mDataProp": "Loc_id", "sWidth": "50px", "bSortable": true },                        
                        { "sDefaultContent": editbtn, "sWidth": "50px", "bSortable": false }
                    ],
                    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (json) {
                            if ( json.sError ){
                                oSettings.oApi._fnLog( oSettings, 0, json.sError );
                            }
                            $(oSettings.oInstance).trigger('xhr', [oSettings, json]);
                            fnCallback( json );
                            $('#teamlist > tbody > tr ').each(function() {
                                $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                                $(this ).find('a:nth-child(1)').on('click',edit);
                                
                            });	
                        },
                      });
                  },
                "fnDrawCallback": function( oSettings ) {
                    $('#teamlist > tbody > tr ').each(function() {
                        $(this ).closest('tr').attr('data-id',$(this ).closest('tr').attr('id'));
                        $(this ).find('a:nth-child(1)').on('click',edit);
                    });	
					
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
                    
                    $(nRow).find("td").eq(2).html(department[aData['Dept_id']]);
                    $(nRow).find("td").eq(3).html(department[aData['Dept_id']]);
                    $(nRow).find("td").eq(4).html(location[aData['Loc_id']]);
                }
            });
                
               $.validationEngine.defaults.scroll = false;
               $("#formsave").validationEngine({
                        prettySelect:true,
                        useSuffix: "_chosen", 
                        maxErrorsPerField: 1,
                        promptPosition : "inline"
                    });
                $("#submit").click(function(){
                    if ( $("#formsave").validationEngine('validate')) {
                        if(checkTeam()){
//                        alert("Department details updated successfully");
                        var data = $("#formsave").serialize();
                        $.ajax({
                            type: "POST",
                            url: "master/team_save",
                            data: data,
                            success: function (result) {
                                $('#repedit-modal').modal('hide');
                                if(result === 1 || result === "1"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"You dont have access to add details","layout":"topCenter","type":"warning"}');
                                    $(".notyerror").click();
                                }else if(result === 2 || result === "2"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Please fill all details","layout":"topCenter","type":"error"}');                                
                                    $(".notyerror").click();
                                }else if(result === 3 || result === "3"){
                                    $(".notyerror").attr("data-noty-options",'{"text":"Sorry, cant save this value","layout":"topCenter","type":"error"}');
                                    $(".notyerror").click();
                                }else if(result === 4 || result === "4"){ 
                                    oTable.fnDraw();
                                    oTable.fnReloadAjax();
                                    $(".notysuccess").click();    
                                }else{
                                    window.location.href = "master/login";
                                }                           
                            }
                        });
            
                    }
                }

                    return false;
                });
                
                $("select").chosen({disable_search_threshold: 10});
                
                 function edit() {
                    $('#formsave').validationEngine('hide');
                    $(".reporting_nameformError").remove();
                    var myModal = $('#repedit-modal');
                    var tr=$(this).closest('tr');
                    $(this).closest('tr').each(function(  ) {
                        $("td", this).each(function( j ) {
                            if(j===0){
                                $("#rep_id").val($(this).text());
                            }
                            if(j===1){
                                $("#team").val($(this).text());
                            }
                            if(j===2){
//                                $("#location").val($(this).text());
                                var i_rep_person = $(this).text();
                                $('#i_rep_person option:contains('+i_rep_person+')').prop("selected", "selected");
                                $('#i_rep_person').trigger('chosen:updated');
                                
                            }
                            if(j===3){
                                
                                var f_rep_person = $(this).text();
                                $('#f_rep_person option:contains('+f_rep_person+')').prop("selected", "selected");
//                                callDepartment($("#bname").val());
                                $('#f_rep_person').trigger('chosen:updated');                               
                                
                            }
                            if(j===4){
                                
                                var brachname = $(this).text();
                                $('#bname option:contains('+brachname+')').prop("selected", "selected");
//                                callDepartment($("#bname").val());
                                $('#bname').trigger('chosen:updated');                               
                                
                            }
                        });
                        myModal.modal({ show: true });
                        return false;
                    });
                }
                $("#bname").change(function(){
//                callDepartment($(this).val());
            });
                
            });
            
            function callDepartment(brachname){
              var toappend;
                var url = "master/dept_list";
                var data =  "bname=" + brachname;                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#prj_dept').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Dept_name']+'</option>';
                                
                            });   
                            $('#prj_dept').append(toappend);
                            $("#prj_dept").trigger('chosen:updated');
                        }
                    }            
                }); 
         
            }
            function checkTeam(field, rules, i, options){
                var url = "master/team_check";
                var data = $("#formsave").serialize();
                var msg = undefined;
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
//                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(vresult) {
                        if(!vresult) {
                            msg = $("#reporting_name").val()+' is already exist with same branch';                            
                        }
                    }            
                });  
                if(msg != undefined) {
                    $(".reporting_nameformError").remove();
                    $("#reporting_name").after('<div class="reporting_nameformError parentFormmteam formError inline" style="opacity: 0.87; position: relative; top: 0px; left: 0px; margin-top: 0px;"><div class="formErrorContent">'+msg+'<br></div></div>');
                    return false;
                }
                return true;
            }
        </script>
        <?php $this->load->view('includes/additional.php');?>
    </body>
</html>