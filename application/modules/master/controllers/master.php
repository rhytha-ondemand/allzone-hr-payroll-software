<?php
class Master extends CI_Controller {
    
    public function __construct() {
      parent::__construct();
      //$this->load->model('blog/blog_model');
	  $this->load->model("master/master_model");
	  $this->load->library('auth');
            $this->load->library('employee_lib',NULL,'emp');
	   $this->load->helper('cookie');
    }
    
        public function index() {
//		  $this->load->view('login/login'); 
                $this->login();
        }
        public function login() {
            if($this->auth->checku()){
                redirect('master/dept_view_all');
            }else{
                $this->form_validation->set_rules('username', 'Username', 'required|strip_tags');
                $this->form_validation->set_rules('password', 'Password', 'required|strip_tags');

                if ($this->form_validation->run() === FALSE)
                {

                    $msg = validation_errors('<p>', '</p>');
                    $this->session->set_flashdata('message', array('class' => 'error', 'msg' => $msg));
                    //redirect(base_url($this->_CI->config->item('login_controller')));
                    $this->load->view('login/login'); 
                }
                else
                {			
		   $res=$this->master_model->user_exist();
		   $users = $this->config->item('users');
		   
		   if($res['id']!=0){
                        $uid=$res['id'];
                        $this->session->set_userdata('uid' , $uid); 
                        $roles =array('user','admin', 'blogger', 'editor', 'umpire');
	           
		        $this->session->set_userdata('role', $roles); 
			     
                        $msg = $this->config->item('login_message'); 
                        $this->session->set_flashdata('message', array('class' => 'success', 'msg' => $msg));
                        $data = array(
                            'uid' => $res['id'],
                            'roles' => array($roles[$res['role']]),
                            'username' => $res['username'],
                        );
                        $this->session->set_userdata($data);
                        $page = isset($redirect) ? $redirect : $this->config->item('login_landing_page');
                        //redirect(base_url($page));
					redirect("master/dept_view_all");	
						//redirect('/blog/preview');
				 //$this->load->view('department'); 
				
		   }
		   else
		   {
                       $data['error']="Invalid Username or Password, Please enter the valid username and password";
                        $this->load->view('login/login',$data); 
		   }
		}

            }
		 
    }
    public function logout()
    {
        $this->auth->logout();
        redirect(base_url());
		//redirect(base_ulr());
    }
	/*  department   */
    public function department() {
		
      /*$this->load->config('blog/config');
      $data['admin_page_nav'] = $this->config->item('admin_page_nav');
      $this->load->language('blog/blog', 'english');
      $data['arr'] = $this->blog_model->getArticles();
      $data['admin'] = $this->blog_model->getAdminByModel();
      
      $this->load->view('blog/blog', $data);*/
        
	  
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('department');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
    }
     public function dept_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
			     $this->form_validation->set_rules('dname', 'Department Name', 'required|strip_tags|trim|xss_clean');
			     $this->form_validation->set_rules('team', 'Team', 'required|strip_tags|trim|xss_clean');
	            /*  $this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean|callback_dept_check');
				 $this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');*/
//				  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
			     if ($this->form_validation->run())
			     {
			     $m=$this->master_model->dept_add();
			     if($m){
			     	//echo "inserted successfully";
				     $this->emp->createcookie($m);
				
				      echo 4;
				 }  
			     else
			     	//echo "error";
				      echo 3;
			     }
			     else 
			     	//echo "process error";
				      echo 2;
			}
			else
				//$this->load->view('department');
			    echo 1;
			
	   }
	   else
		   //$this->load->view('login/login'); 
	         echo 0;
	       
	 }
	  public function dept_save() 
	  {
		  header('Response: HTTP/1.1 200 OK');
		  //header('Content-Type: application/json; charset=utf-8');
		  //$this->response->addHeader('Content-Type: application/json');
    
		  if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
				$this->form_validation->set_rules('did', 'Department id', 'required|strip_tags|trim|xss_clean');
				$this->form_validation->set_rules('dname', 'Department Name', 'required|strip_tags|trim|xss_clean');
			     $this->form_validation->set_rules('team', 'Team', 'required|strip_tags|trim|xss_clean');
	             //$this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');
//				 $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
				 if ($this->form_validation->run())
			     {
			        if($this->master_model->dept_save())
			     	   //echo "Updated successfully";
				         echo 4;
					   //$this->response->setOutput(4);
			        else
			     	   //echo "error";
				         echo 3;
			     }
			     else 
			     	//echo "process error";
				      echo 2;
				
			}
			else
				//$this->load->view('department');
			       echo 1;
		}
		 else
		     //$this->load->view('login/login'); 
		        echo 0;
		  
	  }
	 
	public function dept_view() {
		if($this->auth->checku())
		{	 
		   $res['dept']=$this->master_model->dept_view();
		  $this->load->view('department_view',$res);
		}
		 else
		   $this->load->view('login/login'); 
	 }
	 public function dept_all() {
		   header('Content-Type: text/html; charset=utf-8');
//		  $res['aaData']=$this->master_model->dept_view_all();
		  $res=array("aaData"=>$this->master_model->dept_view_all());
		  //$temp=array(array("Id"=>1,"Dept_name"=>"Medical Billing","Dept_Type"=>"1","DeptId"=>"1","loc_id"=>"100"));
		  //$res=array("aaData"=>$temp);
		  echo json_encode($res);
	 }
	 public function dept_all_c() {
		   header('Content-Type: text/html; charset=utf-8');
		  //$res['aaData']=$this->master_model->dept_view_all();
		  $res=array("aaData"=>$this->master_model->dept_view_all_c());
		  //$temp=array(array("Id"=>1,"Dept_name"=>"Medical Billing","Dept_Type"=>"1","DeptId"=>"1","loc_id"=>"100"));
		  //$res=array("aaData"=>$temp);
		  echo json_encode($res);
	 }
	 
	 public function dept_all_v() {
		   header('Content-Type: text/html; charset=utf-8');
		  //$res['aaData']=$this->master_model->dept_view_all();
		  $res=array("aaData"=>$this->master_model->dept_view_all_v());
		  //$temp=array(array("Id"=>1,"Dept_name"=>"Medical Billing","Dept_Type"=>"1","DeptId"=>"1","loc_id"=>"100"));
		  //$res=array("aaData"=>$temp);
		  echo json_encode($res);
	 }
	 
	 public function dept_view_all() {
		 if($this->auth->checku())
		{
                    if ($this->acl->has_permission(strtolower( __CLASS__), 'view all'))
		    {
			    $res['dept']=$this->master_model->dept_view_all();
		       // $this->load->view('department_view',$res);
			    //$this->load->view('department_list',$res);
				$this->load->view('department_list');
			}
			else
				$this->load->view('department');
		}
		else
		   $this->load->view('login/login'); 
	 }
	 
	 public function dept_check()
	 {
             echo TRUE;
             /*
		 $dname=trim($this->input->post('dname'));
		 $bid=trim($this->input->post('bname'));
		 if ($this->master_model->dept_check($dname,$bid))
		{
			//$this->form_validation->set_message('dept_check', 'The %s field can not be the word $str');
			//return FALSE;
			echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
		 */
	 }
	 public function dept_list()
	 {
		 $str=trim($this->input->post('bname'));
		 echo json_encode($this->master_model->dept_list($str));
		 
	 }
	  public function dept_list_c()
	 {
		 echo json_encode($this->master_model->dept_list_c());
		 
	 }
	 public function dept_list_v()
	 {
		 echo json_encode($this->master_model->dept_list_v());
		 
	 }
	 
	/* Designation   */
	public function designation() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                { 
                    $this->load->view('designation');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
		
		
	}
	public function desig_add(){
		 if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
	              $this->form_validation->set_rules('desig_name', 'Designation Name', 'required|strip_tags|trim|xss_clean');
                      $this->form_validation->set_rules('team', 'Designation Type', 'required|strip_tags|trim|xss_clean');
	              $this->form_validation->set_rules('category', 'Designation Type', 'required|strip_tags|trim|xss_clean');
			     /* $this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');*/
//				 $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
                   	if ($this->form_validation->run())
                     {
	       	           if($this->master_model->desig_add())
	       	      	       //echo "inserted successfully";
					         echo 4;
	       	           else
	       	      	       //echo "error";
					         echo 3;
	                 }
		            else
			            //echo "process error";
					      echo 2;
			}
			else
				//$this->load->view('designation');
			      echo 1;
		}
		else
		   //$this->load->view('login/login'); 
	         echo 0;
	}
	public function desig_save(){
		
		
		 if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
				 $this->form_validation->set_rules('desig_id', 'Designation Id', 'required|strip_tags|trim|xss_clean');
	              $this->form_validation->set_rules('desig_name', 'Designation Name', 'required|strip_tags|trim|xss_clean');
	              $this->form_validation->set_rules('category', 'Designation Type', 'required|strip_tags|trim|xss_clean');
                      $this->form_validation->set_rules('team', 'Designation Type', 'required|strip_tags|trim|xss_clean');
			      //$this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');
//				  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
                   	if ($this->form_validation->run())
                     {
	       	           if($this->master_model->desig_save())
	       	      	      //echo "updated successfully";
					        echo 4;
	       	           else
	       	      	      //echo "error";
					        echo 3;
	                 }
		            else
			          //echo "process error";
				        echo 2;
			}
			else
				//$this->load->view('designation');
			    echo 1;
		}
		else
		   //$this->load->view('login/login'); 
	       echo 0;
	}
	public function desig_view(){
		if($this->auth->checku())
		{
		   $res['desig']=$this->master_model->desig_view();
		   $this->load->view('designation_view',$res);
		}
		else
		   $this->load->view('login/login'); 
		
	}
	public function desig_all(){
		header('Content-Type: text/html; charset=utf-8');
		 // $res['aaData']=$this->master_model->desig_view_all();
		  //$res=array("aaData"=>$this->master_model->desig_view_all());
		  $res=$this->master_model->desig_view_all();
		 
		  echo json_encode($res);
	}
	public function desig_all_c(){
		header('Content-Type: text/html; charset=utf-8');
		 // $res['aaData']=$this->master_model->desig_view_all();
		  //$res=array("aaData"=>$this->master_model->desig_view_all());
		  $res=$this->master_model->desig_view_all_c();
		 
		  echo json_encode($res);
	}
	public function desig_all_v(){
		header('Content-Type: text/html; charset=utf-8');
		 // $res['aaData']=$this->master_model->desig_view_all();
		  //$res=array("aaData"=>$this->master_model->desig_view_all());
		  $res=$this->master_model->desig_view_all_v();
		 
		  echo json_encode($res);
	}
	public function desig_view_all(){
		if($this->auth->checku())
		{
		    //$res['desig']=$this->master_model->desig_view_all();
		    //$this->load->view('designation_view',$res);
			//echo json_encode($this->master_model->desig_view_all());
			$this->load->view('designation_list');
		}
		else
		   $this->load->view('login/login'); 
			
	}
	 public function desig_check()
	 {
             echo TRUE;
/*		 $dname=trim($this->input->post('desig_name'));
		 $dtype=trim($this->input->post('team'));
		 $bid=trim($this->input->post('bname'));
                 $dcategory=trim($this->input->post('category'));
                 $dsub_category = trim($this->input->post('desig_type'));
		 if ($this->master_model->desig_check($dname,$dtype,$bid,$dcategory,$dsub_category))
		{
			//$this->form_validation->set_message('dept_check', 'The %s field can not be the word $str');
			//return FALSE;
			echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
		*/ 
	 }
	  public function desig_list()
	 {
		 $bid=trim($this->input->post('bname'));
		 echo json_encode($this->master_model->desig_list($bid));
	 }
	 
	/*  projects   */
	public function projects() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('projects');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
		 
	}
	public function projects_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
	            /*$this->form_validation->set_rules('prj_id', 'Project id', 'required|strip_tags|trim|xss_clean|callback_prjid_check');*/
			    $this->form_validation->set_rules('prj_id', 'Project id', 'required|strip_tags|trim|xss_clean');
	            $this->form_validation->set_rules('prj_name', 'Project Name', 'required|strip_tags|trim|xss_clean');
	            $this->form_validation->set_rules('prj_dept', 'Project Department', 'required|strip_tags|trim|xss_clean');
			    /*$this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');*/
				$this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
                   	if ($this->form_validation->run())
                   {
	            	if($this->master_model->projects_add())
	            		//echo "inserted successfully";
					    echo 4;
	            	else
	            		//echo "error";
					      echo 3;
	            }
	            else
	            	//echo "process error";
				      echo 2;
			}
			else
				 //$this->load->view('projects');
			     echo 1;
		}
		else
		   //$this->load->view('login/login'); 
	        echo 0;
	}
	public function projects_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
	            /*$this->form_validation->set_rules('prj_id', 'Project id', 'required|strip_tags|trim|xss_clean|callback_prjid_check');*/
				$this->form_validation->set_rules('prj_idd', 'Project id', 'required|strip_tags|trim|xss_clean');
			    $this->form_validation->set_rules('prj_id', 'Project id', 'required|strip_tags|trim|xss_clean');
	            $this->form_validation->set_rules('prj_name', 'Project Name', 'required|strip_tags|trim|xss_clean');
	            $this->form_validation->set_rules('prj_dept', 'Project Department', 'required|strip_tags|trim|xss_clean');
			    //$this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');
				$this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
                   	if ($this->form_validation->run())
                   {
	            	if($this->master_model->projects_save())
	            		//echo "updated successfully";
					    echo 4;
	            	else
	            		//echo "error";
					    echo 3;
	            }
	            else
	            	//echo "process error";
				    echo 2;
			}
			else
				 //$this->load->view('projects');
			     echo 1;
		}
		else
		   //$this->load->view('login/login'); 
	       echo 0;
	}
	public function projects_view(){
		if($this->auth->checku())
		{
		    $res['projects']=$this->master_model->projects_view();
		    $this->load->view('projects_view',$res);
		}
		else
		   $this->load->view('login/login'); 
	}
	public function projects_all(){
		$res=$this->master_model->projects_view_all();
		 
		  echo json_encode($res);
	}
	public function projects_all_c(){
		$res=$this->master_model->projects_view_all_c();
		 
		  echo json_encode($res);
	}
	public function projects_all_v(){
		$res=$this->master_model->projects_view_all_v();
		 
		  echo json_encode($res);
	}
	public function projects_list_det(){
		$res=$this->master_model->projects_list_det();
		echo json_encode($res);
	}
	
	public function projects_view_all(){
		if($this->auth->checku())
		{
	        //$res['projects']=$this->master_model->projects_view_all();
		    //$this->load->view('projects_view',$res);
			//	echo json_encode($this->master_model->projects_view_all());
			$this->load->view('projects_list');
		}
        else
		   $this->load->view('login/login'); 		
	}
	public function prjid_check(){
		 $prid=trim($this->input->post('prj_id'));
		 $prdept=trim($this->input->post('prj_dept'));
		 $bid=trim($this->input->post('bname'));
		 if ($this->master_model->prjid_check($prid,$prdept,$bid))
		{
			//$this->form_validation->set_message('prjid_check', 'The %s field can not be the word $str');
			//return FALSE;
			 echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
	}
	public function projects_list()
	{
		$prdept=trim($this->input->post('prj_dept'));
		 $bid=trim($this->input->post('bname'));
		 //echo json_encode($this->master_model->projects_list($prdept,$bid));
		 echo json_encode($this->master_model->projects_list($bid));
	}
	public function projects_all_det()
	{
		 //echo json_encode($this->master_model->projects_list($prdept,$bid));
		 echo json_encode($this->master_model->projects_all());
	}
	/*  team    */
	public function team() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('team');
                }else{
                $this->load->view('master/noaccess');
//                redirect("master/noaccess");
            }
            }else{
                redirect("master/login");
            }
            
	}
	/*public function team_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		      $this->form_validation->set_rules('reporting_name', 'Reporting Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('prj_dept', 'Project Department', 'required|strip_tags|trim|xss_clean');
			
			  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->team_add())
		      		//echo "inserted successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	        echo 0;
	}
	public function team_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
			  $this->form_validation->set_rules('rep_id', 'Reporting id', 'required|strip_tags|trim|xss_clean');
		      $this->form_validation->set_rules('reporting_name', 'Reporting Name', 'required|strip_tags|trim|xss_clean');
			 // $this->form_validation->set_rules('report_name', 'Reporting Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('prj_dept', 'Project Department', 'required|strip_tags|trim|xss_clean');
			 // $this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->team_save())
		      		//echo "updated successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	         echo 0;
	}*/
	
	public function team_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		      $this->form_validation->set_rules('team', 'Team Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('i_rep_person', 'Immediate reporting person', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('f_rep_person', 'Final reporting person', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->team_add())
		      		//echo "inserted successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	        echo 0;
	}
	public function team_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
			  $this->form_validation->set_rules('rep_id', 'Reporting id', 'required|strip_tags|trim|xss_clean');
		      $this->form_validation->set_rules('team', 'Team Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('i_rep_person', 'Immediate reporting person', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('f_rep_person', 'Final reporting person', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->team_save())
		      		//echo "updated successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	         echo 0;
	}
	
	
	public function team_view(){
		if($this->auth->checku())
		{
		    $res['team']=$this->master_model->team_view();
		    $this->load->view('team_view',$res);
		}
		else
		   $this->load->view('login/login');
	}
	public function team_all(){
		$res=$this->master_model->team_view_all();
		 
		  echo json_encode($res);
	}
	public function team_all_c(){
		
		
		$res=$this->master_model->team_view_all_c();
		 
		 echo json_encode($res);
	}
	
	public function team_all_v(){
		$res=$this->master_model->team_view_all_v();
		 
		  echo json_encode($res);
	}
	public function team_view_list(){
		$res=$this->master_model->team_view_list();
		echo json_encode($res);
	}
	public function team_list(){
		$res=$this->master_model->team_list();
		echo json_encode($res);
	}
	
	public function team_view_all(){
		if($this->auth->checku())
		{
	       // $res['team']=$this->master_model->team_view_all();
		    //$this->load->view('team_view',$res);
			//echo json_encode($this->master_model->team_view_all());
			$this->load->view('team_list');
		}
        else
		   $this->load->view('login/login');		
	}
	public function team_check()
	{
            echo TRUE;
            /*
		 //$prid=trim($this->input->post('reporting_name'));
		 $prid=trim($this->input->post('team'));
		 //$prdept=trim($this->input->post('prj_dept'));
		 $bid=trim($this->input->post('bname'));
		// if ($this->master_model->team_check($prid,$prdept,$bid))
	     if ($this->master_model->team_check($prid,$bid))
		{
			//$this->form_validation->set_message('prjid_check', 'The %s field can not be the word $str');
			//return FALSE;
			 echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}*/
	}
	
	
	/* bank     */
	public function bank() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('bank');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
		 
	}
	public function bank_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		      $this->form_validation->set_rules('ba_name', 'Bank Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('ba_branch', 'Bank Branch', 'required|strip_tags|trim|xss_clean');
			  /*$this->form_validation->set_rules('br_id', 'Branch Name', 'required|strip_tags|trim|xss_clean');*/
			  $this->form_validation->set_rules('ba_add', 'Bank add', 'required|strip_tags|trim|xss_clean');
			   $this->form_validation->set_rules('bname', 'Location Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->bank_add())
		      		//echo "inserted successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	        echo 0;
	}
	
	public function bank_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
			  $this->form_validation->set_rules('id', 'Bank id', 'required|strip_tags|trim|xss_clean');
		      $this->form_validation->set_rules('ba_name', 'Bank Name', 'required|strip_tags|trim|xss_clean');
			 
			  $this->form_validation->set_rules('ba_branch', 'Bank Branch', 'required|strip_tags|trim|xss_clean');
			 
			  $this->form_validation->set_rules('ba_add', 'Bank Address', 'required|strip_tags|trim|xss_clean');
			   $this->form_validation->set_rules('bname', 'Location Name', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->bank_save())
		      		//echo "updated successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	         echo 0;
	}
        
        public function bank_list(){
            $res = $this->master_model->bank_list();
            echo json_encode($res);
        }
	
	public function bank_view(){
		if($this->auth->checku())
		{
//		    $res['bank']=$this->master_model->bank_view_all();
//		    $this->load->view('bank_list',$res);
                     $this->load->view('bank_list');
		}
		else
		   $this->load->view('login/login');
	}
	public function bank_list_det(){
		$res=$this->master_model->bank_list_det();
		echo json_encode($res);
	}
	public function bank_all(){
		$res=$this->master_model->bank_view_all();
		 
		  echo json_encode($res);
	}
	
	public function bank_check(){
		  $baname=trim(strtolower($this->input->post('ba_name')));
		  $ba_branch=trim(strtolower($this->input->post('ba_branch')));
		  $loc=trim(strtolower($this->input->post('bname')));
		// if ($this->master_model->team_check($prid,$prdept,$bid))
	     if ($this->master_model->bank_check($baname,$ba_branch,$loc))
		{
			//$this->form_validation->set_message('prjid_check', 'The %s field can not be the word $str');
			//return FALSE;
			 echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
	}
	
	/* referral incentive */
	public function ref_incentive() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('incentive');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
		 
	}
	public function ref_incentive_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		      $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('cadre', 'cadre', 'required|strip_tags|trim|xss_clean');
			  
			  $this->form_validation->set_rules('amt', 'Amount', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('duration', 'Duration', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('period', 'Period', 'required|strip_tags|trim|xss_clean');
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->ref_incentive_add())
		      		//echo "inserted successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	        echo 0;
	}
	
	public function ref_incentive_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
			  $this->form_validation->set_rules('id', 'id', 'required|strip_tags|trim|xss_clean');
		      $this->form_validation->set_rules('bname', 'Branch Name', 'required|strip_tags|trim|xss_clean');
			 
			  $this->form_validation->set_rules('cadre', 'cadre', 'required|strip_tags|trim|xss_clean');
			  
			  $this->form_validation->set_rules('amt', 'Amount', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('duration', 'Duration', 'required|strip_tags|trim|xss_clean');
			  $this->form_validation->set_rules('period', 'Period', 'required|strip_tags|trim|xss_clean');
			  
              	if ($this->form_validation->run())
              {
		      
		      	if($this->master_model->ref_incentive_save())
		      		//echo "updated successfully";
				      echo 4;
		      	else
		      		//echo "error";
				      echo 3;
		      }
		      else
			      //echo "process error";
			        echo 2;
			}
			else
				 //$this->load->view('team');
			       echo 1;
				
		}
		else
		   //$this->load->view('login/login'); 
	         echo 0;
	}
	
	public function ref_incentive_view(){
		if($this->auth->checku())
		{
		    $res['bank']=$this->master_model->ref_incentive_view();
		    $this->load->view('incentive_list',$res);
		}
		else
		   $this->load->view('login/login');
	}
        public function ref_incentive_list(){
            $res=$this->master_model->ref_incentive_list();
            echo json_encode($res);
        }
	public function ref_incentive_all(){
		
	   
	   
	   $res=$this->master_model->ref_incentive_all();
		 
		  echo json_encode($res);
	}
	public function ref_incentive_check(){
		  $bname=trim(strtolower($this->input->post('bname')));
		  $cadre=trim(strtolower($this->input->post('cadre')));
		
	     if ($this->master_model->ref_incentive_check($bname,$cadre))
		{
			
			 echo FALSE;
		}
		else
		{
			
			echo TRUE;
		}
	}
	
	/* location */
	
	public function location() {
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('location');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
		 
	}
	public function location_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		        /*$this->form_validation->set_rules('loc_name', 'Location Name', 'required|strip_tags|trim|xss_clean|callback_loc_check');*/
				$this->form_validation->set_rules('loc_name', 'Location Name', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {	
		        	if($this->master_model->location_add())
		        		//echo "inserted successfully";
					    echo 4;
		        	else
		        		//echo "error";
					    echo 3;
		        }
		        else
		        	//echo "process error";
				      echo 2;
			}
			else
				 //$this->load->view('location');
			       echo 1;
		}
		else
		   //$this->load->view('login/login');	
	         echo 0;
	}
	public function location_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
				 $this->form_validation->set_rules('loc_id', 'Location id', 'required|strip_tags|trim|xss_clean');
		        /*$this->form_validation->set_rules('loc_name', 'Location Name', 'required|strip_tags|trim|xss_clean|callback_loc_check');*/
				$this->form_validation->set_rules('loc_name', 'Location Name', 'required|strip_tags|trim|xss_clean');
                if ($this->form_validation->run())
                {	
		        	if($this->master_model->location_save())
		        		//echo "updated successfully";
					      echo 4;
		        	else
		        		//echo "error";
					      echo 3;
		        }
		        else
		        	//echo "process error";
				      echo 2;
			}
			else
				 //$this->load->view('location');
			       echo 1;
		}
		else
		   //$this->load->view('login/login');	
	         echo 0;
	}
	public function location_view(){
		if($this->auth->checku())
		{
		    $res['location']=$this->master_model->location_view();
		    $this->load->view('location_view',$res);
		}
		else
		   $this->load->view('login/login');
	}
	
	public function location_all(){
		$res=$this->master_model->location_view_all();
		 
		  echo json_encode($res);
	}
	public function location_list(){
		//$bid=$this->input->post('bname');
		//echo json_encode($this->master_model->location_list($bid));
		//echo json_encode($this->master_model->location_view_list());
		 $res=$this->master_model->location_view_list();
		  echo json_encode($res);
	}
	
	public function location_view_all(){
		if($this->auth->checku())
		{
	       // $res['location']=$this->master_model->location_view_all();
		    //$this->load->view('location_view',$res);
			
			//echo json_encode($this->master_model->location_view_all());
			$this->load->view('location_list');
		}
		else
		   $this->load->view('login/login');
		
	}
	public function loc_check(){
		$str=$this->input->post('loc_name');
		 if ($this->master_model->loc_check($str))
		{
			//$this->form_validation->set_message('loc_check', 'The %s field can not be the word $str');
			//return FALSE;
			echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
	}
	/* location with city  */
	public function location_city_add() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
		    {
		        
				$this->form_validation->set_rules('loc_city', 'City Name', 'required|strip_tags|trim|xss_clean');
				$this->form_validation->set_rules('bname', 'branch Name', 'required|strip_tags|trim|xss_clean');
				$bname=$this->input->post('bname');
				$loc_city=$this->input->post('loc_city');
			    if($this->loc_city_check($bname,$loc_city))
                {    if ($this->form_validation->run())
                    {	
		            	if($this->master_model->location_city_add())
		            		//echo "inserted successfully";
				     	    echo 4;
		            	else
		            		//echo "error";
				     	    echo 3;
		            }
		            else
		            	//echo "process error";
				           echo 2;
				}
				else
					 echo 5;
			}
			else
				 //$this->load->view('location');
			       echo 1;
		}
		else
		   //$this->load->view('login/login');	
	         echo 0;
	}
	
	public function location_city_save() {
		if($this->auth->checku())
		{
			if ($this->acl->has_permission(strtolower( __CLASS__), 'edit'))
		    {
				$this->form_validation->set_rules('bname', 'Branch id', 'required|strip_tags|trim|xss_clean');
		       	$this->form_validation->set_rules('loc_city', 'City Name', 'required|strip_tags|trim|xss_clean');
				$this->form_validation->set_rules('city_id', 'City Id', 'required|strip_tags|trim|xss_clean');
				
				$bname=$this->input->post('bname');
				$loc_city=$this->input->post('loc_city');
				//$bname=4;
				//$loc_city='egmore2';
				//$city_id=1;
				if($this->loc_city_check($bname,$loc_city))
				{
                    if ($this->form_validation->run())
                    {	
		            	if($this->master_model->location_city_save())
					    //  if($this->master_model->location_city_save($bname,$loc_city,$city_id))
		            		//echo "updated successfully";
				     	      echo 4;
		            	else
		            		//echo "error";
				     	      echo 3;
		            }
		            else
		            	//echo "process error";
				           echo 2;
				}
				else
					echo 5;
			}
			else
				 //$this->load->view('location');
			       echo 1;
		}
		else
		   //$this->load->view('login/login');	
	         echo 0;
	}
	
	 
	public function loc_city_check($bname,$loc_city){
		//public function loc_city_check(){
		//$str=$this->input->post('loc_name');
		
		//$bname=$this->input->post('bname');
		//$loc_city=$this->input->post('loc_city');
		//$loc_city="Egmore2";
		 if ($this->master_model->loc_city_check($bname,$loc_city))
		{
			//$this->form_validation->set_message('loc_check', 'The %s field can not be the word $str');
			//return FALSE;
			echo FALSE;
		}
		else
		{
			//return TRUE;
			echo TRUE;
		}
	}
	
	public function location_city_all()
	 {
	//$bid=3;
	//$res=$this->master_model->location_city_all($bid);
	$res=$this->master_model->location_city_all();
		 
		  echo json_encode($res);
	 }
	
	
	/* additional */
    public function preview() {
      $data = array();
      return $this->load->view('blog/preview', $data, true);
    }
	
	public function emp_id_all()
	{
		echo json_encode($this->master_model->emp_id_all());		
	}
	public function emp_id_list()
	{
		echo json_encode($this->master_model->emp_id_list());		
	}
        public function emp_id_list_act()
	{
		echo json_encode($this->master_model->emp_id_list_act());		
	}
	public function emp_team(){
		echo json_encode($this->master_model->emp_team());		
	}
	  public function emp_dynamic(){
            $bname=trim($this->input->post('bname'));
            $dept=$this->master_model->dept_list(4);
            $teamlist=$this->master_model->team_list();
            $designation=$this->master_model->desig_list($bname);
            $project=$this->master_model->projects_list($bname); 
            $location_city = $this->master_model->location_city_list($bname);
            echo json_encode(array("teamlist"=>$teamlist,"dept"=>$dept,"design"=>$designation,"project"=>$project,"location_city"=>$location_city));
             
        }
	public function team_info(){
		
		//$x=array("test1","1","2","3");
		//echo json_encode($x);
		$res=$this->master_model->team_info();
		 
		  echo json_encode($res);
		  //echo json_encode($x);
	}
	public function shift_info(){
		
		//$x=array("test1","1","2","3");
		
		//$x=array("test1","1","2","3");
		$x=array("1"=>"Day",
		"2"=>"General",
		"3"=>"Semi",
		"4"=>"Night",
		);
		//echo json_encode($x);
		//$res=$this->master_model->team_info();
		 
		  echo json_encode($x);
		  //echo json_encode($x);
	}
	public function process_info(){
		
		//$x=array("test1","1","2","3");
		//echo json_encode($x);
		$res=$this->master_model->process_info();
		 
		  echo json_encode($res);
		  //echo json_encode($x);
	}
	
	
	public function sample(){
		//$x=array("label"=>"test1","value"=>"1");
		$x=array("test1","1","2","3");
		echo json_encode($x);
	}
        public function employer(){
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->load->view('employer_add');
                }
                else{
                    $this->load->view('master/noaccess');
                }
            }else{
		redirect("master/login");
            }
            
        }
//        bname=15&comp_name=Allzone&addr1=ewqewq&addr2=qewqe&addr3=wqewqew&pf_no=wqewqe&esi_no=wqewq&tin_no=ewqewqe
//&pan_no=eqwe&location=4&pta_no=eqwew
        public function employer_add(){
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->form_validation->set_rules('comp_name', 'Company Name', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('bname', 'Branch', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr1', 'Address 1', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr2', 'Address 2', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr3', 'Address 3', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pf_no', 'PF No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('esi_no', 'ESI No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('tin_no', 'TIN No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pan_no', 'PAN no', 'required|strip_tags|trim|xss_clean');
//                    $this->form_validation->set_rules('location', 'Location', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pta_no', 'PTA No', 'required|strip_tags|trim|xss_clean');
                    if ($this->form_validation->run())
                    {
                        $m=$this->master_model->employer_add();
                        if($m){
    //                       echo "inserted successfully";
//                            $this->emp->createcookie($m);
                            echo 4;
                        }  
                        else{
    //                      echo "error";
                            echo 3;
                        }
                    }
                    else {
//                      echo "process error";
                        echo 2;
                    }
                }
                else{
//                  $this->load->view('department');
                    echo 1;
                }
            }
	   else{
//              $this->load->view('login/login'); 
	        echo 0;
           }
        }
        public function employer_check()
        {
            $bname=trim($this->input->post('bname'));
            if ($this->master_model->employer_check($bname))
            {
                echo FALSE;
            }
            else
            {
                echo TRUE;
            }
        }
        public function employer_view_all(){
            if($this->auth->checku())
            {
                $this->load->view('employer_list');
            }else{
                redirect(master/login);
            }
        }
        public function employer_all(){
            $result =$this->master_model->employer_all();
            echo json_encode(array("aaData"=>$result));
        }
        public function employer_save(){
            if($this->auth->checku())
            {
                if ($this->acl->has_permission(strtolower( __CLASS__), 'add'))
                {
                    $this->form_validation->set_rules('comp_name', 'Company Name', 'required|strip_tags|trim|xss_clean');
//                    $this->form_validation->set_rules('bname', 'Branch', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr1', 'Address 1', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr2', 'Address 2', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('addr3', 'Address 3', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pf_no', 'PF No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('esi_no', 'ESI No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('tin_no', 'TIN No', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pan_no', 'PAN no', 'required|strip_tags|trim|xss_clean');
//                    $this->form_validation->set_rules('location', 'Location', 'required|strip_tags|trim|xss_clean');
                    $this->form_validation->set_rules('pta_no', 'PTA No', 'required|strip_tags|trim|xss_clean');
                    if ($this->form_validation->run())
                    {
                        $m=$this->master_model->employer_save();
                        if($m){
                            echo 4;
                        }  
                        else{
                            echo 3;
                        }
                    }
                    else {
                        echo 2;
                    }
                }
                else{
                    echo 1;
                }
            }
	   else{
	        echo 0;
           }
        }
        
    public function noaccess(){
        if($this->auth->checku()){
            $this->load->view("noaccess");
        }else{
            redirect(master/login);
        }           
            
    } 
}
