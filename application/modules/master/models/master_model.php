<?php
class Master_model extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
    }
    
    public function getArray() {
      return array(1,2,3,4,5,6);
    }
    
    public function getAdmin() {
      return array('login'=> 'angel', 'fname' => 'Angel', 'lname' => 'Baev');
    }
	
	public function user_exist()
	{
	    if(trim($this->input->post('username'))!='' && trim($this->input->post('password'))!=''){
			$this->db->where('username', trim($this->input->post('username')));
			$this->db->where('password', md5(trim($this->input->post('password'))));
			$this->db->where('status','1');
			$query = $this->db->get("users");

				if( $query->num_rows == 1 )  {
					$users= $query->result_array();
					$this->db->where("Id",$users[0]['Id']);
					$this->db->update('users',array("Last_login"=>date("Y-m-d H:i:s")));
					return array("id"=>$users[0]['Id'],"role"=>$users[0]['Role'],"username"=>$users[0]['Username']);
				}
				else
					return 0;
		}
		else
			return false;
		
	}
	public function dept_add()
	{
		/*if(trim($this->input->post('dname'))!='' && trim($this->input->post('team'))!=0 && trim($this->input->post('br_id')!='' ))*/
		if(trim($this->input->post('dname'))!='' && trim($this->input->post('team'))!=0)
		{
			
			$dname=trim($this->input->post('dname'));
			$dtype=trim($this->input->post('team'));
			/*$loc_id=trim($this->input->post('br_id'));*/
			$loc_id= 4;
			$data=array(
			'dept_name'=>$dname,
			'dept_type'=>$dtype,
			'loc_id'=>$loc_id
			);
			$this->db->insert('department',$data);
			return $this->db->insert_id();
	   }
	   else
		  return false;
	}
	
	public function dept_save()
	{
		if(trim($this->input->post('did'))!='' && trim($this->input->post('dname'))!='' && trim($this->input->post('team'))!=0 )
		{
		/*if(trim($this->input->post('did'))!='' && trim($this->input->post('dname'))!='' && trim($this->input->post('team'))!=0 && trim($this->input->post('br_id')!='' ))
		{
	    /*if(trim($this->input->post('dname'))!='' && trim($this->input->post('team'))!=0 && trim($this->input->post('br_id')!='' ))
		{*/
			$id=trim($this->input->post('did'));
			$dname=trim($this->input->post('dname'));
			$dtype=trim($this->input->post('team'));
			//$loc_id=trim($this->input->post('br_id'));
//			$loc_id=trim($this->input->post('bname'));
			$data=array(
			'dept_name'=>$dname,
			'dept_type'=>$dtype,
			'loc_id'=>4
			);
			$this->db->trans_start();
			$this->db->where('id',$id);
			//$this->db->where('id',2);
			$this->db->update('department',$data);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
				return false;
			else
				return true;
	   }
	   else
		  return false;
	}
	
		
	public function dept_view()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('department',array('dept_type'=>$this->input->post('dept')));
		 
	}
	/*public function dept_view_all()
	{
		 $this->db->cache_off();
		$res=$this->db->get('department');
		  $dept=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 $dept[$temp['Id']]=$temp;
			  }
			  
			  return $dept;
		  }
		  else
			  return false;
	}*/
	public function dept_view_all()
	{
		 $this->db->cache_off();
		$res=$this->db->get('department');
		  $dept=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;
		  }
		  else
			  return false;
	}
	
	public function dept_view_all_c()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('department',array('Loc_id'=>4));
		  $dept=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;
		  }
		  else
			  return false;
	}
		public function dept_view_all_v()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('department',array('Loc_id'=>3));
		  $dept=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;
		  }
		  else
			  return false;
	}
	
	
	public function dept_check($dname,$bid)
	 {
		
		 $this->db->cache_off();
		//$res=$this->db->get_where('department',array('dept_name'=>trim($str))); 
		$res=$this->db->select('Id')->where('dept_name',trim($dname))
		                            ->where('loc_id',trim($bid))
		                            ->get('department');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
	 }
	 
	 public function dept_list($bname){
		 $this->db->cache_off();
                 $res=$this->db->select('Id,Dept_name')->get('department');
//		 $res=$this->db->select('Id,Dept_name')->where('loc_id',trim($bname))->get('department');
		  $dept=array();
		 if($res->num_rows>0)
		 {
			 $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;			 
		 }
		 else
			 return false;
			 
	 }
	 public function dept_list_c(){
		 $this->db->cache_off();
                 $res=$this->db->select('Id,Dept_name')->get('department');
//		 $res=$this->db->select('Id,Dept_name')->where('loc_id',4)->get('department');
		  $dept=array();
		 if($res->num_rows>0)
		 {
			 $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;
			 
		 }
		 else
			 return false;
			 
	 }
	 
	 public function dept_list_v(){
		 $this->db->cache_off();
                 $res=$this->db->select('Id,Dept_name')->get('department');
//		 $res=$this->db->select('Id,Dept_name')->where('loc_id',3)->get('department');
		  $dept=array();
		 if($res->num_rows>0)
		 {
			 $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($dept,$temp);
			  }
			  
			  return $dept;
			 
		 }
		 else
			 return false;
			 
	 }
	 
	/*  designation */
	public function desig_add()
	{
	  /*if(trim($this->input->post('desig_type'))!='' && trim($this->input->post('desig_type')!=0) && trim($this->input->post('br_id')!='')){*/	
	  if(trim($this->input->post('desig_name'))!='' && trim($this->input->post('category')!=0)){
		$dname=trim($this->input->post('desig_name'));
		$dtype=trim($this->input->post('desig_type'));
                $category=trim($this->input->post('category'));
                $team=trim($this->input->post('team'));
		/*$loc_id=trim($this->input->post('br_id'));*/
//		$loc_id=trim($this->input->post('bname'));
		$data=array(
		'design_name'=>$dname,
		'design_type'=>$dtype,
                'category'=>$category,
		'team'=>$team,
                'loc_id'=>4
		);
		$this->db->insert('designation',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
		public function desig_save()
	{
	 // if(trim($this->input->post('desig_id'))!='' && trim($this->input->post('desig_type'))!='' && trim($this->input->post('desig_type')!=0) && trim($this->input->post('br_id')!=''))
		  if(trim($this->input->post('desig_id'))!='' && trim($this->input->post('category'))!='' && trim($this->input->post('team')!=0))
	 {	
	  /*if(trim($this->input->post('desig_type'))!='' && trim($this->input->post('desig_type')!=0) && trim($this->input->post('br_id')!='')){*/
	   $id=trim($this->input->post('desig_id'));
		$dname=trim($this->input->post('desig_name'));
		$dtype=trim($this->input->post('desig_type'));
                $category=trim($this->input->post('category'));
                $team=trim($this->input->post('team'));
		//$loc_id=trim($this->input->post('br_id'));
		$loc_id=4;
		$data=array(
		'design_name'=>$dname,
		'design_type'=>$dtype,
                'category'=>$category,
                'team'=>$team,
		'loc_id'=>$loc_id
		);
		$this->db->trans_start();
		$this->db->where('id',$id);
		//$this->db->where('id',2);
		$this->db->update('designation',$data);
		$this->db->trans_complete();
		
			if ($this->db->trans_status() === FALSE)
			{
				return false;
			}
			else
				return true;
	  }
	  else
		  return false;
		
		
	}
	public function desig_view()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('designation',array('desig_type'=>$this->input->post('desig_type')));
		 
	}
	public function desig_view_all()
	{
		 $this->db->cache_off();
		$res=$this->db->get('designation');
		
		  $desig=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$desig[$temp['Id']]=$temp;
				  array_push($desig,$temp);
			  }
			  $de=array("aaData"=>$desig);
			  return $de;
			  //return $desig;
		  }
		  else
			  return array("aaData"=>'');
	}
	public function desig_view_all_c()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('designation',array('Loc_id'=>4));
		
		  $desig=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$desig[$temp['Id']]=$temp;
				  array_push($desig,$temp);
			  }
			  $de=array("aaData"=>$desig);
			  return $de;
			  //return $desig;
		  }
		  else
			  return false;
	}
	public function desig_view_all_v()
	{
		 $this->db->cache_off();
		$res=$this->db->get_where('designation',array('Loc_id'=>3));
		
		  $desig=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$desig[$temp['Id']]=$temp;
				  array_push($desig,$temp);
			  }
			  $de=array("aaData"=>$desig);
			  return $de;
			  //return $desig;
		  }
		  else
			  return false;
	}
		public function desig_check($dname,$dtype,$bid,$dcategory,$dsub_category='')
	{
		 $this->db->cache_off();
		//$res=$this->db->get_where('projects_det',array('project_name'=>trim($str))); 
		$res=$this->db->select('Id')->where('design_name',trim($dname))
		                            ->where('team',trim($dtype))
                                            ->where('category',trim($dcategory))                                            
                                            ->where('loc_id',trim($bid))
		                            ->get('designation');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
		
	}
	public function desig_list($bid)
	{
            $bid='4';
	   $this->db->cache_off();
		//$res=$this->db->get_where('projects_det',array('project_name'=>trim($str))); 
		$res=$this->db->select('Id,Design_name')->where('loc_id',trim($bid))
		                            ->get('designation');
		$desig=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$desig[$temp['Id']]=$temp;
				  array_push($desig,$temp);
			  }
			  return $desig;
		  }
		  else
			  return false;
	}
	
	
	/*  project */
	public function projects_add()
	{
	/*if(trim($this->input->post('prj_id'))!='' && trim($this->input->post('prj_name')) && trim($this->input->post('prj_dept')) && trim($this->input->post('br_id')) )*/
	if(trim($this->input->post('prj_id'))!='' && trim($this->input->post('prj_name')) && trim($this->input->post('prj_dept')) && trim($this->input->post('bname')) )
		{	
			$prj_id=trim($this->input->post('prj_id'));	
			$prj_name=trim($this->input->post('prj_name'));	
			$prj_dept=trim($this->input->post('prj_dept'));	
			/*$loc_id=trim($this->input->post('br_id'));	*/
			$loc_id=trim($this->input->post('bname'));
			$data=array(
			'project_no'=>$prj_id,
			'project_name'=>$prj_name,
			'project_dept'=>$prj_dept,
			'loc_id'=>$loc_id
			);
			$this->db->insert('projects_det',$data);
		return $this->db->insert_id();
		}
		 else
		  return false;
	}
	
public function projects_save()
	{
	if(trim($this->input->post('prj_idd'))!='' && trim($this->input->post('prj_id'))!='' && trim($this->input->post('prj_name')) && trim($this->input->post('prj_dept')) && trim($this->input->post('bname')) )
		{	
	/*if(trim($this->input->post('prj_idd'))!='' && trim($this->input->post('prj_id'))!='' && trim($this->input->post('prj_name')) && trim($this->input->post('prj_dept')) && trim($this->input->post('br_id')) )
		{	
	if(trim($this->input->post('prj_id'))!='' && trim($this->input->post('prj_name')) && trim($this->input->post('prj_dept')) && trim($this->input->post('br_id')) )
		{*/
	        $id=trim($this->input->post('prj_idd'));	
			$prj_id=trim($this->input->post('prj_id'));	
			$prj_name=trim($this->input->post('prj_name'));	
			$prj_dept=trim($this->input->post('prj_dept'));	
			//$loc_id=trim($this->input->post('br_id'));	
			$loc_id=trim($this->input->post('bname'));	
			$data=array(
			'project_no'=>$prj_id,
			'project_name'=>$prj_name,
			'project_dept'=>$prj_dept,
			'loc_id'=>$loc_id
			);
			$this->db->trans_start();
			$this->db->where('project_id',$id);
			//$this->db->where('project_id',7);
			$this->db->update('projects_det',$data);
			$this->db->trans_complete();
			if($this->db->trans_status()===FALSE)
				return false;
			else
				return true;
		}
		 else
		  return false;
	}
	
	
	/*public function projects_view()
	{
		$res=$this->db->get_where('projects',array('project_dept'=>$this->input->post('prj_dept')));
		
	}*/
	public function projects_view_all()
	{		
	 $this->db->cache_off();
		//$res=$this->db->get('projects_det',50,0);
		$res=$this->db->get('projects_det');
		  
		  $pname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  
			  foreach($res_arr as $key => $temp)
			  {
				  //$pname[$temp['project_id']]=$temp;
				   array_push($pname,$temp);
			  }
			   $pn=array("aaData"=>$pname);
			  return $pn;
			  
		  }
		  else
			  return false;
	}
	public function projects_view_all_c()
	{		
	 $this->db->cache_off();
		//$res=$this->db->get('projects_det',50,0);
		$res=$this->db->get_where('projects_det',array('Loc_id'=>4));
		  
		  $pname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  
			  foreach($res_arr as $key => $temp)
			  {
				  //$pname[$temp['project_id']]=$temp;
				   array_push($pname,$temp);
			  }
			   $pn=array("aaData"=>$pname);
			  return $pn;
			  
		  }
		  else
			  return false;
	}
	public function projects_view_all_v()
	{		
	 $this->db->cache_off();
		//$res=$this->db->get('projects_det',50,0);
		$res=$this->db->get_where('projects_det',array('Loc_id'=>3));
		  
		  $pname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  
			  foreach($res_arr as $key => $temp)
			  {
				  //$pname[$temp['project_id']]=$temp;
				   array_push($pname,$temp);
			  }
			   $pn=array("aaData"=>$pname);
			  return $pn;
			  
		  }
		  else
			  return false;
	}
	public function projects_list_det()
	{		
	 $this->db->cache_off();
		$loc=$this->input->get('bname');
		$res=$this->db->get_where('projects_det',array('Loc_id'=>$loc));
		  
		  $pname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  
			  foreach($res_arr as $key => $temp)
			  {
				  //$pname[$temp['project_id']]=$temp;
				   array_push($pname,$temp);
			  }
			   $pn=array("aaData"=>$pname);
			  return $pn;
			  
		  }
		  else{
			  return array("aaData"=>'');
                  }
	}
	public function prjid_check($prid,$prdept,$bid)
        {
		 $prjname = trim($this->input->post('prj_name'));
                 $prj_idd = trim($this->input->post('prj_idd'));
		 $this->db->cache_off();
		//$res=$this->db->get_where('projects_det',array('project_name'=>trim($str))); 
//		$res=$this->db->select('Project_Id')->where('project_no',trim($prid))	
//	                            ->where('project_dept',trim($prdept))
//                                            ->where('loc_id',trim($bid))
//		                            ->get('projects_det');
                 $where = "Project_id != '$prj_idd' AND (Project_no = '$prid' OR Project_name = '$prjname' ) AND Project_dept = '$prdept' AND Loc_id = '$bid'";
		$res=$this->db->select('Project_Id')->where($where)->get('projects_det');
		if($res->num_rows>0){
			return TRUE;
		}
		else
                    return FALSE;
        }
	 //public function projects_list($prdept,$bid)
	 public function projects_list($bid)
	 {
		  $this->db->cache_off();
		 $res=$this->db->select('Project_Id,Project_no,Project_name')
									->where('loc_id',trim($bid))
		                            ->get('projects_det');
		$prj=array();
		 if($res->num_rows>0)
		 {
			 $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($prj,$temp);
			  }
			  
			  return $prj;
			 
		 }
		 else
			 return false;
		 
	 }
	 
	  public function projects_all()
	 {
		  $this->db->cache_off();
		 $res=$this->db->select('Project_Id,Project_no,Project_name')->get('projects_det');
		$prj=array();
		 if($res->num_rows>0)
		 {
			 $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$dept[$temp['Id']]=$temp;
				 array_push($prj,$temp);
			  }
			  
			  return $prj;
			 
		 }
		 else
			 return false;
		 
	 }
	
	/* team */
	public function team_add()
	{
	  
	  if(trim($this->input->post('team'))!='' && trim($this->input->post('i_rep_person'))!=''&& trim($this->input->post('f_rep_person'))!='' && trim($this->input->post('bname'))!='')
	  {	
		$rname=trim($this->input->post('team'));
		$iper=trim($this->input->post('i_rep_person'));
		$fper=trim($this->input->post('f_rep_person'));
		$loc_id=trim($this->input->post('bname'));
		$data=array(
		'team'=>$rname,
		'i_rep_person'=>$iper,
		'f_rep_person'=>$fper,
		'loc_id'=>$loc_id
		);
		$this->db->insert('reporting_name',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
	 public function team_save()
	{
	if(trim($this->input->post('rep_id'))!='' && trim($this->input->post('team'))!='' && trim($this->input->post('i_rep_person'))!='' && trim($this->input->post('f_rep_person'))!='' && trim($this->input->post('bname'))!='')
	/*if(trim($this->input->post('rep_id'))!='' && trim($this->input->post('team'))!=''&& trim($this->input->post('bname'))!='')*/
	  {
	 
	    $id=trim($this->input->post('rep_id'));
		$rname=trim($this->input->post('team'));
		$iperson=trim($this->input->post('i_rep_person'));
		$fperson=trim($this->input->post('f_rep_person'));
		$loc_id=trim($this->input->post('bname'));
		$data=array(
		'team'=>$rname,
		'i_rep_person'=>$iperson,
		'f_rep_person'=>$fperson,
		'loc_id'=>$loc_id
		);
		$this->db->trans_start();
		$this->db->where('reporting_no',$id);
		$this->db->update('reporting_name',$data);
		$this->db->trans_complete();
		    if($this->db->trans_status()===FALSE)
			   return false;
		    else
				return true;
	  }
	  else
		  return false;
		
		
	}
	/*public function team_add()
	{
	  
	  if(trim($this->input->post('reporting_name'))!='' && trim($this->input->post('prj_dept'))!='' && trim($this->input->post('bname'))!='')
	  {	
		$rname=trim($this->input->post('reporting_name'));
		$pdept=trim($this->input->post('prj_dept'));
		
		$loc_id=trim($this->input->post('bname'));
		$data=array(
		'reporting_name'=>$rname,
		'dept_id'=>$pdept,
		'loc_id'=>$loc_id
		);
		$this->db->insert('reporting_name',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}*/
	/* public function team_save()
	{
	if(trim($this->input->post('rep_id'))!='' && trim($this->input->post('reporting_name'))!='' && trim($this->input->post('prj_dept'))!='' && trim($this->input->post('bname'))!='')
	  {
	 
	    $id=trim($this->input->post('rep_id'));
		$rname=trim($this->input->post('reporting_name'));
		//$rname=trim($this->input->post('report_name'));
		$pdept=trim($this->input->post('prj_dept'));
		//$loc_id=trim($this->input->post('br_id'));
		$loc_id=trim($this->input->post('bname'));
		$data=array(
		'reporting_name'=>$rname,
		'dept_id'=>$pdept,
		'loc_id'=>$loc_id
		);
		$this->db->trans_start();
		$this->db->where('reporting_no',$id);
		//$this->db->where('reporting_no',32);
		$this->db->update('reporting_name',$data);
		$this->db->trans_complete();
		    if($this->db->trans_status()===FALSE)
			   return false;
		    else
				return true;
	  }
	  else
		  return false;
		
		
	}*/
	
	/*public function team_view()
	{
		$res=$this->db->get_where('reporting_name',array('desig_type'=>$this->input->post('desig_type')));
		 
	}*/
	public function team_view_all()
	{
		$this->db->cache_off();
		//$res=$this->db->get('reporting_name');
		$res=$this->db->select('reporting_no,team,i_rep_person,f_rep_person,Loc_id')->get('reporting_name');
		  $rname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($rname,$temp);
			  }
			  $rn=array("aaData"=>$rname);
			  return $rn;
			  
		  }
		  else
			  return false;
	}
	public function team_view_all_c()
	{
		$this->db->cache_off();
		//$res=$this->db->get_where('reporting_name',array('Loc_id'=>4));
		$res=$this->db->select('reporting_no,team,i_rep_person,f_rep_person,Loc_id')->where('Loc_id',4)->get('reporting_name');
		  $rname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($rname,$temp);
			  }
			  $rn=array("aaData"=>$rname);
			  return $rn;
			  
		  }
		  else
			  return false;
	}
	public function team_view_all_v()
	{
		$this->db->cache_off();
		$res=$this->db->select('reporting_no,team,i_rep_person,f_rep_person,Loc_id')->where('Loc_id',3)->get('reporting_name');
			 
		//$res=$this->db->get_where('reporting_name',array('Loc_id'=>3));
		//$res=$this->db->get('reporting_name');
		  $rname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($rname,$temp);
			  }
			  $rn=array("aaData"=>$rname);
			  return $rn;
			  
		  }
		  else
			  return false;
	}
	
	public function team_view_list()
	{
		$lid=$this->input->get('bname');
		$this->db->cache_off();
		$res=$this->db->select('reporting_no,team,i_rep_person,(select Name from employee where Emp_id =i_rep_person ) as i_rep_person_name,f_rep_person,(select Name from employee where Emp_id =f_rep_person ) as f_rep_person_name, Loc_id',FALSE)->where('Loc_id',$lid)->get('reporting_name');
		 $rname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				  array_push($rname,$temp);
			  }
			  $rn=array("aaData"=>$rname);
			  return $rn;
			  
		  }
		  else{
			  return array("aaData"=>'');
                  }
	}
	public function team_list()
	{
		$lid=$this->input->post('bname');
		$this->db->cache_off();
		$res=$this->db->select('reporting_no,team,i_rep_person,f_rep_person,Loc_id')->where('Loc_id',$lid)->get('reporting_name');
		$rname=array();
                if($res->num_rows > 0){
                    $res_arr=$res->result_array();
                    foreach($res_arr as $key => $temp)
                    {
                            array_push($rname,$temp);
                    }
                    //$rn=array("aaData"=>$rname);
                    return $rname;			  
		  }
		  else
			  return false;
	}
	
	//public function team_check($prid,$prdept,$bid)
	public function team_check($prid,$bid)
	 {
//            $f_person = $this->input->post('f_rep_person');
//            $i_person = $this->input->post('i_rep_person');
            $reporting_no = $this->input->post('rep_id');
            $this->db->cache_off();
            
            //$res=$this->db->get_where('projects_det',array('project_name'=>trim($str))); 
//            $res=$this->db->select('reporting_no')->where('team',trim($prid))
//                                                  ->where('loc_id',trim($bid))
//                                                  ->where('i_rep_person',trim($i_person))
//                                                  ->where('f_rep_person',trim($f_person))                        
//                                                  ->get('reporting_name');
            
            
            $where = "team = '$prid' and loc_id = '$bid' ";
            if($reporting_no != ''){
                $where.= "and reporting_no != $reporting_no";
            }
            $res=$this->db->select('reporting_no')->where($where)->get('reporting_name');
            
            if($res->num_rows>0){
                return TRUE;
            }
            else{
                return FALSE;
            }
	 }
	/* bank */
	public function bank_add()
	{
	  
	  if(trim($this->input->post('ba_name'))!='' && trim($this->input->post('ba_branch'))!='' && trim($this->input->post('ba_add'))!='' && trim($this->input->post('bname'))!='')
	  {	
		$bname=trim($this->input->post('ba_name'));
		$bbranch=trim($this->input->post('ba_branch'));
		/*$loc_id=trim($this->input->post('br_id'));*/
		$badd=trim($this->input->post('ba_add'));
		$loc=trim($this->input->post('bname'));
		$data=array(
		'ba_name'=>$bname,
		'ba_branch'=>$bbranch,
		'ba_add'=>$badd,
		'loc_id'=>$loc
		);
		$this->db->insert('bank_details',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
	
	public function bank_save()
	{
	if(trim($this->input->post('id'))!='' && trim($this->input->post('ba_name'))!='' && trim($this->input->post('ba_branch'))!='' && trim($this->input->post('ba_add'))!='' && trim($this->input->post('bname'))!='')
	  {
	 
	    $id=trim($this->input->post('id'));
		$bname=trim($this->input->post('ba_name'));
		
		$branch=trim($this->input->post('ba_branch'));
		
		$badd=trim($this->input->post('ba_add'));
		$loc=trim($this->input->post('bname'));
		$data=array(
		'ba_name'=>$bname,
		'ba_branch'=>$branch,
		'ba_add'=>$badd,
		'loc_id'=>$loc,
		);
		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('bank_details',$data);
		$this->db->trans_complete();
		    if($this->db->trans_status()===FALSE)
			   return false;
		    else
				return true;
	  }
	  else
		  return false;
		
		
	}
	public function bank_check($baname,$ba_branch,$bname)
	 {
		$this->db->cache_off();
		$res=$this->db->select('id')->where('ba_name',trim($baname))
		                            	->where('ba_branch',trim($ba_branch))
										->where('loc_id',trim($bname))
		                            ->get('bank_details');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
	 }
	 public function bank_view()
	{
		$this->db->cache_off();
		$res=$this->db->get('bank_details');
		  $bname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($bname,$temp);
			  }
			 // $rn=array("aaData"=>$bname);
			  return $bname;
			  
		  }
		  else{
			  return false;
		  }
			  
	}
        public function bank_list(){
                $this->db->cache_off();
                $loc=$this->input->post('bname');
                $where = 1;
                if($loc != ''){
                    $where = "Loc_id = $loc";
                }
		$res=$this->db->query("Select * from bank_details where $where");
                $bname=array();
                if($res->num_rows > 0){
                    $res_arr=$res->result_array();
                    foreach($res_arr as $key => $temp)
                    {
                            array_push($bname,$temp);
                    }
                    return $bname;
			  
                }
                else{
                        return false;
                }
        }
	 public function bank_list_det()
	{		
	 $this->db->cache_off();
		$loc=$this->input->get('bname');
		$res=$this->db->get_where('bank_details',array('Loc_id'=>$loc));
		  
		  $pname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  
			  foreach($res_arr as $key => $temp)
			  {
				  //$pname[$temp['project_id']]=$temp;
				   array_push($pname,$temp);
			  }
			   $pn=array("aaData"=>$pname);
			  return $pn;
			  
		  }
		  else
			  return array("aaData"=>'');;
	}
	 public function bank_view_all()
	{
		$this->db->cache_off();
		$res=$this->db->get('bank_details');
		  $bname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($bname,$temp);
			  }
			  $rn=array("aaData"=>$bname);
			  return $rn;
			  
		  }
		  else{
			  $b=array("error"=>false);
			  //return false;
			  return array("aaData"=>$b);
		  }
	}
	
	/*   referral incentive */
	public function ref_incentive_add()
	{
	  
	  if(trim($this->input->post('bname'))!='' && trim($this->input->post('cadre'))!='' && trim($this->input->post('amt'))!='' && trim($this->input->post('duration'))!='' && trim($this->input->post('period'))!='' )
	  {	
		$bname=trim($this->input->post('bname'));
		$cadre=trim($this->input->post('cadre'));
		$amt=trim($this->input->post('amt'));
		$duration=trim($this->input->post('duration'));
		$period=trim($this->input->post('period'));
		$data=array(
		'bname'=>$bname,
		'cadre'=>$cadre,
		'Amount'=>$amt,
		'duration'=>$duration,
		'period'=>$period
		);
		$this->db->insert('emp_ref_incentive',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
	
	public function ref_incentive_save()
	{
	/*if(trim($this->input->post('id'))!='' && trim($this->input->post('ba_name'))!='' && trim($this->input->post('ba_branch'))!='' && trim($this->input->post('ba_add'))!='')
	  {*/
     if(trim($this->input->post('id'))!='' && trim($this->input->post('bname'))!='' && trim($this->input->post('cadre'))!='' && trim($this->input->post('amt'))!='' && trim($this->input->post('duration'))!='' && trim($this->input->post('period'))!='' )
	  {	
	 
	    $id=trim($this->input->post('id'));
		$bname=trim($this->input->post('bname'));
		$cadre=trim($this->input->post('cadre'));
		$amt=trim($this->input->post('amt'));
		$duration=trim($this->input->post('duration'));
		$period=trim($this->input->post('period'));
		
		$data=array(
		'cadre'=>$cadre,
		'Amount'=>$amt,
		'duration'=>$duration,
		'period'=>$period,
		);
		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('emp_ref_incentive',$data);
		$this->db->trans_complete();
		    if($this->db->trans_status()===FALSE)
			   return false;
		    else
				return true;
	  }
	  else
		  return false;
		
		
	}
	 public function ref_incentive_view()
	{
		$this->db->cache_off();
		$res=$this->db->get('emp_ref_incentive');
		  $iname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($iname,$temp);
			  }
			 // $rn=array("aaData"=>$bname);
			  return $iname;
			  
		  }
		  else
			  return false;
	}
        public function ref_incentive_list(){
            $bname = $this->input->post('bname');
            $this->db->select('*')->from('emp_ref_incentive')->where('bname',$bname);
//            $res=$this->db->get('emp_ref_incentive');
            $res=$this->db->get();
            if($res->num_rows > 0){
                    $res_arr=$res->result_array();
                    return $res_arr;
            }else{
                return false;
            }
            
        }
	public function ref_incentive_all()
	{
		$this->db->cache_off();
		$res=$this->db->get('emp_ref_incentive');
		  $iname=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				// $rname[$temp['reporting_no']]=$temp;
				  array_push($iname,$temp);
			  }
			  $in=array("aaData"=>$iname);
			  return $in;
			  
		  }
		  else
			  return array("aaData"=>'');
	}
	public function ref_incentive_check($bname,$cadre)
	 {
                $id = trim($this->input->post('Id'));
		$this->db->cache_off();
		$res=$this->db->select('*')->where('bname',trim($bname))->where('Id !=',trim($id))
		                            ->where('cadre',trim($cadre))
		                            ->get('emp_ref_incentive');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
	 }
	
	
	/* location */
	public function location_add()
	{
		$res=$this->db->query("SHOW TABLE STATUS LIKE 'locations'");
		$res_arr=$res->result_array();
		$bid= $res_arr[0]['Auto_increment'];
		
	  if(trim($this->input->post('loc_name'))!='')
	  {	
	
		$lname=trim($this->input->post('loc_name'));
		$data=array(
		'location'=>$lname,
		'type'=>'b',
		'bid'=>$bid
		);
		$this->db->insert('locations',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
	public function location_save()
	{
	  if( trim($this->input->post('loc_id'))!='' && trim($this->input->post('loc_name'))!='')
	  /*if(trim($this->input->post('loc_name'))!='')*/
	  {	
	    $id=trim($this->input->post('loc_id'));
		$lname=trim($this->input->post('loc_name'));
		$data=array(
		'location'=>$lname
		);
		$this->db->trans_start();
		$this->db->where('id',$id);
		//$this->db->where('id',2);
		$this->db->update('locations',$data);
		$this->db->trans_complete();
		 if($this->db->trans_status()===FALSE)
			 return false;
		 else
			 return true;
		
	  }
	  else
		  return false;
		
		
	}
	/*public function location_view()
	{
		$res=$this->db->get_where('locations',array('desig_type'=>$this->input->post('desig_type')));
		 
	}*/
	public function location_view_list()
	{
		$this->db->cache_off();
		$res=$this->db->get('locations');
		  $location=array();
		if($res->num_rows > 0)
		{
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$location[$temp['Id']]=$temp;
				 array_push($location,$temp);
			  }
			  $loc=array("aaData"=>$location);
			  return $loc;
		}
		else
		  return array("aaData"=>'');
	}
	public function location_view_all()
	{
		$this->db->cache_off();
		$res=$this->db->get_where('locations',array('type'=>'b'));
		  $location=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$location[$temp['Id']]=$temp;
				 array_push($location,$temp);
			  }
			  //$loc=array("aaData"=>$location);
			  return $location;
		  }
		  else
			  return false;
	}
	/*	public function location_view_all()
	{
		$this->db->cache_off();
		$res=$this->db->get('locations');
		  $location=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				 //$location[$temp['Id']]=$temp;
				 array_push($location,$temp);
			  }
			  //$loc=array("aaData"=>$location);
			  return $location;
		  }
		  else
			  return false;
	}*/
		public function loc_check($str)
	 {
		 $this->db->cache_off();
		$res=$this->db->get_where('locations',array('Location'=>trim($str))); 
		//$res=$this->db->select('Id')->where('dept_name',trim($str))->get('department');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
	 }
	 
	 /* location city details */
	 public function location_city_add()
	{
		
	  if(trim($this->input->post('loc_name'))!='' && $this->input->post('bname'))
	  {	
	
		$lname=trim($this->input->post('loc_name'));
		$bname=trim($this->input->post('bname'));
		$data=array(
		'location'=>$lname,
		'type'=>'c',
		'bid'=>$bname
		);
		$this->db->insert('locations',$data);
		return $this->db->insert_id();
	  }
	  else
		  return false;
		
		
	}
	public function location_city_save()
	//public function location_city_save($bid,$lname,$cid)
	{
	  if( trim($this->input->post('bname'))!='' && trim($this->input->post('loc_city'))!='' && trim($this->input->post('city_id'))!='' )
	 
	  {
	   
	   $bid=trim($this->input->post('bname'));
		$lname=trim($this->input->post('loc_city'));
		$cid=trim($this->input->post('city_id'));
		$data=array(
		'location'=>$lname
		);
		$this->db->trans_start();
		/*$this->db->where('bid',$bid);
		$this->db->where('id',$cid);
		$this->db->where('location',$lname);
		$this->db->where('type','c');*/
		$wh=array('bid'=>$bid,'id'=>$cid,'type'=>'c');
		$this->db->where($wh);
		
		$this->db->update('locations',$data);
		echo $this->db->last_query();
		$this->db->trans_complete();
		 if($this->db->trans_status()===FALSE)
			 return false;
		 else
			 return true;
		
	  }
	  else
		  return false;
		
		
	}
	 
	 public function loc_city_check($bname,$loc_city)
	 {
		 $this->db->cache_off();
		 $wh=array("bid"=>trim($bname),'Location'=>trim($loc_city));
		//$res=$this->db->select('Id')->where('bid',trim($bname))->where('Location',trim($loc_city))->get('locations');
		$res=$this->db->select('Id')->where($wh)->get('locations');
		
		//$res=$this->db->select('Id')->where('dept_name',trim($str))->get('department');
		if($res->num_rows>0){
			return TRUE;
		}
		else
			return FALSE;
	 }
	 //public function location_list($bid)
	// public function location_city_all()
	// public function location_city_all($bname)
	 public function location_city_all()
	 {
		  $this->db->cache_off();
		//$res=$this->db->get_where('locations',array('id !='=>3));
		$bname=$this->input->post('bname');
		$wh=array('bid'=>$bname,'type !='=>'b');
		  $this->db->where($wh);
		$res=$this->db->get('locations');
		//$res=$this->db->select('Id')->where('dept_name',trim($str))->get('department');
		
		
		$loc=array();
		if($res->num_rows > 0){
                    $res_arr=$res->result_array();
                    foreach($res_arr as $key => $temp)
                    {
                           //$location[$temp['Id']]=$temp;
                           array_push($loc,$temp);
                    }

                    return $loc;
		  }
		  else
                    return array(array("Id"=>"","Location"=>"No branch"));
	 }
	 public function emp_id_all(){
		  $this->db->cache_off();
		  $res=$this->db->select('Id,Value')->get('id_table');
		  $emp=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				
				 array_push($emp,$temp);
			  }
			  
			  return $emp;
		  }
		  else
			  return false;
	 }
	  public function emp_id_list(){
		  $this->db->cache_off();
		  $loc=$this->input->post('bname');
//                  $res=$this->db->select('Emp_id as Id,concat(Emp_id,"(",name,")") as emp_name',false)->where('Branch',$loc)->get('employee');
		   $res=$this->db->select('Emp_id as Id,concat(Emp_id,"(",concat_ws(" - ",name,Old_Emp_id2) ,")") as emp_name',false)->where("Branch = '$loc' and Status = 1")->get('employee');
		/*  $res=$this->db->select('Emp_id as Id,concat(Emp_id,"(",concat_ws(" - ",name,Old_Emp_id2) ,")") as emp_name',false)->where("Branch = '$loc'")->get('employee'); */
		  $emp=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				
				 array_push($emp,$temp);
			  }
			  
			  return $emp;
		  }
		  else
			  return false;
	 }
         
          public function emp_id_list_act(){
		  $this->db->cache_off();
		  $loc=$this->input->get('bname');
                  $res=$this->db->select('Emp_id as Id,concat(Emp_id,"(",concat_ws(" - ",name,Old_Emp_id2) ,")") as emp_name',false)->where('Branch',$loc)->get('employee');		
		  $emp=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				
				 array_push($emp,$temp);
			  }
			  
			  return $emp;
		  }
		  else
			  return false;
	 }
	public function emp_team(){
		  $this->db->cache_off();
		  $loc=$this->input->post('bname');
		  $team=$this->input->post('team');
                  $type = $this->input->post('type');
		  $this->db->select('Emp_id as Id, concat(Emp_id,"(",concat_ws(" - ",name,Old_Emp_id2) ,")") as emp_name',false);
                  $this->db->where('Branch',$loc)->where('Project_team',$team)->where('Status',1);
                  
                  if($type == 2 || $type == '2'){
                    $this->db->where('nsa_eligible',1);    
                  }  
                  
                  $res=$this->db->get('employee');
		  $emp=array();
		  if($res->num_rows > 0){
			  $res_arr=$res->result_array();
			  foreach($res_arr as $key => $temp)
			  {
				
				 array_push($emp,$temp);
			  }
			  
			  return $emp;
		  }
		  else
			  return false;
	 }
	 public function location_city_list($bname){
            $where=array('bid'=>$bname,'type !='=>'b');
            $this->db->where($where);
            $res=$this->db->get('locations');
            if($res->num_rows > 0){
                return $res->result_array();
                
            }else{
                return array(array("Id"=>"","Location"=>"No branch"));
            }
         }
	public function team_info(){
		$bname=$this->input->get('bname');
		$team=$this->input->get('report_team');
		$res=$this->db->select('reporting_no as value,team as label')->where('loc_id',$bname)->get('reporting_name');
		$team=array();
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     foreach($res_arr as $key => $val)	{
				 
				
				 $team[$val['value']]=$val['label'];
				 
			 }
			 
			return $team;
		}
		return false;
	}
	public function shift_info(){
		$bname=$this->input->get('bname');
		$team=$this->input->get('report_team');
		$res=$this->db->select('reporting_no as value,team as label')->where('loc_id',$bname)->get('reporting_name');
		$team=array();
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     foreach($res_arr as $key => $val)	{
				 
				
				 $team[$val['value']]=$val['label'];
				 
			 }
			 
			return $team;
		}
		return false;
	}
	public function process_info(){
		$bname=$this->input->get('bname');
		$team=$this->input->get('report_team');
		//$res=$this->db->select('reporting_no as value,team as label')->where('loc_id',$bname)->get('process');
		$res=$this->db->select('Process_id as value,Process_name as label')->get('process');
		$proc=array();
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     foreach($res_arr as $key => $val)	{
				 
				 
				 $proc[$val['value']]=$val['label'];
				 
			 }
			 
			return $proc;
		}
		return false;
	}
        public function employer_add(){
            $input=$this->input->post(NULL, TRUE);
            
            if(trim($this->input->post('bname'))!='')
            {
                $data=array();
                if(array_key_exists('addr1',$input)){
                    $data['Addr1']=trim($input['addr1']);
                }
                if(array_key_exists('addr2',$input)){
                    $data['Addr2']=trim($input['addr2']);
                }
                if(array_key_exists('addr3',$input)){
                    $data['Addr3']=trim($input['addr3']);
                }
                if(array_key_exists('bname',$input)){
                    $data['Branch']=trim($input['bname']);
                }
                if(array_key_exists('comp_name',$input)){
                    $data['Comp_name']=trim($input['comp_name']);
                }
                if(array_key_exists('esi_no',$input)){
                    $data['Esi_no']=trim($input['esi_no']);
                }
                if(array_key_exists('location',$input)){
                    $data['Location']=trim($input['location']);
                }
                if(array_key_exists('pan_no',$input)){
                    $data['Pan_no']=trim($input['pan_no']);
                }
                if(array_key_exists('pf_no',$input)){
                    $data['Pf_no']=trim($input['pf_no']);
                }
                if(array_key_exists('pta_no',$input)){
                    $data['PTA_no']=trim($input['pta_no']);
                }
                if(array_key_exists('tin_no',$input)){
                    $data['Tin_no']=trim($input['tin_no']);
                }
                $data['Uid'] =$this->session->userdata('uid');
                $this->db->insert('branch_master',$data);
                return $this->db->insert_id();
	   }
	   else{
		  return false;
           }
        }
        public function employer_check($bname){
            $this->db->select('*')->from('branch_master')->where('Branch',trim($bname));
            $res = $this->db->get();
            if($res->num_rows>0){
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
        public function employer_all(){
            $this->db->select('*,Id as DT_RowId,(select Location from locations where Id = Branch) as Branch_name')->from('branch_master')->where('Status',1);
            $res = $this->db->get();
//            echo "<pre>"; print_r($res->);die();
            if($res->num_rows>0){
                return $res->result_array();
            }
            else{
                return FALSE;
            }
        }
        public function employer_save(){
            $input=$this->input->post(NULL, TRUE);
//            echo "<pre>";print_r($input);
            if(trim($this->input->post('addr1'))!='')
            {
                $data=array();
                if(array_key_exists('addr1',$input)){
                    $data['Addr1']=trim($input['addr1']);
                }
                if(array_key_exists('addr2',$input)){
                    $data['Addr2']=trim($input['addr2']);
                }
                if(array_key_exists('addr3',$input)){
                    $data['Addr3']=trim($input['addr3']);
                }
//                if(array_key_exists('bname',$input)){
//                    $data['Branch']=trim($input['bname']);
//                }
                if(array_key_exists('comp_name',$input)){
                    $data['Comp_name']=trim($input['comp_name']);
                }
                if(array_key_exists('esi_no',$input)){
                    $data['Esi_no']=trim($input['esi_no']);
                }
                if(array_key_exists('location',$input)){
                    $data['Location']=trim($input['location']);
                }
                if(array_key_exists('pan_no',$input)){
                    $data['Pan_no']=trim($input['pan_no']);
                }
                if(array_key_exists('pf_no',$input)){
                    $data['Pf_no']=trim($input['pf_no']);
                }
                if(array_key_exists('pta_no',$input)){
                    $data['PTA_no']=trim($input['pta_no']);
                }
                if(array_key_exists('tin_no',$input)){
                    $data['Tin_no']=trim($input['tin_no']);
                }
//                $data['Uid'] =$this->session->userdata('uid');
//                echo "<pre>";print_r($data);die();
                $this->db->where('Id',$input['empr_id']);
                $this->db->update('branch_master',$data);
//                if($this->db->affected_rows()){
                    return true;
	   }
	   else{
		  return false;
           }
        }
}