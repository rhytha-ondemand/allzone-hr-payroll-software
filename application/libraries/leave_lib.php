<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 error_reporting(0);
class Leave_lib{
	
	private $ci;
	public function __construct()
	{
    
	$this->ci=&get_instance();
	$this->ci->load->database();
	
	
	$this->table_name="employee";
	
    // Rest of the code
	}
	public function pl_details()
	{
		
		$emp_id=$this->ci->input->post('emp_id');
		$emp=array();
		
//		$res=$this->ci->db->select('id,Emp_id,Name,Pl_credit,pl_available')->where_in('Emp_id',$emp_id)->get('employee');
		$res=$this->ci->db->select('emp.id,emp.Emp_id,emp.Name,emp.Pl_credit,emp.pl_available,(select count(*) from compoff where Emp_id = emp.Emp_id and CoffStatus=1 ) as coff_det',FALSE)->where_in('Emp_id',$emp_id)->get('employee emp');
		if($res->num_rows() > 0)
		{
			$res_arr=$res->result_array();
		
			foreach($res_arr as $key => $temp)
			  {
			
				 array_push($emp,$temp);
			  }
			  //$femp=array("aaData"=>$emp);
			  $femp=array("emp"=>$emp);
			  
			  return $femp;
		}
		
		
	}
	public function leave_all()
	{
		$loc=$this->ci->input->get('bname');
		$month=$this->ci->input->get('month');
		$year=$this->ci->input->get('year');
		
                if($month != '' && $month == 13){
                    $where = "and year(l.`Leave_sdate`)=$year ";
                }else{
                    $where = "and year(l.`Leave_sdate`)=$year and month(l.`Leave_sdate`)=$month";
                }
		
		$sqlquery="SELECT l.Id as DT_RowId, l.Id as Id,l.Emp_id as emp_id,e.name as emp_name FROM `emp_leave` l,employee e WHERE l.`Emp_id` in (select CONCAT_WS(',',`Emp_id`)from employee  where Branch=$loc) and l.emp_id=e.emp_id $where and l.Leave_status=1 group by l.Emp_id";
			$res=$this->ci->db->query($sqlquery);
			$data=array();
			if($res->num_rows()>0){
				$resar=$res->result_array();
				foreach($resar as $key =>$value ){
					
					array_push($data,$value);
				}
				
				//$ftemp=array("aaData"=>$data);
				return array("aaData"=>$data);
                        }else{
                            return array("aaData"=>'');
                        }
		
		
	}
        public function coffcr_detail(){
            $coff = '';
            $credit = '';
            $emp_id=$this->ci->input->get('emp_id');
            $query1 ="select Id, Emp_id, DATE_FORMAT( CoffEntryDate ,'%d-%b-%Y') as CoffEntryDate, DATE_FORMAT( CoffworkedDate ,'%d-%b-%Y') as CoffworkedDate, CoffAvailedDate, Noofdays, CoffStatus, Status from compoff where Emp_id = '$emp_id' and CoffStatus=1";
            $res=$this->ci->db->query($query1);
            if($res->num_rows()>0){
               $coff = $res->result_array();
            }
//            $query2 ="select Id, Emp_id, DATE_FORMAT( CrLeaveDate ,'%d-%b-%Y') as CrLeaveDate, DATE_FORMAT( CrLeaveEntryDate ,'%d-%b-%Y') as CrLeaveEntryDate, CrLeaveworkedDate, Noofdays, CrLeaveStatus, Status from creditleave where Emp_id = '$emp_id' and CrLeaveStatus=1";
//            $res2=$this->ci->db->query($query2);
//            if($res2->num_rows()>0){
//               $credit = $res2->result_array();
//            }
            $data=array('Coff'=>$coff,'Credit'=>$credit);
            return $data;
        }
	public function save_emp_leave()
	{	  
            $emp=$this->ci->input->post('uid');
            $empid=$this->ci->input->post('emp_id');
            $ltype=$this->ci->input->post('ltype');
            $fdate=$this->ci->input->post('fdate');
            $tdate=$this->ci->input->post('tdate');
//            $reasen=$this->ci->input->post('reasen');
            $reasen = addslashes($this->ci->input->post('reasen'));
// echo '<pre>'; print_r($this->ci->input->post());die();
            $data=array();
            $data2=array();
            $cdata = array();
            $cdata2 = array();
            for($i=0;$i<count($emp);$i++)
            {
                if($tdate[$i]!='')
                $n=$this->date_calc_leave($fdate[$i],$tdate[$i]);
                if($tdate[$i]=='')
                {
                      $data=array("Emp_id"=>$empid[$i],'Leave_edate'=>date('Y-m-d'),"uid"=>$emp[$i],"Leave_type"=>$ltype[$i],"Leave_sdate"=>date('Y-m-d', strtotime($fdate[$i])),"Leave_endate"=>'' ,"Leave_reason"=>$reasen[$i],"No_of_days"=>1);
                }
                elseif($n>0)
                {			  
                    $data=array("Emp_id"=>$empid[$i],'Leave_edate'=>date('Y-m-d'),"uid"=>$emp[$i],"Leave_type"=>$ltype[$i],"Leave_sdate"=>date('Y-m-d', strtotime($fdate[$i])),"Leave_endate"=>date('Y-m-d',strtotime($tdate[$i])),"Leave_reason"=>$reasen[$i],"No_of_days"=>$n);
                }
		 array_push($data2, $data);		  
                if($ltype[$i]==7)
                {
                    $sqlu="update `compoff` set CoffStatus=0,CoffAvailedDate='".date('Y-m-d',strtotime($fdate[$i]))."' where `Emp_id`='$empid[$i]' and `CoffStatus`=1 order by `CoffworkedDate` asc limit 1";
                    $res=$this->ci->db->query($sqlu);					  
                }
            }
                $this->ci->db->insert_batch('emp_leave', $data2); 
//                echo '<pre>'; print_r($data2);
//                echo '<pre>'; print_r($cdata2);die();
                return true;
	}
	
	public function leave_card($emp_id){
		
		$res=$this->ci->db->select('Name,DATE_FORMAT( D_of_join ,"%d-%b-%Y") AS D_of_join,Pl_credit,pl_available',false)->get_where('employee',array('Emp_id'=>$emp_id));
		$res_array=$res->result_array();
		return $res_array;
		
	}
	public function leave_c(){
		$emp_id=$this->ci->input->get('emp_id');
		$month=$this->ci->input->get('month');
		$year=$this->ci->input->get('year');
                if($month != '' && $month == 13){
                    $where = "and year(Leave_sdate)=$year ";
                }else{
                    $where = "and year(Leave_sdate)=$year and month(Leave_sdate)=$month";
                }
                
                
                $sql="SELECT id as DT_RowId, id as Id,DATE_FORMAT( Leave_edate ,'%d-%b-%Y') as enter_date,DATE_FORMAT( Leave_sdate ,'%d-%b-%Y') as leave_date,(IF(`Leave_type`=1,'T','F')) as 'pl', (if(`Leave_type`=2,'T','F')) as 'lop',(if(`Leave_type`=3,'T','F')) as 'ptr',(if(`Leave_type`=4,'T','F')) as 'bvr',(if(`Leave_type`=5,'T','F')) as 'ml',Leave_reason as reason,(if(`Leave_type`=7,'T','F')) as coff, null as coff_wdate, null as coff_remain  from emp_leave where `Emp_id`='$emp_id' and Leave_status=1 $where ";
                
//                $sql="SELECT id as DT_RowId, id as Id,DATE_FORMAT( Leave_edate ,'%d-%b-%Y') as enter_date,(if(`Leave_type`=5,concat(DATE_FORMAT( Leave_sdate ,'%d-%b-%Y'),' to ',DATE_FORMAT( Leave_endate ,'%d-%b-%Y')),DATE_FORMAT( Leave_sdate ,'%d-%b-%Y'))) as leave_date,(IF(`Leave_type`=1,'T','F')) as 'pl', (if(`Leave_type`=2,'T','F')) as 'lop',(if(`Leave_type`=3,'T','F')) as 'ptr',(if(`Leave_type`=4,'T','F')) as 'bvr',(if(`Leave_type`=5,'T','F')) as 'ml',Leave_reason as reason,(if(`Leave_type`=7,'T','F')) as coff, null as coff_wdate, null as coff_remain  from emp_leave where `Emp_id`='$emp_id' and Leave_status=1 $where ";
                
//              $sql="SELECT id as DT_RowId, id as Id,DATE_FORMAT( Leave_edate ,'%d-%b-%Y') as enter_date,DATE_FORMAT( Leave_sdate ,'%d-%b-%Y') as leave_date,(IF(`Leave_type`=1,1,0)) as 'pl', (if(`Leave_type`=2,1,0)) as 'lop',(if(`Leave_type`=3,1,0)) as 'ptr',(if(`Leave_type`=4,1,0)) as 'bvr',(if(`Leave_type`=5,1,0)) as 'ml',Leave_reason as reason,(if(`Leave_type`=7,1,0)) as coff, null as coff_wdate, (select count(*) from compoff where `Emp_id`='$emp_id' and CoffStatus = 1 and Status = 1 ) as coff_remain  from emp_leave where `Emp_id`='$emp_id' and year(Leave_sdate)=$year and month(Leave_sdate)=$month and Leave_status=1 ";
//		$sql="SELECT id as Id,DATE_FORMAT( Leave_edate ,'%d-%b-%Y') as enter_date,DATE_FORMAT( Leave_sdate ,'%d-%b-%Y') as leave_date,(IF(`Leave_type`=1,1,0)) as 'pl', (if(`Leave_type`=2,1,0)) as 'lop',(if(`Leave_type`=3,1,0)) as 'ptr',(if(`Leave_type`=4,1,0)) as 'bvr',(if(`Leave_type`=5,1,0)) as 'ml',Leave_reason as reason,null as coff, null as coff_wdate, null as coff_remain  from emp_leave where `Emp_id`='$emp_id' and year(Leave_sdate)=$year and month(Leave_sdate)=$month and Leave_status=1 ";
//		$sql="SELECT id as Id,Leave_edate as enter_date,Leave_sdate as leave_date,sum(IF(`Leave_type`=1,1,0)) as 'pl', sum(if(`Leave_type`=2,1,0)) as 'lop',sum(if(`Leave_type`=3,1,0)) as 'ptr',sum(if(`Leave_type`=4,1,0)) as 'bvr',sum(if(`Leave_type`=5,1,0)) as 'ml',Leave_reason as reason  from emp_leave where `Emp_id`='$emp_id' and year(Leave_sdate)=$year and month(Leave_sdate)=$month and Leave_status= 1 ";
            $res=$this->ci->db->query($sql);
			$data=array();
			if($res->num_rows()>0){
				$resar=$res->result_array();
				foreach($resar as $key =>$value ){					
                                    array_push($data,$value);
				}
				
				//$ftemp=array("aaData"=>$data);
				return array("aaData"=>$data);
                        }else{
                            return array("aaData"=>'');
                        }	
		
	}
        public function leavecard_update(){
            $emp_id=  $this->ci->input->post('emp_id');
            $Id=  $this->ci->input->post('Id'); 
            $sql = "select l.Id, l.Emp_id, DATE_FORMAT( l.Leave_edate ,'%d-%b-%Y') as Leave_edate, l.Leave_type, DATE_FORMAT( l.Leave_sdate ,'%d-%b-%Y') as Leave_sdate, DATE_FORMAT( l.Leave_endate ,'%d-%b-%Y') as Leave_endate, l.No_of_days, l.pl_available, l.Leave_reason, l.Leave_status, l.Uid,emp.Name as emp_name from emp_leave l join employee emp on l.Emp_id = emp.Emp_id where l.emp_id = '$emp_id' and l.Id = $Id";
            $res=$this->ci->db->query($sql);
            $data=array();
            if($res->num_rows()>0){
                $resar=$res->result_array();
                foreach($resar as $key =>$value ){					
                    array_push($data,$value);
                }
                    return $data;
            }
            else{
                return false;
            }
        }
        public function leavecard_u(){
            $emp_id=  $this->ci->input->post('emp_id');
            $Id=  $this->ci->input->post('Id');
//            $reason = $this->ci->input->post('reason');
            $reason = addslashes($this->ci->input->post('reason'));
            $sdate1 = $this->ci->input->post('ldate');
            $edate = $this->ci->input->post('enddate');
            $sdate = date('Y-m-d',  strtotime($sdate1));            
            if($edate !=''){
                $end_date = date('Y-m-d',  strtotime($edate));
                $n=$this->date_calc_leave($sdate,$end_date);
            }else{
                $end_date =NULL;
                $n = 0;
            }
            $n = $n;
            $ltype= $this->ci->input->post('type_leave');
//            $query= "update emp_leave set Leave_reason = '$reason',Leave_type='$ltype',Leave_sdate='$sdate' where Id=$Id and Emp_id = '$emp_id' ";
            $query= "update emp_leave set Leave_reason = '$reason',Leave_sdate='$sdate',Leave_endate='$end_date',No_of_days = $n where Id=$Id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);            
            if($this->ci->db->affected_rows()){              
               return true;
            }
            else{
                return false;
            }
        }
        public function leavecard_delete(){
            $emp_id=  $this->ci->input->post('emp_id');
            $Id=  $this->ci->input->post('Id');
            $query= "update emp_leave set Leave_status = 0 where Id=$Id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){              
               return true;
            }
            else{
                return false;
            }
        }
	public function cofff(){
            
		$bname=$this->ci->input->post('bname');
		$status=$this->ci->input->post('status');
		$team=$this->ci->input->post('team');
		$emp_id=$this->ci->input->post('emp_id');
		$table="";
		$data=array();
		if(count($emp_id)>1){
                    for($i=0;$i<count($emp_id);$i++){
                        $e=$emp_id[$i];
                        if($status==1){
                            $table="compoff";
                            $coff_wdate=$this->ci->input->post('coff_wdate');
                            $data=array(
                            'Emp_id'=>$e,
                            'CoffEntryDate'=>date('Y-m-d'),
                            'CoffworkedDate'=>date('Y-m-d', strtotime($coff_wdate)),
                            'CoffStatus'=>1,
                            'Status'=>1
                            );
                        }
                        if($status==2){
                            $table="creditleave";
                            $cr_adate=$this->ci->input->post('cr_adate');
                            $cr_wdate=$this->ci->input->post('cr_wdate');
                            $data=array(
                            'Emp_id'=>$e,
                            'CrLeaveEntryDate'=>date('Y-m-d'),
                            'CrLeaveDate'=>date('Y-m-d', strtotime($cr_adate)),
                            'CrLeaveworkedDate'=>date('Y-m-d', strtotime($cr_wdate)),
                            'CrLeaveStatus'=>1,
                            'Status'=>1
                            );
                        }                        
                        $this->ci->db->insert($table,$data);
                    }
		}
		else
		{
                    if($status==1){
			$table="compoff";
			$coff_wdate=$this->ci->input->post('coff_wdate');
			$data=array(
			'Emp_id'=>$emp_id[0],
			'CoffEntryDate'=>date('Y-m-d'),
			'CoffworkedDate'=>date('Y-m-d', strtotime($coff_wdate)),
			'CoffStatus'=>1,
			'Status'=>1
			);
			
                    }
                    if($status==2){
                            $table="creditleave";
                            $cr_adate=$this->ci->input->post('cr_adate');
                             $cr_wdate=$this->ci->input->post('cr_wdate');
                            $data=array(
                            'Emp_id'=>$emp_id[0],
                            'CrLeaveEntryDate'=>date('Y-m-d'),
                            'CrLeaveDate'=>date('Y-m-d', strtotime($cr_adate)),
                            'CrLeaveworkedDate'=>date('Y-m-d', strtotime($cr_wdate)),                                
                            'CrLeaveStatus'=>1,
                            'Status'=>1
                            );
                    }	
                    
                    $this->ci->db->insert($table,$data);			
		}
		return $this->ci->db->insert_id();
	}
	function coff_overall(){
		
		$bname=$this->ci->input->get('bname');
                $fdate1 = $this->ci->input->get('fdate');
                $tdate1 = $this->ci->input->get('tdate');
                $fdate = date('Y-m-d',  strtotime($fdate1));
                $tdate = date('Y-m-d',  strtotime($tdate1));
		$sql="select c.Id as DT_RowId, c.Emp_id, DATE_FORMAT( c.CoffEntryDate ,'%d-%b-%Y') as CoffEntryDate, DATE_FORMAT( c.CoffworkedDate ,'%d-%b-%Y') as CoffworkedDate, DATE_FORMAT( c.CoffAvailedDate ,'%d-%b-%Y') as CoffAvailedDate, c.Noofdays, c.CoffStatus, c.Status,e.Name as Emp_name,e.Emp_id as Emp_id from compoff c,employee e where c.`Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employee` WHERE branch=$bname)and e.Emp_id=c.Emp_id and ( c.CoffworkedDate BETWEEN '$fdate' AND '$tdate') and c.Status =1";
		$res=$this->ci->db->query($sql);
		if($res->num_rows >0)
		{
			$res_arr=$res->result_array();
			$emp=array();
			foreach($res_arr as $key => $val)
			{
				array_push($emp,$val);
				
			}
			return array("aaData"=>$emp);
		}
		else{
			return array("aaData"=>'');
                }
	}
        function coff_empdet(){
            $bname=$this->ci->input->get('bname');
            $emp_id = $this->ci->input->get('emp_id');
            $fdate1 = $this->ci->input->get('fdate');
            $tdate1 = $this->ci->input->get('tdate');
            $fdate = date('Y-m-d',  strtotime($fdate1));
            $tdate = date('Y-m-d',  strtotime($tdate1));
		$sql="select c.Id as DT_RowId, c.Emp_id, DATE_FORMAT( c.CoffEntryDate ,'%d-%b-%Y') as CoffEntryDate, DATE_FORMAT( c.CoffworkedDate ,'%d-%b-%Y') as CoffworkedDate, DATE_FORMAT( c.CoffAvailedDate ,'%d-%b-%Y') as CoffAvailedDate, c.Noofdays, c.CoffStatus, c.Status,e.Name as Emp_name,e.Emp_id as Emp_id from compoff c,employee e where c.`Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employee` WHERE branch=$bname)and e.Emp_id=c.Emp_id and c.Status = 1 and ( c.CoffworkedDate BETWEEN '$fdate' AND '$tdate')  and c.Emp_id = '$emp_id'";
		$res=$this->ci->db->query($sql);
		if($res->num_rows >0)
		{
			$res_arr=$res->result_array();
			$emp=array();
			foreach($res_arr as $key => $val)
			{
				array_push($emp,$val);
				
			}
			return array("aaData"=>$emp);
		}
		else{
			return array("aaData"=>'');
                }
        }
        
         function coff_update(){
            
            $coff_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $coff_wdate1 = $this->ci->input->post('coff_wdate');
            $coff_adate1 = $this->ci->input->post('coff_adate');
            $coff_wdate=  date('Y-m-d',  strtotime($coff_wdate1));
            $coff_adate= date('Y-m-d',  strtotime($coff_adate1));
            $query= "update compoff set CoffworkedDate = '$coff_wdate',CoffAvailedDate='$coff_adate',CoffStatus=0 where Id=$coff_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        function coff_delete(){
            
            $coff_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update compoff set Status = 0 where Id=$coff_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
	
        function credit_overall(){
            $bname=$this->ci->input->get('bname');
            $fdate1 = $this->ci->input->get('fdate');
            $tdate1 = $this->ci->input->get('tdate');
            $fdate = date('Y-m-d',  strtotime($fdate1));
            $tdate = date('Y-m-d',  strtotime($tdate1));
		$sql="select c.Id as DT_RowId, c.Emp_id, DATE_FORMAT( c.CrLeaveEntryDate ,'%d-%b-%Y') as CrLeaveEntryDate, DATE_FORMAT( c.CrLeaveDate ,'%d-%b-%Y') as CrLeaveDate, DATE_FORMAT( c.CrLeaveworkedDate ,'%d-%b-%Y') as CrLeaveworkedDate, c.Noofdays, c.CrLeaveStatus, c.Status ,e.Name as Emp_name,e.Emp_id as Emp_id from creditleave c,employee e where c.`Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employee` WHERE branch=$bname)and e.Emp_id=c.Emp_id and ( c.CrLeaveDate BETWEEN '$fdate' AND '$tdate')  and c.Status =1";
		$res=$this->ci->db->query($sql);
		if($res->num_rows >0)
		{
			$res_arr=$res->result_array();
			$emp=array();
			foreach($res_arr as $key => $val)
			{
				array_push($emp,$val);
				
			}
			return array("aaData"=>$emp);
		}
		else{
			return array("aaData"=>'');
                }
        }
        function credit_empdet(){
            $bname=$this->ci->input->get('bname');
            $emp_id = $this->ci->input->get('emp_id');
            $fdate1 = $this->ci->input->get('fdate');
            $tdate1 = $this->ci->input->get('tdate');
            $fdate = date('Y-m-d',  strtotime($fdate1));
            $tdate = date('Y-m-d',  strtotime($tdate1));
            $sql="select c.Id as DT_RowId, c.Emp_id, DATE_FORMAT( c.CrLeaveEntryDate ,'%d-%b-%Y') as CrLeaveEntryDate, DATE_FORMAT( c.CrLeaveDate ,'%d-%b-%Y') as CrLeaveDate, DATE_FORMAT( c.CrLeaveworkedDate ,'%d-%b-%Y') as CrLeaveworkedDate, c.Noofdays, c.CrLeaveStatus, c.Status ,e.Name as Emp_name,e.Emp_id as Emp_id from creditleave c,employee e where c.`Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employee` WHERE branch=$bname)and e.Emp_id=c.Emp_id and c.Status =1 and ( c.CrLeaveDate BETWEEN '$fdate' AND '$tdate') and c.Emp_id = '$emp_id'";
		$res=$this->ci->db->query($sql);
		if($res->num_rows >0)
		{
			$res_arr=$res->result_array();
			$emp=array();
			foreach($res_arr as $key => $val)
			{
				array_push($emp,$val);
				
			}
			return array("aaData"=>$emp);
		}
		else{
			return array("aaData"=>'');
                }
        }
        
        function credit_update(){
            
            $credit_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $credit_adate1 = $this->ci->input->post('credit_adate');
            $credit_adate =  date('Y-m-d',  strtotime($credit_adate1));
            $credit_wdate1 = $this->ci->input->post('credit_wdate');
            $credit_wdate = date('Y-m-d',  strtotime($credit_wdate1));
            $query= "update creditleave set CrLeaveDate = '$credit_adate',CrLeaveworkedDate='$credit_wdate',CrLeaveStatus=0 where Id=$credit_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        function credit_delete(){
            
            $credit_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update creditleave set Status = 0 where Id=$credit_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        
	function monthlydet(){
		$month = $this->ci->input->post('month');    
        $bname=	$this->ci->input->post('bname');    
		$year=	$this->ci->input->post('year');  
		
		$sql="SELECT e.Emp_id as emp_id,e.name as emp_name,l.id as Id,sum(IF(`Leave_type`=1,1,0)) as pl, sum(if(`Leave_type`=2,1,0)) as lop,sum(if(`Leave_type`=3,1,0)) as ptr,sum(if(`Leave_type`=4,1,0)) as bvr,sum(if(`Leave_type`=5,No_of_days,0)) as ml,(sum(IF(`Leave_type`=1,1,0))+sum(if(`Leave_type`=2,1,0))+ sum(if(`Leave_type`=3,1,0))+sum(if(`Leave_type`=4,1,0))+sum(if(`Leave_type`=5,No_of_days,0))) as total,min(l.pl_available) as bal_pl,Leave_reason as reason from emp_leave l,employee e where l.`Emp_id`in(select concat_ws(',',Emp_id) from employee where branch=$bname) and year(l.`Leave_sdate`)=$year and month(l.`Leave_sdate`)=$month and l.Emp_id=e.Emp_id and l.Leave_status=1 GROUP BY e.emp_id";
		$res=$this->ci->db->query($sql);
		if($res->num_rows()>0){
			$res_arr=$res->result_array();
			$emp=array();
			foreach($res_arr as $key => $val)
			{
				array_push($emp,$val);
				
			}
//			print_r($emp);
                        return $emp;
                        
		}
		else
			return false;
		
	}
        
        function status_teamwisedetails(){
		$bname=	$this->ci->input->post('bname');    
		$team=	$this->ci->input->post('team');  
//		$year = $this->ci->input->post('year');
                $fdate1 = $this->ci->input->post('fdate');
                $tdate1 = $this->ci->input->post('tdate');
                $fdate = date('y-m-d',  strtotime($fdate1));
                $tdate = date('Y-m-d', strtotime($tdate1));
                
		//$res=$this->ci->db->query("SELECT emp_id,if(Month(`Leave_sdate`)=1,sum(IF(`Leave_type`=1,1,0)),0) as 'jan lop' , if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` WHERE `Emp_id` in (select concat_ws(',',Emp_id)from employee where branch=$bname and `Project_team`=$team) and month(`Leave_sdate`) in(1,2,3,4,5,6,7,8,9,10,11,12) and year(`Leave_sdate`)=year(curdate()) group by month(`Leave_sdate`),`Emp_id`");
//		$res=$this->ci->db->query("SELECT emp_id,if(Month(`Leave_sdate`)=1,sum(IF(`Leave_type`=1,1,0)),0) as 'jan lop' , if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` WHERE `Emp_id` in (select concat_ws(',',Emp_id)from employee where branch=$bname and `Project_team`=$team) and month(`Leave_sdate`) in(1,2,3,4,5,6,7,8,9,10,11,12) and year(`Leave_sdate`)=$year group by month(`Leave_sdate`),`Emp_id`");
//		$res=$this->ci->db->query("SELECT l.emp_id,emp.Name,t.team, if(Month(`Leave_sdate`)=1,sum(IF(`Leave_type`=1,1,0)),0) as 'jan lop' , if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` l,employee emp,reporting_name t WHERE l.`Emp_id` in (select concat_ws(',',Emp_id)from employee where branch=$bname and `Project_team`=$team) and month(`Leave_sdate`) in(1,2,3,4,5,6,7,8,9,10,11,12) and year(`Leave_sdate`)=$year and l.Emp_id=emp.Emp_id and t.reporting_no = $team group by month(`Leave_sdate`),l.`Emp_id`");
                $res=$this->ci->db->query("SELECT l.emp_id,emp.Name,t.team, if(Month(`Leave_sdate`)=1,sum(IF(`Leave_type`=1,1,0)),0) as 'jan lop' , if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` l,employee emp,reporting_name t WHERE l.`Emp_id` in (select concat_ws(',',Emp_id)from employee where branch=$bname and `Project_team`=$team) and `Leave_sdate`between '$fdate' and '$tdate' and l.Emp_id=emp.Emp_id and t.reporting_no = $team group by month(`Leave_sdate`),l.`Emp_id`");if($res->num_rows()>0){
                    $res_arr=$res->result_array();

                    $fres=array();
                    $jan=array();
                    $feb=array();
                    $mar=array();
                    $apr=array();
                    $may=array();
                    $jun=array();
                    $july=array();
                    $aug=array();
                    $sep=array();
                    $oct=array();
                    $nov=array();
                    $dec=array();

                    $emp_name = array();
                    $team = array();
                    foreach($res_arr as $key => $val){
                            if(!array_key_exists($val['emp_id'],$fres))
                            $fres[$val['emp_id']]=$val['emp_id'];

                            $emp_name[$val['emp_id']] = $val['Name'];
                            $team[$val['emp_id']] = $val['team'];
                            if($val["JAN"]>0)
                                    $jan[$val['emp_id']]=$val["JAN"];
    //                        else
    //                            $jan[$val['emp_id']]=0;
                            if($val["FEB"]>0)
                                    $feb[$val['emp_id']]=$val["FEB"];
    //                        else
    //                            $feb[$val['emp_id']]=0;
                            if($val["MAR"]>0)
                                    $mar[$val['emp_id']]=$val["MAR"];
    //                        else
    //                            $mar[$val['emp_id']]=0;
                            if($val["APR"]>0)
                                    $apr[$val['emp_id']]=$val["APR"];
    //                        else
    //                            $apr[$val['emp_id']]=0;
                            if($val["MAY"]>0)
                                    $may[$val['emp_id']]=$val["MAY"];
    //                        else
    //                            $may[$val['emp_id']]=0;
                            if($val["JUNE"]>0)
                                    $jun[$val['emp_id']]=$val["JUNE"];
    //                        else
    //                            $jun[$val['emp_id']]=0;
                            if($val["JULY"]>0)
                                    $july[$val['emp_id']]=$val["JULY"];
    //                        else
    //                            $july[$val['emp_id']]=0;
                            if($val["AUG"]>0)
                                    $aug[$val['emp_id']]=$val["AUG"];
    //                        else
    //                            $aug[$val['emp_id']]=0;
                            if($val["SEP"]>0)
                                    $sep[$val['emp_id']]=$val["SEP"];
    //                        else
    //                            $sep[$val['emp_id']]=0;
                            if($val["OCT"]>0)
                                    $oct[$val['emp_id']]=$val["OCT"];
    //                        else
    //                            $oct[$val['emp_id']]=0;
                            if($val["NOV"]>0)
                                    $nov[$val['emp_id']]=$val["NOV"];
    //                        else
    //                            $nov[$val['emp_id']]=0;
                            if($val["DEC"]>0)
                                    $dec[$val['emp_id']]=$val["DEC"];
    //                        else
    //                            $dec[$val['emp_id']]=0;
    //			
                 $total=$jan[$val['emp_id']]+$feb[$val['emp_id']]+$mar[$val['emp_id']]+$apr[$val['emp_id']]+$may[$val['emp_id']]+$jun[$val['emp_id']]+$july[$val['emp_id']]+$aug[$val['emp_id']]+$sep[$val['emp_id']]+$oct[$val['emp_id']]+$nov[$val['emp_id']]+$dec[$val['emp_id']];

                                   $fres[$val['emp_id']]=array("JAN"=>$jan[$val['emp_id']],"FEB"=>$feb[$val['emp_id']],"MAR"=>$mar[$val['emp_id']],"APR"=>$apr[$val['emp_id']],"MAY"=>$may[$val['emp_id']],"JUN"=>$jun[$val['emp_id']],"JUL"=>$july[$val['emp_id']],"AUG"=>$aug[$val['emp_id']],"SEP"=>$sep[$val['emp_id']],"OCT"=>$oct[$val['emp_id']],"NOV"=>$nov[$val['emp_id']],"DEC"=>$dec[$val['emp_id']],"TOTAL"=>$total,"Emp_name"=>$emp_name[$val['emp_id']],"Team"=>$team[$val['emp_id']]);


                    }
                    /*
                    Array
    (
        [AMSC3] => Array
            (
                [JAN] => 2
                [FEB] => 12
                [mar] => 5
                [TOTAL] => 19
            )

        [AMSC34] => Array
            (
                [JAN] => 
                [FEB] => 3
                [mar] => 
                [TOTAL] => 3
            )

    )

                    */
//                    echo "<pre>";
//                    print_r($fres);
//                    die();
                    return $fres;
        }else{
            return false;
        }
		
		
	}
        
        
        function appraisal_individualdet(){
            $bname = $this->ci->input->post('bname');
            $emp_id = $this->ci->input->post('emp_id');
            $year = $this->ci->input->post('year');
//            $to_date = $this->ci->input->post('dot');
            $query ="SELECT (case when Month(`Leave_sdate`)=1 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'JAN_PL' ,
(case when Month(`Leave_sdate`)=1 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'JAN_LOP' ,
(case when Month(`Leave_sdate`)=1 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'JAN_PTR' ,
(case when Month(`Leave_sdate`)=1 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'JAN_BVR' ,
(case when Month(`Leave_sdate`)=1 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'JAN_ML' ,

(case when Month(`Leave_sdate`)=2 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'FEB_PL' ,
(case when Month(`Leave_sdate`)=2 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'FEB_LOP' ,
(case when Month(`Leave_sdate`)=2 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'FEB_PTR' ,
(case when Month(`Leave_sdate`)=2 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'FEB_BVR' ,
(case when Month(`Leave_sdate`)=2 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'FEB_ML' ,

(case when Month(`Leave_sdate`)=3 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'MAR_PL' ,
(case when Month(`Leave_sdate`)=3 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'MAR_LOP' ,
(case when Month(`Leave_sdate`)=3 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'MAR_PTR' ,
(case when Month(`Leave_sdate`)=3 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'MAR_BVR' ,
(case when Month(`Leave_sdate`)=3 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'MAR_ML' ,

(case when Month(`Leave_sdate`)=4 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'APR_PL' ,
(case when Month(`Leave_sdate`)=4 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'APR_LOP' ,
(case when Month(`Leave_sdate`)=4 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'APR_PTR' ,
(case when Month(`Leave_sdate`)=4 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'APR_BVR' ,
(case when Month(`Leave_sdate`)=4 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'APR_ML' ,

(case when Month(`Leave_sdate`)=5 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'MAY_PL' ,
(case when Month(`Leave_sdate`)=5 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'MAY_LOP' ,
(case when Month(`Leave_sdate`)=5 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'MAY_PTR' ,
(case when Month(`Leave_sdate`)=5 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'MAY_BVR' ,
(case when Month(`Leave_sdate`)=5 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'MAY_ML' ,

(case when Month(`Leave_sdate`)=6 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'JUN_PL' ,
(case when Month(`Leave_sdate`)=6 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'JUN_LOP' ,
(case when Month(`Leave_sdate`)=6 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'JUN_PTR' ,
(case when Month(`Leave_sdate`)=6 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'JUN_BVR' ,
(case when Month(`Leave_sdate`)=6 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'JUN_ML' ,

(case when Month(`Leave_sdate`)=7 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'JUL_PL' ,
(case when Month(`Leave_sdate`)=7 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'JUL_LOP' ,
(case when Month(`Leave_sdate`)=7 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'JUL_PTR' ,
(case when Month(`Leave_sdate`)=7 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'JUL_BVR' ,
(case when Month(`Leave_sdate`)=7 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'JUL_ML' ,

(case when Month(`Leave_sdate`)=8 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'AUG_PL' ,
(case when Month(`Leave_sdate`)=8 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'AUG_LOP' ,
(case when Month(`Leave_sdate`)=8 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'AUG_PTR' ,
(case when Month(`Leave_sdate`)=8 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'AUG_BVR' ,
(case when Month(`Leave_sdate`)=8 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'AUG_ML' ,

(case when Month(`Leave_sdate`)=9 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'SEP_PL' ,
(case when Month(`Leave_sdate`)=9 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'SEP_LOP' ,
(case when Month(`Leave_sdate`)=9 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'SEP_PTR' ,
(case when Month(`Leave_sdate`)=9 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'SEP_BVR' ,
(case when Month(`Leave_sdate`)=9 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'SEP_ML' ,

(case when Month(`Leave_sdate`)=10 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'OCT_PL' ,
(case when Month(`Leave_sdate`)=10 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'OCT_LOP' ,
(case when Month(`Leave_sdate`)=10 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'OCT_PTR' ,
(case when Month(`Leave_sdate`)=10 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'OCT_BVR' ,
(case when Month(`Leave_sdate`)=10 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'OCT_ML' ,

(case when Month(`Leave_sdate`)=11 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'NOV_PL' ,
(case when Month(`Leave_sdate`)=11 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'NOV_LOP' ,
(case when Month(`Leave_sdate`)=11 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'NOV_PTR' ,
(case when Month(`Leave_sdate`)=11 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'NOV_BVR' ,
(case when Month(`Leave_sdate`)=11 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'NOV_ML' ,

(case when Month(`Leave_sdate`)=12 Then sum(IF(`Leave_type`=1,`No_of_days`,0)) end) as 'DEC_PL' ,
(case when Month(`Leave_sdate`)=12 Then sum(IF(`Leave_type`=2,`No_of_days`,0)) end) as 'DEC_LOP' ,
(case when Month(`Leave_sdate`)=12 Then sum(IF(`Leave_type`=3,`No_of_days`,0)) end) as 'DEC_PTR' ,
(case when Month(`Leave_sdate`)=12 Then sum(IF(`Leave_type`=4,`No_of_days`,0)) end) as 'DEC_BVR' ,
(case when Month(`Leave_sdate`)=12 Then sum(IF(`Leave_type`=5,`No_of_days`,0)) end) as 'DEC_ML' ,

e.Name,e.Emp_id,DATE_FORMAT( e.D_of_join ,'%d-%b-%Y') as D_of_join,r.team,d.Design_name, 
if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` l,employee e,reporting_name r,designation d WHERE l.`Emp_id`='$emp_id' and e.Emp_id='$emp_id' and  r.reporting_no=e.project_team and d.Id=e.Design and month(`Leave_sdate`) in(1,2,3,4,5,6,7,8,9,10,11,12) and year(`Leave_sdate`)=$year and Leave_type < 6 group by month(`Leave_sdate`)";
            
            $db = $this->ci->db->query($query);

            if($db->num_rows()>0){
                $result = $db->result_array();
               
		$jan="";
		$feb="";
		$mar="";
		$apr="";
		$may="";
		$jun="";
		$july="";
		$aug="";
		$sep="";
		$oct="";
		$nov="";
		$dec="";
//                ECHO "<PRE>";
//                print_r($result);
//                die();
                foreach($result as $val){
                    
                   if($val["JAN"]>0)
				$jan=$val["JAN"];
			if($val["FEB"]>0)
				$feb=$val["FEB"];
			if($val["MAR"]>0)
				$mar=$val["MAR"];
			if($val["APR"]>0)
				$apr=$val["APR"];
			if($val["MAY"]>0)
				$may=$val["MAY"];
			if($val["JUNE"]>0)
				$jun=$val["JUNE"];
			if($val["JULY"]>0)
				$july=$val["JULY"];
			if($val["AUG"]>0)
				$aug=$val["AUG"];
			if($val["SEP"]>0)
				$sep=$val["SEP"];
			if($val["OCT"]>0)
				$oct=$val["OCT"];
			if($val["NOV"]>0)
				$nov=$val["NOV"];
			if($val["DEC"]>0)
				$dec=$val["DEC"];
                        if($val["JAN_PL"]>0)
				$jan_pl=$val["JAN_PL"];
			if($val["JAN_LOP"]>0)
				$jan_lop=$val["JAN_LOP"];			
			if($val["JAN_PTR"]>0)
				$jan_ptr=$val["JAN_PTR"];
			if($val["JAN_BVR"]>0)
				$jan_bvr=$val["JAN_BVR"];
			if($val["JAN_ML"]>0)
				$jan_ml=$val["JAN_ML"];
                        if($val["JAN_PL"]>0)
				$jan_pl=$val["JAN_PL"];
			if($val["JAN_LOP"]>0)
				$jan_lop=$val["JAN_LOP"];			
			if($val["JAN_PTR"]>0)
				$jan_ptr=$val["JAN_PTR"];
			if($val["JAN_BVR"]>0)
				$jan_bvr=$val["JAN_BVR"];
			if($val["JAN_ML"]>0)
				$jan_ml=$val["JAN_ML"];
                        if($val["FEB_PL"]>0)
				$feb_pl=$val["FEB_PL"];
			if($val["FEB_LOP"]>0)
				$feb_lop=$val["FEB_LOP"];			
			if($val["FEB_PTR"]>0)
				$feb_ptr=$val["FEB_PTR"];
			if($val["FEB_BVR"]>0)
				$feb_bvr=$val["FEB_BVR"];
			if($val["FEB_ML"]>0)
				$feb_ml=$val["FEB_ML"];
                        if($val["MAR_PL"]>0)
				$mar_pl=$val["MAR_PL"];
			if($val["MAR_LOP"]>0)
				$mar_lop=$val["MAR_LOP"];			
			if($val["MAR_PTR"]>0)
				$mar_ptr=$val["MAR_PTR"];
			if($val["MAR_BVR"]>0)
				$mar_bvr=$val["MAR_BVR"];
			if($val["MAR_ML"]>0)
				$mar_ml=$val["MAR_ML"];
                        if($val["APR_PL"]>0)
				$apr_pl=$val["APR_PL"];
			if($val["APR_LOP"]>0)
				$apr_lop=$val["APR_LOP"];			
			if($val["APR_PTR"]>0)
				$apr_ptr=$val["APR_PTR"];
			if($val["APR_BVR"]>0)
				$apr_bvr=$val["APR_BVR"];
			if($val["APR_ML"]>0)
				$apr_ml=$val["APR_ML"];
                        if($val["MAY_PL"]>0)
				$may_pl=$val["MAY_PL"];
			if($val["MAY_LOP"]>0)
				$may_lop=$val["MAY_LOP"];			
			if($val["MAY_PTR"]>0)
				$may_ptr=$val["MAY_PTR"];
			if($val["MAY_BVR"]>0)
				$may_bvr=$val["MAY_BVR"];
			if($val["MAY_ML"]>0)
				$may_ml=$val["MAY_ML"];
                        if($val["JUN_PL"]>0)
				$jun_pl=$val["JUN_PL"];
			if($val["JUN_LOP"]>0)
				$jun_lop=$val["JUN_LOP"];			
			if($val["JUN_PTR"]>0)
				$jun_ptr=$val["JUN_PTR"];
			if($val["JUN_BVR"]>0)
				$jun_bvr=$val["JUN_BVR"];
			if($val["JUN_ML"]>0)
				$jun_ml=$val["JUN_ML"];
                        if($val["JUL_PL"]>0)
				$jul_pl=$val["JUL_PL"];
			if($val["JUL_LOP"]>0)
				$jul_lop=$val["JUL_LOP"];			
			if($val["JUL_PTR"]>0)
				$jul_ptr=$val["JUL_PTR"];
			if($val["JUL_BVR"]>0)
				$jul_bvr=$val["JUL_BVR"];
			if($val["JUL_ML"]>0)
				$jul_ml=$val["JUL_ML"];
			if($val["AUG_PL"]>0)
				$aug_pl=$val["AUG_PL"];
			if($val["AUG_LOP"]>0)
				$aug_lop=$val["AUG_LOP"];			
			if($val["AUG_PTR"]>0)
				$aug_ptr=$val["AUG_PTR"];
			if($val["AUG_BVR"]>0)
				$aug_bvr=$val["AUG_BVR"];
			if($val["AUG_ML"]>0)
				$aug_ml=$val["AUG_ML"];
			if($val["SEP_PL"]>0)
				$sep_pl=$val["SEP_PL"];
			if($val["SEP_LOP"]>0)
				$sep_lop=$val["SEP_LOP"];			
			if($val["SEP_PTR"]>0)
				$sep_ptr=$val["SEP_PTR"];
			if($val["SEP_BVR"]>0)
				$sep_bvr=$val["SEP_BVR"];
			if($val["SEP_ML"]>0)
				$sep_ml=$val["SEP_ML"];
			if($val["OCT_PL"]>0)
				$oct_pl=$val["OCT_PL"];
			if($val["OCT_LOP"]>0)
				$oct_lop=$val["OCT_LOP"];			
			if($val["OCT_PTR"]>0)
				$oct_ptr=$val["OCT_PTR"];
			if($val["OCT_BVR"]>0)
				$oct_bvr=$val["OCT_BVR"];
			if($val["OCT_ML"]>0)
				$oct_ml=$val["OCT_ML"];
			if($val["NOV_PL"]>0)
				$nov_pl=$val["NOV_PL"];
			if($val["NOV_LOP"]>0)
				$nov_lop=$val["NOV_LOP"];			
			if($val["NOV_PTR"]>0)
				$nov_ptr=$val["NOV_PTR"];
			if($val["NOV_BVR"]>0)
				$nov_bvr=$val["NOV_BVR"];
			if($val["NOV_ML"]>0)
				$nov_ml=$val["NOV_ML"];
			if($val["DEC_PL"]>0)
				$dec_pl=$val["DEC_PL"];
			if($val["DEC_LOP"]>0)
				$dec_lop=$val["DEC_LOP"];			
			if($val["DEC_PTR"]>0)
				$dec_ptr=$val["DEC_PTR"];
			if($val["DEC_BVR"]>0)
				$dec_bvr=$val["DEC_BVR"];
			if($val["DEC_ML"]>0)
				$dec_ml=$val["DEC_ML"];
                        
                        
                $emp_idd = $val['Emp_id'];
                $emp_name = $val['Name'];
                $doj = $val['D_of_join'];
                $team  = $val['team'];
                $desig = $val['Design_name'];
//                $fres=array("JAN"=>$jan[$val['emp_id']],"FEB"=>$feb[$val['emp_id']],"MAR"=>$mar[$val['emp_id']],"APR"=>$apr[$val['emp_id']],"MAY"=>$may[$val['emp_id']],"JUN"=>$jun[$val['emp_id']],"JUL"=>$july[$val['emp_id']],"AUG"=>$aug[$val['emp_id']],"SEP"=>$sep[$val['emp_id']],"OCT"=>$oct[$val['emp_id']],"NOV"=>$nov[$val['emp_id']],"DEC"=>$dec[$val['emp_id']],"TOTAL"=>$total,"Emp_name"=>$emp_name[$val['emp_id']],"Team"=>$team[$val['emp_id']]);
//                return $fres;
                }
                $fres=array("jan_pl"=>$jan_pl,"jan_lop"=>$jan_lop,"jan_ptr"=>$jan_ptr,"jan_bvr"=>$jan_bvr,"jan_ml"=>$jan_ml,"feb_pl"=>$feb_pl,"feb_lop"=>$feb_lop,"feb_ptr"=>$feb_ptr,"feb_bvr"=>$feb_bvr,"feb_ml"=>$feb_ml,"mar_pl"=>$mar_pl,"mar_lop"=>$mar_lop,"mar_ptr"=>$mar_ptr,"mar_bvr"=>$mar_bvr,"mar_ml"=>$mar_ml,"apr_pl"=>$apr_pl,"apr_lop"=>$apr_lop,"apr_ptr"=>$apr_ptr,"apr_bvr"=>$apr_bvr,"apr_ml"=>$apr_ml,"may_pl"=>$may_pl,"may_lop"=>$may_lop,"may_ptr"=>$may_ptr,"may_bvr"=>$may_bvr,"may_ml"=>$may_ml,"jun_pl"=>$jun_pl,"jun_lop"=>$jun_lop,"jun_ptr"=>$jun_ptr,"jun_bvr"=>$jun_bvr,"jun_ml"=>$jun_ml,"jul_pl"=>$jul_pl,"jul_lop"=>$jul_lop,"jul_ptr"=>$jul_ptr,"jul_bvr"=>$jul_bvr,"jul_ml"=>$jul_ml,"aug_pl"=>$aug_pl,"aug_lop"=>$aug_lop,"aug_ptr"=>$aug_ptr,"aug_bvr"=>$aug_bvr,"aug_ml"=>$aug_ml,"sep_pl"=>$sep_pl,"sep_lop"=>$sep_lop,"sep_ptr"=>$sep_ptr,"sep_bvr"=>$sep_bvr,"sep_ml"=>$sep_ml,"oct_pl"=>$oct_pl,"oct_lop"=>$oct_lop,"oct_ptr"=>$oct_ptr,"oct_bvr"=>$oct_bvr,"oct_ml"=>$oct_ml,"nov_pl"=>$nov_pl,"nov_lop"=>$nov_lop,"nov_ptr"=>$nov_ptr,"nov_bvr"=>$nov_bvr,"nov_ml"=>$nov_ml,"dec_pl"=>$dec_pl,"dec_lop"=>$dec_lop,"dec_ptr"=>$dec_ptr,"dec_bvr"=>$dec_bvr,"dec_ml"=>$dec_ml,"jan_total"=>$jan,"feb_total"=>$feb,"mar_total"=>$mar,"apr_total"=>$apr,"may_total"=>$may,"jun_total"=>$jun,"jul_total"=>$july,"aug_total"=>$aug,"sep_total"=>$sep,"oct_total"=>$oct,"nov_total"=>$nov,"dec_total"=>$dec,"Emp_name"=>$emp_name,"Team"=>$team,"Design_name"=>$desig,"Emp_id"=>$emp_idd,"D_of_join"=>$doj);
//                ECHO "<PRE>";
//                print_r($fres);
//                die();
                return $fres;
               
            }else{
                return false;
            }
        }
	/*
	Month wise query
	SELECT if(Month(`Leave_sdate`)=1,sum(`No_of_days`),0) as 'JAN',if(Month(`Leave_sdate`)=2,sum(`No_of_days`),0) as 'FEB', if(Month(`Leave_sdate`)=3,sum(`No_of_days`),0) as 'MAR', if(Month(`Leave_sdate`)=4,sum(`No_of_days`),0) as 'APR', if(Month(`Leave_sdate`)=5,sum(`No_of_days`),0) as 'MAY', if(Month(`Leave_sdate`)=6,sum(`No_of_days`),0) as 'JUNE', if(Month(`Leave_sdate`)=7,sum(`No_of_days`),0) as 'JULY', if(Month(`Leave_sdate`)=8,sum(`No_of_days`),0) as 'AUG', if(Month(`Leave_sdate`)=9,sum(`No_of_days`),0) as 'SEP', if(Month(`Leave_sdate`)=10,sum(`No_of_days`),0) as 'OCT', if(Month(`Leave_sdate`)=11,sum(`No_of_days`),0) as 'NOV', if(Month(`Leave_sdate`)=12,sum(`No_of_days`),0) as 'DEC' FROM `emp_leave` WHERE `Emp_id`='AMSC3' and month(`Leave_sdate`) in(1,2,3,4,5,6,7,8,9,10,11,12) and year(`Leave_sdate`)=year(curdate()) group by month(`Leave_sdate`)
	
	
	
	*/
        
	
       function appraisal_consolidatedet(){
            $bname = $this->ci->input->post('bname');
            $dept = $this->ci->input->post('department');
            $team = $this->ci->input->post('team');
            $project = $this->ci->input->post('project');
            $desig = $this->ci->input->post('designation');
            $month = $this->ci->input->post('month');
            $year = $this->ci->input->post('year');
//            if($project != ''){
//				$query="SELECT l.Id,l.Emp_id as emp_id, l.Leave_edate AS enter_date, l.Leave_sdate AS leave_date, SUM( IF( l.Leave_type =1, l.No_of_days, 0 ) ) AS  'pl', SUM( IF( l.Leave_type =2, l.No_of_days, 0 ) ) AS  'lop', SUM( IF( l.Leave_type =3, l.No_of_days, 0 ) ) AS  'ptr', SUM( IF( l.Leave_type =4, l.No_of_days, 0 ) ) AS  'bvr', SUM( IF( l.Leave_type =5, l.No_of_days, 0 ) ) AS  'ml', Leave_reason AS reason ,(select Design_name from designation where Id = emp.Design limit 1) as design,emp.Name as emp_name,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team , sum(l.No_of_days) as total,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as doj FROM emp_leave l, employee emp WHERE YEAR( l.Leave_sdate ) ='$year' and emp.Project_team ='$team' and emp.Design ='$desig' and emp.Dept = '$dept' and emp.Project = '$project' and l.Emp_id = emp.Emp_id AND l.Leave_status =1 and Leave_type < 6 group by l.Emp_id";
//			}else{
//				$query="SELECT l.Id,l.Emp_id as emp_id, l.Leave_edate AS enter_date, l.Leave_sdate AS leave_date, SUM( IF( l.Leave_type =1, l.No_of_days, 0 ) ) AS  'pl', SUM( IF( l.Leave_type =2, l.No_of_days, 0 ) ) AS  'lop', SUM( IF( l.Leave_type =3, l.No_of_days, 0 ) ) AS  'ptr', SUM( IF( l.Leave_type =4, l.No_of_days, 0 ) ) AS  'bvr', SUM( IF( l.Leave_type =5, l.No_of_days, 0 ) ) AS  'ml', Leave_reason AS reason ,(select Design_name from designation where Id = emp.Design limit 1) as design,emp.Name as emp_name,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team , sum(l.No_of_days) as total,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as doj FROM emp_leave l, employee emp WHERE YEAR( l.Leave_sdate ) ='$year' and emp.Project_team ='$team' and emp.Design ='$desig' and emp.Dept = '$dept' and l.Emp_id = emp.Emp_id AND l.Leave_status =1 and Leave_type < 6 group by l.Emp_id";
//			}
			
            $where = " emp.Branch = '$bname' and YEAR( l.Leave_sdate ) ='$year' and l.Emp_id = emp.Emp_id AND l.Leave_status =1 and Leave_type < 6 ";

if($dept != ''){
	$where .= " and emp.Dept = '$dept' " ;
}	
if($team != ''){
	$where .= " and emp.Project_team ='$team'" ;
}	
if($project != ''){
	$where .= " and emp.Project = '$project'" ;
}		
if($desig != ''){
	$where .= " and emp.Design ='$desig'" ;
}

$query="SELECT l.Id,l.Emp_id as emp_id, l.Leave_edate AS enter_date, l.Leave_sdate AS leave_date, SUM( IF( l.Leave_type =1, l.No_of_days, 0 ) ) AS  'pl', SUM( IF( l.Leave_type =2, l.No_of_days, 0 ) ) AS  'lop', SUM( IF( l.Leave_type =3, l.No_of_days, 0 ) ) AS  'ptr', SUM( IF( l.Leave_type =4, l.No_of_days, 0 ) ) AS  'bvr', SUM( IF( l.Leave_type =5, l.No_of_days, 0 ) ) AS  'ml', Leave_reason AS reason ,(select Design_name from designation where Id = emp.Design limit 1) as design,emp.Name as emp_name,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team , sum(l.No_of_days) as total,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as doj FROM emp_leave l, employee emp WHERE $where group by l.Emp_id";

//            echo $query; die();
            $db = $this->ci->db->query($query);
            $data = array();
            if($db->num_rows()>0){
//                return ($db->result_array());
                $resar=$db->result_array();
                foreach($resar as $key =>$value ){
                        array_push($data,$value);
                }
//                echo "<pre>";
//                echo "dample";
//                print_r($resar);
//                die();
                return $data;
                
            }else{
                return false;
            }
            
       }
               
         function sda_all(){
           $from_date1 = $this->ci->input->get('from_date');
            $to_date1 = $this->ci->input->get('to_date');
            $from_date = date('Y-m-d',  strtotime($from_date1));
            $to_date = date('Y-m-d', strtotime($to_date1));
            $bname = $this->ci->input->get('bname');
            $query = " select sda.Id as DT_RowId, DATE_FORMAT( sda.Sda_date ,'%d-%b-%Y') as Sda_date, sda.Id, sda.Emp_id, sda.Work_type, sda.Entry_date, sda.No_of_days, sda.Status, sda.Uid,emp.Name as Emp_name,(select Design_name from designation where Id = emp.Design limit 1) as design from sda sda join employee emp on sda.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (sda.Sda_date BETWEEN '$from_date' and '$to_date') and sda.Status = 1;";
//            $this->ci->db->select('sda.*,emp.Name as emp_name,emp_Design as design')->from('sda')->join("employee emp","sda.Emp_id = emp.Emp_id");
            $db = $this->ci->db->query($query);
            $data = array();
            if($db->num_rows()>0){
//                return ($db->result_array());
                $resar=$db->result_array();
                foreach($resar as $key =>$value ){
                        array_push($data,$value);
                }
                return $data;
            }else{
                return null;
            }
        }
        function sdansa_add(){
            $bname=$this->ci->input->post('bname');
            $type=$this->ci->input->post('type');
            $team=$this->ci->input->post('team');
            $emp_id=$this->ci->input->post('emp_id');
            $table="";
            $data=array();
            if(count($emp_id)>1){
                for($i=0;$i<count($emp_id);$i++){
                    $e=$emp_id[$i];
                    if($type==1){
                            $table="sda";
                            $sda_date1=$this->ci->input->post('sda_date');
                            $sda_date = date('Y-m-d',  strtotime($sda_date1));
                            $data=array(
                            'Emp_id'=>$e,
                            'Entry_date'=>date('Y-m-d'),
                            'Work_type'=>'SDA',
                            'Sda_date'=>$sda_date,
                            'No_of_days'=>1,
                            'Status'=>1
                            );

                    }
                    if($type==2){
                        $table="nsa";
                        $nsa_fdate1=$this->ci->input->post('nsa_fdate');
                        $nsa_tdate1=$this->ci->input->post('nsa_tdate');
                        $nsa_fdate = date('y-m-d',  strtotime($nsa_fdate1));
                        $nsa_tdate = date('Y-m-d', strtotime($nsa_tdate1));
                        $data=array(
                            'Emp_id'=>$e,                    
                            'Work_type'=>'NSA',
                            'Entry_date'=>date('Y-m-d'),
                            'Nsa_date'=>$nsa_fdate,
                            'Nsa_todate'=>$nsa_tdate,
                            'No_of_days'=>$this->date_calc($nsa_fdate,$nsa_tdate),
                            'Status'=>1
                            );
                    }
                    if($type==3){
                        $table="emp_holidayallow";
                        $ha_date1=$this->ci->input->post('ha_date');
                        $ha_date = date('y-m-d',  strtotime($ha_date1));
                        $data=array(
                            'Emp_id'=>$e,                    
                            'Entry_date'=>date('Y-m-d'),
                            'Hol_date'=>$ha_date,
                            'Status'=>1
                            );
                    }
                    $this->ci->db->insert($table,$data);
                }
            }
            else{
                if($type==1){
                    $table="sda";
                    $sda_date1=$this->ci->input->post('sda_date');
                    $sda_date = date('Y-m-d',  strtotime($sda_date1));
                    $data=array(
                    'Emp_id'=>$emp_id[0],
                    'Entry_date'=>date('Y-m-d'),
                    'Work_type'=>'SDA',
                    'Sda_date'=>$sda_date,
                    'No_of_days'=>1,
                    'Status'=>1
                    );

                }
                if($type==2){
                    $table="nsa";
                    $nsa_fdate1=$this->ci->input->post('nsa_fdate');
                    $nsa_tdate1=$this->ci->input->post('nsa_tdate');
                    $nsa_fdate = date('y-m-d',  strtotime($nsa_fdate1));
                    $nsa_tdate = date('Y-m-d', strtotime($nsa_tdate1));
                    $data=array(
                    'Emp_id'=>$emp_id[0],                    
                    'Work_type'=>'NSA',
                    'Entry_date'=>date('Y-m-d'),
                    'Nsa_date'=>$nsa_fdate,
                    'Nsa_todate'=>$nsa_tdate,
                    'No_of_days'=>$this->date_calc($nsa_fdate,$nsa_tdate),
                    'Status'=>1
                    );
                }
                if($type==3){
                    $table="emp_holidayallow";
                    $ha_date1=$this->ci->input->post('ha_date');
                    $ha_date = date('y-m-d',  strtotime($ha_date1));
                    $data=array(
                        'Emp_id'=>$emp_id[0],                    
                        'Entry_date'=>date('Y-m-d'),
                        'Hol_date'=>$ha_date,
                        'Status'=>1
                        );
                }
                $this->ci->db->insert($table,$data);
            }
            return $this->ci->db->insert_id();
            
        }
        function sda_update(){
            $sda_date1=$this->ci->input->post('sda_date');
            $sda_date = date('Y-m-d',  strtotime($sda_date1));
            $sda_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update sda set Sda_date = '$sda_date' where Id=$sda_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        function sda_delete(){
            
            $sda_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update sda set Status = 0 where Id=$sda_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        
        function nsa_all(){
            $from_date1 = $this->ci->input->get('from_date');
            $to_date1 = $this->ci->input->get('to_date');
            $from_date = date('Y-m-d',  strtotime($from_date1));
            $to_date = date('Y-m-d', strtotime($to_date1));
            $bname = $this->ci->input->get('bname');
            $query = " select nsa.Id as DT_RowId, nsa.Id, nsa.Emp_id, nsa.Work_type, DATE_FORMAT( nsa.Nsa_date ,'%d-%b-%Y') as Nsa_date, DATE_FORMAT(  nsa.Nsa_todate ,'%d-%b-%Y') as Nsa_todate, DATE_FORMAT( nsa.Entry_date ,'%d-%b-%Y') as Entry_date, nsa.No_of_days, nsa.Status, nsa.Uid,emp.Name as Emp_name, sum(nsa.No_of_days) as total_days,(select Design_name from designation where Id = emp.Design limit 1) as design,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team from nsa nsa join employee emp on nsa.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (nsa.Nsa_date BETWEEN '$from_date' and '$to_date') and nsa.Status = 1 group by nsa.Emp_id";
            $db = $this->ci->db->query($query);
            $data = array();
            if($db->num_rows()>0){
//                return ($db->result_array());
                $resar=$db->result_array();
                foreach($resar as $key =>$value ){
                        array_push($data,$value);
                }
                return $data;
            }else{
                return null;
            }
        }
        function nsa_emp_all(){
            $from_date1 = $this->ci->input->get('from_date');
            $to_date1 = $this->ci->input->get('to_date');
            $from_date = date('Y-m-d',  strtotime($from_date1));
            $to_date = date('Y-m-d', strtotime($to_date1));
            $emp_id = $this->ci->input->get('emp_id');
            $query = " select nsa.Id as DT_RowId, nsa.Id, nsa.Emp_id, nsa.Work_type, DATE_FORMAT( nsa.Nsa_date ,'%d-%b-%Y') as Nsa_date, DATE_FORMAT(  nsa.Nsa_todate ,'%d-%b-%Y') as Nsa_todate,  DATE_FORMAT( nsa.Entry_date ,'%d-%b-%Y') as Entry_date, nsa.No_of_days, nsa.Status, nsa.Uid,emp.Name as Emp_name,(select Design_name from designation where Id = emp.Design limit 1) as design,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team from nsa nsa join employee emp on nsa.Emp_id = emp.Emp_id  where nsa.Emp_id = '$emp_id' and (nsa.Nsa_date BETWEEN '$from_date' and '$to_date') and nsa.Status = 1";
            $db = $this->ci->db->query($query);
            $data = array();
            if($db->num_rows()>0){
//                return ($db->result_array());
                $resar=$db->result_array();
                foreach($resar as $key =>$value ){
                        array_push($data,$value);
                }
                return $data;
            }else{
                return null;
            }
        }
        function nsa_delete(){
            $nsa_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update nsa set Status = 0 where Id=$nsa_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        function nsa_update(){
            $nsa_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $nsa_fdate1=$this->ci->input->post('nsa_fdate');
            $nsa_tdate1=$this->ci->input->post('nsa_tdate');
            $nsa_fdate = date('y-m-d',  strtotime($nsa_fdate1));
            $nsa_tdate = date('Y-m-d', strtotime($nsa_tdate1));
            $diff = $this->date_calc($nsa_fdate, $nsa_tdate);
            $query= "update nsa set Nsa_date = '$nsa_fdate',Nsa_todate = '$nsa_tdate',No_of_days = '$diff' where Id=$nsa_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        
        function ha_all(){
            $from_date1 = $this->ci->input->get('from_date');
            $to_date1 = $this->ci->input->get('to_date');
            $from_date = date('Y-m-d',  strtotime($from_date1));
            $to_date = date('Y-m-d', strtotime($to_date1));
            $bname = $this->ci->input->get('bname');
            $query = "select ha.Id as DT_RowId, DATE_FORMAT( ha.Hol_date ,'%d-%b-%Y') as Hol_date, ha.Id, ha.Emp_id, ha.Entry_date,ha.Status, ha.Uid,emp.Name as Emp_name,(select Design_name from designation where Id = emp.Design limit 1) as design from emp_holidayallow ha join employee emp on ha.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (ha.Hol_date BETWEEN '$from_date' and '$to_date') and ha.Status = 1;";
            $db = $this->ci->db->query($query);
            $data = array();
            if($db->num_rows()>0){
//                return ($db->result_array());
                $resar=$db->result_array();
                foreach($resar as $key =>$value ){
                        array_push($data,$value);
                }
                return $data;
            }else{
                return null;
            }
        }        
        
        function ha_update(){
            $ha_date1=$this->ci->input->post('ha_date');
            $ha_date = date('Y-m-d',  strtotime($ha_date1));
            $ha_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update emp_holidayallow set Hol_date = '$ha_date' where Id=$ha_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        function ha_delete(){
            
            $ha_id = $this->ci->input->post('Id');
            $emp_id = $this->ci->input->post('emp_id');
            $query= "update emp_holidayallow set Status = 0 where Id=$ha_id and Emp_id = '$emp_id' ";
            $update_db = $this->ci->db->query($query);
            if($this->ci->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        
        function nsa_calc(){
            // $nsa_fdate = '2016-02-19';
            // $nsa_tdate ='2016-02-29';
            // $diff = $this->date_calc($nsa_fdate, $nsa_tdate);
            $bname = $this->ci->input->post('bname');
			$sql="select e.*,n.* from nsa n,employee e where month(`Nsa_date`)=month(CURRENT_DATE)-1 and month(`Nsa_todate`)=month(CURRENT_DATE)-1 and n.`Emp_id` in (select concat_ws(',',`Emp_id`) from employee where branch='$bname') and e.Emp_id=n.`Emp_id`";
			$res = $this->ci->db->query($sql);
			if($res->num_rows()>0){
				
				$res_array=$res->result_array();
				foreach($res_array as $key =>$value)
				{
					$cat=array("1"=>"A","2"=>"B","3"=>"S","4"=>"Z");
					//$result[$value['Emp_id']]=$value;
					if(isset($result[$value['Emp_id']]['tnsa']))
					{
						$temp=$this->date_calc($value['Nsa_date'], $value['Nsa_todate']);
						$result[$value['Emp_id']]['tnsa']=$result[$value['Emp_id']]['tnsa']+$temp;
						$result[$value['Emp_id']]['Name']=$value['Name'];
					$result[$value['Emp_id']]['Emp.no']=$value['Emp_id'];
					$result[$value['Emp_id']]['Allowance']=$result[$value['Emp_id']]['tnsa']*50;
					if($value['Ctc_range']==2)
					{
					$result[$value['Emp_id']]['Emp_esi']=round($result[$value['Emp_id']]['Allowance']*1.75/100);
                    $result[$value['Emp_id']]['net_pay']=$result[$value['Emp_id']]['Allowance']-$result[$value['Emp_id']]['Emp_esi'];				
					$result[$value['Emp_id']]['Categoty']='B';			
					}
					else
					{
						
						$result[$value['Emp_id']]['Categoty']=$cat[$value['Ctc_range']];
                       $result[$value['Emp_id']]['net_pay']=$result[$value['Emp_id']]['Allowance'];						
					}
					}	
					else{
					$result[$value['Emp_id']]['tnsa']=$this->date_calc($value['Nsa_date'], $value['Nsa_todate']);
					$result[$value['Emp_id']]['Name']=$value['Name'];
					$result[$value['Emp_id']]['Emp.no']=$value['Emp_id'];
					$result[$value['Emp_id']]['Allowance']=$result[$value['Emp_id']]['tnsa']*50;
					if($value['Ctc_range']==2)
					{
					$result[$value['Emp_id']]['Emp_esi']=round($result[$value['Emp_id']]['Allowance']*1.75/100);
                    $result[$value['Emp_id']]['net_pay']=$result[$value['Emp_id']]['Allowance']-$result[$value['Emp_id']]['Emp_esi'];				
					$result[$value['Emp_id']]['Categoty']='B';			
					}
					else
					{
						
						$result[$value['Emp_id']]['Categoty']=$cat[$value['Ctc_range']];	
						 $result[$value['Emp_id']]['net_pay']=$result[$value['Emp_id']]['Allowance'];	
					}
					}
					
					
				}
				return $result;
			}else{
//                            $result = array('no data in db');
                            return false;
                        }
			
//            return $diff;
        }
        
/*	function date_calc($d1,$d2)
	{
		
		$res=strtotime($d2)-strtotime($d1);
		$res=floor($res/(60*60*24));
		
		if($res>0)
			return $res;
		else
			return 1;
		
		
	}*/
        
        function date_calc($d1,$d2)
	{
	$start = new DateTime($d1);
$end = new DateTime($d2);
$end->modify('+1 day');
$interval = $end->diff($start);
$days = $interval->days;
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

foreach($period as $dt) {
    $curr = $dt->format('D');


    if ($curr == 'Sat' || $curr == 'Sun') {
        $days--;
    }
}


return $days; 
	}
	
        function date_calc_leave($d1,$d2)
	{
	$start = new DateTime($d1);
$end = new DateTime($d2);
$end->modify('+1 day');
$interval = $end->diff($start);
$days = $interval->days;
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

foreach($period as $dt) {
    $curr = $dt->format('D');


//    if ($curr == 'Sat' || $curr == 'Sun') {
    if ($curr == 'Sun') {
        $days--;
    }
}


return $days; 
	}
	
    public function createcookie($x)
	{
		 $cookie = array(
    'name'   => 'demo2',
      'value'  => $x,
       'expire' => '86500',
    'domain' => '127.0.0.1',
    'path'   => '/',
    'prefix' => 'myprefix_',
    'secure' => FALSE
);
if(get_cookie("myprefix_demo2")){
delete_cookie("myprefix_demo2");
}
$this->ci->input->set_cookie($cookie);
	}
	public function getcook()
	{
		
		return $this->ci->input->cookie('myprefix_demo2');
		
	}
   
  
}