<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting('1');
class Employee_lib{
		
	private $Id;
	private $Branch;
	private $Emp_id;
	private	$Name;
	private	$Off_name;
	private	$Design;
	private	$Dob;
	private	$D_of_join;
	private	$Ctc;
	private	$Ctc_range;
	private	$Dept;
	private	$Work_type;
	private	$Email;
	private	$Mobile1;
	private	$Mobile2;
	private	$Phone;
	private	$Gender;	
	private	$Location;	
	private	$Project;
	private	$Project_team;
	private	$Process;	
	private $Report_to;
	private $Shift;
	private $Qualification1;
	private $Qualification2;
	private $Exp_year;
	private $Exp_month;
	private $Referral_name;
	private $Avatar;
	private $Pl_credit;
	private $Status;
	private $Uid;
	private $Old_emp_id;
	private $ci;	
	//public function __construct($params)
	public function __construct()
	{
    
	$this->ci=&get_instance();
	$this->ci->load->database();
	$this->ci->load->library('session');
	$this->ci->load->helper('cookie');
	
	$this->table_name="employee";
	
    // Rest of the code
	}
    
    public function get_all_employee()
    {
		$query = $this->ci->db->get($this->table_name);
		$arr = array();

		foreach ($query->result_array() as $row)
		{
			
			if(array_key_exists("id",$row))
				unset($params['id']);
			if(array_key_exists("old_emp_id",$row))
				unset($params['old_emp_id']);
			array_push($arr,$row);
		}    

		return $arr;
    }
    public function get_employee($params)
    {
		$params = array_filter($params, 'strlen');
		$query = $this->ci->db->get_where($this->table_name,$params);
		
	
		$arr=$query->result_array();
		if(array_key_exists("id",$arr))
				unset($params['id']);
		if(array_key_exists("old_emp_id",$row))
				unset($params['old_emp_id']);
		return $arr;
    }
	
	
 
    public function get_prop()
    {
	   $res=array();
      foreach ($this as $key => $val)
      {
           array_push($res,$key);
      }
	  return $res;
    }
    public function valid_params($params)
    {
		$req=$this->get_prop();  
		$req=array_flip($req);
		foreach($params as $key => $val)
		{
			if(!array_key_exists($key,$req))
			{
				unset($params[$key]);
			}
		}
		
		   if(count($params)) {
		    return $params;
	   }
		else
			return false;
   }
   
   public function emp_add()
   {
	    $x=$this->ci->input->post(NULL, TRUE);
	   $fname="";
	   $tid="";
	   if (isset($_FILES['emp_photo']) && is_uploaded_file($_FILES['emp_photo']['tmp_name'])) {
    
	   $this->ci->load->helper('inflector');
       $file_name = underscore($_FILES['emp_photo']['name']);
       $config['file_name'] = $x['emp_id']."_".$file_name;
	   $fname=$x['emp_id']."_".$file_name;
	   $config['upload_path'] = './assets/uploads/';
       $config['allowed_types'] = 'gif|jpg|jpeg|png';
	   $config['remove_spaces']=TRUE;
	   
	   $this->ci->load->library('upload',$config);
           $idata = $this->ci->upload->do_upload('emp_photo');
//	   if($idata)
//		   echo "uploaded";
//	   else
//		   echo "no upload<br>";
	   }
	   $data=array();
	   $data2=array();
	 if(array_key_exists('emp_id',$x))
	 {
		 $data['Emp_id']=trim($x['emp_id']);
		 $data2['Emp_id']=trim($x['emp_id']);
	 }
	  if(array_key_exists('emp_name',$x))
	 {
		 $data['Name']=trim($x['emp_name']);
		 $data2['Name']=trim($x['emp_name']);
	 }
	 if(array_key_exists('sex',$x))
	 {
		 $data['Gender']=trim($x['sex']);
	 }
	 if(array_key_exists('dob',$x))
	 {
		 $data['Dob']=date('Y-m-d',  strtotime(trim($x['dob'])));
		 $data2['Dob']=date('Y-m-d',  strtotime(trim($x['dob'])));
	 }         
	 if(array_key_exists('doj',$x))
	 {
		 $data['D_of_join']=date('Y-m-d',  strtotime(trim($x['doj'])));
	 }
	 if(array_key_exists('emp_pl',$x))
	 {
		 $data['Pl_credit']=trim($x['emp_pl']);
                 $data['pl_available']=trim($x['emp_pl']);
	 }
	 if(array_key_exists('emp_desig',$x))
	 {
		 $data['Design']=trim($x['emp_desig']);
	 }
	 if(array_key_exists('emp_dept',$x))
	 {
		 $data['Dept']=trim($x['emp_dept']);
	 }
	 if(array_key_exists('bname',$x))
	 {
		 $data['Branch']=trim($x['bname']);
	 }
	 if(array_key_exists('emp_proj',$x))
	 {
		 $data['Project']=trim($x['emp_proj']);
	 }
	 if(array_key_exists('emp_reportteam',$x))
	 {
		 $data['Project_team']=trim($x['emp_reportteam']);
	 }
         if(array_key_exists('emp_loc',$x))
	 {
		 $data['Location']=trim($x['emp_loc']);
	 }
	 if(array_key_exists('emp_stime',$x))
	 {
		 $data['Shift']=trim($x['emp_stime']);
	 }
	 if(array_key_exists('emp_process',$x))
	 {
		 $data['Process']=trim($x['emp_process']);
	 }
	 if(array_key_exists('status',$x))
	 {
		 $data['Status']=trim($x['status']);
	 }
	 if(array_key_exists('official_name',$x))
	 {
		 $data['off_name']=trim($x['official_name']);
	 }
	 if(array_key_exists('emp_type',$x))
	 {
		 $data['Work_type']=trim($x['emp_type']);
	 }
	 if(array_key_exists('office_email',$x))
	 {
		 $data['Email']=trim($x['office_email']);
	 }
         if(array_key_exists('emp_nsa',$x))
	 {
		 $data['nsa_eligible']=trim($x['emp_nsa']);
	 }
	 if($fname!='')
	 {
		 $data['Avatar']=trim($fname);
	 }
         
	  $this->ci->db->insert('employee',$data);
	   $tid=$this->ci->db->insert_id();
	 
	 //  $res=$this->ci->select('Emp_id')->get_where('employee',array('Id'=>$tid));
	 $res=$this->ci->db->select('Emp_id')->where('id',trim($tid))
		                            ->get('employee');
	  
	   $res_arr=$res->result_array();
	  
	   if(array_key_exists('emp_id',$x))
	 {
		 
		 $data2['Emp_id']=$res_arr[0]['Emp_id'];
	 }
	   
	 if(array_key_exists('emp_fname',$x))
	 {
		 $data2['Father_name']=trim($x['emp_fname']);
	 }
	 if(array_key_exists('emp_hname',$x))
	 {
		 $data2['Husband_name']=trim($x['emp_hname']);
	 }
	 if(array_key_exists('emp_wname',$x))
	 {
		 $data2['Wife_name']=trim($x['emp_wname']);
	 }
	  
	   $this->ci->db->insert('emp_personal',$data2);
           
	   return $tid;
   }
   
   public function emp_personal()
   {
	    $x=$this->ci->input->post(NULL, TRUE);
	    $data=array();
	    $tid="";
	    	$tid_res=$this->ci->db->select('Emp_id,Branch')->get_where('employee',array('Id'=>$x['Emp_id']));
                $tid_arr = $tid_res->result_array();
                $tid = $tid_arr[0]['Emp_id'];
                $tBranch = $tid_arr[0]['Branch'];
	    /*if(array_key_exists('Emp_id',$x))
	     {
	    	 //$data['Emp_id']=trim($x['Emp_id']);
	    	 $tid=trim($x['Emp_id']);
	     }*/
	     
//            
	    if(array_key_exists('cur_add1',$x))
	     {
	    	 $data['Temp_add1']=trim($x['cur_add1']);
	     }
	     if(array_key_exists('cur_add2',$x))
	     {
	    	 $data['Temp_add2']=trim($x['cur_add2']);
	     }
	    if(array_key_exists('cur_add3',$x))
	     {
	    	 $data['Temp_add3']=trim($x['cur_add3']);
	     }
	     if(array_key_exists('cur_city',$x))
	     {
	    	 $data['Temp_city']=trim($x['cur_city']);
	     }
	     if(array_key_exists('cur_pincode',$x))
	     {
	    	 $data['Temp_pincode']=trim($x['cur_pincode']);
	     }
	    if(array_key_exists('cur_state',$x))
	     {
	    	 $data['Temp_state']=trim($x['cur_state']);
	     }
	     if(array_key_exists('cur_country',$x))
	     {
	    	 $data['Temp_country']=trim($x['cur_country']);
	     }
	     if(array_key_exists('cur_mobile',$x))
	     {
	    	 $data['Temp_mobile']=trim($x['cur_mobile']);
	     }
	     if(array_key_exists('cur_phone',$x))
	     {
	    	 $data['Temp_phone']=trim($x['cur_phone']);
	     }
	     
	     if(array_key_exists('per_add1',$x))
	     {
	    	 $data['Perm_add1']=trim($x['per_add1']);
	     }
	     if(array_key_exists('per_add2',$x))
	     {
	    	 $data['Perm_add2']=trim($x['per_add2']);
	     }
	    if(array_key_exists('per_add3',$x))
	     {
	    	 $data['Perm_add3']=trim($x['per_add3']);
	     }
	     if(array_key_exists('per_city',$x))
	     {
	    	 $data['Perm_city']=trim($x['per_city']);
	     }
	     if(array_key_exists('per_pincode',$x))
	     {
	    	 $data['Perm_pincode']=trim($x['per_pincode']);
	     }
	    if(array_key_exists('per_state',$x))
	     {
	    	 $data['Perm_state']=trim($x['per_state']);
	     }
	     if(array_key_exists('per_country',$x))
	     {
	    	 $data['Perm_country']=trim($x['per_country']);
	     }
	     if(array_key_exists('per_mobile',$x))
	     {
	    	 $data['Perm_mobile']=trim($x['per_mobile']);
	     }
	     if(array_key_exists('per_phone',$x))
	     {
	    	 $data['Perm_phone']=trim($x['per_phone']);
	     }
	      
	     if(array_key_exists('personal_email',$x))
	     {
	    	 $data['Perm_email']=trim($x['personal_email']);
	     }
	     if(array_key_exists('bgroup',$x))
	     {
	    	 $data['Blood_group']=trim($x['bgroup']);
	     }
	    if(array_key_exists('ref1_name',$x))
	     {
	    	 $data['Ref1_name']=trim($x['ref1_name']);
	     }
	     if(array_key_exists('ref1_add',$x))
	     {
	    	 $data['Ref1_addr']=trim($x['ref1_add']);
	     }
	     if(array_key_exists('ref1_mobile',$x))
	     {
	    	 $data['Ref1_phone']=trim($x['ref1_mobile']);
	     }
	     if(array_key_exists('ref2_name',$x))
	     {
	    	 $data['Ref2_name']=trim($x['ref2_name']);
	     }
	     if(array_key_exists('ref2_add',$x))
	     {
	    	 $data['Ref2_addr']=trim($x['ref2_add']);
	     }
	     if(array_key_exists('ref2_mobile',$x))
	     {
	    	 $data['Ref2_phone']=trim($x['ref2_mobile']);
	     }
             if(array_key_exists('emp_qua',$x))
                {
                    $data['Qualification1']=trim($x['emp_qua']);                   
                }
                if(array_key_exists('emp_qua1',$x))
                {
                    if(trim($x['emp_qua1']) != ''){                        
                        $data['Qualification1']=trim($x['emp_qua1']);                    
                    }                                     
                }
                if(array_key_exists('emp_qua2',$x))
                {
                    $data['Qualification2']=trim($x['emp_qua2']);                   
                }
                if(array_key_exists('emp_qua3',$x))
                {
                    $data['Qualification3']=trim($x['emp_qua3']);                   
                }
                if(array_key_exists('emp_qua4',$x))
                {
                    $data['Qualification4']=trim($x['emp_qua4']);                   
                }
//	    echo "<pre>"; print_r($x); print_r($data);die();
	        $this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
            $this->ci->db->update('emp_personal' ,$data);
	    	$this->ci->db->trans_complete();
	    	    if($this->ci->db->trans_status()===FALSE){
                        return false;
                    }
	    	    else{
                            return array('Emp_id'=>$tid,'Branch'=>$tBranch);
                    }
	}
   public function emp_ctc()
   {
	    $x=$this->ci->input->post(NULL, TRUE);
	    $data=array();   //emp_personal
            $data2=array();  //employee
            $data3=array();  //employ_salary
            $data4=array();  //emp_pf_naminee_details
            $tid="";        
            
            
                if(array_key_exists('Emp_id',$x))
                {
                    $tid=trim($x['Emp_id']);
//                    $data2['Emp_id']=trim($x['Emp_id']);
                    $data3['Emp_id']=trim($x['Emp_id']);
                    $data4['Emp_id']=trim($x['Emp_id']);
                }
                if(array_key_exists('emp_pan',$x))
                {	    	 
                    $data['Pancard_no']=trim($x['emp_pan']);
                }
                
	        $this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('emp_personal' ,$data);
	    	$this->ci->db->trans_complete();
                
            if(array_key_exists('ctc',$x))
            {	    	
               $data2['Ctc']=trim($x['ctc']);
               $app_data['ctc']=trim($x['ctc']);
//               $data3['employ_ctc']=trim($x['ctc']);
            }
            if(array_key_exists('ctcr',$x))
            {
                $data2['Ctc_range']=trim($x['ctcr']);
                $app_data['ctc_range']=trim($x['ctcr']);
//                $data3['employ_ctc_range']=trim($x['ctcr']);
                $ctcr = trim($x['ctcr']);

                    if($ctcr==1)
                        $res=$this->sal_cata(trim($x['ctc']));
                    if($ctcr==2)
                           $res=$this->sal_catb(trim($x['ctc']));
                    if($ctcr==3)
                           $res=$this->sal_cats(trim($x['ctc']));
                    if($ctcr==4)
                           $res=$this->sal_catz(trim($x['ctc']),trim($x['emp_additional1']),trim($x['emp_additional2']));

			 $data3['employ_basic_pay']=$res['basic'];
			 $data3['employ_hra']=$res['hra'];
			 $data3['employ_cony_allow']=$res['conv'];
			 $data3['employ_gross']=$res['gross'];
			 $data3['Total_Earning']=$res['tot_ear'];
			 $data3['employ_net']=$res['rev_net_pay'];
                         $data3['employ_other_allow'] = $res['allowance'];
                         $data3['employ_rev_ear_ctc'] = $res['rev_ear_ctc'];
                         $data3['employer_pf'] = $res['epf_1361rn'];
                         $data3['employer_esi'] = $res['esi_475'];
                         $data3['employ_pf'] = $res['epf_12_new'];
                         $data3['employ_esi'] = $res['esi_175'];
            }
            if(array_key_exists('emp_additional1',$x))
            {
                $data2['Zadditional1']=trim($x['emp_additional1']);
            }
            if(array_key_exists('emp_additional2',$x))
            {
                $data2['Zadditional2']=trim($x['emp_additional2']);
            }
            
		$this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('employee' ,$data2);
	    	$this->ci->db->trans_complete();
                
                $this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('emp_appraisal' ,$app_data);
		 
	    if(array_key_exists('pf_no',$x))
            {
                $data3['employ_pf_no']=trim($x['pf_no']);
            }
		 
            if(array_key_exists('pf_nname',$x))
            {
                $data4['Naminee_name']=trim($x['pf_nname']);
            }
            if(array_key_exists('pf_nrel',$x))
            {
                $data4['Naminee_relation']=trim($x['pf_nrel']);
            }
            if(array_key_exists('pf_nadd',$x))
            {
                $data4['Naminee_addr']=trim($x['pf_nadd']);
            }
                $this->ci->db->where("Emp_id",$tid);
		$emp_nominee_result = $this->ci->db->get('emp_pf_naminee_details');
                if($emp_nominee_result->num_rows() > 0){
                    unset($data4['Emp_id']);
                    $this->ci->db->where('Emp_id', $tid);
                    $this->ci->db->update('emp_pf_naminee_details',$data4);
                }
                else{   
                    $this->ci->db->insert('emp_pf_naminee_details',$data4);
                }
                
                
            if(array_key_exists('emp_esino',$x))
            {
                $data3['employ_esi_no']=trim($x['emp_esino']);
            }
            if(array_key_exists('emp_bname',$x))
            {
                $data3['bank_name']=trim($x['emp_bname']);
            }
            if(array_key_exists('emp_bbranch',$x))
            {
                $data3['bbranch']=trim($x['emp_bbranch']);
            }		 
            if(array_key_exists('emp_bacno',$x))
            {
                $data3['employ_bank_ac_no']=trim($x['emp_bacno']);
            }  
            $this->ci->db->where("Emp_id = '$tid' and Status = 0");
            $emp_sal_result = $this->ci->db->get('employ_salary');
            if($emp_sal_result->num_rows() > 0){
                unset($data3['Emp_id']);
                $this->ci->db->where("Emp_id = '$tid' and Status = 0");
                $this->ci->db->update('employ_salary',$data3);
            }
            else{
                $data3['Status'] = 0;
                $this->ci->db->insert('employ_salary',$data3);
            }
//		  return $this->ci->db->insert_id();
//        echo "<pre>";
//            print_r($data);
//        print_r($data2);
//        print_r($data3);
//        print_r($data4);
//        die();
              return $tid;
   }
   
   public function emp_payroll($emp_id)
   {
        $this->ci->db->select('employee.Emp_id, employee.Branch , employee.Name,DATE_FORMAT( employee.D_of_join ,"%d-%b-%Y") as D_of_join, (select Design_name from designation where Id = employee.Design limit 1) as Design, sal.employ_bank_ac_no, sal.employ_uan_no, format(sal.employ_basic_pay,0) as employ_basic_pay, format(sal.employ_hra,0) as employ_hra , sal.employ_cca, format(sal.employ_cony_allow,0) as employ_cony_allow, sal.employ_enter_allow, sal.employ_perform_incen, format(sal.employ_other_allow,0) as employ_other_allow, format(sal.employ_gross,0) as employ_gross,format(sal.employ_pf,0) as employ_pf, sal.employ_pf_no, format(sal.employ_esi,0) as employ_esi, sal.employ_esi_no, sal.employ_pt, sal.employ_it,sal.employ_rev_ear_ctc , sal.employ_gen_insure, sal.employ_other_deduct, format(sal.employ_net,0) as employ_net, format(employee.Ctc,0) as employ_ctc, employee.Ctc_range as employ_ctc_range,format(sal.employer_pf,0) as employer_pf ,format(sal.employer_esi,0) as employer_esi, sal.tot_deduction, sal.pl_credit, sal.leave_balance, sal.ac_no_status, sal.leave_avail, sal.employ_ptr, sal.employ_bvr, sal.employ_lop, sal.employ_tot_perm, sal.employ_coff_worked, sal.employ_coff_taken, sal.employ_coff_avail, sal.Bank_operating_name, sal.bank_name, sal.bbranch, sal.TransEmp_Original_Pl_Cr, sal.MobileReimnt, sal.employ_loyalty, sal.Total_Earning',false)
         ->from('employee')
         ->join('employ_salary sal', 'employee.Emp_id = sal.Emp_id and sal.Status = 0')->where("employee.Emp_id = '$emp_id'");
        $result = $this->ci->db->get();
//        echo '<pre>';print_r($result->result_array());die();
        return $result->result_array();
		
	   
   }
   
/*    public function emp_transfer()
   {
	    $x=$this->ci->input->post(NULL, TRUE);
		$data=array();
		$data2=array();
		
		 if(array_key_exists('bname',$x))
	     {
	    	// $data['bname']=trim($x['bname']);
	     }
	   if(array_key_exists('emp_id',$x))
	     {
	    	 $data['Old_empid']=trim($x['emp_id']);
	     }
        if(array_key_exists('bname_transfer',$x))
	     {
	    	//$data['bname_transfer']=trim($x['bname_transfer']);
	     }
	    if(array_key_exists('transfer_date',$x))
	     {
	    	 $data['trans_date']=trim($x['transfer_date']);
	     }
		 
		 $res1=$this->ci->db->select('Doj')->get_where('employee',array('Emp_id'=>$data['Old_empid']));
		 $res1_arr=$res1->result_array();
		  $data['Original_doj']=$res1_arr[0]['Doj'];
		   $this->ci->db->insert('emp_transfer',$data);
		   
		return $this->ci->db->insert_id();
                
		
	   
   }*/
    public function emp_transfer(){
	   
	   $bname=$this->ci->input->post('bname');
	   $bname_tr=$this->ci->input->post('bname_transfer');
	   $emp=$this->ci->input->post('emp_id');
	   $tr_date=$this->ci->input->post('transfer_date');
	  $uid = $this->ci->session->userdata('uid');
          
	   $data=array('Branch'=>$bname_tr,'Status'=>2);
	   $data2 = array('Dot'=>date('Y-m-d', strtotime($tr_date)));
	      $this->ci->db->trans_start();
		  $this->ci->db->where('Emp_id',$emp);
			$this->ci->db->update('emp_personal',$data2);
                        
	      $this->ci->db->where('Emp_id',$emp);
		  $this->ci->db->where('Branch',$bname);
		  $this->ci->db->update('employee',$data);
                  if($this->ci->db->affected_rows()){
                    $query =  $this->ci->db->query("select * from employee where Emp_id = '$emp'");
                      if($query->num_rows() > 0){
                        $get_employee =  $query->result_array();
                        $employee = $get_employee[0];
                        $emp_new_id = $employee['Old_emp_id'];
                         $employ_doj = $employee['D_of_join'];
                        $employee_array = array("Branch"=>$bname_tr, "Emp_id"=>$employee['Old_emp_id'],  
                            "Name"=>$employee['Name'], "Off_name"=>$employee['Off_name'], "Design"=>$employee['Design'], "Dob"=>$employee['Dob'], "D_of_join"=>$employee['D_of_join'], 
                            "Ctc"=>$employee['Ctc'], "Ctc_range"=>$employee['Ctc_range'], "Zadditional1"=>$employee['Zadditional1'], "Zadditional2"=>$employee['Zadditional2'], 
                            "Modified_ctc"=>$employee['Modified_ctc'], "Dept"=>'', "Work_type"=>$employee['Work_type'], "Email"=>$employee['Email'],
                            "Mobile1"=>$employee['Mobile1'], "Mobile2"=>$employee['Mobile2'], "Phone"=>$employee['Phone'], "Gender"=>$employee['Gender'], "Location"=>$employee['Location'], 
                            "Project"=>$employee['Project'], "Project_team"=>$employee['Project_team'], "teamch_date"=>$employee['teamch_date'], "Process"=>$employee['Process'], 
                            "Report_to"=>$employee['Report_to'], "Shift"=>$employee['Shift'], "nsa_eligible"=>$employee['nsa_eligible'], "Qualification1"=>$employee['Qualification1'], 
                            "Qualification2"=>$employee['Qualification2'], "Texp_year"=>$employee['Texp_year'], "Texp_month"=>$employee['Texp_month'], "Exp_MB_Year"=>$employee['Exp_MB_Year'], 
                            "Exp_MB_Month"=>$employee['Exp_MB_Month'], "Referral_name1"=>$employee['Referral_name1'], "Referral_name2"=>$employee['Referral_name2'], "Avatar"=>$employee['Avatar'], 
                            "Pl_credit"=>$employee['pl_available'], "pl_available"=>$employee['pl_available'], "Prob_date"=>$employee['Prob_date'], "Status"=>1);
                        
                        $this->ci->db->insert('employee',$employee_array);
                      }
                      
                    $employ_transfer = array("Curr_loc"=>$bname_tr, "Curr_emp_id"=>$emp_new_id, "Date_of_transfer"=>date('Y-m-d', strtotime($tr_date)), 
                                            "Prev_loc"=>$bname, "Prev_emp_id"=>$emp, "Date_of_join"=>$employ_doj, "Status"=>1, "Entry_date"=>date('Y-m-d H:i:s'), "Uid"=>$uid);
                      $this->ci->db->insert('employee_transfer',$employ_transfer); 
                      
                      $query_personal =  $this->ci->db->query("select * from emp_personal where Emp_id = '$emp'");
                      if($query_personal->num_rows() > 0){
                        $get_employee_personal =  $query_personal->result_array();
                        $employee_personal = $get_employee_personal[0];
                        $employee_personal_array = array( "Emp_id"=>$emp_new_id, "Name"=>$employee_personal['Name'], "Father_name"=>$employee_personal['Father_name'], "Husband_name"=>$employee_personal['Husband_name'],
                            "Wife_name"=>$employee_personal['Wife_name'], "Dob"=>$employee_personal['Dob'], "Blood_group"=>$employee_personal['Blood_group'], "Temp_mobile"=>$employee_personal['Temp_mobile'], 
                            "Perm_mobile"=>$employee_personal['Perm_mobile'], "Qualification1"=>$employee_personal['Qualification1'], "Qualification2"=>$employee_personal['Qualification2'], "Qualification3"=>$employee_personal['Qualification3'],
                            "Qualification4"=>$employee_personal['Qualification4'], "Temp_add1"=>$employee_personal['Temp_add1'], "Temp_add2"=>$employee_personal['Temp_add2'], "Temp_add3"=>$employee_personal['Temp_add3'],
                            "Temp_city"=>$employee_personal['Temp_city'], "Temp_state"=>$employee_personal['Temp_state'], "Temp_pincode"=>$employee_personal['Temp_pincode'], "Temp_country"=>$employee_personal['Temp_country'], 
                            "Temp_phone"=>$employee_personal['Temp_phone'], "Perm_add1"=>$employee_personal['Perm_add1'], "Perm_add2"=>$employee_personal['Perm_add2'], "Perm_add3"=>$employee_personal['Perm_add3'], 
                            "Perm_city"=>$employee_personal['Perm_city'], "Perm_state"=>$employee_personal['Perm_state'], "Perm_pincode"=>$employee_personal['Perm_pincode'], "Perm_country"=>$employee_personal['Perm_country'],
                            "Perm_phone"=>$employee_personal['Perm_phone'], "Perm_email"=>$employee_personal['Perm_email'], "Ref1_name"=>$employee_personal['Ref1_name'], "Ref1_addr"=>$employee_personal['Ref1_addr'], 
                            "Ref1_phone"=>$employee_personal['Ref1_phone'], "Ref2_name"=>$employee_personal['Ref2_name'], "Ref2_addr"=>$employee_personal['Ref2_addr'], "Ref2_phone"=>$employee_personal['Ref2_phone'], 
                            "Pancard_no"=>$employee_personal['Pancard_no'], "Pancard_name"=>$employee_personal['Pancard_name'], "Pancard_copy"=>$employee_personal['Pancard_copy'], "Pancard_image"=>$employee_personal['Pancard_image'], 
                            "uan_image"=>$employee_personal['uan_image'], "ProfileStatus"=>$employee_personal['ProfileStatus'], "Status"=>$employee_personal['Status']);
                        
                        $this->ci->db->insert('emp_personal',$employee_personal_array);
                        
//                        $this->ci->db->query("UPDATE emp_leave SET Emp_id = '$emp_new_id' WHERE Emp_id = '$emp' and Leave_type =1 order by Id desc limit 1 ");
                        
                        
//                        echo ''; print_r($employee_array); print_r($employee_personal_array); die();
                      }
                      
                  }
                  
		  $this->ci->db->trans_complete();
			if ($this->ci->db->trans_status() === FALSE)
				return false;
			else{                            
				return 4;
			}
			
			/*$res=$this->ci->db->select('Emp_id')->where('Old_emp_id',$emp)->get('employee');
			$res_array=$res->result_array();
			
			$temp_id=$res_array[0]['Emp_id'];*/
			
		  
   }

     
   //public function sal_cata($ctc_param,$nod=1,$sda="",$hda="",$ita=0)
//   public function sal_cata($ctc_param,$nod=0,$ml=0,$sda="",$hda="",$ita=0)
   public function sal_cata($ctc_param,$nod=0,$ml=0,$sda="",$hda="",$ita=0,$ref_inc=0,$half_attn=0)
  // public function sal_cata()
   {

/* required variables start */

/* adjust the ctc based on number of days leave */
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$tempdays;
}
else
	$ctc=$ctc_param;
//$gfsda=1000;
$no_of_days=$nod;
$day_in_month=date('t');
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=$ref_inc;
$half_attn_inc=$half_attn;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=0;
//$pt=7500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */

$saldet['basic']=($ctc*56)/100;
$saldet['hra']=round(($ctc*24)/100);

$saldet['conv']=round($ctc*6/100);
if($saldet['basic']>6500)
	$saldet['epf_1361o']=round(6500*13.61/100);
else
	$saldet['epf_1361o']=round($saldet['basic']*13.61/100);


/*$saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_1361o']);*/
$saldet['allowance']=round(($ctc*14)/100);
$bca=$saldet['basic']+$saldet['conv']+$saldet['allowance'];
if($bca>15000)
	$saldet['epf_1361n']=15000*13.61/100;
else
	$saldet['epf_1361n']=$bca*13.61/100;

$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];

/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['spd_allowance']=round((($saldet['gross']/$day_in_month)*1.5)*$sdadays); 
$saldet['hda_allowance']=round((($saldet['gross']/$day_in_month)*1)*$hdadays); 



/* $saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate; */

$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;
        
/* professional tax calulation */
      if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		 elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095; 
	  }
	  else
		    unset($saldet['pt']);
          
          if(isset($saldet['pt']))
  $pt=$saldet['pt'];
 else
  $pt=0;
  
	
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_12_new']=round((15000*12)/100);
else
	$saldet['epf_12_new']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);
/*if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.61)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361rn']=round((15000*13.61)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);
$saldet['ear_ctc']= $ctc;*/

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.36)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361rn']=round((15000*13.36)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);
$saldet['ear_ctc']= $ctc;

    $saldet['rev_ear_ctc1']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

	   $saldet['cat']="a";
	  if($ml>0){
		    $saldet['ml_details']=$this->sal_cata_ml($ctc_param,$ml);
	   $saldet['rev_ear_ctc2']=$saldet['ml_details']['rev_ear_ctc'];
	   $saldet['rev_ear_ctc']=$saldet['rev_ear_ctc1']+$saldet['ml_details']['rev_ear_ctc'];
		   
	
	   }
	   else
		   $saldet['rev_ear_ctc']= $saldet['rev_ear_ctc1'];
	   
	   return $saldet;
   }
     public function sal_cata_ml($ctc_param,$mlnod=0)
  // public function sal_cata()
   {

/* required variables start */

/* adjust the ctc based on number of days leave */

$no_of_days=$mlnod;
$day_in_month=date('t');

if($mlnod>0){
$tempdim=date('t');
$ctc=($ctc_param/$day_in_month)*$mlnod;
}
else
	$ctc=$ctc_param;

$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=0;
//$pt=7500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */

$saldet['basic']=($ctc*56)/100;
/* $saldet['hra']=round(($ctc*23.5)/100);

$saldet['conv']=round($ctc*5.75/100); */
$saldet['hra']=0;
$saldet['conv']=0;
if($saldet['basic']>6500)
	$saldet['epf_1361o']=round(6500*13.61/100);
else
	$saldet['epf_1361o']=round($saldet['basic']*13.61/100);


/* $saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_1361o']); */
$saldet['allowance']=0;
$bca=$saldet['basic']+$saldet['conv']+$saldet['allowance'];
if($bca>15000)
	$saldet['epf_1361n']=15000*13.36/100;
else
	$saldet['epf_1361n']=$bca*13.36/100;

$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];

/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['spd_allowance']=round((($saldet['gross']/$day_in_month)*1.5)*$sdadays); 
$saldet['hda_allowance']=round((($saldet['gross']/$day_in_month)*1)*$hdadays); 



/* $saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate; */

$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;
        
/* professional tax calulation */
      if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		 elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095; 
	  }
	  else
		    unset($saldet['pt']);
	  
		
	
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_12_new']=round((15000*12)/100);
else
	$saldet['epf_12_new']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);
/* if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.61)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361rn']=round((15000*13.61)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100); */

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.36)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361rn']=round((15000*13.36)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);


     $saldet['rev_ear_ctc']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	  /* echo "<pre>";
	   print_r($saldet);  */
	   $saldet['cat']="a";
	   
	   
	   return $saldet;
   }
   
    
  public function leave_sal_cata($ctc_param,$nold=1,$tpl=1,$sal_year,$sda=0,$hda=0,$pt=0)
   {
	   
$dim=31;
 $sal_year=date('Y');
 $tpl=12;
 $nod=1;
 
 if($tpl==15)
 {
	 $temp=3;
 }
 else
 {
	 $temp=0;
 $sal_month=$tpl;
 }
 for($i=$nold;$i>0;$i--){
	 if($tpl==15 && $temp>0){
	 $sal_month=12;
	
	 }
                  switch($sal_month)
				  {
				     Case 1:
					 case 3:
					 case 5:
					 case 7:
					 case 8:
					 case 10:
					 case 12:
						$dim= 31; 
                        break;						
				     Case 4:
					 case 6:
					 case 9:
					 case 11:
						$dim=30;     
						break;
				     Case 2:
						if($sal_year % 4 != 0 && $sal_year % 100 != 0 && $sal_year % 400 != 0 )
							$dim= 28;
						else
							$dim= 29;
						break;
						   
				  }
				  /* calculation part start*/
	if($tpl==15 && $temp>0){
	$sal_month=$sal_month+$temp;
	 $temp--;
	}
$ctc=(($ctc_param/$dim)*$nod);
//$gfsda=1000;
$no_of_days=$nod;
//$day_in_month=date('t');
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=$pt;
$pt=$pt;
//$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
$lev_saldet[$sal_month]['ctc']=$ctc_param;
$lev_saldet[$sal_month]['basic']=round(($ctc*56)/100);
$lev_saldet[$sal_month]['hra']=round(($ctc*24)/100);

$lev_saldet[$sal_month]['conv']=round($ctc*6/100);



/* $lev_saldet[$sal_month]['allowance']=round($ctc-$lev_saldet[$sal_month]['basic']-$lev_saldet[$sal_month]['hra']-$lev_saldet[$sal_month]['conv']-$pt); */

$lev_saldet[$sal_month]['allowance']=round($ctc*14/100);



$lev_saldet[$sal_month]['gross']=round($lev_saldet[$sal_month]['basic']+$lev_saldet[$sal_month]['hra']+$lev_saldet[$sal_month]['conv']+$lev_saldet[$sal_month]['allowance']);
$lev_saldet[$sal_month]['rev_gross']=round($lev_saldet[$sal_month]['gross']+$sda);
$lev_saldet[$sal_month]['epf_12']=0;
$lev_saldet[$sal_month]['net_pay']=$lev_saldet[$sal_month]['gross']-$lev_saldet[$sal_month]['epf_12']-$pt;

			$lev_saldet[$sal_month]['d_in_mo']=$dim;
				  
				 /* calculation part end */
				  $sal_month--;
    } 
   

	   $lev_saldet['cat']="a";
	 
	   return $lev_saldet;
   }
   
   
   
   /* category                    b  */
   
   //public function sal_catb($ctc_param,$nod=1,$sda="",$hda="",$ita=0)
 /*  public function sal_catb($ctc_param,$nod=0,$sda="",$hda="",$ita=0)
  // public function sal_catb()
   {
      
	
// required variables start 


//$atc_ctc_old=1000;
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$atc_ctc_old=($ctc_param/$tempdim)*$tempdays;
}
else
	$atc_ctc_old=$ctc_param;

$day_in_month=date('t');

if($nod==0)
$no_of_days=date('t');
elseif($nod>0)
 $no_of_days=$nod;


$gross=0;
//$gfsda=1000;
$gfsda=0;
//$sdadays=2;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
//$qua_attn_inc=1000;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
//$pt=0;
$pt=0;
$it=$ita;
$oth_ded=0;
$esi_475=0;
//$rev_emp_esi=32;
$rev_emp_esi=0;
$saldet=array();
// required variables end
$ctc=round(($atc_ctc_old/$day_in_month)*$no_of_days);

$gross=$ctc*88.56/100;
 $esi_475=$gross*4.75/100;  
  $esi_175=$gross*1.75/100;

//echo $gross;
$saldet['basic']=round(($gross*60)/100);
$saldet['hra']=round(($gross*25)/100);
$saldet['conv']=round($gross*6/100);
$saldet['allowance']=round($gross*9/100);
$gfsda=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];
 $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; 
 $saldet['hda_allowance']=(($gfsda/30)*1)*$hdadays; 

$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;


// professional tax calulation 
     if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
	 }
	 else
		 unset($saldet['pt']);

$saldet['gross']=$ctc*88.56/100;
$rev_esi=($gross+$saldet['spd_allowance'])*1.75/100;
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);


if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_12_new']=round((15000*12)/100);
else
	$saldet['epf_12_new']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);
$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.61)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361rn']=round((15000*13.61)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);
$saldet['ear_ctc']= $ctc;
$saldet['rev_ear_ctc']=round(($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate));
	
	  $saldet['esi_475']=$esi_475;
    $saldet['esi_175']=$esi_175;
	   $saldet['cat']="b";
	   return $saldet;
   }
   
   */
   
   
//   public function sal_catb($ctc_param,$nod=0,$sda="",$hda="",$ita=0)
    public function sal_catb($ctc_param,$nod=0,$sda="",$hda="",$ita=0,$ref_inc=0,$half_attn=0)
  // public function sal_catb()
   {
	   
	
/* required variables start */


//$atc_ctc_old=1000;
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$atc_ctc_old=($ctc_param/$tempdim)*$tempdays;
}
else
	$atc_ctc_old=$ctc_param;

$day_in_month=date('t');
if($nod==0)
$no_of_days=date('t');
elseif($nod>0)
	$no_of_days=date('t')-$nod;

$gross=0;
//$gfsda=1000;
$gfsda=0;
//$sdadays=2;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=$ref_inc;
//$qua_attn_inc=1000;
$half_attn_inc=$half_attn;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
//$pt=0;
$pt=0;
$it=$ita;
$oth_ded=0;
$esi_475=0;
//$rev_emp_esi=32;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */

//$ctc=round(($atc_ctc_old/$day_in_month)*$no_of_days);
//$ctc=round($atc_ctc_old);
$ctc=$atc_ctc_old;
 $saldet['ear_ctc']= $ctc;
 /* modified */
/*$gross=round($ctc*88.56/100);*/
$gross=$ctc;

//echo $gross;
if($gross <= 15000){
 $esi_475=$gross*4.75/100;
  $esi_175=$gross*1.75/100;
}else{
 $esi_475=0;
  $esi_175=0;   
}
//echo $gross;
$saldet['basic']=round(($gross*56)/100);
$saldet['basicw']=($gross*56)/100;
$saldet['hra']=round(($gross*24)/100);
$saldet['hraw']=($gross*24)/100;
$saldet['conv']=round($gross*6/100);
$saldet['convw']=$gross*6/100;
/* modified */
/* $saldet['allowance']=round($gross*9/100); 
$saldet['allowancew']=$gross*9/100;*/

$saldet['allowance']=round($gross*14/100); 
$saldet['allowancew']=$gross*14/100;

$gfsda=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];
 $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; 
 $saldet['hda_allowance']=(($gfsda/30)*1)*$hdadays; 

$saldet['gross_pt']=round($saldet['basicw']+$saldet['hraw']+$saldet['convw']+$saldet['allowancew']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate);


/* professional tax calulation */
     if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
	 }
	 else
		 unset($saldet['pt']);
         
                   if(isset($saldet['pt']))
  $pt=$saldet['pt'];
 else
  $pt=0;

//$saldet['gross']=round($ctc*88.56/100);
 $saldet['gross']=($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']);
 
//$rev_esi=  ($gross+$saldet['spd_allowance'])*1.75/100; 
 $rev_esi= $esi_175;
$saldet['tot_ear']=round($saldet['basicw']+$saldet['hraw']+$saldet['convw']+$saldet['allowancew']+$saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

$saldet['tot_earw']=$saldet['basicw']+$saldet['hraw']+$saldet['convw']+$saldet['allowancew']+$saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate;

if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_12_new']=round((15000*12)/100);
else
	$saldet['epf_12_new']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);
$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);
/* modified */
/*if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.61)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);

if(($saldet['basicw']+$saldet['convw']+$saldet['allowancew'])>15000)
	$saldet['epf_1361rn']=round((15000*13.61)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basicw']+$saldet['convw']+$saldet['allowancew'])*13.61/100);*/


if(($saldet['basic']+$saldet['conv']+$saldet['allowance'])>15000)
	$saldet['epf_1361n']=round((15000*13.36)/100);
else
	$saldet['epf_1361n']=round(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);

if(($saldet['basicw']+$saldet['convw']+$saldet['allowancew'])>15000)
	$saldet['epf_1361rn']=round((15000*13.36)/100);
else
	$saldet['epf_1361rn']=round(($saldet['basicw']+$saldet['convw']+$saldet['allowancew'])*13.36/100);

$saldet['rev_ear_ctc']=round(($saldet['tot_earw']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate));
	
	  $saldet['esi_475']=$esi_475;
	   $saldet['esi_175']=$esi_175;
	   $saldet['cat']="b";
	   return $saldet;
   }
   /* Leave salary category b    */
   
    public function leave_sal_catb($ctc_param,$nold=11,$tpl=1,$sal_year,$sda=0,$hda=0,$pt=0)
   {
	  
$dim=31;
 $sal_year=date('Y');
 $tpl=12;
 $nod=1;

 if($tpl==15)
 {
	 $temp=3;
 }
 else
 {
	 $temp=0;
 $sal_month=$tpl;
 }
 for($i=$nold;$i>0;$i--){
	 if($tpl==15 && $temp>0){
	 $sal_month=12;
	
	 }
                  switch($sal_month)
				  {
				     Case 1:
					 case 3:
					 case 5:
					 case 7:
					 case 8:
					 case 10:
					 case 12:
						$dim= 31; 
                        break;						
				     Case 4:
					 case 6:
					 case 9:
					 case 11:
						$dim=30;     
						break;
				     Case 2:
						if($sal_year % 4 != 0 && $sal_year % 100 != 0 && $sal_year % 400 != 0 )
							$dim= 28;
						else
							$dim= 29;
						break;
						   
				  }
				  /* calculation part start*/
	if($tpl==15 && $temp>0){
	$sal_month=$sal_month+$temp;
	 $temp--;
	}
$ctc=(($ctc_param/$dim)*$nod);
$lev_saldet[$sal_month]['ctc']=$ctc;
//$gfsda=1000;
$no_of_days=$nod;
//$day_in_month=date('t');
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=$pt;
$pt=$pt;
//$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
$lev_saldet[$sal_month]['ctc']=$ctc_param;
//$lev_saldet[$sal_month]['gross']=round(($ctc*88.56)/100);
$lev_saldet[$sal_month]['gross']=($ctc);

$lev_saldet[$sal_month]['basic']=round(($lev_saldet[$sal_month]['gross']*56)/100);
$lev_saldet[$sal_month]['hra']=round(($lev_saldet[$sal_month]['gross']*24)/100);

$lev_saldet[$sal_month]['conv']=round(($lev_saldet[$sal_month]['gross']*6)/100);



$lev_saldet[$sal_month]['allowance']=round(($lev_saldet[$sal_month]['gross']*14)/100);
$lev_saldet[$sal_month]['rgross']=$lev_saldet[$sal_month]['gross']+$sdadays;

//$lev_saldet[$sal_month]['gross']=round($lev_saldet[$sal_month]['basic']+$lev_saldet[$sal_month]['hra']+$lev_saldet[$sal_month]['conv']+$lev_saldet[$sal_month]['allowance']);

$lev_saldet[$sal_month]['epf_12']=0;
$lev_saldet[$sal_month]['rev_gross']=$lev_saldet[$sal_month]['gross']+$sda;
$lev_saldet[$sal_month]['net_pay']=$lev_saldet[$sal_month]['gross']-$lev_saldet[$sal_month]['epf_12']-$pt;

				  
		$lev_saldet[$sal_month]['d_in_mo']=$dim;		  
				 /* calculation part end */
				  $sal_month--;
    } 
   

	   $lev_saldet['cat']="b";
	   
	   return $lev_saldet;
   }
   
   
   
   // public function sal_cats($ctc_param,$nod=1,$sda="",$hda="",$ita=0)
  /*  public function sal_cats($ctc_param,$nod=0,$sda="",$hda="",$ita=0)
	//public function sal_cats()
   {
	
// required variables start 



$day_in_month=date('t');
//$no_of_days=20;
$no_of_days=$nod;


$gross=0;
//$gfsda=1000;
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
$pt=0;
//$pt=1000;
//$it=2500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
// required variables end 
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$tempdays;
}
else
	$ctc=$ctc_param;


//$ctc=$ctc_param;


 

$saldet['basic']=($ctc*50)/100;
$saldet['hra']=round(($ctc*25)/100);
$saldet['conv']=round($ctc*5.75/100);
$saldet['epf_13610']=0;
$saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_13610']);
// $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; 
$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];

$saldet['spd_allowance']=(($saldet['gross']/$day_in_month)*1.5)*$sdadays;
$saldet['hda_allowance']=(($saldet['gross']/$day_in_month)*1)*$hdadays;



$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;

// professional tax calulation 
    if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
    }
	else
		unset($saldet['pt']);

// $saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']; 
$rev_esi=($gross+$saldet['spd_allowance'])*1.75/100;
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

$saldet['epf_12_new']="";

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

$saldet['epf_1361n']="";
$saldet['epf_1361rn']="";
$saldet['ear_ctc']= $ctc;
$saldet['rev_ear_ctc']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	

	   $saldet['cat']="s";
	   return $saldet;
   }
   */
//    public function sal_cats($ctc_param,$nod=0,$ml=0,$sda="",$hda="",$ita=0)
    public function sal_cats($ctc_param,$nod=0,$ml=0,$sda="",$hda="",$ita=0,$ref_inc=0,$half_attn=0)
	//public function sal_cats()
   {
	
/* required variables start */



$day_in_month=date('t');
//$no_of_days=20;
$no_of_days=$nod;


$gross=0;
//$gfsda=1000;
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=$ref_inc;
$half_attn_inc=$half_attn;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
$pt=0;
//$pt=1000;
//$it=2500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$tempdays;
}
else
	$ctc=$ctc_param;


//$ctc=$ctc_param;


  $saldet['ear_ctc']= $ctc;
/* modified */
$saldet['basic']=round(($ctc*56)/100);
$saldet['hra']=round(($ctc*24)/100);
$saldet['conv']=round($ctc*6/100);
$saldet['epf_13610']=0;

/* modified */
/*$saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_13610']);*/
$saldet['allowance']=round($ctc*14/100);



/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];

$saldet['spd_allowance']=(($saldet['gross']/$day_in_month)*1.5)*$sdadays;
$saldet['hda_allowance']=(($saldet['gross']/$day_in_month)*1)*$hdadays;



$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;

/* professional tax calulation */
    if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
    }
	else
		unset($saldet['pt']);
        
                  if(isset($saldet['pt']))
  $pt=$saldet['pt'];
 else
  $pt=0;

/* $saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']; */
$rev_esi=($gross+$saldet['spd_allowance'])*1.75/100;
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

$saldet['epf_12_new']="";

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

$saldet['epf_1361n']="";
$saldet['epf_1361rn']="";

$saldet['rev_ear_ctc1']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	

	   $saldet['cat']="s";
           
           
//            if($ml>0){
//		    $saldet['ml_details']=$this->sal_cata_ml($ctc_param,$ml);
//	   $saldet['rev_ear_ctc2']=$saldet['ml_details']['rev_ear_ctc'];
//	   $saldet['rev_ear_ctc']=$saldet['rev_ear_ctc1']+$saldet['ml_details']['rev_ear_ctc'];
//		   
//	
//	   }
//	   else
//		   $saldet['rev_ear_ctc']= $saldet['rev_ear_ctc1'];
           
           
           
            if($ml>0){
	   $saldet['ml_details']=$this->sal_cats_ml($ctc_param,$ml);
	   $saldet['rev_ear_ctc2']=$saldet['ml_details']['rev_ear_ctc'];
	   $saldet['rev_ear_ctc']=$saldet['rev_ear_ctc1']+$saldet['ml_details']['rev_ear_ctc'];
           }
	   else
		   $saldet['rev_ear_ctc']= $saldet['rev_ear_ctc1'];
           
	   return $saldet;
   }
   
   public function sal_cats_ml($ctc_param,$mlnod=0)
	
   {
	
/* required variables start */



$day_in_month=date('t');
//$no_of_days=20;
$no_of_days=$mlnod;


$gross=0;
//$gfsda=1000;
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
$pt=0;
//$pt=1000;
//$it=2500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
if($mlnod>0){
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$mlnod;
}
else
	$ctc=$ctc_param;


//$ctc=$ctc_param;


 
$saldet['ctc']=$ctc;
$saldet['basic']=round(($ctc*56)/100);
/* $saldet['hra']=round(($ctc*25)/100);
$saldet['conv']=round($ctc*5.75/100);
$saldet['epf_13610']=0;
$saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_13610']);
/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['hra']=0;
$saldet['conv']=0;
$saldet['allowance']=0;
$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];

$saldet['spd_allowance']=(($saldet['gross']/$day_in_month)*1.5)*$sdadays;
$saldet['hda_allowance']=(($saldet['gross']/$day_in_month)*1)*$hdadays;



$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;

/* professional tax calulation */
    if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
    }
	else
		unset($saldet['pt']);

/* $saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']; */
$rev_esi=($gross+$saldet['spd_allowance'])*1.75/100;
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

$saldet['epf_12_new']="";

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

$saldet['epf_1361n']="";
$saldet['epf_1361rn']="";

$saldet['rev_ear_ctc']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	

	   $saldet['cat']="s";
	   return $saldet;
   }
   
   
   /* Leave salary category s  not checked as its b    */
   
    public function leave_sal_cats($ctc_param,$nold=11,$tpl=1,$sal_year,$sda=0,$hda=0,$pt=0)
   {
	  
$dim=31;
 $sal_year=date('Y');
 $tpl=12;
 $nod=1;

 if($tpl==15)
 {
	 $temp=3;
 }
 else
 {
	 $temp=0;
 $sal_month=$tpl;
 }
 for($i=$nold;$i>0;$i--){
	 if($tpl==15 && $temp>0){
	 $sal_month=12;
	
	 }
                  switch($sal_month)
				  {
				     Case 1:
					 case 3:
					 case 5:
					 case 7:
					 case 8:
					 case 10:
					 case 12:
						$dim= 31; 
                        break;						
				     Case 4:
					 case 6:
					 case 9:
					 case 11:
						$dim=30;     
						break;
				     Case 2:
						if($sal_year % 4 != 0 && $sal_year % 100 != 0 && $sal_year % 400 != 0 )
							$dim= 28;
						else
							$dim= 29;
						break;
						   
				  }
				  /* calculation part start*/
	if($tpl==15 && $temp>0){
	$sal_month=$sal_month+$temp;
	 $temp--;
	}
$ctc=(($ctc_param/$dim)*$nod);
$lev_saldet[$sal_month]['ctc']=$ctc;
//$gfsda=1000;
$no_of_days=$nod;
//$day_in_month=date('t');
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=$pt;
$pt=$pt;
//$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
$lev_saldet[$sal_month]['ctc']=$ctc_param;
// $lev_saldet[$sal_month]['gross']=round(($ctc*88.56)/100);

$lev_saldet[$sal_month]['basic']=round(($lev_saldet[$sal_month]['ctc']*56)/100);
$lev_saldet[$sal_month]['hra']=round(($lev_saldet[$sal_month]['ctc']*24)/100);

$lev_saldet[$sal_month]['conv']=round(($lev_saldet[$sal_month]['ctc']*6)/100);



$lev_saldet[$sal_month]['allowance']=round(($lev_saldet[$sal_month]['gross']*14)/100);

$lev_saldet[$sal_month]['gross']=$lev_saldet[$sal_month]['basic']+$lev_saldet[$sal_month]['hra']+$lev_saldet[$sal_month]['conv']+$lev_saldet[$sal_month]['allowance'];

$lev_saldet[$sal_month]['rgross']=$lev_saldet[$sal_month]['gross']+$sdadays;


//$lev_saldet[$sal_month]['gross']=round($lev_saldet[$sal_month]['basic']+$lev_saldet[$sal_month]['hra']+$lev_saldet[$sal_month]['conv']+$lev_saldet[$sal_month]['allowance']);
$lev_saldet[$sal_month]['epf_12']=0;
$lev_saldet[$sal_month]['rev_gross']=$lev_saldet[$sal_month]['gross']+$sda;
$lev_saldet[$sal_month]['net_pay']=$lev_saldet[$sal_month]['gross']-$lev_saldet[$sal_month]['epf_12']-$pt;
	
				  
			$lev_saldet[$sal_month]['d_in_mo']=$dim;	  
				 /* calculation part end */
				  $sal_month--;
    } 
   

	   $lev_saldet['cat']="b";
	 
	   return $lev_saldet;
   }
   
   
   
   // public function sal_catz($ctc_param=1,$nod=1,$sda="",$hda="",$ita=0,$add1=2968,$add2=2617)
//    public function sal_catz($ctc_param=1,$add1=2968,$add2=2617,$nod=0,$ml=0,$sda="",$hda="",$ita=0)
    public function sal_catz($ctc_param=1,$add1=2968,$add2=2617,$nod=0,$ml=0,$sda="",$hda="",$ita=0,$ref_inc=0,$half_attn=0)
  //  public function sal_catz()
   {
$atc_ctc_old=$ctc_param;
$day_in_month=date('t');
/* $no_of_days=1; */
$no_of_days=$nod;


$gross=0;
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=$ref_inc;
$half_attn_inc=$half_attn;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
//$pt=0;
$pt=0;
//$it=6500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
if($nod>0){
$tempdays=date('t')-$nod;
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$tempdays;
}
else
	$ctc=$ctc_param;
/* modified */
$saldet['basic']=round(($ctc*56)/100);
$saldet['hra']=round(($ctc*24)/100);
$saldet['conv']=round($ctc*6/100);
 $at=$add1;
 $as=$add2;
 
$temp1=round($saldet['basic']*13.36/100);
if($temp1>=$at)
	$saldet['epf_13610']=$at;
else
	$saldet['epf_13610']=$temp1;
/* modified */
/*$saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_13610']);*/
$saldet['allowance']=round($ctc*14/100);



$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];
/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['spd_allowance']=(($saldet['gross']/$day_in_month)*1.5)*$sdadays; 
$saldet['hda_allowance']=(($saldet['gross']/$day_in_month)*1)*$hdadays; 

/* $saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate; */


$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;

/* professional tax calulation */
    if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
    }
	else
		unset($saldet['pt']);


          if(isset($saldet['pt']))
  $pt=$saldet['pt'];
 else
  $pt=0;

$rev_esi="";
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);

/* modified */
/*$temp2=(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);
if(($temp2)>=$as)
	$saldet['epf_12_new']=$as;
else
	$saldet['epf_12_new']=$temp2;*/
$saldet['epf_12_new']=$at;



$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

$saldet['epf_1361n']="";
$saldet['epf_1361rn']="";
/* modified */
/*$temp3=(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);
$temp3=(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.61/100);
if($temp3>=$at)
	$saldet['epf_1361n']=$at;
else
	$saldet['epf_1361n']=$temp3;

$temp4=round($temp3);
if(($temp4)>=$at)
	$saldet['epf_1361rn']=round($at);
else
	$saldet['epf_1361rn']=round($temp4);*/
$saldet['epf_1361rn']=round($as);



$saldet['ear_ctc']= $ctc;
$saldet['rev_ear_ctc1']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$half_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	
	
	    $saldet['cat']="z";
if($ml>0){
		    $saldet['ml_details']=$this->sal_catz_ml($ctc_param,$add1,$add2,$ml);
	   $saldet['rev_ear_ctc2']=$saldet['ml_details']['rev_ear_ctc'];
	   $saldet['rev_ear_ctc']=$saldet['rev_ear_ctc1']+$saldet['rev_ear_ctc2'];
		   
	
	   }
	   else
		   $saldet['rev_ear_ctc']= $saldet['rev_ear_ctc1'];
	   
	   return $saldet;
	   
   }
   public function sal_catz_ml($ctc_param=1,$add1=2968,$add2=2617,$ml=0)
  //  public function sal_catz()
   {
$atc_ctc_old=$ctc_param;
$day_in_month=date('t');
/* $no_of_days=1; */
$no_of_days=$ml;


$gross=0;
$gfsda=1000;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=12;
//$pt=0;
$pt=0;
//$it=6500;
$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
if($ml>0){
$tempdim=date('t');
$ctc=($ctc_param/$tempdim)*$no_of_days;
}
else
	$ctc=$ctc_param;

$saldet['basic']=round(($ctc*56)/100);
/* $saldet['hra']=round(($ctc*23.5)/100);
$saldet['conv']=round($ctc*5.75/100); */
 $saldet['hra']=0;
 $saldet['conv']=0;
 $at=$add1;
 $as=$add2;
 
$temp1=round($saldet['basic']*13.36/100);
if($temp1>=$at)
	$saldet['epf_13610']=$at;
else
	$saldet['epf_13610']=$temp1;

/* $saldet['allowance']=round($ctc-$saldet['basic']-$saldet['hra']-$saldet['conv']-$saldet['epf_13610']); */
$saldet['allowance']=0;
$saldet['gross']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance'];
/* $saldet['spd_allowance']=(($gfsda/30)*1.5)*$sdadays; */
$saldet['spd_allowance']=(($saldet['gross']/$day_in_month)*1.5)*$sdadays; 
$saldet['hda_allowance']=(($saldet['gross']/$day_in_month)*1)*$hdadays; 

/* $saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate; */


$saldet['gross_pt']=$saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['hda_allowance']+$saldet['spd_allowance']+$oth_ear_loy+$oth_ear_mate;

/* professional tax calulation */
    if(date('m')==3 || date('m')==9 ){
		if($saldet['gross_pt'] >= 0 && $saldet['gross_pt'] <= 21000) 
			$saldet['pt']=0;								
		elseif($saldet['gross_pt'] >= 21001 && $saldet['gross_pt'] <= 30000) 
			$saldet['pt']=100;	
        elseif($saldet['gross_pt'] >= 30001 && $saldet['gross_pt'] <= 45000) 
			$saldet['pt']=235;		
		elseif($saldet['gross_pt'] >= 45001 && $saldet['gross_pt'] <= 60000) 
			$saldet['pt']=510;	
        elseif($saldet['gross_pt'] >= 60001 && $saldet['gross_pt'] <= 75000) 
			$saldet['pt']=760;
		elseif($saldet['gross_pt'] >= 75001) 
			$saldet['pt']=1095;
    }
	else
		unset($saldet['pt']);




$rev_esi="";
$saldet['tot_ear']=round($saldet['basic']+$saldet['hra']+$saldet['conv']+$saldet['allowance']+$saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);


$temp2=(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*12/100);
if(($temp2)>=$at)
	$saldet['epf_12_new']=$at;
else
	$saldet['epf_12_new']=$temp2;

$saldet['rev_net_pay']=round($saldet['tot_ear']-$saldet['epf_12_new']-$esi_osf-$rev_esi-$pt-$it-$oth_ded);

$saldet['epf_1361n']="";
$saldet['epf_1361rn']="";
/* modified */
/* $temp3=(($saldet['basic']+$saldet['conv']+$saldet['allowance'])*13.36/100);
if($temp3>=$as)
	$saldet['epf_1361n']=$as;
else
	$saldet['epf_1361n']=$temp3;

$temp4=round($temp3);
if(($temp4)>=$as)
	$saldet['epf_1361rn']=round($as);
else
	$saldet['epf_1361rn']=round($temp4); */

$saldet['epf_1361rn']=round($as);


$saldet['rev_ear_ctc']=($saldet['tot_ear']+$saldet['epf_1361rn']+$esi_475+$rev_emp_esi)-($saldet['spd_allowance']+$ref_incentive+$qua_attn_inc+$yea_attn_inc+$mob_reim+$oth_ear_loy+$oth_ear_mate);
	
	
	    $saldet['cat']="z";
		
		/* echo "<pre>";
		print_r($saldet); */
	   return $saldet;
	   
   }
  /* leave salary category z */
  public function leave_sal_catz($ctc_param,$nold=11,$tpl=1,$sal_year,$sda=0,$hda=0,$pt=0)
   {
	  
$dim=date('t');
 $sal_year=date('Y');
 $tpl=12;
 $nod=1;

 if($tpl==15)
 {
	 $temp=3;
 }
 else
 {
	 $temp=0;
 $sal_month=$tpl;
 }
 for($i=$nold;$i>0;$i--){
	 if($tpl==15 && $temp>0){
	 $sal_month=12;
	
	 }
                  switch($sal_month)
				  {
				     Case 1:
					 case 3:
					 case 5:
					 case 7:
					 case 8:
					 case 10:
					 case 12:
						$dim= 31; 
                        break;						
				     Case 4:
					 case 6:
					 case 9:
					 case 11:
						$dim=30;     
						break;
				     Case 2:
						if($sal_year % 4 != 0 && $sal_year % 100 != 0 && $sal_year % 400 != 0 )
							$dim= 28;
						else
							$dim= 29;
						break;
						   
				  }
				  /* calculation part start*/
	if($tpl==15 && $temp>0){
	$sal_month=$sal_month+$temp;
	 $temp--;
	}
$ctc=(($ctc_param/$dim)*$nod);
$lev_saldet[$sal_month]['ctc']=($ctc);
//$gfsda=1000;
$no_of_days=$nod;
//$day_in_month=date('t');
$gfsda=0;
$sdadays=$sda;
$hdadays=$hda;
$oth_ear_loy=0;
$oth_ear_mate=0;
$ref_incentive=0;
$qua_attn_inc=0;
$yea_attn_inc=0;
$mob_reim=0;
$esi_osf=0;
$rev_esi=0;
$pt=$pt;
$pt=$pt;
//$it=$ita;
$oth_ded=0;
$esi_475=0;
$rev_emp_esi=0;
$saldet=array();
/* required variables end */
$lev_saldet[$sal_month]['ctc']=$ctc_param;
$lev_saldet[$sal_month]['basic']=round(($ctc*56)/100);

$lev_saldet[$sal_month]['hra']=round(($ctc*24)/100);

$lev_saldet[$sal_month]['conv']=round(($ctc*6)/100);



/* $lev_saldet[$sal_month]['allowance']=round($ctc-$lev_saldet[$sal_month]['basic']-$lev_saldet[$sal_month]['hra']-$lev_saldet[$sal_month]['conv']-$pt); */

$lev_saldet[$sal_month]['allowance']=round(($ctc*14)/100);


$lev_saldet[$sal_month]['gross']=round($lev_saldet[$sal_month]['basic']+$lev_saldet[$sal_month]['hra']+$lev_saldet[$sal_month]['conv']+$lev_saldet[$sal_month]['allowance']);

$lev_saldet[$sal_month]['rev_gross']=$lev_saldet[$sal_month]['gross']+$sdadays;


$lev_saldet[$sal_month]['epf_12']=0;
$lev_saldet[$sal_month]['net_pay']=$lev_saldet[$sal_month]['gross']-$lev_saldet[$sal_month]['epf_12']-$pt;

				$lev_saldet[$sal_month]['d_in_mo']=$dim;  
				  
				 /* calculation part end */
				  $sal_month--;
    } 
   

	   $lev_saldet['cat']="z";
	   
	   return $lev_saldet;
   }
   
  
  
   
 public function form16_calc(){
	   $x=$this->ci->input->post();
	 $emp_id = trim($x['emp_id']);
         $bname = trim($x['bname']);
         $year = trim($x['year']);
         
	 	$Basic		= "0";
       	$HRA		= "0";
	   	$SDAamt		= "0";
	   	$Gross		= "0";
	   	$Net		= "0";
	   	$Pf			= "0";
	   	$IT			= "0";
	   	$PT			= "0";
	   	$Lop		= "0";
	   	$Mobile		= "0";
	   $Bonus		= "0";
	    $MediInsure	= "0";
	   	$RefIncv	= "0";
	   	$QtrIncv	= "0";
	   	$YearIncv	= "0";
	   	$Pf		= "0";
	   	$Loyalty		= "0";
	   	$TotSDA		= "0";
	   	$TotGross	= "0";
	   	$TotMobile	= "0";
	   	//$TotBonus	= "0";
	   	$TotPf		= "0";
	   	$TotIT		= "0";
	   	$TotNet		= "0";
	   	$TotLop		= "0";
	   	$TotBasic	= "0";
	   	$TotHRA		= "0";
	   	$TotPT		= "0";
	   	$TotRefIncv	= "0";
	   	$TotQtrIncv	= "0";
	   	$TotYearIncv ="0";
	   	$TotLoyalty  ="0";
	   	$OverallGross="0";
	   	$HalfBasic	= "0";
	   	$Ten_Percnt_Basic= "0";
	   	$LessSal_Rent	= "0";
	   	$Sum_of_Exp		= "0";
	   	$Less_from_Sal	= "0";
	   	$Taxable_Sal		= "0";
	   	$NetAnnualIncome	= "0";
	   	$Repair_of_Nav	= "0";
	   	$Amt1 = "0";
	   	$Amt2 = "0";
	   	$Amt3 = "0";
		$mobile_allow=0;
	 	 
	    $Convy=9600;			//----- Constant
     
	    $GrossFor=15000;
        $Compare80C=100000;
        $Compare80D=15000;
        $Compare80CCF=20000;
		$Status= "Active";
		$MobileStatus="Yes";
		
		/*Testing purpose  */
		
		$sql="select Emp_id,sum(IT_Basic) as basic,sum(IT_HRA) as hra,sum(IT_SDA)as sda,sum(IT_Gross)as gross,sum(IT_Net)as net,sum(IT_PF) as pf,sum(IT_IncomeTax) as it,sum(IT_PT)as pt,sum(IT_Lop) as lop,sum(IT_Mobile)as mobile,sum(IT_Bonus)as bonus,sum(IT_Ref_incntv) as refincv,sum(IT_Qtr_incntv) as qtrtncv,sum(IT_Year_incntv) as yearincv,sum(IT_Loyalty) as loyalty from incometax where Emp_id='$emp_id' group by Emp_id";
		
		$sql2="SELECT `Emp_id`,sum(`employ_basic_pay`) as basic,sum(`employ_hra`) as hra,sum(`employ_gross`) as gross,sum(`employ_pt`) as pt,sum(`employer_pf`) as pf,sum(`MobileReimnt`)as mobile,sum(`employ_basic_pay`)+sum(`MobileReimnt`) as tgross FROM `employ_salary` WHERE (`sal_month`=4 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=5 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=6 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=7 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=8 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=9 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=10 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=11 and `sal_year`=YEAR(CURDATE())-1) or (`sal_month`=12 and `sal_year`=YEAR(CURDATE())-1 ) or (`sal_month`=1 and `sal_year`=YEAR(CURDATE()) ) or (`sal_month`=2 and `sal_year`=YEAR(CURDATE())) or (`sal_month`=3 and `sal_year`=YEAR(CURDATE()) ) group by `Emp_id` ";
		
		
		$res=$this->ci->db->query($sql);
	       if($res->num_rows()>0){
		$res_arr= $res->result_array();
	    $itarray=array();
		$fit=array();
		foreach($res_arr as $key=>$value)
		{
			/* 
			Testing purpose start*/
			
/*			$value['basic']=400969;
			$value['hra']=169780;
			//$value['mobile']=3500;
			$value['gross']=729514;
			$value['pt']=2190;
			$value['pf']=15552; 
			/* 
			Testing purpose end */
			$itarray[$value['Emp_id']]=$value;
			$itarray[$value['Emp_id']]['overallgross']=$value['gross']+$value['mobile']+$value['refincv']+$value['qtrtncv']+$value['yearincv']+$value['loyalty']+$value['sda'];
			$mobile_allow=$value['mobile'];
		}
		   }
		   /*Testing purpose  */
		$sql2="select * from income_tax_sup where Emp_id='$emp_id'";
		$res2=$this->ci->db->query($sql2);
	    if($res2->num_rows()>0)
		{
		   $res_arr2= $res2->result_array();
		  foreach($res_arr2 as $key2 =>$value2){
			
			if(array_key_exists($value2['Emp_id'],$itarray))
			{
				$itarray[$value2['Emp_id']]['rentpaid']=$value2['Rent_Paid'];
				$itarray[$value2['Emp_id']]['mediexp']=$value2['Medical_Exp'];
				$itarray[$value2['Emp_id']]['rentrecv']=$value2['Rent_Recv'];
				$itarray[$value2['Emp_id']]['munitax']=$value2['Municipal_Tax'];
				$itarray[$value2['Emp_id']]['loanintr']=$value2['Loan_Interest'];
				$itarray[$value2['Emp_id']]['homeloan']=$value2['Housing_Principle'];
				$itarray[$value2['Emp_id']]['lic']=$value2['LIC'];
				$itarray[$value2['Emp_id']]['mediinsur']=$value2['Medi_Insure'];
				$itarray[$value2['Emp_id']]['infbond']=$value2['InfraStruct_Bond'];
				$itarray[$value2['Emp_id']]['ltinve']=$value2['LT_Investment'];
				$itarray[$value2['Emp_id']]['bonus']=$value2['Bonus'];
				$itarray[$value2['Emp_id']]['prevemppf']=$value2['Prev_Empr_PF'];
				$itarray[$value2['Emp_id']]['gratuity']=$value2['Gratuity'];
				
			
			}
		 }
		}
			foreach($itarray as $fkey => $fvalue)
			{
			$fit=$fvalue;	
			}
			
		 	
		
		$overallgross=$fit['overallgross'];
		$halfbasic=round($fit['basic']/2);
		
		
		
		$ten_percent_basic=round(($fit['basic']/100)*10);
		
		
		
		
		$less_sal_rent=round($fit['rentpaid']- $ten_percent_basic);
		  if ($fit['hra'] < $halfbasic)
                  $mini_exp = $fit['hra'];
		  Else
				 $mini_exp =$halfbasic;
						
		if($mini_exp > $less_sal_rent)
			 $mini_exp =$less_sal_rent;
		                   
		$sum_of_exp=round($Convy+$mini_exp+$fit['mediexp']);
		
		
		$less_from_sal=round($overallgross-$sum_of_exp);
		
		
		$taxable_sal= round($less_from_sal - $fit['pt']);
		
		
		
		
		$net_annual_income=round($fit['rentrecv'] - $fit['munitax']);
		$repair_of_nav=round(($net_annual_income/100)*30);
		$Sum_of_80C		= round($fit['pf'] + $fit['homeloan'] + $fit['lic']);
		if ($Sum_of_80C < $Compare80C) 
			$Sum_of_80C = $Sum_of_80C;
		Else
		    $Sum_of_80C = $Compare80C;
							
		if($fit['mediinsur']>$Compare80D)
			$Medi_Insure = $Compare80D;
		if ($fit['infbond'] > $Compare80CCF)
			$inf_bond= $Compare80CCF;
		
		$InterestOnload2 = round($net_annual_income - $repair_of_nav - $fit['loanintr']);
		
		
		$TaxableIncome	= round($taxable_sal -  $Sum_of_80C +$InterestOnload2 );
		
		   $Amt1=180000;
		   //$Amt1=200000;
	       $Amt2 =500000;
		   $Amt3 =800000;
		   
		 
		 $TaxCalc=0;
		if($TaxableIncome <= $Amt1)
			$TaxCalc =0;
		elseif($TaxableIncome <= $Amt2)
		    $TaxCalc = round((($TaxableIncome - $Amt1)*10)/100);
		 elseif ($TaxableIncome <= $Amt3){
			 
		$Tax1 = round((($Amt2 - $Amt1)*10)/100);
		$Tax2 = round((($TaxableIncome - $Amt2)*20)/100);
		$TaxCalc = round($Tax1+$Tax2);
		
		}
		elseif($TaxableIncome >= $Amt3){
			
			$Tax1 = round((($Amt2 - $Amt1)* 10)/100);
			$Tax2 = round((($Amt3 - $Amt2)* 20)/100);
			$Tax3 = round((($TaxableIncome - $Amt3)*30)/100);
			$TaxCalc = round($Tax1+$Tax2+$Tax3);
		}
		
		$AddCess = round(($TaxCalc *3)/100);
		
		
		
		$TotalIT = round(($TaxCalc + $AddCess));
		
		
		$result=array();
		$result['overall']=$overallgross;
		$result['cony']=$Convy;
		$result['hra']=$fit['hra'];
		$result['fifty_basic']=$halfbasic;
		$result['rent_paid']=$fit['rentpaid'];
		$result['mini_exp']=$mini_exp;
		$result['med_exp_final']=$sum_of_exp;
		$result['less_from_sal']=$less_from_sal;
		$result['taxable_salary']=$taxable_sal;
		$result['mob_allow']=round($mobile_allow);
        $result['ten_basic']=$ten_percent_basic;
		$result['pt']=$fit['pt'];
		$result['rentrecv']=$fit['rentrecv'];
		$result['munitax']=$fit['munitax'];
		$result['loanintr']=$fit['loanintr'];
		$result['homeloan']=$fit['homeloan'];
		$result['lic']=$fit['lic'];
		$result['mediinsur']=$fit['mediinsur'];
		$result['infbond']=$fit['infbond'];
		$result['ltinve']=$fit['ltinve'];
		
		
		
		
		
		
		$result['pf']=$fit['pf'];
		$result['bef_taxableincome']= $Sum_of_80C +$InterestOnload2;
		$result['taxableincome']=$TaxableIncome;
		$result['taxcalc']=$TaxCalc;
		$result['addcess']=$AddCess;
		$result['totalit']=$TotalIT;
		
		return $result;
	   
   }
   
   public function emp_teamlist_all(){
	$bname=$this->ci->input->get('bname');   
	$team=$this->ci->input->get('report_team');
	$this->ci->db->select('Emp_id as DT_RowId,Emp_id as id,emp_id,Name as emp_name,Project_team as team_change,shift as shift_time,Process as process, teamch_date as tdate');
	$this->ci->db->where('branch',$bname);
	$this->ci->db->where('Project_team',$team);
	$res=$this->ci->db->get('employee');
	   
	if($res->num_rows()>0)  
	{
		$res_arr=$res->result_array();
		
	   return $res_arr;
	}		
	
		
   }

   
   public function updateteam()
   {
            $ut=$this->ci->input->post(NULL, TRUE);
	   $col="";
	   $id=$ut['id'];
	   if($ut['columnId']==3)
		   $col="Project_team";
	    else if($ut['columnId']==4)
		   $col="teamch_date";
	   else if($ut['columnId']==5)
		   $col="Shift";
	   else if($ut['columnId']==6)
		   $col="Process";
	  
	   if($ut['columnId']==4){
	   $data=array(
		   $col=>date('Y-m-d', strtotime($ut['value']))
		   );
	   }
	   else{
		   $data=array(
		   $col=>$ut['value']
		   );
	   }
		   
		   $this->ci->db->where('Emp_id',$id);
		  $this->ci->db->update('employee',$data);
		if($ut['columnId']==4){
			return $ut['value'];
		}
		else
	        return true;

	   
    }

    public function user_search_data($par)
   {
	   $si_id=$this->ci->session->userdata('search_id');
	   $data=array("cus_id"=>$par);
	   $this->ci->db->where('id',$si_id);
	   $this->ci->db->update('employee',$data);
   }
  
    public function createcookie($x)
	{
		 $cookie = array(
    'name'   => 'demo2',
      'value'  => $x,
       'expire' => '86500',
    'domain' => '192.168.1.197',
    'path'   => '/',
    'prefix' => 'myprefix_',
    'secure' => FALSE
);
if(get_cookie("myprefix_demo2")){
delete_cookie("myprefix_demo2");
}
$this->ci->input->set_cookie($cookie);
	}
	public function getcook()
	{
		
		return $this->ci->input->cookie('myprefix_demo2');
		
	}
   
      public function emp_modify(){
          $bname = $this->ci->input->post('bname');
          $emp_id = $this->ci->input->post('emp_id');
          
          $this->ci->db->select('emp.Id, emp.Branch,emp.Old_Emp_id2, emp.Emp_id, emp.Name, emp.Off_name, emp.Design, DATE_FORMAT( emp.Dob ,"%d-%b-%Y") as Dob,DATE_FORMAT( emp.D_of_join ,"%d-%b-%Y") as D_of_join, emp.Ctc, emp.Ctc_range, emp.Modified_ctc, emp.Dept, emp.Work_type, emp.Email, emp.Mobile1, emp.Mobile2, emp.Phone, emp.Gender, emp.Location, emp.Project, emp.Project_team, emp.Process, emp.Report_to, emp.Shift, emp.nsa_eligible, emp.Texp_year, emp.Texp_month, emp.Exp_MB_Year, emp.Exp_MB_Month, emp.Avatar, emp.Pl_credit, emp.pl_available, DATE_FORMAT( emp.Dofresign ,"%d-%b-%Y") as Dofresign, emp.Curr_notice_period, emp.Curr_notice_reason, emp.Status, emp.Uid, emp.Old_emp_id,per.Father_name, per.Husband_name, per.Wife_name, DATE_FORMAT( per.Dot ,"%d-%b-%Y") as Dot, DATE_FORMAT( per.Dol ,"%d-%b-%Y") as Dol,DATE_FORMAT( emp.Dofresign ,"%d-%b-%Y") as Dofresign',FALSE)
         ->from('employee emp')
           ->join('emp_personal per', 'emp.Emp_id = per.Emp_id','left')->where('emp.Emp_id',$emp_id);
        $result = $this->ci->db->get();       
//        print_r($result->result_array());
        return $result->result_array();
      }  
     
      public function empmodify_update(){
          $x=$this->ci->input->post(NULL, TRUE);
	   $fname="";
	   $tid="";
	   if (isset($_FILES['emp_photo']) && is_uploaded_file($_FILES['emp_photo']['tmp_name'])) {
    
	   $this->ci->load->helper('inflector');
       $file_name = underscore($_FILES['emp_photo']['name']);
       $config['file_name'] = $x['emp_id']."_".$file_name;
	   $fname=$x['emp_id']."_".$file_name;
	   $config['upload_path'] = './assets/uploads/';
       $config['allowed_types'] = 'gif|jpg|jpeg|png';
	   $config['remove_spaces']=TRUE;
	   
	   $this->ci->load->library('upload',$config);
           $this->ci->upload->do_upload('emp_photo');
//	   if()
//		   echo "uploaded";
//	   else
//		   echo "no upload<br>";
	   }
           $emp_id = trim($x['emp_id']);
	   $data=array();
	   $data2=array();
//	 if(array_key_exists('emp_id',$x))
//	 {
//		 $data['Emp_id']=trim($x['emp_id']);
//		 $data2['Emp_id']=trim($x['emp_id']);
//	 }
	  if(array_key_exists('emp_name',$x))
	 {
		 $data['Name']=trim($x['emp_name']);
		 $data2['Name']=trim($x['emp_name']);
	 }
	 if(array_key_exists('sex',$x))
	 {
		 $data['Gender']=trim($x['sex']);
	 }
	 if(array_key_exists('dob',$x))
	 {
		 $data['Dob']=  date('Y-m-d',  strtotime(trim($x['dob'])));
		 $data2['Dob']= date('Y-m-d',  strtotime(trim($x['dob'])));
	 }
	 if(array_key_exists('doj',$x))
	 {
		 $data['D_of_join']=  date('Y-m-d',  strtotime(trim($x['doj'])));
	 }
	 if(array_key_exists('emp_pl',$x))
	 {
		 $data['Pl_credit']=trim($x['emp_pl']);
	 }
	 if(array_key_exists('emp_desig',$x))
	 {
		 $data['Design']=trim($x['emp_desig']);
	 }
	 if(array_key_exists('emp_dept',$x))
	 {
		 $data['Dept']=trim($x['emp_dept']);
	 }
//	 if(array_key_exists('bname',$x))
//	 {
//		 $data['Branch']=trim($x['bname']);
//	 }
         if(array_key_exists('emp_loc',$x))
	 {
		 $data['Location']=trim($x['emp_loc']);
	 }
	 if(array_key_exists('emp_proj',$x))
	 {
		 $data['Project']=trim($x['emp_proj']);
	 }
	 if(array_key_exists('emp_reportteam',$x))
	 {
		 $data['Project_team']=trim($x['emp_reportteam']);
	 }
	 if(array_key_exists('emp_stime',$x))
	 {
		 $data['Shift']=trim($x['emp_stime']);
	 }
	 if(array_key_exists('emp_process',$x))
	 {
		 $data['Process']=trim($x['emp_process']);
	 }
	 if(array_key_exists('status',$x))
	 {
		 $data['Status']=trim($x['status']);
	 }
	 if(array_key_exists('official_name',$x))
	 {
		 $data['off_name']=trim($x['official_name']);
	 }
	 if(array_key_exists('emp_type',$x))
	 {
		 $data['Work_type']=trim($x['emp_type']);
	 }
	 if(array_key_exists('office_email',$x))
	 {
		 $data['Email']=trim($x['office_email']);
	 }
         if(array_key_exists('emp_nsa',$x))
	 {
		 $data['nsa_eligible']=trim($x['emp_nsa']);
	 }else{
              $data['nsa_eligible']=NULL;
         }
	 if($fname!='')
	 {
		 $data['Avatar']=trim($fname);
	 }
         if(array_key_exists('date_of_transfer',$x))
	 {
             if(trim($x['date_of_transfer'] != '')){
		$data2['Dot']=date('Y-m-d', strtotime(trim($x['date_of_transfer'])));
             }
	 }
         if(array_key_exists('date_of_resign',$x))
	 {
             if(trim($x['date_of_resign'] != '')){
		$data['Dofresign']=date('Y-m-d',  strtotime(trim($x['date_of_resign'])));
//                $data['Status']=0;
             }
	 }
         if(array_key_exists('date_of_left',$x))
	 {
             if(trim($x['date_of_left'] != '')){
		 $data2['Dol']=date('Y-m-d',  strtotime(trim($x['date_of_left'])));
                 $data['Status']=0;
             }
	 }
         if(array_key_exists('emp_reason',$x))
	 {
             if(trim($x['emp_reason'] != '')){
		 $data['Curr_notice_reason'] =  trim($x['emp_reason']);
             }
	 }
         
         $this->ci->db->where('Emp_id',$emp_id);
	  $this->ci->db->update('employee',$data);
//	   $tid=$this->ci->db->insert_id();
//	 
//	 //  $res=$this->ci->select('Emp_id')->get_where('employee',array('Id'=>$tid));
//	 $res=$this->ci->db->select('Emp_id')->where('id',trim($tid))
//		                            ->get('employee');
//	  
//	   $res_arr=$res->result_array();
//	  
//	   if(array_key_exists('emp_id',$x))
//	 {
//		 
//		 $data2['Emp_id']=$res_arr[0]['Emp_id'];
//	 }
	   
	 if(array_key_exists('emp_fname',$x))
	 {
		 $data2['Father_name']=trim($x['emp_fname']);
	 }
	 if(array_key_exists('emp_hname',$x))
	 {
		 $data2['Husband_name']=trim($x['emp_hname']);
	 }
	 if(array_key_exists('emp_wname',$x))
	 {
		 $data2['Wife_name']=trim($x['emp_wname']);
	 }
           $this->ci->db->where('Emp_id',$emp_id);
	   $this->ci->db->update('emp_personal',$data2);
           
	   return $emp_id;
      }
      
      function modifyemp_personal($emp_id){
          $this->ci->db->select(' (SELECT Branch FROM employee WHERE Emp_id =  per.Emp_id ) as Branch ,per.Id, per.Emp_id, per.Name, per.Father_name, per.Husband_name, per.Wife_name, per.Dob, per.Blood_group, per.Temp_mobile, per.Perm_mobile, per.Qualification1, per.Qualification2,per.Qualification3,per.Qualification4, per.Temp_add1, per.Temp_add2, per.Temp_add3, per.Temp_city, per.Temp_state, per.Temp_pincode, per.Temp_country, per.Temp_phone, per.Perm_add1, per.Perm_add2, per.Perm_add3, per.Perm_city, per.Perm_state, per.Perm_pincode, per.Perm_country, per.Perm_phone, per.Perm_email, per.Ref1_name, per.Ref1_addr, per.Ref1_phone, per.Ref2_name, per.Ref2_addr, per.Ref2_phone, per.Pancard_no, per.Pancard_name, per.Pancard_copy, per.Pancard_image, per.Last_comp_name, per.Last_comp_design, per.Last_comp_ctc, per.Last_comp_resign_reason, per.Last_comp_notice_period, per.last_comp_emp_id, per.Resume, per.ProfileStatus, per.ProbnDocument',FALSE)
         ->from('emp_personal per')->where('per.Emp_id',$emp_id);
//           ->join('emp_personal per', 'emp.Emp_id = per.Emp_id')
                  
        $result = $this->ci->db->get();       
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
      }
      
      public function emp_personalupdate(){
          
	    $x=$this->ci->input->post(NULL, TRUE);
	    $data=array();
	    $tid = $x['Emp_id'];
	      
	    if(array_key_exists('cur_add1',$x))
	     {
	    	 $data['Temp_add1']=trim($x['cur_add1']);
	     }
	     if(array_key_exists('cur_add2',$x))
	     {
	    	 $data['Temp_add2']=trim($x['cur_add2']);
	     }
	    if(array_key_exists('cur_add3',$x))
	     {
	    	 $data['Temp_add3']=trim($x['cur_add3']);
	     }
	     if(array_key_exists('cur_city',$x))
	     {
	    	 $data['Temp_city']=trim($x['cur_city']);
	     }
	     if(array_key_exists('cur_pincode',$x))
	     {
	    	 $data['Temp_pincode']=trim($x['cur_pincode']);
	     }
	    if(array_key_exists('cur_state',$x))
	     {
	    	 $data['Temp_state']=trim($x['cur_state']);
	     }
	     if(array_key_exists('cur_country',$x))
	     {
	    	 $data['Temp_country']=trim($x['cur_country']);
	     }
	     if(array_key_exists('cur_mobile',$x))
	     {
	    	 $data['Temp_mobile']=trim($x['cur_mobile']);
	     }
	     if(array_key_exists('cur_phone',$x))
	     {
	    	 $data['Temp_phone']=trim($x['cur_phone']);
	     }
	     
	     if(array_key_exists('per_add1',$x))
	     {
	    	 $data['Perm_add1']=trim($x['per_add1']);
	     }
	     if(array_key_exists('per_add2',$x))
	     {
	    	 $data['Perm_add2']=trim($x['per_add2']);
	     }
	    if(array_key_exists('per_add3',$x))
	     {
	    	 $data['Perm_add3']=trim($x['per_add3']);
	     }
	     if(array_key_exists('per_city',$x))
	     {
	    	 $data['Perm_city']=trim($x['per_city']);
	     }
	     if(array_key_exists('per_pincode',$x))
	     {
	    	 $data['Perm_pincode']=trim($x['per_pincode']);
	     }
	    if(array_key_exists('per_state',$x))
	     {
	    	 $data['Perm_state']=trim($x['per_state']);
	     }
	     if(array_key_exists('per_country',$x))
	     {
	    	 $data['Perm_country']=trim($x['per_country']);
	     }
	     if(array_key_exists('per_mobile',$x))
	     {
	    	 $data['Perm_mobile']=trim($x['per_mobile']);
	     }
	     if(array_key_exists('per_phone',$x))
	     {
	    	 $data['Perm_phone']=trim($x['per_phone']);
	     }
	      
	     if(array_key_exists('personal_email',$x))
	     {
	    	 $data['Perm_email']=trim($x['personal_email']);
	     }
	     if(array_key_exists('bgroup',$x))
	     {
	    	 $data['Blood_group']=trim($x['bgroup']);
	     }
	    if(array_key_exists('ref1_name',$x))
	     {
	    	 $data['Ref1_name']=trim($x['ref1_name']);
	     }
	     if(array_key_exists('ref1_add',$x))
	     {
	    	 $data['Ref1_addr']=trim($x['ref1_add']);
	     }
	     if(array_key_exists('ref1_mobile',$x))
	     {
	    	 $data['Ref1_phone']=trim($x['ref1_mobile']);
	     }
	     if(array_key_exists('ref2_name',$x))
	     {
	    	 $data['Ref2_name']=trim($x['ref2_name']);
	     }
	     if(array_key_exists('ref2_add',$x))
	     {
	    	 $data['Ref2_addr']=trim($x['ref2_add']);
	     }
	     if(array_key_exists('ref2_mobile',$x))
	     {
	    	 $data['Ref2_phone']=trim($x['ref2_mobile']);
	     }
             if(array_key_exists('emp_qua',$x))
                {
                    $data['Qualification1']=trim($x['emp_qua']);                   
                }
                if(array_key_exists('emp_qua1',$x))
                {
                    if(trim($x['emp_qua1']) != ''){
                        $data['Qualification1']=trim($x['emp_qua1']);                   
                    }
                }
                if(array_key_exists('emp_qua2',$x))
                {
                    $data['Qualification2']=trim($x['emp_qua2']);                   
                }
                if(array_key_exists('emp_qua3',$x))
                {
                    $data['Qualification3']=trim($x['emp_qua3']);                   
                }
                if(array_key_exists('emp_qua4',$x))
                {
                    $data['Qualification4']=trim($x['emp_qua4']);                   
                }
	    
	        $this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('emp_personal' ,$data);
	    	$this->ci->db->trans_complete();
                if($this->ci->db->trans_status()===FALSE){
                    return false;
                }else{
                    return $tid;
                }
      }
      
      function modifyctc($emp_id){
                    
          $this->ci->db->select('emp.Branch,emp.Emp_id,emp.D_of_join,sal.employ_bank_ac_no, sal.employ_uan_no, sal.employ_esi_no, sal.employ_pf_no, sal.Bank_operating_name, sal.bank_name, sal.bbranch,nom.Naminee_name,nom.Naminee_addr,nom.Naminee_relation,per.Pancard_no,per.Pancard_name,per.Pancard_copy,per.Pancard_image,per.Name,per.uan_image , emp.Zadditional1, emp.Zadditional2, emp.Ctc as employ_ctc,emp.Ctc_range as employ_ctc_range ',FALSE)
            ->from('employ_salary sal, employee emp')           
            ->join('emp_pf_naminee_details nom', 'nom.Emp_id = sal.Emp_id')      
            ->join('emp_personal per', 'per.Emp_id = nom.Emp_id')
            ->where("sal.Emp_id = '$emp_id' and sal.Emp_id = emp.Emp_id and sal.Status = 0 limit 1");
        $result = $this->ci->db->get();       
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return false;
        }
      }
      public function modifyctc_update()
   {
	    $x=$this->ci->input->post(NULL, TRUE);
	    $data=array();   //emp_personal
            $data2=array();  //employee
            $data3=array();  //employ_salary
            $data4=array();  //emp_pf_naminee_details
            $tid="";
            $pancard_name ='';
            $pancardimg_name ='';
            $uan_name ='';
            if (isset($_FILES['pancard']) && is_uploaded_file($_FILES['pancard']['tmp_name'])) {
    
                $this->ci->load->helper('inflector');
                $file_name12 = underscore($_FILES['pancard']['name']);
                $config['file_name'] = $x['Emp_id']."_pancard_".$file_name12;
                $pancard_name=$x['Emp_id']."_pancard_".$file_name12;
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'pdf';
//                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['remove_spaces']=TRUE;            
    //           $this->ci->upload->initialize($config);
                $this->ci->load->library('upload',$config);
                $res_img =  $this->ci->upload->do_upload('pancard');
    //	   if()
    //		   echo "uploaded";
    //	   else
    //		   echo "no upload<br>";
	   }
           
            if (isset($_FILES['pancard_image']) && is_uploaded_file($_FILES['pancard_image']['tmp_name'])) {
    
                $this->ci->load->helper('inflector');
                $file_name = underscore($_FILES['pancard_image']['name']);
                $configimg['file_name'] = $x['Emp_id']."_pancard_image_".$file_name;
                $pancardimg_name=$x['Emp_id']."_pancard_image_".$file_name;
                $configimg['upload_path'] = './assets/uploads/';
                $configimg['allowed_types'] = 'gif|jpg|jpeg|png';
                $configimg['remove_spaces']=TRUE;
                // $this->ci->upload->initialize($configimg);
                $this->ci->load->library('upload',$configimg);           
                $res_pan_img =   $this->ci->upload->do_upload('pancard_image');
//	   if()
//		   echo "uploaded";
//	   else
//		   echo "no upload<br>";
	   }
           if (isset($_FILES['emp_uanfile']) && is_uploaded_file($_FILES['emp_uanfile']['tmp_name'])) {
    
                $this->ci->load->helper('inflector');
                $file_name = underscore($_FILES['emp_uanfile']['name']);
                $configuan['file_name'] = $x['Emp_id']."emp_uanfile".$file_name;
                $uan_name=$x['Emp_id']."emp_uanfile".$file_name;
                $configuan['upload_path'] = './assets/uploads/';
                $configuan['allowed_types'] = 'gif|jpg|jpeg|png';
                $configuan['remove_spaces']=TRUE;
                // $this->ci->upload->initialize($configuan);
                $this->ci->load->library('upload',$configuan);           
                $res_uan_img =   $this->ci->upload->do_upload('emp_uanfile');
//	   if()
//		   echo "uploaded";
//	   else
//		   echo "no upload<br>";
	   }
           
            
            if($pancard_name!='')
	 {
		 $data['Pancard_copy']=trim($pancard_name);
	 }
         if($pancardimg_name!='')
	 {
		 $data['Pancard_image']=trim($pancardimg_name);
	 }
         if($uan_name!='')
	 {
		 $data['uan_image']=trim($uan_name);
	 }
            
            
                if(array_key_exists('Emp_id',$x))
                {
                    $tid=trim($x['Emp_id']);
//                    $data2['Emp_id']=trim($x['Emp_id']);
                    $data3['Emp_id']=trim($x['Emp_id']);
                    $data4['Emp_id']=trim($x['Emp_id']);
                }
                if(array_key_exists('emp_pan',$x))
                {	    	 
                    $data['Pancard_no']=trim($x['emp_pan']);
                }
                if(array_key_exists('emp_panname',$x))
                {	    	 
                    $data['Pancard_name']=trim($x['emp_panname']);
                }
	        $this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('emp_personal' ,$data);
	    	$this->ci->db->trans_complete();
                
            if(array_key_exists('ctc',$x))
            {	    	
               $data2['Ctc']=trim($x['ctc']);
//               $data3['employ_ctc']=trim($x['ctc']);
            }
            if(array_key_exists('ctcr',$x))
            {
                $data2['Ctc_range']=trim($x['ctcr']);
//                $data3['employ_ctc_range']=trim($x['ctcr']);
                $ctcr = trim($x['ctcr']);

                    if($ctcr==1)
                        $res=$this->sal_cata(trim($x['ctc']));
                    if($ctcr==2)
                           $res=$this->sal_catb(trim($x['ctc']));
                    if($ctcr==3)
                           $res=$this->sal_cats(trim($x['ctc']));
                    if($ctcr==4)
                           $res=$this->sal_catz(trim($x['ctc']),trim($x['emp_additional1']),trim($x['emp_additional2']));

//                    echo "<pre>";print_r($res);die();
			 $data3['employ_basic_pay']=$res['basic'];
			 $data3['employ_hra']=$res['hra'];
			 $data3['employ_cony_allow']=$res['conv'];
			 $data3['employ_gross']=$res['gross'];
			 $data3['Total_Earning']=$res['tot_ear'];
			 $data3['employ_net']=$res['rev_net_pay'];
                         $data3['employ_other_allow'] = $res['allowance'];
                         $data3['employ_rev_ear_ctc'] = $res['rev_ear_ctc'];
                         $data3['employer_pf'] = $res['epf_1361rn'];
                         $data3['employer_esi'] = $res['esi_475'];
                         $data3['employ_pf'] = $res['epf_12_new'];
                         $data3['employ_esi'] = $res['esi_175'];
            }
            if(array_key_exists('emp_additional1',$x))
            {
                $data2['Zadditional1']=trim($x['emp_additional1']);
            }
            if(array_key_exists('emp_additional2',$x))
            {
                $data2['Zadditional2']=trim($x['emp_additional2']);
            }
            
		$this->ci->db->trans_start();
	    	$this->ci->db->where('Emp_id', $tid);
                $this->ci->db->update('employee' ,$data2);
	    	$this->ci->db->trans_complete();
		 
	    if(array_key_exists('pf_no',$x))
            {
                $data3['employ_pf_no']=trim($x['pf_no']);
            }
            if(array_key_exists('emp_uan',$x))
            {
                $data3['employ_uan_no']=trim($x['emp_uan']);
            }
		 
            if(array_key_exists('pf_nname',$x))
            {
                $data4['Naminee_name']=trim($x['pf_nname']);
            }
            if(array_key_exists('pf_nrel',$x))
            {
                $data4['Naminee_relation']=trim($x['pf_nrel']);
            }
            if(array_key_exists('pf_nadd',$x))
            {
                $data4['Naminee_addr']=trim($x['pf_nadd']);
            }
                $this->ci->db->where("Emp_id",$tid);
		$emp_nominee_result = $this->ci->db->get('emp_pf_naminee_details');
                if($emp_nominee_result->num_rows() > 0){
                    unset($data4['Emp_id']);
                    $this->ci->db->where('Emp_id', $tid);
                    $this->ci->db->update('emp_pf_naminee_details',$data4);
                }
                else{   
                    $this->ci->db->insert('emp_pf_naminee_details',$data4);
                }
                
                
            if(array_key_exists('emp_esino',$x))
            {
                $data3['employ_esi_no']=trim($x['emp_esino']);
            }
            if(array_key_exists('emp_bname',$x))
            {
                $data3['bank_name']=trim($x['emp_bname']);
            }
            if(array_key_exists('emp_bbranch',$x))
            {
                $data3['bbranch']=trim($x['emp_bbranch']);
            }		 
            if(array_key_exists('emp_bacno',$x))
            {
                $data3['employ_bank_ac_no']=trim($x['emp_bacno']);
            }  
            if(array_key_exists('emp_bacname',$x))
            {
                $data3['Bank_operating_name']=trim($x['emp_bacname']);
            }
//            $this->ci->db->where("Emp_id",$tid);
            $this->ci->db->where("Emp_id = '$tid' and Status = 0");
            $emp_sal_result = $this->ci->db->get('employ_salary');
            if($emp_sal_result->num_rows() > 0){
                unset($data3['Emp_id']);
//                $this->ci->db->where('Emp_id', $tid);
                $this->ci->db->where("Emp_id = '$tid' and Status = 0");
                $this->ci->db->update('employ_salary',$data3);
            }
            else{
                $data3['Status'] = 0;
                $this->ci->db->insert('employ_salary',$data3);
            }
//		  return $this->ci->db->insert_id();
//        echo "<pre>";
//            print_r($data);
//        print_r($data2);
//        print_r($data3);
//        print_r($data4);
//        die();
              return $tid;
   }
   
    public function trackingsheet(){
//       $x=$this->ci->input->post(NULL, TRUE);
       $emp_id=$this->ci->input->post('emp_id');
//       $emp_id='AMSC75';       
       $sql = "select id, (select min(id) from emp_appraisal where Emp_id = '$emp_id') as joining_id , (select Name from employee where Emp_id = '$emp_id') as Name, Emp_id, Desic, DATE_FORMAT( Date1 ,'%d-%b-%Y')  as Date1, Location, (SELECT Design_name FROM designation WHERE Id = Desig ) as Desig_name, Desig, ctc, ctc_range, DATE_FORMAT( Date2 ,'%d-%b-%Y')  as Date2, process, Uid from emp_appraisal where Emp_id = '$emp_id' order by id Asc";
        
        $query = $this->ci->db->query($sql);   
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
   }
   
   public function trackingsheet_update(){
       
       $emp_id=$this->ci->input->post('emp_id');
       $process=$this->ci->input->post('process');
       $date1=$this->ci->input->post('date1');
       $date2=$this->ci->input->post('date2');
       $desig=$this->ci->input->post('desig');
       $ctc=$this->ci->input->post('ctc');
       $appr_refix=$this->ci->input->post('appr_refix');
       $uid = $this->ci->session->userdata('uid');
       
       $sql_array = array('Emp_id'=>$emp_id, 'Desic'=>$appr_refix, 'Date1'=>  date('Y-m-d',  strtotime($date1)), 'Desig'=>$desig, 'ctc'=>$ctc, 'ctc_range'=>'', 'Date2'=>date('Y-m-d',  strtotime($date2)), 'process'=>$process, 'Uid'=>$uid);
       $query = $this->ci->db->insert('emp_appraisal',$sql_array);   
      
        if($this->ci->db->affected_rows()){
            $array_emp = array('Ctc'=>$ctc,'Design'=>$desig);
//            $array_sal = array('employ_ctc'=>$ctc);
//            $this->ci->db->where('Emp_id', $emp_id);
//            $this->ci->db->update('employ_salary',$array_sal);
            $this->ci->db->where('Emp_id', $emp_id);
            $this->ci->db->update('employee',$array_emp);
            
            $select  = $this->ci->db->query("SELECT Ctc_range,Zadditional1,Zadditional2 FROM employee WHERE Emp_id = '$emp_id'");
            if($select->num_rows()>0){
            $employ_det = $select->result_array();
            $ctc_range = $employ_det[0]['Ctc_range'];
            $z = $employ_det[0]['Zadditional1'];
            $z2 = $employ_det[0]['Zadditional2'];
            
                    if($ctc_range==1)
                        $res=$this->sal_cata(trim($ctc));
                    if($ctc_range==2)
                        $res=$this->sal_catb(trim($ctc));
                    if($ctc_range==3)
                        $res=$this->sal_cats(trim($ctc));
                    if($ctc_range==4)
                        $res=$this->sal_catz(trim($ctc),trim($z),trim($z2));
                    
                $data3['employ_ctc']=trim($ctc);
                $data3['employ_ctc_range']=trim($ctc_range);
                $data3['employ_basic_pay']=$res['basic'];
                $data3['employ_hra']=$res['hra'];
                $data3['employ_cony_allow']=$res['conv'];
                $data3['employ_gross']=$res['gross'];
                $data3['Total_Earning']=$res['tot_ear'];
                $data3['employ_net']=$res['rev_net_pay'];
                $data3['employ_other_allow'] = $res['allowance'];
                $data3['employ_rev_ear_ctc'] = $res['rev_ear_ctc'];
                $data3['employer_pf'] = $res['epf_1361rn'];
                $data3['employer_esi'] = $res['esi_475'];
                $data3['employ_pf'] = $res['epf_12_new'];
                $data3['employ_esi'] = $res['esi_175'];
                
                $this->ci->db->where("Emp_id = '$emp_id' and Status = 0");
                $this->ci->db->update('employ_salary',$data3);   
//                if($this->ci->db->affected_rows()){
//                    
//                }
            }
            return true;
        }else{
            return FALSE;
        }
   }
   public function referral_by(){
       
       $remp_id=trim($this->ci->input->get('remp_id'));
       $sql = "select Id, Emp_id,Emp_id as Emp_id_ref, blood_rel, Cadre, Amt, DATE_FORMAT( rdate ,'%d-%b-%Y') as rdate, rmonth, ryear, Status, J_Emp_id, entry_date, Uid, (select concat('',Emp_id,'(',Name,')') from employee where Emp_id = Emp_id_ref ) as Emp_name  from emp_refferal where J_Emp_id = '$remp_id' and Status = 1 ";
       $query = $this->ci->db->query($sql);
       if($query->num_rows()>0){
           return $query->result_array();
       }else{
           return false;
       }
       
   }
   
   public function refferal_update(){
//       $remp_id=trim($this->ci->input->post('remp_id'));
       
        $date=trim($this->ci->input->post('date'));
        $date2=trim($this->ci->input->post('date2'));
        
        $id=trim($this->ci->input->post('id1'));
        $id2=trim($this->ci->input->post('id2'));
        
         $uid = $this->ci->session->userdata('uid');
        
        $sql_array = array('rmonth'=>date('m',  strtotime($date)),'ryear'=>date('Y',  strtotime($date)), 'rdate'=>date('Y-m-d',  strtotime($date)), "modified_date"=>date('Y-m-d'), 'Uid'=>$uid);
       $this->ci->db->where('Id',$id);
       $this->ci->db->update('emp_refferal',$sql_array); 
       
       if($id2 != '' && $date2 != ''){       
            $sql_array2 = array('rmonth'=>date('m',  strtotime($date2)),'ryear'=>date('Y',  strtotime($date2)), 'rdate'=>date('Y-m-d',  strtotime($date2)), "modified_date"=>date('Y-m-d'), 'Uid'=>$uid);
            $this->ci->db->where('Id',$id2);
            $this->ci->db->update('emp_refferal',$sql_array2);            
       }
       
       if($this->ci->db->affected_rows()){
           return true;
       }else{
            return FALSE;
        }
   }
   
   public function refferal_add(){
        $remp_id=trim($this->ci->input->post('remp_id'));
        $bname=trim($this->ci->input->post('bname'));
        $emp_id=trim($this->ci->input->post('emp_id'));
        $b_rel=trim($this->ci->input->post('b_rel'));
        $cadre=trim($this->ci->input->post('cadre'));
        $amt=trim($this->ci->input->post('amt'));
        $date=trim($this->ci->input->post('date'));
        $amt2=trim($this->ci->input->post('amt2'));
        $date2=trim($this->ci->input->post('date2'));
       $uid = $this->ci->session->userdata('uid');
       
       $sql_array = array('Emp_id'=>$emp_id, 'blood_rel'=>$b_rel, 'Cadre'=> $cadre, 'Amt'=>$amt, 'rmonth'=>date('m',  strtotime($date)),'ryear'=>date('Y',  strtotime($date)), 'rdate'=>date('Y-m-d',  strtotime($date)), 'Status'=>1, 'J_Emp_id'=>$remp_id, "entry_date"=>date('Y-m-d'), 'Uid'=>$uid);
       $query = $this->ci->db->insert('emp_refferal',$sql_array); 
       
       if($amt2 != '' && $date2 != ''){       
            $sql_array2 = array('Emp_id'=>$emp_id, 'blood_rel'=>$b_rel, 'Cadre'=> $cadre, 'Amt'=>$amt2, 'rmonth'=>date('m',  strtotime($date2)),'ryear'=>date('Y',  strtotime($date2)), 'rdate'=>date('Y-m-d',  strtotime($date2)), 'Status'=>1, 'J_Emp_id'=>$remp_id, "entry_date"=>date('Y-m-d'), 'Uid'=>$uid);
            $query2 = $this->ci->db->insert('emp_refferal',$sql_array2);            
       }
       
       if($this->ci->db->affected_rows()){
           return true;
       }else{
            return FALSE;
        }
   }
   
   public function emp_experience(){
       $emp_id = $this->ci->input->post('emp_id');
       
       $where = "e.`Emp_id`=a.`Emp_id` and e.Emp_id = '$emp_id' ";
       $sql = " SELECT e.`Emp_id`,e.Name, e.Texp_year,e.Texp_month,e.Exp_MB_Year,e.Exp_MB_Month,e.`D_of_join`,CONCAT(TIMESTAMPDIFF( YEAR, e.D_of_join, now() ),' Year(s), ', TIMESTAMPDIFF( MONTH, e.D_of_join, now() ) % 12,' Month(s), ',  TIMESTAMPDIFF( DAY, DATE_ADD(  DATE_ADD( e.D_of_join , INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,CURDATE() ) YEAR),INTERVAL TIMESTAMPDIFF( MONTH, DATE_ADD( e.D_of_join ,INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,CURDATE()) YEAR ), CURDATE()  ) MONTH ), CURDATE()) ,' Day(s) ' ) as totalin_allzone,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) ) as joining,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) ) as present,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) as join_ctc,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) as present_ctc,(select count(ap1.`Desic`) from emp_appraisal ap1 where ap1.emp_id=e.Emp_id and ap1.`Desic`=1) as appr,(select count(ap2.`Desic`) from emp_appraisal ap2 where ap2.emp_id=e.Emp_id and ap2.`Desic`=2) as refix FROM emp_appraisal a ,employee e where $where";
       
        $query = $this->ci->db->query($sql);   
        if($query->num_rows()>0){
            $emp_info = $query->result_array();
            $result = $emp_info[0];
            
            $sql2 = "select * from emp_prev_company where Emp_id = '$emp_id' ";
            
            $query2 = $this->ci->db->query($sql2);   
            if($query2->num_rows()>0){
                $record = $query2->result_array();
            }
            else{
                $record = 0;
            }            
            $result["records"]=$record;
            return $result;
        }else{
            return FALSE;
        }
   }
   
   public function emp_experience_update(){
       $Texp_year = $this->ci->input->post('Texp_year');
       $Texp_month = $this->ci->input->post('Texp_month');
       $Exp_MB_Year = $this->ci->input->post('Exp_MB_Year');
       $Exp_MB_Month = $this->ci->input->post('Exp_MB_Month');
       $emp_id = $this->ci->input->post('emp_id');
       $Comp_name = $this->ci->input->post('Comp_name');
       $Address = $this->ci->input->post('Address');
       $Design = $this->ci->input->post('Design');
       $Ctc= $this->ci->input->post('Ctc');
       $Net = $this->ci->input->post('Net');
       $Period= $this->ci->input->post('Period');
       $Id = $this->ci->input->post('Id');
       
       $employ_table = array('Texp_month'=>$Texp_month,'Texp_year'=>$Texp_year,'Exp_MB_Month'=>$Exp_MB_Month,'Exp_MB_Year'=>$Exp_MB_Year);
       $this->ci->db->where('Emp_id',$emp_id);
       $query = $this->ci->db->update('employee',$employ_table); 
       
       $count_insert = sizeof($Comp_name);
       if($count_insert >0){
           for($i=0;$i<$count_insert;$i++){
               
               $iComp_name = $Comp_name[$i];
               $iAddress = $Address[$i];
               $iDesign = $Design[$i];
               $iCtc = $Ctc[$i];
               $iNet = $Net[$i];
               $iPeriod = $Period[$i];
               $iId = $Id[$i];
               if(trim($iComp_name) != ''){
                    $exp_array=array('Emp_id'=>$emp_id, 'Comp_name'=>$iComp_name,'Address'=>$iAddress,'Design'=>$iDesign,'Ctc'=>$iCtc,'Net'=>$iNet,'Period'=>$iPeriod);
                    if(trim($iId) != '' ){
                        unset($exp_array['Emp_id']);
                        $this->ci->db->where('Id',$iId);
                        $this->ci->db->update('emp_prev_company',$exp_array);
                    }else{
                        $this->ci->db->insert('emp_prev_company',$exp_array);
                    }
               }
               
           }
       }
       
       
       if($this->ci->db->affected_rows()){
           return true;
       }else{
            return FALSE;
        }
   }
   public function trackingsheet_updateback(){
       
       $emp_id=$this->ci->input->post('emp_id');
       $process=$this->ci->input->post('process');
       $date1=$this->ci->input->post('date1');
       $date2=$this->ci->input->post('date2');
       $desig=$this->ci->input->post('desig');
       $ctc=$this->ci->input->post('ctc');
       $appr_refix=$this->ci->input->post('appr_refix');
       $tracking_id=$this->ci->input->post('tracking_id');
       $uid = $this->ci->session->userdata('uid');
       
       $sql_array = array('Desic'=>$appr_refix, 'Date1'=>  date('Y-m-d',  strtotime($date1)), 'Desig'=>$desig, 'ctc'=>$ctc, 'ctc_range'=>'', 'Date2'=>date('Y-m-d',  strtotime($date2)), 'process'=>$process, 'Uid'=>$uid);
       
       $this->ci->db->where('id', $tracking_id);
       $query = $this->ci->db->update('emp_appraisal',$sql_array);   
      
        if($this->ci->db->affected_rows()){
            $array_emp = array('Ctc'=>$ctc,'Design'=>$desig);
//            $array_sal = array('employ_ctc'=>$ctc);
//            $this->ci->db->where('Emp_id', $emp_id);
//            $this->ci->db->update('employ_salary',$array_sal);
            $this->ci->db->where('Emp_id', $emp_id);
            $this->ci->db->update('employee',$array_emp);
            
            $select  = $this->ci->db->query(" SELECT Ctc_range,Zadditional1,Zadditional2 FROM employee WHERE Emp_id = '$emp_id'");
            if($select->num_rows()>0){
            $employ_det = $select->result_array();
            $ctc_range = $employ_det[0]['Ctc_range'];
            $z = $employ_det[0]['Zadditional1'];
            $z2 = $employ_det[0]['Zadditional2'];
            
                    if($ctc_range==1)
                        $res=$this->sal_cata(trim($ctc));
                    if($ctc_range==2)
                        $res=$this->sal_catb(trim($ctc));
                    if($ctc_range==3)
                        $res=$this->sal_cats(trim($ctc));
                    if($ctc_range==4)
                        $res=$this->sal_catz(trim($ctc),trim($z),trim($z2));
                    
                $data3['employ_ctc']=trim($ctc);
                $data3['employ_ctc_range']=trim($ctc_range);
                $data3['employ_basic_pay']=$res['basic'];
                $data3['employ_hra']=$res['hra'];
                $data3['employ_cony_allow']=$res['conv'];
                $data3['employ_gross']=$res['gross'];
                $data3['Total_Earning']=$res['tot_ear'];
                $data3['employ_net']=$res['rev_net_pay'];
                $data3['employ_other_allow'] = $res['allowance'];
                $data3['employ_rev_ear_ctc'] = $res['rev_ear_ctc'];
                $data3['employer_pf'] = $res['epf_1361rn'];
                $data3['employer_esi'] = $res['esi_475'];
                $data3['employ_pf'] = $res['epf_12_new'];
                $data3['employ_esi'] = $res['esi_175'];
                
                $this->ci->db->where("Emp_id = '$emp_id' and Status = 0");
                $this->ci->db->update('employ_salary',$data3);   
//                if($this->ci->db->affected_rows()){
//                    
//                }
            }
            
            return true;
        }else{
            return FALSE;
        }
   }
   
   public function eadd(){
        $id = $this->ci->input->post('uemp_id');
        $this->ci->db->select('emp.Id, emp.Branch, emp.Emp_id, emp.Name, emp.Off_name, emp.Design, DATE_FORMAT( emp.Dob ,"%d-%b-%Y") as Dob,DATE_FORMAT( emp.D_of_join ,"%d-%b-%Y") as D_of_join, emp.Ctc, emp.Ctc_range, emp.Modified_ctc, emp.Dept, emp.Work_type, emp.Email, emp.Mobile1, emp.Mobile2, emp.Phone, emp.Gender, emp.Location, emp.Project, emp.Project_team, emp.Process, emp.Report_to, emp.Shift, emp.nsa_eligible, emp.Texp_year, emp.Texp_month, emp.Exp_MB_Year, emp.Exp_MB_Month, emp.Avatar, emp.Pl_credit, emp.pl_available, DATE_FORMAT( emp.Dofresign ,"%d-%b-%Y") as Dofresign, emp.Curr_notice_period, emp.Curr_notice_reason, emp.Status, emp.Uid, emp.Old_emp_id,per.Father_name, per.Husband_name, per.Wife_name, DATE_FORMAT( per.Dot ,"%d-%b-%Y") as Dot, DATE_FORMAT( per.Dol ,"%d-%b-%Y") as Dol,DATE_FORMAT( emp.Dofresign ,"%d-%b-%Y") as Dofresign',FALSE)
                     ->from('employee emp')
                     ->join('emp_personal per', 'emp.Emp_id = per.Emp_id','left')->where('emp.Id',$id);
        $result = $this->ci->db->get();                       
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }        
   }
}