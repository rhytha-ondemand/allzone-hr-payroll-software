<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(1);
class Reports_lib{
	
	private $ci;
	
	public function __construct()
	{
    
	$this->ci=&get_instance();
	$this->ci->load->database();
	$this->ci->load->dbutil();
	  $this->ci->load->helper('download');
	  $this->ci->load->library('employee_lib',NULL,'emp');
	
	

	}
	

	public function rep_field_list()
	{
		$rep_no=$this->ci->input->post('rep_list');
		
		if($rep_no!=''){
		
		 $str="select id,field from table_details where find_in_set(id,(select rep_fname from reports where rep_id=$rep_no))";
		  $str2="select id,field,1 as addf from table_details where find_in_set(id,(select rep_fname2 from reports where rep_id=$rep_no))";
		
		$res=$this->ci->db->query($str);
       $res2=$this->ci->db->query($str2);
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			if($res2->num_rows()>0){
				$res_arr2= $res2->result_array();
			
			foreach($res_arr2 as $key => $value)
			
				array_push($res_arr,$value);
			}
			$res_arr['rep_no']=$rep_no;
				
			return $res_arr;
		}
		}
		return false;
		
	}
	/* public function rep_field_list2($out,$temp2)
	{
		$rno=$this->ci->input->post('rep_no');
		
		$this->ci->load->library('excel');
		$data=array('rep_fname2'=>$temp2);
        $this->ci->db->where('rep_id',$rno);
        $this->ci->db->update('reports',$data);
		
	
	$temp= "CALL teamwise(?,?,?,?,?,?,?)";
	$branch=$this->ci->input->post('branch');
	$gender=$this->ci->input->post('gen');
	$team=$this->ci->input->post('emp_rteam');
	$desig=$this->ci->input->post('emp_rdesig');
	$prj=$this->ci->input->post('emp_rtprj');
	$dept=$this->ci->input->post('emp_rtdept');
	$data=array('CLAIM_NOS'=>$out,
	'Branch'=>$branch,
	//'Branch'=>4,
	'GENDER'=>$gender,
	'TEAM'=>$team,
	'DESIGNATION'=>$desig,
	'PROJECT'=>$prj,
	'DEPT'=>$dept
	);
         $res = $this->ci->db->query($temp,$data);
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
         
			 
			return $res_arr;
		}
		return false;	
		
	} */
	public function rep_field_list2($out,$temp2)
	{
		
		
		$rno=$this->ci->input->post('rep_no');
		 $branch=$this->ci->input->post('branch');
	        $gender=$this->ci->input->post('gen');
	        $team=$this->ci->input->post('emp_rteam');
	        $desig=$this->ci->input->post('emp_rdesig');
	        $prj=$this->ci->input->post('emp_rtprj');
	        $dept=$this->ci->input->post('emp_rtdept');
			$process=$this->ci->input->post('emp_process');
			$sdate1=$this->ci->input->post('sdate');
			$edate1=$this->ci->input->post('edate');
			$sdate=date('Y-m-d',  strtotime($sdate1));
			$edate=date("Y-m-d",  strtotime($edate1));
                        
                $lyear = $this->ci->input->post('lyear');
                $msmonth = $this->ci->input->post('msmonth');
                $msyear = $this->ci->input->post("msyear");
                        
//		$this->ci->load->library('excel');
		if(isset($temp2)){
		 $data=array('rep_fname2'=>$temp2);
        $this->ci->db->where('rep_id',$rno);
        $this->ci->db->update('reports',$data); 
		}
		if($rno==6)
		{
	
	    $temp= "CALL simpleproc(?)";
		$data=array('CLAIM_NOS'=>$out,
		'Branch'=>$branch,
		);
		}
		if($rno==13)
		{
	
	    $temp= "CALL monthsalsumm(?,?,?,?)";
		$data=array('CLAIM_NOS'=>$out,
		'Branch'=>$branch,
                 'mon'=>$msmonth,
                  'year'=>$msyear,  
		);
		}
		if($rno==9)
		{
	
	    $temp= "CALL lsalsumm(?,?,?)";
		$data=array('CLAIM_NOS'=>$out,
		'Branch'=>$branch,
                 'year'=>$lyear ,  
		);
		}
		elseif($rno==5)
		{
	
	    $temp= "CALL conref(?,?,?,?)";
		$data=array('CLAIM_NOS'=>$out,
		'Branch'=>$branch,
		'sdate'=>$sdate,
		'edate'=>$edate
		);
		}
		elseif($rno==1)
		{
	
	    $temp= "CALL monthlysal(?,?,?,?)";
		$data=array('CLAIM_NOS'=>$out,
		'Branch'=>$branch,
		'TEAM'=>$team,
		'PROCESS'=>$process
		);
		
		}
		else
		{
	        $temp= "CALL teamwise(?,?,?,?,?,?,?)";
	       
	        $data=array('CLAIM_NOS'=>$out,
	        //'Branch'=>$branch,
	        'Branch'=>$branch,
	        'GENDER'=>$gender,
	        'TEAM'=>$team,
	        'DESIGNATION'=>$desig,
	        'PROJECT'=>$prj,
	        'DEPT'=>$dept
	        );	
	   		
		}
		 
		 $res = $this->ci->db->query($temp,$data);
		if(is_object($res)){
                    if($res->num_rows()>0)
                    {
                            $res_arr= $res->result_array();


                            return $res_arr;
                    }
                    return false;	
                    }
                else{
                    return false;
                }
	} 
	
	public function team_list()
	{
		
		$str="select reporting_no,team from reporting_name";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
	public function branch_list()
	{
		
		$str="select Id,Location from locations where type='b'";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
	public function desig_list()
	{
		
		$str="select Id,Design_name from designation";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
	public function emp_field_list()
	{
		
		
		$str="select id,field from table_details where value='employee' and status=1";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
	public function per_field_list()
	{
		
		
		$str="select id,field from table_details where value='emp_personal' and status=1";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
	
	
	public function master_salary()
	{
		$branch = trim($this->ci->input->post('bname'));
                /*$str="select e.*,s.`employ_bank_ac_no`,s.bbranch,s.bank_name,s.Bank_operating_name,s.`employ_uan_no`,s.`employ_pf_no`,`employ_esi_no`,s.employ_it as it from employee e left join employ_salary s on(e.`Emp_id`=s.`Emp_id`) where e.status=1 and e.Branch=$branch and s.sal_month = month(CURRENT_DATE) and s.sal_year = year(CURRENT_DATE) "; */
		$str="select e.*,s.`employ_bank_ac_no`,s.bbranch,s.bank_name,s.Bank_operating_name,s.`employ_uan_no`,s.`employ_pf_no`,`employ_esi_no`,s.employ_it as it from employee e left join employ_salary s on(e.`Emp_id`=s.`Emp_id` AND s.Status =0 ) where e.status=1 and e.Branch=$branch"; 
		$res=$this->ci->db->query($str);
                
		$mon= date('m');
	
		
		$yea=date('Y');
		 if($mon==1){
						   $date3=($yea-1)."-12-26";
						   }
						   else
							$date3=$yea."-".($mon-1)."-26";   
						    $date4=$yea."-".$mon."-25";
		$mlarray=array();
	    /* lop */
		$str2="select * from leave_log where month=$mon and year=$yea";
		$res2=$this->ci->db->query($str2);
		
		/* maternity leave */
		
		$str4="select * from leave_log_ml where (month=$mon or month2=$mon or month3=$mon) and year=$yea";
		$res4=$this->ci->db->query($str4);
		if($res4->num_rows()>0)
		{
		$res_arr4=$res4->result_array();
		
		foreach($res_arr4 as $key4 =>$values4){
			$mlarray[$values4['Emp_id']][$key4]=$values4;
		
			}
			
        }
		
		/* sda */
		$pmon=$mon-1;
		$pyear=0;
		if($pmon==0)
		{
			$pmon=12;
			$pyear=$yea-1;
		}
		else
		{
			$pmon=$mon-1;
			$pyear=$yea;
		}
			
		
		$str3="SELECT `Emp_id`,sum(`No_of_days`) as sda FROM `sda` WHERE ((month(`Sda_date`)=$pmon and day(`Sda_date`)>=26 and year(`Sda_date`)=$pyear) or (month(`Sda_date`)=$mon and day(`Sda_date`)<=25) and year(`Sda_date`)=$yea) and status=1 group by `Emp_id`";
		$res3=$this->ci->db->query($str3);
		
		
		/* referral Incentive */
		$str4="select *,sum(Amt) as total from emp_refferal WHERE month(`rdate`)=$mon and rmonth=$mon and year(`rdate`)=$yea group by Emp_id";
		$res4=$this->ci->db->query($str4);
		
		/* half yearly attance Incentive */
		if($mon==6 || $mon==12)
		{
			if($mon==6){
			$ayea1= date("Y")-1;
		    $ayea2= date("Y");
			$d1=$ayea1."-12-26";
			$d2=$ayea2."-06-25";
			}
			if($mon==12){
			 $ayea1= date("Y");
		     $ayea2= date("Y");
			 $d1=$ayea1."-06-26";
			 $d2=$ayea1."-12-25";
			}
		$str5="SELECT * FROM `emp_leave` WHERE Leave_type in(1,7) and `Leave_sdate` between '".$d1."' and '".$d2."' group by `Emp_id`";
		$res5=$this->ci->db->query($str5);
		}
		
		/*yearly attendance Incentive */
		
			if($mon==12){
			$ayea1= date("Y")-1;
		    $ayea2= date("Y");
			$d1=$ayea1."-12-26";
			$d2=$ayea2."-12-25";
			}
			
		$str6="SELECT * FROM `emp_leave` WHERE Leave_type in(1,7) and `Leave_sdate` between '".$d1."' and '".$d2."' group by `Emp_id`";
		$res6=$this->ci->db->query($str6);
		
		/* for it */
		$str7="select * from income_tax_monthly where sal_mon=$mon and sal_year=$yea";
		$res7=$this->ci->db->query($str7);
		
		$larray=array();
		$earray=array();
		$sal=array();
		$ref_array=array();
		$haf_attn_array=array();
		$yearly_attn_array=array();
		$it_array=array();
		
		if(is_object($res))
		{
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			if(is_object($res2))
				{
			if($res2->num_rows()>0)
				{		
			
			$res_arr2= $res2->result_array();
			foreach($res_arr2 as $key2 =>$values2){
			$larray[$values2['Emp_id']]=$values2['lop'];
			}



			   }
				}
			   
				if(is_object($res3))
				{
				if($res3->num_rows()>0)
				{	
			$res_arr3= $res3->result_array();
			foreach($res_arr3 as $key3 =>$values3){
			$sdarray[$values3['Emp_id']]=$values3['sda'];
			     }	
				}
				}
				if(is_object($res4))
				{
					if($res4->num_rows()>0)
				    {	
			            $res_arr4= $res4->result_array();
			              foreach($res_arr4 as $key4 =>$values4){
			              $ref_array[$values4['Emp_id']]=$values4['total'];
			              }	
				    }      
				}

				if(is_object($res5))
				{
					if($res5->num_rows()>0)
				    {	
			            $res_arr5= $res5->result_array();
			              foreach($res_arr5 as $key5 =>$values5){
			              $haf_attn_array[$values5['Emp_id']]=2500;
			              }	
				    }      
				}
				if(is_object($res6))
				{
					if($res6->num_rows()>0)
				    {	
			            $res_arr6= $res6->result_array();
			              foreach($res_arr6 as $key6 =>$values6){
			              $yearly_attn_array[$values5['Emp_id']]="1";
			              }	
				    }      
				}
				if(is_object($res7))
				{
					if($res7->num_rows()>0)
				    {	
			            $res_arr7= $res7->result_array();
			              foreach($res_arr7 as $key7 =>$values7){
			              $it_array[$values7['Emp_id']]=$values7['It_amt'];
			              }	
				    }      
				}
			foreach($res_arr as $key => $values)
			{
			
				$earray[$values['Emp_id']]=$values;	
				$earray[$values['Emp_id']]['sal_month']=date('m');
			$earray[$values['Emp_id']]['sal_year']=date('Y');
				foreach($larray as $lkey=>$v1)
				{
					if(array_key_exists($lkey,$earray)){
					$earray[$lkey]['lop']=$v1;	
					
					}
					
				}
				
				
				foreach($ref_array as $ref_key=>$ref_v)
				{
					
					if(array_key_exists($ref_key,$earray))
					$earray[$ref_key]['ref_inc']=$ref_v;	
					
				}
				foreach($haf_attn_array as $half_key=>$half_v)
				{
					
					if(array_key_exists($half_key,$earray))
					$earray[$half_key]['half_attn_inc']=2500;	
					
				}
                foreach($yearly_attn_array as $yea_key=>$yea_v)
				{
					
					if(array_key_exists($yea_key,$earray))
					$earray[$yea_key]['yearly_attn_inc']="1";	
					
				}				
				foreach($it_array as $it_key=>$it_v)
				{
					
					if(array_key_exists($it_key,$earray))
					$earray[$it_key]['it']=$it_v;	
					
				}
				foreach($sdarray as $skey=>$v2)
				{
					
					if(array_key_exists($skey,$earray))
					$earray[$skey]['sda']=$v2;	
					
				}
				foreach($mlarray as $mkey=>$v3)
				{
					
					
			      foreach($v3 as $mkey2=>$v4){
									
					$temp=$v4['sdate'];
					if(array_key_exists($mkey,$earray))
					{
                         if($mon==date('m',strtotime($temp))){
					
							if(strtotime($temp)>strtotime($date3) && date('d',strtotime($temp))<=25)
							{
								
								$date1 = new DateTime($temp);
						        $date2 = new DateTime($date4);
								 $interval = $date1->diff($date2);
							      $earray[$mkey]['ml']=($interval->d)+1;
								  if(isset( $earray[$mkey]['lop']))
							         $earray[$mkey]['ml_lop']=$earray[$mkey]['lop']+$earray[$mkey]['ml'];		  
							}
						  
					    }
						else					
				        {
						
							if(!isset($earray[$mkey]['ml']))
							{
								
								$temp2='2016-'.($mon-1)."-26";
								if(date('m',strtotime($temp))==date('m',strtotime($temp2)) && date('d',strtotime($temp))>=26)
								{
									
									
								   $date1 = new DateTime($temp);
						           $date2 = new DateTime($date4);
								   $interval = $date1->diff($date2);
								   
							       $earray[$mkey]['ml']=($interval->d)+1;
									  if(isset( $earray[$mkey]['lop']))
							         $earray[$mkey]['ml_lop']=$earray[$mkey]['lop']+$earray[$mkey]['ml'];
								}
								else
								{
									if(strtotime($temp)<strtotime($v4['sdate']))
									{
																
									   $earray[$mkey]['ml']=date('t');
									     if(isset( $earray[$mkey]['lop']))
							         $earray[$mkey]['ml_lop']=$earray[$mkey]['lop']+$earray[$mkey]['ml'];
									}
								}
								
							}
							else
								{
									
                                    if($earray[$mkey]['ml']=='')
									  $earray[$mkey]['ml']=date('t');
								       
                                          if(isset( $earray[$mkey]['lop']))
							         $earray[$mkey]['ml_lop']=$earray[$mkey]['lop']+$earray[$mkey]['ml'];									   
								}
						
				        }
					}
                    
				 } 
					
				}
				
				
				$lop=0;
                                $ml = 0;
								$ref_incv=0;
								$half_incv=0;
								$ita=0;
			  // if(trim(strtoupper($values['Ctc_range']))=="A"){
				  if(trim($values['Ctc_range'])==1){
				$lop=0;
                                $ml = 0;
								$ref_incv=0;
			     if(isset($earray[$values['Emp_id']]['lop'])){
					
					$lop=$earray[$values['Emp_id']]['lop'];
				  }
				  if(isset($earray[$values['Emp_id']]['ml_lop'])){
					
					$lop=$earray[$values['Emp_id']]['ml_lop'];
				  }
				if(isset($earray[$values['Emp_id']]['ml'])){
					
					$ml=$earray[$values['Emp_id']]['ml'];
				  }
				if(isset($earray[$values['Emp_id']]['sda'])){
				
					$sda=$earray[$values['Emp_id']]['sda'];
				}
				else
					$sda="";
				

				if(isset($earray[$values['Emp_id']]['ref_inc'])){
				
					$ref_incv=$earray[$values['Emp_id']]['ref_inc'];
				}
				else
					$ref_incv="";
				
				if(isset($earray[$values['Emp_id']]['half_attn_inc'])){
					$half_incv=$earray[$values['Emp_id']]['half_attn_inc'];
				}
				else
					$half_incv="";
				if(isset($earray[$values['Emp_id']]['it'])){
					$ita=$earray[$values['Emp_id']]['it'];
				}
				else
					$ita=0;			
				
			    if($ref_incv>0)
				$sal=$this->ci->emp->sal_cata($values['Ctc'],$lop,$ml,$sda,$hda,$ita,$ref_incv,$half_incv);	
				else
				$sal=$this->ci->emp->sal_cata($values['Ctc'],$lop,$ml,$sda,$hda,$ita,0,$half_incv);
			
				if(isset($sal))
				{
			    
				  foreach($sal as $akey=>$aval){
			          $earray[$values['Emp_id']][$akey]=$aval;
					}
			    }
			} 
			 // elseif(trim(strtoupper($values['Ctc_range']))=="B"){
			elseif(trim($values['Ctc_range'])==2){
                            $lop=0;
                                $ml = 0;
								$ref_incv=0;
								$half_incv=0;
								$ita=0;
				//sal_catb($ctc_param,$nod=1,$sda="",$ita=0)
				if(isset($earray[$values['Emp_id']]['lop'])){
					
					$lop=$earray[$values['Emp_id']]['lop'];
				  }
				  
				  if(isset($earray[$values['Emp_id']]['ml'])){
                                      if($lop > 0){
					$lop = $lop + $earray[$values['Emp_id']]['ml'];                                        
                                      }else{                                          
                                          $lop = $earray[$values['Emp_id']]['ml'];
                                      }
					$ml=$earray[$values['Emp_id']]['ml'];
//                                        if($ml>20)
//                                            unset($earray[$values['Emp_id']]);
				  }
                                  if(isset($earray[$values['Emp_id']]['ml_lop'])){
					
					$lop=$earray[$values['Emp_id']]['ml_lop'];
				  }
				/* else
					$lop=date('t'); */
				
				if(isset($earray[$values['Emp_id']]['sda'])){
				
					$sda=$earray[$values['Emp_id']]['sda'];
				}
				else
					$sda="";
				if(isset($earray[$values['Emp_id']]['ref_inc'])){
				
					$ref_incv=$earray[$values['Emp_id']]['ref_inc'];
				}
				else
					$ref_incv="";
				if(isset($earray[$values['Emp_id']]['half_attn_inc'])){
					$half_incv=$earray[$values['Emp_id']]['half_attn_inc'];
				}
				else
					$half_incv="";
				if(isset($earray[$values['Emp_id']]['it'])){
					$ita=$earray[$values['Emp_id']]['it'];
				}
				else
					$ita=0;				
							
				
				if($ref_incv>0)
					$sal=$this->ci->emp->sal_catb($values['Ctc'],$lop,$sda,$hda,$ita,$ref_incv,$half_incv);
				else
				$sal=$this->ci->emp->sal_catb($values['Ctc'],$lop,$sda,$hda,$ita,$ref_incv,$half_incv);
                                
                                if(date('m') == 2){
                                   $tdays = 22; 
                                }else {
                                    $tdays = 25;
                                }
                                
                                if($ml >= $tdays){
                                            unset($earray[$values['Emp_id']]);
                                            unset($sal);
                                }
				if(isset($sal))
				{
			      
				  	foreach($sal as $bkey=>$bval){
			          $earray[$values['Emp_id']][$bkey]=$bval;
					}
			    }
			} 
			// elseif(trim(strtoupper($values['Ctc_range']))=='S'  ){
				elseif(trim($values['Ctc_range'])==3  ){
                                    $lop=0;
                                $ml = 0;
								$ref_incv=0;
								$half_incv=0;
								$ita=0;
//                                echo 'total_ml-- > '.$ml.'<br>';
				//sal_cats($ctc_param,$nod=1,$sda="",$ita=0)
				
				if(isset($earray[$values['Emp_id']]['lop'])){
					
					$lop=$earray[$values['Emp_id']]['lop'];
				  }
				   if(isset($earray[$values['Emp_id']]['ml_lop'])){
					
					$lop=$earray[$values['Emp_id']]['ml_lop'];
				  }
				  if(isset($earray[$values['Emp_id']]['ml'])){
					
					$ml=$earray[$values['Emp_id']]['ml'];
				  }
				if(isset($earray[$values['Emp_id']]['sda'])){
				
					$sda=$earray[$values['Emp_id']]['sda'];
				}
				else
					$sda="";
				if(isset($earray[$values['Emp_id']]['ref_inc'])){
				
					$ref_incv=$earray[$values['Emp_id']]['ref_inc'];
				}
				else
					$ref_incv="";
				if(isset($earray[$values['Emp_id']]['half_attn_inc'])){
					$half_incv=$earray[$values['Emp_id']]['half_attn_inc'];
				}
				else
					$half_incv="";
				if(isset($earray[$values['Emp_id']]['it'])){
					$ita=$earray[$values['Emp_id']]['it'];
				}
				else
					$ita=0;				
						
//				echo 'total_ml curremt-- > '.$ml.'<br>';
                if($ref_incv>0)
					$sal=$this->ci->emp->sal_cats($values['Ctc'],$lop,$ml,$sda,$hda,$ita,$ref_incv,$half_incv);
				else
                	$sal=$this->ci->emp->sal_cats($values['Ctc'],$lop,$ml,$sda,$hda,$ita,$ref_incv,$half_incv);
				if(isset($sal))
				{
			      /* $earray[$values['Emp_id']]['sal']=$sal; */
				  	foreach($sal as $skey=>$sval){
			          $earray[$values['Emp_id']][$skey]=$sval;
					}
			    }
			}
			// elseif(trim(strtoupper($values['Ctc_range']))=='Z'  ){
				elseif(trim($values['Ctc_range'])==4  ){
                                    $lop=0;
                                $ml = 0;
								$ref_incv=0;
								$half_incv=0;
								$ita=0;
				//sal_catz($ctc_param=1,$nod=1,$sda="",$add1=2968,$add2=2617)
				
				
				if(isset($earray[$values['Emp_id']]['lop'])){
					
					$lop=$earray[$values['Emp_id']]['lop'];
				  }
				  if(isset($earray[$values['Emp_id']]['ml_lop'])){
					
					$lop=$earray[$values['Emp_id']]['ml_lop'];
				  }
				  if(isset($earray[$values['Emp_id']]['ml'])){
					
					$ml=$earray[$values['Emp_id']]['ml'];
				  }
				if(isset($earray[$values['Emp_id']]['sda'])){
				
					$sda=$earray[$values['Emp_id']]['sda'];
				}
				else
					$sda="";
				if(isset($earray[$values['Emp_id']]['ref_inc'])){
				
					$ref_incv=$earray[$values['Emp_id']]['ref_inc'];
				}
				else
					$ref_incv="";
				
				if(isset($earray[$values['Emp_id']]['half_attn_inc'])){
					$half_incv=$earray[$values['Emp_id']]['half_attn_inc'];
				}
				else
					$half_incv="";
				if(isset($earray[$values['Emp_id']]['it'])){
					$ita=$earray[$values['Emp_id']]['it'];
				}
				else
					$ita=0;				

				
//				$sal=$this->ci->emp->sal_catz($values['Ctc'],$lop,$sda);
                if($ref_incv>0)
					$sal=$this->ci->emp->sal_catz($values['Ctc'],$values['Zadditional1'],$values['Zadditional2'],$lop,$ml,$sda,$hda,$ita,$ref_incv,$half_incv);
				else
                    $sal=$this->ci->emp->sal_catz($values['Ctc'],$values['Zadditional1'],$values['Zadditional2'],$lop,$ml,$sda,$hda,$ita,$ref_incv,$half_incv);
				if(isset($sal))
				{
					foreach($sal as $zkey=>$zval){
			          $earray[$values['Emp_id']][$zkey]=$zval;
					}
			    }
            }				
			
			 	
			}
			
//			echo "<pre>";
//			print_r($earray);
//                        die();
                       foreach ($earray as $row){
                            $insert_array = array( "Emp_id"=>$row['Emp_id'],
                                                    "employ_basic_pay"=>$row['basic'],
                                                    "employ_hra"=>$row['hra'],
                                                    "employ_cony_allow"=>$row['conv'],
                                                    "employ_other_allow"=>$row['allowance'],
                                                    "attn_half_incent"=>isset($row['half_attn_inc'])?$row['half_attn_inc']:0,
                                                    "attn_year_incent"=>isset($row['yearly_attn_inc'])?$row['yearly_attn_inc']:0,
//                                                    ""=>$row['gross_pt'],
                                                    "employ_pt"=>$row['pt'],
                                                    "employ_it"=>$row['it'],
                                                    "employ_rev_ear_ctc" => $row['rev_ear_ctc'],
                                                    "employ_sda_allow"=>isset($row['spd_allowance'])?$row['spd_allowance']:0,
                                                    "employ_ha_allow"=>isset($row['hda_allowance'])?$row['hda_allowance']:0,
                                                    "employ_reff_incent"=>isset($row['ref_inc'])?$row['ref_inc']:0,
                                                    "employ_lop"=>isset($row['lop'])?$row['lop']:0,
                                                    "employ_gross"=>$row['gross'],
                                                    "Total_Earning"=>$row['tot_ear'],
                                                    "employ_pf"=>$row['epf_12_new'],
                                                    "employ_net"=>$row['rev_net_pay'],
                                                    "employer_pf"=>$row['epf_1361n'],
                                                    "employ_ctc"=>$row['Ctc'],
                                                    "employ_ctc_range"=>$row['Ctc_range'],
                                                    "sal_month"=>$row['sal_month'],
                                                    "sal_year"=>$row['sal_year'],
                                                    "employer_esi"=>isset($row['esi_475'])?$row['esi_475']:0,
                                                    "employ_esi"=>isset($row['esi_175'])?$row['esi_175']:0,
                                                    "employ_bank_ac_no"=>$row['employ_bank_ac_no'],
                                                    "employ_uan_no"=>$row['employ_uan_no'],
                                                    "employ_pf_no"=>$row['employ_pf_no'],
                                                    "employ_esi_no"=>$row['employ_esi_no'],
                                                    "Bank_operating_name"=>$row['Bank_operating_name'],
                                                    "bank_name"=>$row['bank_name'],
                                                    "bbranch"=>$row['bbranch']);
                            $emp_id = $row['Emp_id'];
                            $sal_month = $row['sal_month'];
                            $sal_year = $row['sal_year'];
                            $query = $this->ci->db->query("select Emp_id,Id from employ_salary where Emp_id = '$emp_id' and sal_month = '$sal_month' and sal_year = '$sal_year' ");                           
                            if($query->num_rows() > 0){
                                unset($insert_array['Emp_id']);
                                $query_result = $query->result_array();                                
                                $Id = $query_result[0]['Id'];
                                $this->ci->db->where("Id = $Id and Emp_id = '$emp_id' ");
                                $this->ci->db->update('employ_salary',$insert_array);
                            }else{                                
                                $this->ci->db->insert('employ_salary',$insert_array);
                            }
                            
                            $it_query = $this->ci->db->query("select Emp_id,Id from income_tax_monthly where Emp_id = '$emp_id' and sal_mon = '$sal_month' and sal_year = '$sal_year' ");
                            if($it_query->num_rows() > 0){
                                $it_update_array = array("Net_amt"=>$row['rev_net_pay']);
                                 $it_query_result = $it_query->result_array();                                   
                                 $it_Id = $it_query_result[0]['Id'];
                                $this->ci->db->where("Id = $it_Id and Emp_id = '$emp_id' ");
                                $this->ci->db->update('income_tax_monthly',$it_update_array);
                            }
                        }

                        return $earray;
		}else{
                    return false;
                }
                }else{
                    return false;
                }
		
	}
	public function payslip()
	{
            $bname = trim($this->ci->input->post('bname'));
            $month = trim($this->ci->input->post('month'));
            $year = trim($this->ci->input->post('year'));
            
            $str3="SELECT e.name,e.D_of_join as doj ,(SELECT Design_name FROM designation WHERE Id = e.Design ) as Designation  ,s.*,p.Pancard_no as pan from employ_salary s,employee e,emp_personal p where e.`Emp_id`=s.`Emp_id` and e.`Emp_id`=p.`Emp_id` and e.Branch = '$bname' and s.`sal_month`= '$month' and s.`sal_year`='$year' ";
            $res3=$this->ci->db->query($str3);


            $farray=array();

            if($res3->num_rows()>0)
            {
                $res_arr= $res3->result_array();
                foreach($res_arr as $key =>$values)
                $farray[$values['Emp_id']]=$values;
                return $farray;
            }


            return false;
		
	}
	public function it2(){
//		$x=$this->ci->emp->form16_calc();
            $x=$this->ci->emp->form16_calc();
		
		return $x;
	}
	public function leave_sal_cata($ctc,$nopl){
		
	$x=$this->ci->emp->leave_sal_cata($ctc,$nopl);
	return $x;
	}
   public function leave_sal_catb($ctc,$nopl){
		
	$x=$this->ci->emp->leave_sal_catb($ctc,$nopl);
	return $x;
	}
        public function leave_sal_cats($ctc,$nopl){
		
	$x=$this->ci->emp->leave_sal_cats($ctc,$nopl);
	return $x;
	}
  public function leave_sal_catz($ctc,$nopl){
		
	$x=$this->ci->emp->leave_sal_catz($ctc,$nopl);
	return $x;
	}
        
        public function forms(){
		//$x=$this->ci->emp->form16_calc();
                
                $branch = $this->ci->input->post('bname');
                $year = $this->ci->input->post('year');
                $month1 = $this->ci->input->post('month1');
		
		 $sql="SELECT e.*,p.Father_name,(select Design_name from designation where id=e.`Design`)as odesign,if(e.`Gender`=1,'MALE','FEMALE')as gender FROM `employee` e,emp_personal p WHERE e.`Emp_id`=p.`Emp_id` and e.`Emp_id`in(select concat_ws(',',Emp_id) from employ_salary where sal_year='$year' and sal_month='$month1' and e.Branch ='$branch' group by Emp_id)group by e.Emp_id";
		
		 $res=$this->ci->db->query($sql);  
		
		
	 	$farray=array();
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			foreach($res_arr as $key =>$values)
			$farray[$values['Emp_id']]=$values;
			
			
	       return $farray;
		}
	 
		      return false;
		
		
	
	} 
	 public function form3(){
		//$x=$this->ci->emp->form16_calc();
                
                $branch = $this->ci->input->post('bname');
		$year = $this->ci->input->post('year');
                $month1 = $this->ci->input->post('month1');
		 $sql="SELECT count(DISTINCT s.`Emp_id`) as temp,sum(`employ_ctc`) as total FROM `employ_salary` s,employee e WHERE `sal_year`='$year' and e.`Emp_id`=s.`Emp_id` and e.branch='$branch'";
		
		 $res=$this->ci->db->query($sql);  
		
		
	 	$farray=array();
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			// foreach($res_arr as $key =>$values)
			// $farray[$values['Emp_id']]=$values;
			
			$farray=$res_arr[0];
			
			// echo "<pre>";
			// print_r($farray);
			// die();
			
	       return $farray;
		}
	 
		      return false;
		
		
	
	} 
	
	  public function formo(){
		//$x=$this->ci->emp->form16_calc();
                
                $branch = $this->ci->input->post('bname');
//		 $branch=4;
		// $sql="SELECT e.*,p.Father_name,(select Design_name from designation where id=e.`Design`)as odesign,if(e.`Gender`=1,'MALE','FEMALE')as gender FROM `employee` e,emp_personal p WHERE e.`Emp_id`=p.`Emp_id` and e.`Emp_id`in(select concat_ws(',',Emp_id) from employ_salary where sal_year=YEAR(current_date) and sal_month=MONTH(current_date) and e.Branch ='$branch' group by Emp_id)group by e.Emp_id";
		if(date(m)<=6)
		 $sql="SELECT sum(if(`Gender`=1,1,0)) as male,sum(if(`Gender`=2,1,0)) as female FROM `employee` WHERE `Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employ_salary` WHERE `sal_month` between 1 and 6 group by `Emp_id`) and branch=".$branch;
	    elseif(date(m)>6 && date(m)<=12)
		 $sql="SELECT sum(if(`Gender`=1,1,0)) as male,sum(if(`Gender`=2,1,0)) as female FROM `employee` WHERE `Emp_id` in (SELECT concat_ws(',',`Emp_id`) FROM `employ_salary` WHERE `sal_month` between 7 and 12 group by `Emp_id`) and branch=".$branch;
		 $res=$this->ci->db->query($sql);  
		
		
	 	$farray=array();
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			foreach($res_arr as $key =>$values){
			$farray[$key]=$values;
			$farray[$key]['Total']=$values['male']+$values['female'];
			}
			
			
			
	       return $farray;
		}
	 
		      return false;
		
		
	
	} 
	 public function form5(){
		//$x=$this->ci->emp->form16_calc();
                
                $branch = $this->ci->input->post('bname');
                $year = $this->ci->input->post('year');
                $month1 = $this->ci->input->post('month1');
//		 $branch=4;
//		 $sql="select s.employ_pf_no as acc_no,e.Name,p.Father_name,YEAR(CURRENT_DATE)-YEAR(p.Dob) as age,p.Dob,if(e.Gender=1,'M','F') as Sex,e.D_of_join from employee e,employ_salary s,emp_personal p where e.`Emp_id` in (select concat_ws(',',`Emp_id`) from employ_salary where sal_month='$month1' and sal_year='$year') and e.`Emp_id`=p.`Emp_id` and e.`Emp_id`=s.`Emp_id` and e.Ctc_range!=3 and e.branch=".$branch." group by e.`Emp_id` ";
		 $sql = "select s.employ_pf_no as acc_no,e.Name,p.Father_name,YEAR(CURRENT_DATE)-YEAR(p.Dob) as age,p.Dob,if(e.Gender=1,'M','F') as Sex,e.D_of_join from employee e,employ_salary s,emp_personal p where e.`Emp_id` in (select concat_ws(',',`Emp_id`) from employ_salary where sal_month='$month1' and sal_year='$year') and e.`Emp_id`=p.`Emp_id` and e.`Emp_id`=s.`Emp_id` and e.Ctc_range!=3 and e.branch=".$branch." and month(e.D_of_join) = '$month1' and year(e.D_of_join) = '$year' group by e.`Emp_id` ";
                 $res=$this->ci->db->query($sql);  
		
		
	 	$farray=array();
		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			foreach($res_arr as $key =>$values){
			$farray[$key]=$values;
			$farray[$key]['Total']=$values['male']+$values['female'];
			}
			
			// echo "<pre>";
			// print_r($farray);
			// die();
			
	       return $farray;
		}
	 
		      return false;
		
		
	
	}
        public function dept_list()
	{
		
		$str="select Id,Dept_name from department";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
        
        public function project_list()
	{
		
		$str="SELECT Project_id, Project_no, Project_name FROM projects_det WHERE 1";
		
		
		$res=$this->ci->db->query($str);

		
		if($res->num_rows()>0)
		{
			$res_arr= $res->result_array();
			
		     
			return $res_arr;
		}
		
		return false;
		
	}
        
        public function incometax_update(){
            $bname = $this->ci->input->post('bname');
            $month = $this->ci->input->post('month1');
            $year = $this->ci->input->post('year');
                        
            $sql = "SELECT e.Emp_id,e.Name,it.Id,it.It_amt, it.Net_amt, s.employ_it, s.employ_net FROM employ_salary s,employee e left join income_tax_monthly it on e.Emp_id = it.Emp_id WHERE s.Emp_id = e.Emp_id and s.sal_month='$month' and s.sal_year='$year' and e.Branch='$bname' and e.Status=1";
            $query = $this->ci->db->query($sql);
            if($query->num_rows() > 0){                
                return $query->result_array();
            }else{
                return false;
            }
            
        }
        
        public function incometax_updatesal(){
            
            $Id = $this->ci->input->post('Id');
            $Emp_id = $this->ci->input->post('Emp_id');
            $it_amount = $this->ci->input->post('it_amount');
            $net_amount = $this->ci->input->post('net_amount');
            $uid = $this->ci->session->userdata('uid');
            
            if(count($Emp_id) > 0){
                for($i=0;$i<count($Emp_id);$i++){
                    $array = array("Emp_id"=> $Emp_id[$i], "It_amt"=>$it_amount[$i], "Net_amt"=>$net_amount[$i], "sal_mon"=>date('m'), "sal_year"=>  date('Y'), "Uid"=>$uid );
                    $update_id = trim($Id[$i]);
                    if( $update_id != '' ){
                        unset($array['Emp_id']);
                        $this->ci->db->where("Id",$update_id);
                        $this->ci->db->update('income_tax_monthly',$array);
                    }else{
                        $this->ci->db->insert('income_tax_monthly',$array);                    
                    }
                }
                return TRUE;
            }else{
                return FALSE;
            }
            
        }

}