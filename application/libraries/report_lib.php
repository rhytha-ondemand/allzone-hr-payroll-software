<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of report_lib
 *
 * @author RHYTHA-JRPHP
 */
class report_lib {
    //put your code here
    
    public function __construct(){
        $this->ci=&get_instance();
        
        $this->ci->load->database();
	$this->ci->load->library('session');
	$this->ci->load->helper('cookie');
	
    }
    
    public function birthday_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));
        $month = trim($this->ci->input->post('month'));
        $where = "Branch = $bname and Status = 1";
        if($month != 13){
            $where.= " and month( Dob ) = $month";
        }else{
            $where.=" and Dob is NOT NULL";
        }
        if($dept != ''){
            $where.= " and Dept = $dept";
        }
        if($team != ''){
            $where.= " and Project_team = $team";
        }
        $sql = "select Id,Emp_id,Branch,Name,DATE_FORMAT( D_of_join ,'%d-%b-%Y') as D_of_join,DATE_FORMAT( Dob ,'%d-%b-%Y') as Dob, (SELECT Design_name FROM designation WHERE Id = Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = Project_team ) as Project_team from employee where $where order by MONTH( Dob ) ASC , DAY( Dob ) ASC ";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
    }
    
    public function appraisal_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));
        $desig= trim($this->ci->input->post('desig'));
        $project = trim($this->ci->input->post('project'));
        $month = trim($this->ci->input->post('month'));
        $year = trim($this->ci->input->post('year'));
        
        $where = "emp.Emp_id = app.Emp_id and  emp.Branch = $bname and year( app.Date2 ) = $year and emp.Status = 1 ";
        if($month != 13){
            $where.= " and month( app.Date2 ) = $month";
        }
        if($dept != ''){
            $where.= " and emp.Dept = $dept";
        }
        if($team != ''){
            $where.= " and emp.Project_team = $team";
        }
         if($desig != ''){
            $where.= " and emp.Design = $desig";
        }
         if($project != ''){
            $where.= " and emp.Project = $project";
        }
//        $sql = "select emp.Id,emp.Emp_id,emp.Branch,DATE_FORMAT( app.Date2 ,'%M - %Y') as Date2 , emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team from employee emp,emp_appraisal app  where $where";
          $sql = "select emp.Id,emp.Emp_id,emp.Branch,DATE_FORMAT( app.Date2 ,'%M - %Y') as Date2 , emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team from employee emp,emp_appraisal app inner join (select Emp_id, max(Id) as maxid from emp_appraisal group by Emp_id) as b on app.Id = b.maxid  where $where order by month( app.Date2 ) asc, emp.Id asc ";
//        $sql = "select emp.Id,emp.Emp_id,emp.Branch,app.Date2 ,emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team , (SELECT Dept_name FROM department WHERE Id = emp.Dept ) as Dept ,(SELECT Project_name FROM projects_det WHERE Project_Id = emp.Project ) as Project from employee emp,emp_appraisal app  where $where";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function confirmation_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));
        $month = trim($this->ci->input->post('month'));
        $year = trim($this->ci->input->post('year'));
        
        $where = "Branch = $bname and year( Prob_date ) = $year and Status = 1";
        
        if($month != 13){
            $where.= " and month( Prob_date ) = $month";
        }        
        if($dept != ''){
            $where.= " and Dept = $dept";
        }
        if($team != ''){
            $where.= " and Project_team = $team";
        }
        $sql = "select Id,Emp_id,Emp_id as Emp_id1, Branch, Name,DATE_FORMAT( D_of_join ,'%d-%b-%Y') as D_of_join, D_of_join as D_of_join1, Prob_date as Prob_dating, (SELECT Design_name FROM designation WHERE Id = Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = Project_team ) as Project_team, (select count(*) from emp_leave where Emp_id = Emp_id1 and Leave_type < 6 and (Leave_sdate BETWEEN D_of_join1  and Prob_dating )  and Status = 1) as nod , null as remarks , DATE_FORMAT( Prob_date ,'%d-%b-%Y') as Prob_date from employee where $where order by month( Prob_date ) asc, day( Prob_date ) asc, Id asc ";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function entry_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));        
        $project = trim($this->ci->input->post('project'));
//        $status = trim($this->ci->input->post('status'));
        $from_date1 = trim($this->ci->input->post('from_period'));
        $to_date1 = trim($this->ci->input->post('to_period'));
        
        $from_date = date('Y-m-d',  strtotime($from_date1));
        $to_date = date('Y-m-d',  strtotime($to_date1));
        
//        $where = "emp.Emp_id = sal.Emp_id and  emp.Branch = $bname and (emp.D_of_join BETWEEN '$from_date' and '$to_date')";
        $where = "emp.Branch = $bname and (emp.D_of_join BETWEEN '$from_date' and '$to_date')";
        if($dept != ''){
            $where.= " and emp.Dept = $dept";
        }
        if($team != ''){
            $where.= " and emp.Project_team = $team";
        }
        if($project != ''){
            $where.= " and emp.Project = $project";
        }
//        $sql = "select emp.Id,emp.Emp_id,emp.Branch,emp.ctc,emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team , (SELECT Dept_name FROM department WHERE Id = emp.Dept ) as Dept ,(SELECT Project_name FROM projects_det WHERE Project_Id = emp.Project ) as Project,sal.employ_gross,sal.employ_net from employee emp, employ_salary sal where $where";
        $sql = "select emp.Id,emp.Emp_id,emp.Branch,emp.ctc,emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team , (SELECT Dept_name FROM department WHERE Id = emp.Dept ) as Dept ,(SELECT Project_name FROM projects_det WHERE Project_Id = emp.Project ) as Project from employee emp  where $where";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
    }    
    public function exit_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));        
        $project = trim($this->ci->input->post('project'));
//        $status = trim($this->ci->input->post('status'));        
        $from_date1 = trim($this->ci->input->post('from_period'));
        $to_date1 = trim($this->ci->input->post('to_period'));
        
        $from_date = date('Y-m-d',  strtotime($from_date1));
        $to_date = date('Y-m-d',  strtotime($to_date1));
        
        $where = "emp.Emp_id = per.Emp_id and emp.Branch = $bname and ( (emp.Dofresign BETWEEN '$from_date' and '$to_date') or (per.Dol BETWEEN '$from_date' and '$to_date') )";
        
        if($dept != ''){
            $where.= " and emp.Dept = $dept";
        }
        if($team != ''){
            $where.= " and emp.Project_team = $team";
        }
        if($project != ''){
            $where.= " and emp.Project = $project";
        }
        
        $sql = "select emp.Id,emp.Emp_id,emp.Branch,emp.ctc,emp.Name,DATE_FORMAT( emp.D_of_join ,'%d-%b-%Y') as D_of_join, (SELECT Design_name FROM designation WHERE Id = emp.Design ) as Design, (SELECT team FROM reporting_name WHERE reporting_no = emp.Project_team ) as Project_team , (SELECT Dept_name FROM department WHERE Id = emp.Dept ) as Dept ,(SELECT Project_name FROM projects_det WHERE Project_Id = emp.Project ) as Project,emp.Curr_notice_reason,DATE_FORMAT( emp.Dofresign ,'%d-%b-%Y') as Dofresign,DATE_FORMAT( per.Dol ,'%d-%b-%Y') as Dol from employee emp, emp_personal per  where $where";
       
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
        
    }
    
    public function employee_report_list(){
        $bname = trim($this->ci->input->post('bname'));
        $dept = trim($this->ci->input->post('dept'));
        $team = trim($this->ci->input->post('team'));        
        $project = trim($this->ci->input->post('project'));
        $status = trim($this->ci->input->post('status')); 
        $min = trim($this->ci->input->post('minimum')); 
        $max = trim($this->ci->input->post('maximum')); 
        
        if($status == 1){
            $where = "e.`Emp_id`=a.`Emp_id` and ( TIMESTAMPDIFF( YEAR, e.`D_of_join`, now() ) >= $min and TIMESTAMPDIFF( YEAR, e.`D_of_join`, now() ) <= $max ) and e.Branch = '$bname' and e.Status = 1 ";
        }else {
            $where = "e.`Emp_id`=a.`Emp_id` and ( TIMESTAMPDIFF( YEAR, e.`D_of_join`, e.`Dofresign` ) >= $min and TIMESTAMPDIFF( YEAR, e.`D_of_join`, e.`Dofresign` ) <= $max ) and e.Branch = '$bname' and e.Status = 0 ";
        }
        if($dept != ''){
            $where.= " and e.Dept = $dept";
        }
        if($team != ''){
            $where.= " and e.Project_team = $team";
        }
        if($project != ''){
            $where.= " and e.Project = $project";
        }   
        
        if($status == 1){
            $sql = " SELECT e.`Emp_id`,e.Name, concat(e.Texp_year,' Year(s), ',e.Texp_month,' Month(s)')as total,concat(e.Exp_MB_Year,' Year(s), ',e.Exp_MB_Month,' Month(s) ')as mb,e.`D_of_join`,CONCAT(TIMESTAMPDIFF( YEAR, e.D_of_join, now() ),' Year(s),', TIMESTAMPDIFF( MONTH, e.D_of_join, now() ) % 12,' Month(s), ',  TIMESTAMPDIFF( DAY, DATE_ADD(  DATE_ADD( e.D_of_join , INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,CURDATE() ) YEAR),INTERVAL TIMESTAMPDIFF( MONTH, DATE_ADD( e.D_of_join ,INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,CURDATE()) YEAR ), CURDATE()  ) MONTH ), CURDATE()) ,' Day(s) ' ) as totalin_allzone,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) ) as joining,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) ) as present,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) as join_ctc,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) as present_ctc,(select count(ap1.`Desic`) from emp_appraisal ap1 where ap1.emp_id=e.Emp_id and ap1.`Desic`=1) as appr,(select count(ap2.`Desic`) from emp_appraisal ap2 where ap2.emp_id=e.Emp_id and ap2.`Desic`=2) as refix,if(e.Status=1,'ACTIVE','INACTIVE') as status FROM emp_appraisal a ,employee e where $where  group by e.`Id`";
        }else{
            $sql = " SELECT e.`Emp_id`,e.Name, concat(e.Texp_year,' Year(s), ',e.Texp_month,' Month(s)')as total,concat(e.Exp_MB_Year,' Year(s), ',e.Exp_MB_Month,' Month(s) ')as mb,e.`D_of_join`,CONCAT(TIMESTAMPDIFF( YEAR, e.D_of_join,e.Dofresign ),' Year(s),', TIMESTAMPDIFF( MONTH, e.D_of_join, e.Dofresign ) % 12,' Month(s),',  TIMESTAMPDIFF( DAY, DATE_ADD(  DATE_ADD( e.D_of_join , INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,e.Dofresign) YEAR),INTERVAL TIMESTAMPDIFF( MONTH, DATE_ADD( e.D_of_join ,INTERVAL TIMESTAMPDIFF(YEAR,e.D_of_join,e.Dofresign) YEAR ), e.Dofresign  ) MONTH ), e.Dofresign) ,' Day(s) ' ) as totalin_allzone,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) ) as joining,(select Design_name from designation where id=(select ap.`Desig` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) ) as present,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id asc limit 1) as join_ctc,(select ap.`ctc` from emp_appraisal ap where ap.`Emp_id`=e.`Emp_id` order by ap.id desc limit 1) as present_ctc,(select count(ap1.`Desic`) from emp_appraisal ap1 where ap1.emp_id=e.Emp_id and ap1.`Desic`=1) as appr,(select count(ap2.`Desic`) from emp_appraisal ap2 where ap2.emp_id=e.Emp_id and ap2.`Desic`=2) as refix,if(e.Status=1,'ACTIVE','INACTIVE') as status FROM emp_appraisal a ,employee e where $where  group by e.`Id`";
        }
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    
    public function sda_list(){
        
        $bname = trim($this->ci->input->post('bname'));
        $from_period = trim($this->ci->input->post('from_period'));
        $to_period = trim($this->ci->input->post('to_period'));
        $from_date = date('Y-m-d',  strtotime($from_period));
        $to_date = date('Y-m-d', strtotime($to_period));
        
        $sql = " select sda.Id , DATE_FORMAT( sda.Sda_date ,'%d-%b-%Y') as Sda_date, sda.Id, sda.Emp_id, sda.Work_type, sda.Entry_date, sum(sda.No_of_days) as No_of_days , sda.Status, sda.Uid,emp.Name as Emp_name,(select Design_name from designation where Id = emp.Design limit 1) as design,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team from sda sda join employee emp on sda.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (sda.Sda_date BETWEEN '$from_date' and '$to_date') and sda.Status = 1 group by Emp_id;";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function nsa_list(){
        
        $bname = trim($this->ci->input->post('bname'));
        $from_period = trim($this->ci->input->post('from_period'));
        $to_period = trim($this->ci->input->post('to_period'));
        $bank_name = trim($this->ci->input->post('bank_name'));
        $from_date = date('Y-m-d',  strtotime($from_period));
        $to_date = date('Y-m-d', strtotime($to_period));
        
        $sql="select nsa.Id, nsa.Emp_id, nsa.Work_type, DATE_FORMAT( nsa.Nsa_date ,'%d-%b-%Y') as Nsa_date, DATE_FORMAT(  nsa.Nsa_todate ,'%d-%b-%Y') as Nsa_todate, DATE_FORMAT( nsa.Entry_date ,'%d-%b-%Y') as Entry_date, nsa.No_of_days, nsa.Status, nsa.Uid,emp.Name as Emp_name, sum(nsa.No_of_days) as total_days,(select Design_name from designation where Id = emp.Design limit 1) as design,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team from nsa nsa join employee emp on nsa.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (nsa.Nsa_date BETWEEN '$from_date' and '$to_date') and nsa.Status = 1 group by nsa.Emp_id";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function ha_list(){
        
        $bname = trim($this->ci->input->post('bname'));
        $from_period = trim($this->ci->input->post('from_period'));
        $to_period = trim($this->ci->input->post('to_period'));
        $from_date = date('Y-m-d',  strtotime($from_period));
        $to_date = date('Y-m-d', strtotime($to_period));
        
        $sql = "select ha.Id, DATE_FORMAT( ha.Hol_date ,'%d-%b-%Y') as Hol_date, ha.Id, ha.Emp_id, ha.Entry_date,ha.Status,count(*) as  total_days, ha.Uid,emp.Name as Emp_name,(select Design_name from designation where Id = emp.Design limit 1) as design,(select team from reporting_name where reporting_no = emp.Project_team limit 1) as Team from emp_holidayallow ha join employee emp on ha.Emp_id = emp.Emp_id  where emp.Branch = '$bname' and (ha.Hol_date BETWEEN '$from_date' and '$to_date') and ha.Status = 1 group by Emp_id;";
        $query = $this->ci->db->query($sql);        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    
    public function it_update_detail(){
        $bname = trim($this->ci->input->post('bname'));
        $emp_id = trim($this->ci->input->post('emp_id'));
        $year = trim($this->ci->input->post('year'));
        
        $sql = "select * from employee where Emp_id = $emp_id";
        
        $query = $this->ci->db->query($sql);   
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
}
