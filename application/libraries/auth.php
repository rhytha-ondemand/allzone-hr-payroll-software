<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {
	private $_CI;
	private $auth;

	function __construct() 
	{
		$this->_CI = & get_instance();
		//$this->_CI->load->library('session');
		
		
	}

	
	/**
	 * Function to see if a user is logged in
	 */
	public function checku()
	{
		$uid = $this->_CI->session->userdata('uid');
		
		if ($uid)
		{
			return TRUE;
		}
		else
			return FALSE;
	}
    public function logout()
	{
		
		$this->_CI->session->sess_destroy();
		
	}
	
}

/* End of application/libraries/acl.php */