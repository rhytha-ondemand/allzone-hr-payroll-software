<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['permission'] = array(
	'master' => array(
		'add' => array('admin'),
		//'add' => array('umpire', 'admin'),
		'edit own' => array(), // not applicable
		'edit all' => array('umpire', 'admin'),
		'edit' => array('admin'),
		'delete own' => array(), // not applicable
		'delete all' => array('umpire', 'admin'),
		'view all'=>array('user','admin')
	),
	'leave' => array(
		'add' => array('admin'),
		//'add' => array('umpire', 'admin'),
		'edit own' => array(), // not applicable
		'edit all' => array('umpire', 'admin'),
		'edit' => array('admin'),
		'delete own' => array(), // not applicable
		'delete all' => array('umpire', 'admin'),
		'view all'=>array('user','admin')
	),
);

$config['roles'] = array('user', 'admin','blogger', 'editor', 'umpire');
/* End of applications/config/acl.php */
