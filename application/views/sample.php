<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>PAYROLL - ALLZONE MANAGEMENT SOLUTIONS</title>
    <base href="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- The styles -->
    <link id="bs-css" href="assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="assets/css/charisma-app.css" rel="stylesheet">
    <link href='assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='assets/css/uploadify.css' rel='stylesheet'>
    <link href='assets/css/animate.min.css' rel='stylesheet'>
    <link href='assets/css/jquery-ui-1.8.21.custom.css' rel='stylesheet'>
    <link rel="stylesheet" href="assets/validation/css/validationEngine.jquery.css" type="text/css"/>

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

</head>
    <body>
        <style>
            #leave_type_chosen{
                width: 100%!important;
            }
            .form-control[readonly]{
                cursor: text;
                background-color:#fff;
            }
            #availl,#creditl{
                font-weight: bold;
                font-size: 18px;
            }
        </style>
       <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""> <img alt="Charisma Logo" src="assets/img/logo20.png" class="hidden-xs"/>
                <span>Allzone</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->


        </div>
    </div>
    <div class="ch-container">
        <div class="row"><!--fluid-row-->
              <div class="col-sm-2 col-lg-2">
        <div class="sidebar-nav">
            <div class="nav-canvas">
                <div class="nav-sm nav nav-stacked">

                </div>
                <ul class="nav nav-pills nav-stacked main-menu">
                    <li class="accordion">
                        <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Master</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo base_url().'master/department';?>">Department</a></li>
                            <li><a href="<?php echo base_url().'master/designation';?>">Designation</a></li>
                            <li><a href="<?php echo base_url().'master/projects';?>">Projects</a></li>
                            <li><a href="<?php echo base_url().'master/team';?>">Team</a></li>
                            <li><a href="<?php echo base_url().'master/location';?>">Location</a></li>
                            <li><a href="<?php echo base_url().'master/bank';?>">Bank</a></li>
                            <li><a href="<?php echo base_url().'master/ref_incentive';?>">Referral Incentive</a></li>
                            
                        </ul>
                    </li>
                    <li class="accordion">
                        <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Employee</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo base_url().'employee/add';?>">New</a></li>
                            <li><a href="<?php echo base_url().'employee/modify';?>">Modify</a></li>
                            <li><a href="<?php echo base_url().'employee/transfer';?>">Transfer</a></li>
                            <li><a href="<?php echo base_url().'employee/team';?>">Team Change</a></li>
                    </ul>
                    </li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span> Leave</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Leave</span></a>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/leave_add';?>">Add</a></li>
                                    <li><a href="<?php echo base_url().'leave/leave_view';?>">View</a></li>
                                 </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>C-Off</span></a>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/coff_add';?>">Add</a></li>
                                    <li><a href="<?php echo base_url().'leave/coff_view';?>">View</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Credit Leave</span></a>
                                <ul class="nav nav-pills nav-stacked">
                                     <li><a href="<?php echo base_url().'leave/creditleave_add';?>">Add</a></li>
                                    <li><a href="<?php echo base_url().'leave/creditleave_view';?>">View</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Status</span></a>
                                 <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/leave_status';?>">Monthly-salary</a></li>
                                    <li><a href="<?php echo base_url().'leave/status_monthly';?>">Monthly</a></li>
                                    <li><a href="<?php echo base_url().'leave/status_teamwise';?>">Teamwise</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Appraisal</span></a>
                                 <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/appraisal_individual';?>">Individual</a></li>
                                    <li><a href="<?php echo base_url().'leave/appraisal_consolidate';?>">Consolidate</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>SDA</span></a>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/sda_add';?>">Add</a></li>
                                    <li><a href="<?php echo base_url().'leave/sda_view';?>">View</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>NSA</span></a>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/nsa_add';?>">Add</a></li>
                                    <li><a href="<?php echo base_url().'leave/nsa_view';?>">View</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="accordion">
                        <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Report</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>HR Report</span></a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="#">Birth Day</a></li>
                                        <li><a href="#">Appraisal</a></li>
                                        <li><a href="#">Confirmation</a></li>
                                        <li><a href="#">Entry & Exit</a></li>
                                        <li><a href="#">Team wise Report</a></li>
                                        <li><a href="#">Employee personal Report</a></li>
                                        <li><a href="#">Each Employee Report</a></li>
                                        <li><a href="#">Master Salary Report</a></li>
                                        <li><a href="#">Consolidate Referral incentive </a></li>
                                        <li><a href="#">Appraisal/Refix Report </a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-pills nav-stacked">
                                <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Salary Report</span></a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Leave Salary</span></a>
                                            <ul class="nav nav-pills nav-stacked">
                                                <li><a href="#">Salary Creator</a></li>
                                                <li><a href="#">Pay Slip</a></li>
                                                <li><a href="#">Summary Report</a></li>
                                                <li><a href="#">Bank Credit</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Monthly Salary</span></a>
                                            <ul class="nav nav-pills nav-stacked">
                                                <li><a href="#">Salary Creator</a></li>
                                                <li><a href="#">Salary Update</a></li>
                                                <li><a href="#">Summary Report</a></li>
                                                <li><a href="#">Payslip</a></li>
                                                <li><a href="#">Salary Bank Credit</a></li>
                                                <li><a href="#">Spl. Duty Allowance</a></li>
                                                <li><a href="#">Night Shift Allowance</a></li>
                                                <li><a href="#">Loyalty</a></li>
                                                <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Incentive Allowance<span></a>
                                                    <ul class="nav nav-pills nav-stacked">
                                                        <li><a href="#">Referral</a></li>
                                                        <li><a href="#">Attendance Quarterly</a></li>
                                                        <li><a href="#">Attendance Yearly</a></li>
                                                        <li><a href="#">Holiday Allowance</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Bonus</a></li>
                                                <li><a href="#">Medical Insurance</a></li>
                                            </ul>

                                        </li>
                                    </ul>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Statutory Report</span></a>
                                            <ul class="nav nav-pills nav-stacked">
                                                    <li><a href="<?php echo base_url().'report/sat_esi_sum';?>">Summary Report-ESI</a></li>
                                                        <li><a href="#">Employee Salary Report</a></li>
                                                        <li><a href="#">Summary Report</a></li>
                                                        <li><a href="<?php echo base_url().'report/sat_fm_s';?>">Form-s</a></li>
                                                        <li><a href="#">PF Details</a></li>
                                             </ul>
                                        </li>
                                    </ul>
                                </li>
                                <ul class="nav nav-pills nav-stacked">
                                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Tax Report</span></a>
                                        <ul class="nav nav-pills nav-stacked">
                                                <li><a href="<?php echo base_url().'report/it_update';?>">Income Tax Update</a></li>
                                                <li><a href="<?php echo base_url().'report/pf_tax';?>">Create professional Tax</a></li>
                                                <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span>Income Tax</span></a>
                                                    <ul class="nav nav-pills nav-stacked">
                                                        <li><a href="<?php echo base_url().'report/it_create';?>">IT Calc</a></li>
                                                        <li><a href="#">IT Update</a></li>
                                                        <li><a href="#">IT Statement</a></li>
                                                        <li><a href="#">IT Support</a></li>
                                                        <li><a href="#">IT List</a></li>
                                                        <li><a href="#">IT Details</a></li>
                                                    </ul>
                                                </li>
                                        </ul>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span> Month End Process</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span> Reset</span></a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="<?php echo base_url().'end_process/res_lop';?>">Reset LOP</a></li>
                                    </ul>
                                </li>
                                <li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span> Delete</span></a>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="<?php echo base_url().'end_process/del_sal';?>">Delete Salary</a></li>
                                        <li><a href="<?php echo base_url().'end_process/del_lsal';?>">Delete Leave Salary</a></li>
                                        <li><a href="<?php echo base_url().'end_process/del_it';?>">Delete Income TAX</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>	
                </ul>
                </div>
            </div>
    </div>
		
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Leave</a>
                    </li>
                    <li>
                        <a href="#">Add</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-edit"></i> Add Leave </h2>
                            <div class="box-icon">
                                <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                        <br>
<!--                        <div id="hookError" class="alert alert-danger" style="">
                            
                        </div>-->
<div class="box-content col-sm-offset-0" id="leavediv">
                            <!--working content start-->
                            <form class="form-horizontal sqborder" id="leaveadd" role="form" method="post" action="">
                                <div class="form-group">
                                    <label class="control-label col-xs-2"  for="bname">Select Branch</label>
                                    <div class="col-xs-4">
                                        <select id="bname" name="bname" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>
                                            <option value="4">Chennai</option>
                                            <option value="3">Vellore</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-xs-2" for="emp_id">Employee ID</label>
                                    <div class="col-xs-4">
                                        <!--<input type="text" class="validate[required]  form-control" id="inputSuccess4">-->
                                        <select id="emp_id" name="emp_id" data-placeholder="Select branch" class="validate[required] form-control" >
                                            <option value=""></option>                                                                                       
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-xs-3 col-sm-offset-3">
                                       <!--<input type="submit"  id="submit" class="btn btn-primary" value="Submit">-->
                                    </div>
                                </div>
                            </form>
                            <!--working content end-->
                        </div>

<div class="box-content col-sm-offset-0" id="add_details" >

                  

                        <br>
                        <form class="form-horizontal sqborder" id="leaveadddet" role="form" action="" method="post">
                             <div class="form-group">
                                <label class="control-label col-xs-3"  for="doj">Date Of Joining</label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" readonly name="doj" id="doj" value=""/>
                                </div>
                            </div>
                                                       
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="cdate">Date</label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" readonly name="cdate" id="cdate" value="<?php echo date("Y-m-d")?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="leave_emp_name">Employee Name</label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" readonly="" name="leave_emp_name" value="" id="leave_emp_name">                                     
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="leave_type">Type of leave</label>
                                <div class="col-xs-6">
                                    <select class="validate[required] form-control" name="leave_type" id="leave_type" data-placeholder="Select leave">
                                        <option value=""></option>
                                        <option value="CL/SL">PL</option>
                                        <option value="LOP">LOP</option>
                                        <option value="PAT">PTR</option>
                                        <option value="BVR">BVR</option>
                                        <option value="ML">ML</option>
                                        <option value="COFF">C-OFF</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="norm_leave">
                                <label class="control-label col-xs-3"  for="leave_date">Leave Date</label>
                                <div class="col-xs-6">
                                    <input type="text" class="validate[required] form-control datepicker" readonly="" name="leave_date" id="leave_date"  >
                                </div>
                            </div>
                            <div id="other_leave" style="display:none">
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="from_date">From Date</label>
                                <div class="col-xs-6">
                                    <input type="text" class="validate[required] form-control datepicker" readonly name="from_date" id="from_date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="to_date">To Date</label>
                                <div class="col-xs-6">
                                    <input type="text" class="validate[required] form-control datepicker" readonly="" name="to_date" id="to_date">
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3"  for="leave_reason">Reason</label>
                                <div class="col-xs-6">
                                    <input type="text" class="validate[required] form-control" name="leave_reason" id="leave_reason" >
                                </div>
                            </div>
                        
                        
                            <div class="form-group">
                                <div class="col-sm-offset-5 col-xs-1 ">
                                    <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    
                        
                    <!--working content end-->
                    
</div>
                    </div>
                </div>
            </div>
        </div>
  </div><!--/fluid-row-->
  
   <!--Modal dialog box start-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
<!-- Modal dialog box End --> 	

        <hr>
         <footer class="row">
    <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://www.rhytha.com" target="_blank">Rhytha Web Solutions</a> 2015 - 2016</p>

    <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Contact No:044-26561032</p>
</footer>
        </div>
        <script>
        $(document).ready(function(){
            $.validationEngine.defaults.scroll = false;
            $("#leaveadddet").validationEngine({
		prettySelect:true,
		useSuffix: "_chosen", 
		maxErrorsPerField: 1,
                promptPosition : "inline"
            });
//            $("#leaveadd").bind("jqv.form.validating", function(event){
//                $("#hookError").css('display','none');
//                $("#hookError").html("");
//            });
//            $("#leaveadd").bind("jqv.form.result", function(event , errorFound){
//                    if(errorFound){ 
//                        $("#hookError").append("Please fill all required fields");
//                        $("#hookError").css('display','block');
//                    }
//            });
            
            $("select").chosen({disable_search_threshold: 10});
           
            $( "select" ).change(function() {
                var err = '.'+this.id+'_chosenformError';
                $(err).remove();
            });
            
            $("#bname").change(function(){
              var toappend;
                var url = "master/emp_id_list";
                var data =  "bname=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $('#emp_id').find('option').remove();
                        if(json) {
                             toappend+='<option value=""></option>';
                             $.each(json, function(i, value) {
                                 toappend+='<option value='+value['Id']+'>'+value['Id']+'</option>';                                
                            });   
                            $('#emp_id').append(toappend);                           
                        }
                         $("#emp_id").trigger('chosen:updated');
                    }            
                });                 
          });
          
          $("#emp_id").change(function(){
//              $("#leavediv").css('display','none');
                $("#add_details").css('display','block');
                $("#leaveadddet")[0].reset();
                $("#norm_leave").css('display','block');
                $("#other_leave").css('display','none');
                $("select").trigger('chosen:updated');
                 var url = "master/emp_details";
                var data =  "emp_id=" + $(this).val();                
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    dataType: "json",
                    data: data,
                    async: false,
                    success: function(json) {
                        $("#leave_emp_name").val(json.emp_name);
                        $("#doj").val(json.date_of_join);
                        $("#creditl").html(json.credit_leave);
                        $("#availl").html(json.avail_leave);
                    }            
                });     
          });
            
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd',minDate:-10 });      
            $("#leave_type").change(function(){
                if($(this).val() === "ML"){
                    $("#norm_leave").css('display','none');
                    $("#other_leave").css('display','block');
                }else{
                    $("#norm_leave").css('display','block');
                    $("#other_leave").css('display','none');
                }
            });
            
            $("#submit").click(function(){
                if ( $("#leaveadddet").validationEngine('validate')) {
                    alert("Success");
                }
                return false;
            });
        });
        </script>
       
<!-- external javascript -->

<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='assets/bower_components/moment/min/moment.min.js'></script>
<script src='assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='assets/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="assets/js/charisma.js"></script>

<!--validation engine-->
<script src="assets/validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>


<!--datepicker-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.8.21.custom.min.js"></script>

<!--datatables-->
<script type="text/javascript" language="javascript" src="assets/bower_components/datatables/media/js/jquery.dataTables.js"></script>
    </body>
</html>