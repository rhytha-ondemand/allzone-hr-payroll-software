    <div class="col-sm-2 col-lg-2">
        <div class="sidebar-nav">
            <div class="nav-canvas">
                <div class="nav-sm nav nav-stacked">

                </div>
                <ul class="nav nav-pills nav-stacked main-menu">
                    <li class="active">
                        <a href="#"><i class="glyphicon glyphicon-home"></i><span> Master</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo base_url().'master/dept_view_all';?>"><i class="glyphicon glyphicon-book"></i> Department</a></li>
                            <li><a href="<?php echo base_url().'master/desig_view_all';?>"><i class="glyphicon glyphicon-flag"></i> Designation</a></li>
                            <li><a href="<?php echo base_url().'master/projects_view_all';?>"><i class="glyphicon glyphicon-folder-close"></i> Projects</a></li>
                            <li><a href="<?php echo base_url().'master/team_view_all';?>"><i class="glyphicon glyphicon-user"></i> Team</a></li>
                            <li><a href="<?php echo base_url().'master/location_view_all';?>"><i class="glyphicon glyphicon-map-marker"></i> Location</a></li>
                            <li><a href="<?php echo base_url().'master/bank_view';?>"><i class="glyphicon glyphicon-home"></i> Bank</a></li>
                            <li><a href="<?php echo base_url().'master/ref_incentive_view';?>"><i class="glyphicon glyphicon-paperclip"></i> Referral Incentive</a></li>
                            <li><a href="<?php echo base_url().'master/employer_view_all';?>"><i class="glyphicon glyphicon-paperclip"></i> Employer</a></li>
                        </ul>
                    </li>
                    <li class="accordion">
                        <a href="#"><i class="glyphicon glyphicon-user"></i><span> Employee</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo base_url().'employee/add';?>"><i class="glyphicon glyphicon-edit"></i> New</a></li>
                            <li><a href="<?php echo base_url().'employee/modify';?>"><i class="glyphicon glyphicon-pencil"></i> Modify</a></li>
                            <li><a href="<?php echo base_url().'employee/transfer';?>"><i class="glyphicon glyphicon-share"></i> Transfer</a></li>
                            <li><a href="<?php echo base_url().'employee/team';?>"><i class="glyphicon glyphicon-resize-horizontal"></i> Team Change</a></li>
                    </ul>
                    </li>
                    <li class="accordion"><a href="#"><i class="glyphicon glyphicon-calendar"></i><span> Leave</span></a>
                        <ul class="nav nav-pills nav-stacked">
                            <li  class=""><a href="<?php echo base_url().'leave/leave_view';?>"><i class="glyphicon glyphicon-log-out"></i><span> Leave</span></a>

                            </li>

                            <li  class=""><a href="<?php echo base_url().'leave/coff_view';?>"><i class="glyphicon glyphicon-off"></i><span> C-Off / Credit</span></a>
                            </li>
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-time"></i><span> Status</span></a>
                                 <ul class="nav nav-pills nav-stacked">
                                    <!--<li><a href="<?php echo base_url().'leave/leave_status';?>">Monthly-salary</a></li>-->
                                    <li><a href="<?php echo base_url().'leave/status_monthly';?>"><i class="glyphicon glyphicon-stats"></i> Monthly</a></li>
                                    <li><a href="<?php echo base_url().'leave/status_teamwise';?>"><i class="glyphicon glyphicon-file"></i> Teamwise</a></li>
                                </ul>
                            </li>
                            <li  class="accordion"><a href="#"><i class="glyphicon glyphicon-file"></i><span> Appraisal</span></a>
                                 <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url().'leave/appraisal_individual';?>"><i class="glyphicon glyphicon-file"></i> Individual</a></li>
                                    <li><a href="<?php echo base_url().'leave/appraisal_consolidate';?>"><i class="glyphicon glyphicon-file"></i> Consolidate</a></li>
                                </ul>
                            </li>
                            </li>
                        </ul>

                    </li>
                    <li class="accordion">
                        <a href="#"><i class="glyphicon glyphicon-stats"></i><span> Report</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<?php echo base_url().'report/birthday';?>"><i class="glyphicon glyphicon-gift"></i> Birth Day</a></li>
                                <li><a href="<?php echo base_url().'report/appraisal';?>"><i class="glyphicon glyphicon-file"></i> Appraisal</a></li>
                                <li><a href="<?php echo base_url().'report/confirmation';?>"><i class="glyphicon glyphicon-thumbs-up"></i> Confirmation</a></li>
                                <li><a href="<?php echo base_url().'report/entry_exit';?>"><i class="glyphicon glyphicon-remove-circle"></i> Entry & Exit</a></li>
                                <li><a href="<?php echo base_url().'report/employee_report';?>"><i class="glyphicon glyphicon-file"></i> Each Employee Report</a></li>
                                <li><a href="<?php echo base_url().'report/allowance_details';?>"><i class="glyphicon glyphicon-list-alt"></i> Allowance Report</a></li>							
                                <li><a href="<?php echo base_url().'reports/master_salary_det';?>"><i class="glyphicon glyphicon-paperclip"></i> Master Salary Report</a></li>
                                <li><a href="<?php echo base_url().'reports/nsa_salary_det';?>"><i class="glyphicon glyphicon-credit-card"></i> NSA Salary</a></li>
                                <li><a href="<?php echo base_url().'reports/leave_salary_det';?>"><i class="glyphicon glyphicon-credit-card"></i> Leave Salary</a></li>
                                <li><a href="<?php echo base_url().'reports/incometax_update_det';?>"><i class="glyphicon glyphicon-file "></i> Income Tax Update</a></li>
                                <li><a href="<?php echo base_url().'reports/form_detail';?>"><i class="glyphicon glyphicon-list-alt"></i> Forms</a></li>
                                <li><a href="<?php echo base_url().'reports/payslip_det';?>"><i class="glyphicon glyphicon-paperclip"></i> Pay-slip </a></li>
                                <li><a href="<?php echo base_url().'reports/it_update_det';?>"><i class="glyphicon glyphicon-repeat"></i> IT Update</a></li>							
                                <li><a href="<?php echo base_url().'reports/query_build';?>"><i class="glyphicon glyphicon-list-alt"></i> Additional Report </a></li>
                            </ul>
                        </li>
                        </li>	
                </ul>
                </div>
            </div>
    </div>
		