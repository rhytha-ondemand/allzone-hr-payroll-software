<head>
    <meta charset="utf-8">
    <title>PAYROLL - ALLZONE MANAGEMENT SOLUTIONS</title>
    <base href="<?php echo base_url(); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- The styles -->
    <link id="bs-css" href="assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="assets/css/charisma-app.css" rel="stylesheet">
    <link href='assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='assets/css/uploadify.css' rel='stylesheet'>
    <link href='assets/css/animate.min.css' rel='stylesheet'>
    <link href='assets/css/jquery-ui-1.8.21.custom.css' rel='stylesheet'>
    <link rel="stylesheet" href="assets/validation/css/validationEngine.jquery.css" type="text/css"/>

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">
  <style>
        .breadcrumb > li > a{
            cursor:default;
        }
        .breadcrumb > li > a:focus,.breadcrumb > li > a:hover{
            text-decoration: inherit;
            cursor:default;
        }
		.btn-setting,.btn-minimize ,.btn-close {
            display:none;
        }
    </style>
</head>