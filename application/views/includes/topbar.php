 <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""> <img style=" float: left; height: 60px; width: 200px; margin-right: 5px; margin-top: -20px;" alt="allzone Logo" src="assets/img/allzone.png" class="hidden-xs"/>
                <!--<span>Allzone</span>-->
            </a>
            
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                <!--      <li><a href="#">Profile</a></li>
                    <li class="divider"></li>-->
                    <li><a href="<?= base_url()?>master/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-bookmark"></i><span class="hidden-sm hidden-xs"> Bookmarks</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
					<li><a href="<?php echo base_url().'leave/leave_add';?>">Add Leave</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url().'employee/add';?>">Add Employee</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url().'employee/modify';?>">Modify Employee</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url().'employee/team';?>">Team Change</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Bookmarks 5</a></li>                    
                </ul>
            </div>


        </div>
    </div>